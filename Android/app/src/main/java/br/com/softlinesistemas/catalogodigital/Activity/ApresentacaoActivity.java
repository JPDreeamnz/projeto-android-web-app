package br.com.softlinesistemas.catalogodigital.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import br.com.softlinesistemas.catalogodigital.Activity.Login.LoginActivity;
import br.com.softlinesistemas.catalogodigital.Business.LoginBusiness;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 22/12/2015.
 */
public class ApresentacaoActivity extends Activity implements Runnable {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apresentacao);

        Handler handler = new Handler();
        handler.postDelayed(this, 2500);
    }

    /**
     * Starts executing the active part of the class' code. This method is
     * called when a thread is started that has been created with a class which
     * implements {@code Runnable}.
     */
    @Override
    public void run() {
        Intent intent;

        //Testa Login
        LoginBusiness login = new LoginBusiness(ApresentacaoActivity.this);

        if(login.VerificaToken()) 
            intent = new Intent(this, DashboardActivity.class);
        else
            intent = new Intent(this, LoginActivity.class);



        startActivity(intent);
        finish();
    }
}
