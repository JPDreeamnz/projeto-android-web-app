package br.com.softlinesistemas.catalogodigital.Datasource.Produto;

import android.content.ContentValues;
import android.database.Cursor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Produto;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public class ProdutoDataHandler {
    public static Produto fromCursor(Cursor cursor) throws ParseException
    {
        Produto produto = null;

        if(cursor.moveToFirst())
        {
            produto = BindCursorProduto(cursor);
        }

        cursor.close();
        return produto;
    }

    public static List<Produto> fromCursorToList(Cursor cursor)
    {
        List<Produto> produtos = new ArrayList<Produto>();

        if(cursor.moveToFirst())
        {
            do{
                produtos.add(BindCursorProduto(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return produtos;
    }

    public static ContentValues toContentValue(Produto produto)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.Produto.Coluna_Codigo, produto.Codigo);
        values.put(DatabaseContract.Produto.Coluna_CodigoBarras, produto.CodigoBarras);
        values.put(DatabaseContract.Produto.Coluna_CodigoFabrica, produto.CodigoFabrica);
        values.put(DatabaseContract.Produto.Coluna_CodigoOriginal, produto.CodigoOriginal);
        values.put(DatabaseContract.Produto.Coluna_Cor, produto.Cor);
        values.put(DatabaseContract.Produto.Coluna_Marca, produto.Marca);
        values.put(DatabaseContract.Produto.Coluna_Nome, produto.Nome);
        values.put(DatabaseContract.Produto.Coluna_QuantidadeEstoque, produto.QuantidadeEstoque);
        values.put(DatabaseContract.Produto.Coluna_Tamanho, produto.Tamanho);
        values.put(DatabaseContract.Produto.Coluna_Unidade, produto.Unidade);
        values.put(DatabaseContract.Produto.Coluna_ValorVenda, produto.ValorVenda);

        return values;
    }

    public static int FromCursorToInt(Cursor cursor)
    {
        int retorno = 0;
        if(cursor.moveToFirst())
        {
            retorno = cursor.getInt(cursor.getColumnIndex("Quantidade"));
        }

        cursor.close();

        return retorno;
    }

    private static Produto BindCursorProduto(Cursor cursor)
    {
        Produto produto = new Produto();

        produto.Codigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_Codigo)
        );
        produto.CodigoBarras = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_CodigoBarras)
        );
        produto.CodigoFabrica = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_CodigoFabrica)
        );
        produto.CodigoOriginal = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_CodigoOriginal)
        );
        produto.Cor = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_Cor)
        );
        produto.Marca = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_Marca)
        );
        produto.Nome = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_Nome)
        );
        produto.QuantidadeEstoque = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_QuantidadeEstoque)
        );
        produto.Tamanho = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_Tamanho)
        );
        produto.Unidade = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_Unidade)
        );
        produto.ValorVenda = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Produto.Coluna_ValorVenda)
        );

        return produto;
    }
}
