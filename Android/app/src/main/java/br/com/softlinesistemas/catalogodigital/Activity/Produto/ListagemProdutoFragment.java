package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Activity.EndlessScroll;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Produto;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardGridView;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public class ListagemProdutoFragment extends BaseV4Fragment implements SearchView.OnQueryTextListener{
    private boolean _isPedido;
    private ProdutoBusiness _produtoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private Fragment pedidoFragment;
    private CardGridArrayAdapter mCardArrayAdapter;
    private EndlessScroll _endlessScroll;
    private ArrayList<Card> _cards;
    private boolean _filtrado;
    private String _busca;


    public static ListagemProdutoFragment novaInstancia(ProdutoBusiness produtoBusiness, ItensPedidoBusiness itensPedidoBusiness, Fragment pedidoFragment, int fragmentContainer, boolean isPedido){
        ListagemProdutoFragment listagemProdutoFragment = new ListagemProdutoFragment();
        listagemProdutoFragment._produtoBusiness = produtoBusiness;
        listagemProdutoFragment._isPedido = isPedido;
        listagemProdutoFragment._itensPedidoBusiness = itensPedidoBusiness;
        listagemProdutoFragment.pedidoFragment = pedidoFragment;
        listagemProdutoFragment.setFragmentContainer(fragmentContainer);
        return listagemProdutoFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View listagemView = inflater.inflate(R.layout.produto_listagem_fragment, null);
        this.setView(listagemView);
        setHasOptionsMenu(true);
        return listagemView;
    }

    private void setView(View view)
    {
        this._filtrado = false;
        this._cards = new ArrayList<>();
        final Context contexto = view.getContext();

        mCardArrayAdapter = new CardGridArrayAdapter(contexto, _cards);

        CardGridView gridView = (CardGridView) view.findViewById(R.id.grid_lista_produtos);
        if (gridView!=null){
            gridView.setAdapter(mCardArrayAdapter);

            _endlessScroll = new EndlessScroll(gridView, new EndlessScroll.RefreshList() {
                @Override
                public void onRefresh(int pageNumber) {
                    PreencheListCards(pageNumber, (_filtrado ? _busca : ""), contexto);
                    mCardArrayAdapter.notifyDataSetChanged();
                }
            });

            gridView.setOnScrollListener(_endlessScroll);
        }
    }

    private void PreencheListCards(int pagina, String filtro, Context contexto)
    {
        List<Produto> produtos = filtro.isEmpty() ?
                this._produtoBusiness.ObterTodos(pagina) : this._produtoBusiness.ObterTodos(filtro, pagina);

        for (Produto produto : produtos)
        {
            ProdutoCard card = new ProdutoCard(contexto, produto);

            CardHeader header = new CardHeader(contexto);
            header.setTitle(produto.Nome);
            card.addCardHeader(header);
            card.setId(String.valueOf(produto.Codigo));
            card.setOnClickListener(this.clickProduto());

            _cards.add(card);
        }

        if((pagina * produtos.size()) < this._produtoBusiness.ObterQuantidadeProdutos(filtro))
            _endlessScroll.notifyMorePages();
        else
            _endlessScroll.noMorePages();

    }

    private Card.OnCardClickListener clickProduto()
    {
        return new Card.OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                int produtoCodigo = Integer.parseInt(card.getId());

                ProdutoTabFragment fragment = ProdutoTabFragment.novaInstancia(produtoCodigo, _produtoBusiness, _itensPedidoBusiness, pedidoFragment, _isPedido, getFragmentContainer());

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(
                        getFragmentContainer(),
                        fragment,
                        "PRODUTO_VISUALIZAR_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        };
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_filtro, menu);

        MenuItem itemFiltro = menu.findItem(R.id.filtro);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(itemFiltro);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(this);
    }

    /**
     * Called when the user submits the query. This could be due to a key press on the
     * keyboard or due to pressing a submit button.
     * The listener can override the standard behavior by returning true
     * to indicate that it has handled the submit request. Otherwise return false to
     * let the SearchView handle the submission by launching any associated intent.
     *
     * @param query the query text that is to be submitted
     * @return true if the query has been handled by the listener, false to let the
     * SearchView perform the default action.
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Called when the query text is changed by the user.
     *
     * @param newText the new content of the query text field.
     * @return false if the SearchView should perform the default action of showing any
     * suggestions if available, true if the action was handled by the listener.
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        this._cards.clear();
        this._filtrado = true;
        this._busca = newText;
        View view = getView();

        if(view != null) {
            Context contexto = this.getActivity();
            PreencheListCards(0, newText, contexto);
            mCardArrayAdapter.notifyDataSetChanged();
        }

        return true;
    }
}

class ProdutoCard extends Card{

    private Produto _produto;
    private TextView mMarca,
            mEstoque,
            mValor,
            mCor,
            mTamanho;
    /**
     * Constructor with a base inner layout defined by R.layout.inner_base_main
     *
     * @param context context
     */
    public ProdutoCard(Context context, Produto produto) {
        super(context, R.layout.produto_card);
        this._produto = produto;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        this.mEstoque = (TextView) view.findViewById(R.id.produto_card_txtEstoque);
        this.mMarca = (TextView) view.findViewById(R.id.produto_card_txtMarca);
        this.mValor = (TextView) view.findViewById(R.id.produto_card_txtValor);
        this.mCor = (TextView) view.findViewById(R.id.produto_card_txtCor);
        this.mTamanho = (TextView) view.findViewById(R.id.produto_card_txtTamanho);

        this.mEstoque.setText(getContext().getResources().getString(R.string.Quantidade_em_estoque) +
                " " + this._produto.QuantidadeEstoque);
        this.mMarca.setText(this._produto.Marca);
        this.mValor.setText(NumberFormat.getCurrencyInstance().format(this._produto.ValorVenda));

        if(this._produto.Cor == null)
            this.mCor.setText("Cor não cadastrada");
        else if (this._produto.Cor.isEmpty())
            this.mCor.setText("Cor não cadastrada");
        else
            this.mCor.setText(this._produto.Cor);

        if(this._produto.Tamanho == null)
            this.mTamanho.setText("Tamanho não cadastrado");
        else if(this._produto.Tamanho.isEmpty())
            this.mTamanho.setText("Tamanho não cadastrado");
        else
            this.mTamanho.setText(this._produto.Tamanho);
    }
}