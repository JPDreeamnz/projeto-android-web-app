package br.com.softlinesistemas.catalogodigital.Activity.Pedido;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import br.com.softlinesistemas.catalogodigital.Activity.BaseActivity;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PlanoPagamentoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.VendedorBusiness;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 25/07/2016.
 */

public class PedidoActivity extends BaseActivity implements IPedidoActivity{
    private PedidoBusiness _pedidoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private ProdutoBusiness _produtoBusiness;
    private VendedorBusiness _vendedorBusiness;
    private PlanoPagamentoBusiness _planoPagamentoBusiness;
    private ImagemProdutoDatasource _imagemProdutoDataSource;
    private ClientesBusiness _clientesBusiness;

    public PedidoActivity() {
        super(R.id.pedido_drawer_layout, R.layout.pedido_activity);

        _pedidoBusiness = new PedidoBusiness(this);
        _itensPedidoBusiness = new ItensPedidoBusiness();
        _produtoBusiness = new ProdutoBusiness(this);
        _planoPagamentoBusiness = new PlanoPagamentoBusiness(this);
        _vendedorBusiness = new VendedorBusiness(this);
        _clientesBusiness = new ClientesBusiness(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _imagemProdutoDataSource = new ImagemProdutoDatasource(this);

        if(savedInstanceState == null)
        {
            this.AttachFragment(ListagemPedidoFragment.novaInstancia(
                    this,
                    _pedidoBusiness,
                    _itensPedidoBusiness,
                    _produtoBusiness,
                    _vendedorBusiness,
                    _planoPagamentoBusiness,
                    _imagemProdutoDataSource,
                    _clientesBusiness,
                    R.id.pedido_fragment_container), "LISTAGEM_PEDIDO_FRAGMENT");
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if(fragmentManager.getBackStackEntryCount() > 0){
            fragmentManager.popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    public void AttachFragment(Fragment fragment, String tag){
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.add(
                R.id.pedido_fragment_container,
                fragment,
                tag);
        transaction.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out);
        transaction.commit();
    }
}
