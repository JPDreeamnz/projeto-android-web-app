package br.com.softlinesistemas.catalogodigital.Activity.Pedido;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Activity.MoneyTextWatcher;
import br.com.softlinesistemas.catalogodigital.Activity.Produto.ListagemProdutoFragment;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;

/**
 * Created by João Pedro R. Carvalho on 28/07/2016.
 */

public class ListagemItensPedidoCard extends CardWithList {
    private final String _title = "Itens de Pedido";
    private boolean _isVisualizar;
    private ProdutoBusiness _produtoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private ImagemProdutoDatasource _dtImagemProduto;
    private Fragment pedidoFragment;
    private IPedidoActivity _pedidoActivity;

    /**
     * Constructor with a base inner layout defined by R.layout.inner_base_main_cardwithlist
     *
     * @param context context
     */
    public ListagemItensPedidoCard(Context context,
                                   ProdutoBusiness produtoBusiness,
                                   ItensPedidoBusiness itensPedidoBusiness,
                                   boolean isVisualizar,
                                   ImagemProdutoDatasource dtImagemProduto,
                                   IPedidoActivity pedidoActivity,
                                   Fragment pedidoFragment) {
        super(context);

        this._produtoBusiness = produtoBusiness;
        this._itensPedidoBusiness = itensPedidoBusiness;
        this._isVisualizar = isVisualizar;
        this._dtImagemProduto = dtImagemProduto;
        this._pedidoActivity = pedidoActivity;
        this.pedidoFragment = pedidoFragment;
    }

    /**
     * Implement this method to initialize your CardHeader.
     * <p/>
     * An example:
     * <pre><code>
     *     //Add Header
     *     CardHeader header = new CardHeader(getContext());
     *     //Add a popup menu. This method set OverFlow button to visible
     *     header.setPopupMenu(R.menu.popupmain, new CardHeader.OnClickCardHeaderPopupMenuListener() {
     *          @Override
     *          public void onMenuItemClick(BaseCard card, MenuItem item) {
     *              Toast.makeText(getContext(), "Click on " + item.getTitle(), Toast.LENGTH_SHORT).show();
     *          }
     *      });
     *      header.setTitle("Weather"); //should use R.string.
     *      return header;
     * <p/>
     * </code></pre>
     *
     * @return the {@link CardHeader}
     */
    @Override
    protected CardHeader initCardHeader() {
        CardHeader header = new CardHeader(getContext());
        header.setTitle(this._title);

        if(!this._isVisualizar) {
            header.setOtherButtonVisible(true);
            header.setOtherButtonClickListener(this.CriarItemPedido());
        }

        return header;
    }

    /**
     * Implement this method to initialize your Card.
     * <p/>
     * An example:
     * <pre><code>
     *      setSwipeable(true);
     *      setOnSwipeListener(new OnSwipeListener() {
     *          @Override
     *              public void onSwipe(Card card) {
     *                  Toast.makeText(getContext(), "Swipe on " + card.getCardHeader().getTitle(), Toast.LENGTH_SHORT).show();
     *              }
     *      });
     *  </code></pre>
     */
    @Override
    protected void initCard() {

    }

    /**
     * Implement this method to initialize the list of objects
     * <p/>
     * An example:
     * <pre><code>
     * <p/>
     *      List<ListObject> mObjects = new ArrayList<ListObject>();
     * <p/>
     *      WeatherObject w1= new WeatherObject();
     *      mObjects.add(w1);
     * <p/>
     *      return mObjects;
     * </code></pre>
     *
     * @return the List of ListObject. Return <code>null</code> if the list is empty.
     */
    @Override
    protected List<ListObject> initChildren() {
        List<ListObject> lista = new ArrayList<ListObject>();

        List<ItensPedido> itens = this._itensPedidoBusiness.ObterLista();
        for (ItensPedido item : itens) {
            lista.add(ItensPedidoListObject.toContatoListObject(item));
        }

        return lista.size() > 0 ? lista : null;
    }

    /**
     * This method is called by the {@link LinearListAdapter} for each row.
     * You can provide your layout and setup your ui elements.
     *
     * @param childPosition position inside the list of objects
     * @param object        {@link ListObject}
     * @param convertView   view used by row
     * @param parent        parent view
     * @return
     */
    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent) {
        ImageView mImagemProduto = (ImageView) convertView.findViewById(R.id.pedido_item_card_imgImagemProduto);
        TextView mNomeProduto = (TextView) convertView.findViewById(R.id.pedido_item_card_tvNomeProduto);
        TextView mQuantidade = (TextView) convertView.findViewById(R.id.pedido_item_card_tvQuantidade);
        TextView mValorUnitario = (TextView) convertView.findViewById(R.id.pedido_item_card_tvValorUnitario);
        TextView mValorDesconto = (TextView) convertView.findViewById(R.id.pedido_item_card_tvValorDesconto);
        TextView mValorTotal = (TextView) convertView.findViewById(R.id.pedido_item_card_tvValorTotal);
        ImageButton mEditar = (ImageButton) convertView.findViewById(R.id.pedido_btnEditarItemPedido);
        ImageButton mExcluir = (ImageButton) convertView.findViewById(R.id.pedido_btnExcluirItemPedido);

        ItensPedido item = (ItensPedido) object;

        this._dtImagemProduto.MudarDiretorio(item.Produto);
        String imagem = this._dtImagemProduto.ObterPrimeiraImagem();
        if(imagem != null) {
            Picasso.with(convertView.getContext())
                    .load(imagem).fit().into(mImagemProduto);
        }

        mNomeProduto.setText(this._produtoBusiness.ObterNomeProduto(item.Produto));
        mQuantidade.setText(String.valueOf(item.Quantidade));

        NumberFormat format = NumberFormat.getCurrencyInstance();
        mValorUnitario.setText(format.format(item.ValorUnitario));
        mValorUnitario.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color.colorPrimaryGreen));

        mValorDesconto.setText(format.format(item.ValorDesconto));
        mValorDesconto.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color.colorPrimaryRed));

        mValorTotal.setText(format.format(item.ValorTotal));
        mValorTotal.setTypeface(null, Typeface.BOLD);

        if(this._isVisualizar){
            mExcluir.setImageResource(android.R.color.transparent);
            mExcluir.setEnabled(false);

            mEditar.setImageResource(android.R.color.transparent);
            mEditar.setEnabled(false);
        } else {
            mExcluir.setImageResource(R.drawable.ic_trash_dark);
            mExcluir.setOnClickListener(this.BotaoExcluirItem(object));

            mEditar.setImageResource(R.drawable.ic_editar_dark_certo);
            mEditar.setOnClickListener(this.EditarItem(object, convertView));
        }

        return convertView;
    }

    /**
     * Implement this method to specify the layoutId used for each row in the list
     *
     * @return the layoutId
     */
    @Override
    public int getChildLayoutId() {
        return R.layout.pedido_item_card;
    }

    private View.OnClickListener BotaoExcluirItem(final ListObject object)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItensPedido item = (ItensPedido) object;
                _itensPedidoBusiness.Excluir(item);
                getLinearListAdapter().remove(object);

                RecalculaValores(_itensPedidoBusiness.getPedidoMemoria());
            }
        };
    }

    private View.OnClickListener EditarItem(final ListObject object, final View convertView)
    {
        return new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                final AlertDialog alertDialog;

                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                LayoutInflater inflater = ((PedidoActivity)v.getContext()).getLayoutInflater();
                builder.setTitle(R.string.informe_detalhes_produto);

                builder.setView(inflater.inflate(R.layout.pedido_finaliz_produto, null))
                        .setPositiveButton(R.string.OK, null)
                        .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                alertDialog = builder.create();

                ItensPedido itemPedido = (ItensPedido) object;

                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        b.setOnClickListener(setPositiveButton(alertDialog, object, convertView));
                    }
                });

                alertDialog.show();

                EditText txtQuantidade = (EditText) alertDialog.findViewById(R.id.pedido_txtQuantidade);
                EditText txtValorDesconto = (EditText) alertDialog.findViewById(R.id.pedido_txtVlrDesconto);
                EditText txtValorUnitario = (EditText) alertDialog.findViewById(R.id.pedido_txtValorUnitario);

                txtValorDesconto.addTextChangedListener(new MoneyTextWatcher(txtValorDesconto));
                txtValorUnitario.addTextChangedListener(new MoneyTextWatcher(txtValorUnitario));

                txtQuantidade.setText(String.valueOf(itemPedido.Quantidade));
                txtValorDesconto.setText(String.valueOf(itemPedido.ValorDesconto.setScale(2, BigDecimal.ROUND_HALF_EVEN)));
                txtValorUnitario.setText(String.valueOf(itemPedido.ValorUnitario.setScale(2, BigDecimal.ROUND_HALF_EVEN)));
            }
        };
    }

    private View.OnClickListener setPositiveButton(final AlertDialog alert, final ListObject object, final View convertView)
    {
        return new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                boolean salvar = true;

                EditText txtQuantidade = (EditText) alert.findViewById(R.id.pedido_txtQuantidade);
                EditText txtValorDesconto = (EditText) alert.findViewById(R.id.pedido_txtVlrDesconto);
                EditText txtValorUnitario = (EditText) alert.findViewById(R.id.pedido_txtValorUnitario);
                TextView mQuantidade = (TextView) convertView.findViewById(R.id.pedido_item_card_tvQuantidade);
                TextView mValorUnitario = (TextView) convertView.findViewById(R.id.pedido_item_card_tvValorUnitario);
                TextView mValorDesconto = (TextView) convertView.findViewById(R.id.pedido_item_card_tvValorDesconto);
                TextView mValorTotal = (TextView) convertView.findViewById(R.id.pedido_item_card_tvValorTotal);

                if(txtQuantidade.getText().length() == 0)
                {
                    txtQuantidade.setError(v.getResources().getString(R.string.erro_campo_obrigatorio));
                    salvar = false;
                }

                if(txtValorDesconto.getText().length() == 0)
                {
                    txtValorDesconto.setError(v.getResources().getString(R.string.erro_campo_obrigatorio));
                    salvar = false;
                }

                if(txtValorUnitario.getText().length() == 0)
                {
                    txtValorUnitario.setError(v.getResources().getString(R.string.erro_campo_obrigatorio));
                    salvar = false;
                }

                if(salvar == true) {
                    ItensPedido itemPedido = (ItensPedido) object;
                    ItensPedido item = new ItensPedido();

                    item.Produto = itemPedido.Produto;
                    item.Quantidade = Float.parseFloat(String.valueOf(txtQuantidade.getText()));
                    item.ValorDesconto = BigDecimal.valueOf(MoneyTextWatcher.unmaskMoney(String.valueOf(txtValorDesconto.getText())));
                    item.ValorUnitario = BigDecimal.valueOf(MoneyTextWatcher.unmaskMoney(String.valueOf(txtValorUnitario.getText())));
                    BigDecimal qtd = new BigDecimal(item.Quantidade);
                    item.ValorTotal = item.ValorUnitario.multiply(qtd)
                            .subtract(item.ValorDesconto.multiply(qtd));

                    _itensPedidoBusiness.Editar(item);

                    mQuantidade.setText(String.valueOf(item.Quantidade));

                    NumberFormat format = NumberFormat.getCurrencyInstance();
                    mValorUnitario.setText(format.format(item.ValorUnitario));
                    mValorUnitario.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color.colorPrimaryGreen));

                    mValorDesconto.setText(format.format(item.ValorDesconto));
                    mValorDesconto.setTextColor(ContextCompat.getColor(convertView.getContext(), R.color.colorPrimaryRed));

                    mValorTotal.setText(format.format(item.ValorTotal));
                    mValorTotal.setTypeface(null, Typeface.BOLD);

                    RecalculaValores(_itensPedidoBusiness.getPedidoMemoria());

                    alert.dismiss();
                }
            }
        };
    }

    private CardHeader.OnClickCardHeaderOtherButtonListener CriarItemPedido(){
        return new CardHeader.OnClickCardHeaderOtherButtonListener() {
            @Override
            public void onButtonItemClick(Card card, View view) {

                AutoCompleteTextView cliente = (AutoCompleteTextView) view.findViewById(R.id.pedido_txtCliente);

                Pedido pedido = new Pedido();

                ListagemProdutoFragment fragment = ListagemProdutoFragment.novaInstancia(_produtoBusiness, _itensPedidoBusiness, pedidoFragment, R.id.pedido_fragment_container, true);

                FragmentTransaction transaction = _pedidoActivity.getSupportFragmentManager().beginTransaction();
                transaction.replace(
                        R.id.pedido_fragment_container,
                        fragment,
                        "PEDIDO_CRIAR_PEDIDO_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        };
    }

    private void RecalculaValores(Pedido pedido){
        View view = this.pedidoFragment.getView();

        EditText valorTotalProdutos = (EditText) view.findViewById(R.id.pedido_txtValorTotalProd);
        if(pedido.ValorProduto != null) {
            valorTotalProdutos.setText(String.valueOf(pedido.ValorProduto.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            valorTotalProdutos.setText("0");
        }

        EditText valorTotalDesconto = (EditText) view.findViewById(R.id.pedido_txtValorTotalDesconto);
        if(pedido.ValorDesconto != null) {
            valorTotalDesconto.setText(String.valueOf(pedido.ValorDesconto.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            valorTotalDesconto.setText("0");
        }

        EditText valorTotalAcrescimo = (EditText) view.findViewById(R.id.pedido_txtValorTotalAcrescimo);
        if(pedido.ValorAcrescimo != null) {
            valorTotalAcrescimo.setText(String.valueOf(pedido.ValorAcrescimo.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            valorTotalAcrescimo.setText("0");
        }

        EditText valorTotalPedido = (EditText) view.findViewById(R.id.pedido_txtValorTotalPedido);
        if(pedido.Valor != null) {
            valorTotalPedido.setText(String.valueOf(pedido.Valor.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            valorTotalPedido.setText("0");
        }
    }
}

class ItensPedidoListObject extends ItensPedido implements CardWithList.ListObject
{

    public static ItensPedidoListObject toContatoListObject(ItensPedido item)
    {
        ItensPedidoListObject itensPedidoListObject = new ItensPedidoListObject();

        itensPedidoListObject.Pedido = item.Pedido;
        itensPedidoListObject.ValorUnitario = item.ValorUnitario;
        itensPedidoListObject.ValorTotal = item.ValorTotal;
        itensPedidoListObject.ValorDesconto = item.ValorDesconto;
        itensPedidoListObject.Item = item.Item;
        itensPedidoListObject.Produto = item.Produto;
        itensPedidoListObject.Quantidade = item.Quantidade;

        return itensPedidoListObject;
    }

    /**
     * Returns the object id
     *
     * @return
     */
    @Override
    public String getObjectId() {
        return null;
    }

    /**
     * Returns the parent card
     */
    @Override
    public Card getParentCard() {
        return null;
    }

    /**
     * Register a callback to be invoked when an item in this LinearListView has
     * been clicked.
     *
     * @param onItemClickListener
     * @return The callback to be invoked with an item in this LinearListView has
     * been clicked, or null id no callback has been set.
     */
    @Override
    public void setOnItemClickListener(CardWithList.OnItemClickListener onItemClickListener) {

    }

    /**
     * @return The callback to be invoked with an item in this LinearListView has
     * been clicked, or null id no callback has been set.
     */
    @Override
    public CardWithList.OnItemClickListener getOnItemClickListener() {
        return null;
    }

    /**
     * Indicates if the item is swipeable
     */
    @Override
    public boolean isSwipeable() {
        return false;
    }

    /**
     * Set the item as swipeable
     *
     * @param isSwipeable
     */
    @Override
    public void setSwipeable(boolean isSwipeable) {

    }

    /**
     * Returns the callback to be invoked when item has been swiped
     *
     * @return listener
     */
    @Override
    public CardWithList.OnItemSwipeListener getOnItemSwipeListener() {
        return null;
    }

    /**
     * Register a callback to be invoked when an item in this LinearListView has
     * been swiped.
     *
     * @param onSwipeListener listener
     */
    @Override
    public void setOnItemSwipeListener(CardWithList.OnItemSwipeListener onSwipeListener) {

    }
}