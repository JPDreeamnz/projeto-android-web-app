package br.com.softlinesistemas.catalogodigital.Service;

/**
 * Created by João Pedro R. Carvalho on 13/09/2016.
 */

public enum TipoIntegracaoEnum {
    DOWNLOAD_TABELAS,
    UPLOAD_PEDIDO,
    UPLOAD_TODOS_PEDIDOS
}
