package br.com.softlinesistemas.catalogodigital.Service;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by João Pedro R. Carvalho on 14/09/2016.
 */

public class LoginTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    private String URL;
    private IIntegracaoService _integracao;
    private TipoIntegracaoEnum tipoIntegracao;
    private String TOKEN;

    public LoginTask(String URL, IIntegracaoService integracao, TipoIntegracaoEnum tipoIntegracao)
    {
        this.URL = URL;
        this._integracao = integracao;
        this.tipoIntegracao = tipoIntegracao;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        Pair<Integer, String> resposta;
        try {
            Map<String, String> data = new HashMap<>();
            data.put("Username", "Master");
            data.put("Password", "master123");

            HttpRequest requestLogin = HttpRequest.post("http://" + this.URL + "Api/Auth/Post")
                    .accept("application/json")
                    .form(data);

            int code = requestLogin.code();
            JSONObject json = new JSONObject(requestLogin.body());
            this.TOKEN = json.getString("Token");

            resposta = new Pair<>(code, this.TOKEN);
        } catch (HttpRequest.HttpRequestException e) {
            resposta = new Pair<>(500, "Não foi possível validar o login.");
        } catch (JSONException e) {
            resposta = new Pair<>(500, "Não foi possível validar o login.");
        }

        return resposta;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        if (resposta.first != 200) {
            this._integracao.AtualizarPush(false, "Não foi possível executar a operação. Tente novamente.");
            this._integracao.Dispose();
        } else {
            switch (this.tipoIntegracao) {
                case DOWNLOAD_TABELAS: {
                    this._integracao.DownloadTabelas(this.TOKEN);
                }
                break;
                case UPLOAD_TODOS_PEDIDOS: {
                    this._integracao.UploadPedidos(this.TOKEN);
                }
                break;
            }
        }
    }
}
