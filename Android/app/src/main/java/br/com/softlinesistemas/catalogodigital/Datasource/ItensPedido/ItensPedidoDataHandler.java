package br.com.softlinesistemas.catalogodigital.Datasource.ItensPedido;

import android.content.ContentValues;
import android.database.Cursor;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;

/**
 * Created by João Pedro R. Carvalho on 24/07/2016.
 */

public class ItensPedidoDataHandler {
    public static ItensPedido fromCursor(Cursor cursor) throws ParseException {
        ItensPedido itensPedido = null;

        if(cursor.moveToFirst())
        {
            itensPedido = BindCursorItensPedido(cursor);
        }

        cursor.close();
        return itensPedido;
    }

    public static List<ItensPedido> fromCursorToList(Cursor cursor) throws ParseException {
        List<ItensPedido> itensPedidos = new ArrayList<ItensPedido>();

        if(cursor.moveToFirst())
        {
            do{
                itensPedidos.add(BindCursorItensPedido(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return itensPedidos;
    }

    public static ContentValues toContentValues(ItensPedido itensPedido)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.ItensPedido.Coluna_Pedido, itensPedido.Pedido);
        values.put(DatabaseContract.ItensPedido.Coluna_Produto, itensPedido.Produto);
        values.put(DatabaseContract.ItensPedido.Coluna_Quantidade, itensPedido.Quantidade);
        values.put(DatabaseContract.ItensPedido.Coluna_ValorDesconto, itensPedido.ValorDesconto.doubleValue());
        values.put(DatabaseContract.ItensPedido.Coluna_ValorTotal, itensPedido.ValorTotal.doubleValue());
        values.put(DatabaseContract.ItensPedido.Coluna_ValorUnitario, itensPedido.ValorUnitario.doubleValue());

        return values;
    }

    private static ItensPedido BindCursorItensPedido(Cursor cursor) throws ParseException {
        ItensPedido itensPedido = new ItensPedido();

        itensPedido.Item = cursor.getInt(
                cursor.getColumnIndex(DatabaseContract.ItensPedido.Coluna_Item)
        );
        itensPedido.Pedido = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.ItensPedido.Coluna_Pedido)
        );
        itensPedido.Produto = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.ItensPedido.Coluna_Produto)
        );
        itensPedido.Quantidade = cursor.getFloat(
                cursor.getColumnIndex(DatabaseContract.ItensPedido.Coluna_Quantidade)
        );
        itensPedido.ValorDesconto = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.ItensPedido.Coluna_ValorDesconto)
        ));
        itensPedido.ValorTotal = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.ItensPedido.Coluna_ValorTotal)
        ));
        itensPedido.ValorUnitario = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.ItensPedido.Coluna_ValorUnitario)
        ));

        return itensPedido;
    }
}
