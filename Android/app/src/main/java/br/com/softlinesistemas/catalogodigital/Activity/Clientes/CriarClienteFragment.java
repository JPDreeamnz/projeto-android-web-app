package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.content.Context;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import br.com.softlinesistemas.catalogodigital.Activity.BaseFragment;
import br.com.softlinesistemas.catalogodigital.Activity.MoneyTextWatcher;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.view.CardViewNative;

/**
 * Created by João Pedro R. Carvalho on 07/06/2016.
 */

public class CriarClienteFragment extends BaseFragment implements IClientesActivity {
    private ClientesBusiness _clienteBusiness;

    private ListagemEnderecoCard _listagemEnderecoCard;
    private ListagemContatoCard _listagemContatoCard;

    private Spinner _spTipoDocumento;

    private EditText _txtNome,
                     _txtDocumento,
                     _txtInscricaoEstadual,
                     _txtLimiteCredito;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewCriar = inflater.inflate(R.layout.clientes_criar_fragment, null);
        this.setView(viewCriar);
        this.setHasOptionsMenu(true);

        return viewCriar;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.clientes_criar_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.clientes_salvar_button){
            View view = this.getView();
            Clientes cliente = new Clientes();

            this._txtDocumento = (EditText) view.findViewById(R.id.clientes_textDocumento);
            this._txtInscricaoEstadual = (EditText) view.findViewById(R.id.clientes_textInscricaoEstadual);
            this._txtLimiteCredito = (EditText) view.findViewById(R.id.clientes_textLimiteCredito);
            this._txtNome = (EditText) view.findViewById(R.id.clientes_textNome);
            this._spTipoDocumento = (Spinner) view.findViewById(R.id.clientes_spTipoDocumento);

            if (this._txtNome.getText().toString() == null || this._txtNome.getText().toString().isEmpty()) {
                this._txtNome.setError("Campo nome obrigatório.");
                return false;
            }

            cliente.Enderecos = this._clienteBusiness.getEnderecosSalvar();
            cliente.Contatos = this._clienteBusiness.getContatosSalvar();
            cliente.InscricaoEstadual = this._txtInscricaoEstadual.getText().toString();
            cliente.Documento = this._txtDocumento.getText().toString();

            if(!this._txtLimiteCredito.getText().toString().isEmpty())
                cliente.LimiteCredito = MoneyTextWatcher.unmaskMoneyBigDecimal(this._txtLimiteCredito.getText().toString());

            cliente.Nome = this._txtNome.getText().toString();

            if(this._spTipoDocumento.getSelectedItem().toString().toUpperCase().equals("CNPJ")){
                cliente.TipoDocumento = 1;
            } else {
                cliente.TipoDocumento = 2;
            }

            Pair<Boolean, String> existeCliente = this._clienteBusiness.ExisteCliente(cliente.Codigo, cliente.TipoDocumento, cliente.Documento);
            if(existeCliente != null && existeCliente.first)
            {
                this._txtDocumento.setError("O cliente " + existeCliente.second + " foi cadastrado com esse documento.");
                return false;
            }

            this._clienteBusiness.Incluir(cliente);

            EscondeKeyboard(view, view.getContext());

            getActivity().getFragmentManager().popBackStack();
        }

        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public void setClienteBusiness(ClientesBusiness clientesBusiness){
        this._clienteBusiness = clientesBusiness;
    }

    private void setView(View view)
    {
        //Iniciando o dropdown
        this._spTipoDocumento = (Spinner) view.findViewById(R.id.clientes_spTipoDocumento);
        ArrayAdapter<CharSequence> adaptador = ArrayAdapter.createFromResource(view.getContext(),
                R.array.tipo_documento_spinner, android.R.layout.simple_spinner_item);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this._spTipoDocumento.setAdapter(adaptador);

        this._txtLimiteCredito = (EditText) view.findViewById(R.id.clientes_textLimiteCredito);
        this._txtLimiteCredito.addTextChangedListener(new MoneyTextWatcher(this._txtLimiteCredito));

        // Iniciando a visualização da listagem de endereços
        this._listagemEnderecoCard = new ListagemEnderecoCard(
                this.getActivity().getBaseContext(),
                this._clienteBusiness,
                this, false);
        this._listagemEnderecoCard.init();
        CardViewNative cardViewEndereco = (CardViewNative) view.findViewById(R.id.clientes_enderecos);
        cardViewEndereco.setCard(_listagemEnderecoCard);

        // Iniciando a visualização da listagem de contatos
        this._listagemContatoCard = new ListagemContatoCard(this.getActivity().getBaseContext(),
                this._clienteBusiness, this, false);
        this._listagemContatoCard.init();
        CardViewNative cardViewContato = (CardViewNative) view.findViewById(R.id.clientes_contatos);
        cardViewContato.setCard(_listagemContatoCard);
    }

    @Override
    public void onBackPressed() {}

    public void EscondeKeyboard(View v, Context context)
    {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
