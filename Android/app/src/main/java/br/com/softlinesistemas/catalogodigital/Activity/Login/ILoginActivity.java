package br.com.softlinesistemas.catalogodigital.Activity.Login;

/**
 * Created by João Pedro R. Carvalho on 20/12/2015.
 */
public interface ILoginActivity {
    void LoginSucesso();
    void LoginErro(String erro);
}
