package br.com.softlinesistemas.catalogodigital.Datasource.PlanoPagamento;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.PlanoPagamento;

/**
 * Created by João Pedro R. Carvalho on 27/07/2016.
 */

public class PlanoPagamentoDatasource extends BaseHelper {
    /**
     * BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     * do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     *
     * @param context
     */
    public PlanoPagamentoDatasource(Context context) {
        super(context);
    }

    public List<PlanoPagamento> ObtemLista()
    {
        List<PlanoPagamento> entidade;
        this.OpenRead();
        entidade = PlanoPagamentoDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.PlanoPagamento.Tabela_Nome,
                        DatabaseContract.PlanoPagamento.Tabela_TodasColunas,
                        null,null,null, null, null
                )
        );
        this.Close(false);

        return entidade;
    }

    public PlanoPagamento ObterPlano(long codigo) throws ParseException {
        this.OpenWrite();
        PlanoPagamento plano = PlanoPagamentoDataHandler.fromCursor(
                database.query(DatabaseContract.PlanoPagamento.Tabela_Nome,
                        DatabaseContract.PlanoPagamento.Tabela_TodasColunas,
                        DatabaseContract.PlanoPagamento.Coluna_Codigo + " = ?",
                        new String[]{ String.valueOf(codigo)}, null, null, null));
        this.Close(false);

        return plano;
    }

    public boolean ExitePlano(long codigo)
    {
        this.OpenRead();
        Cursor cursor = database.query(DatabaseContract.PlanoPagamento.Tabela_Nome,
                DatabaseContract.PlanoPagamento.Tabela_TodasColunas,
                DatabaseContract.PlanoPagamento.Coluna_Codigo + " = ?",
                new String[]{ String.valueOf(codigo)}, null, null, null);

        boolean retorno = cursor.moveToFirst();
        cursor.close();
        this.Close(false);

        return retorno;
    }

    public PlanoPagamento Incluir(PlanoPagamento instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.Codigo = database.insert(DatabaseContract.PlanoPagamento.Tabela_Nome, null, PlanoPagamentoDataHandler.toContentValue(instancia));
        this.Close(false);

        return instancia;
    }

    public PlanoPagamento Editar(PlanoPagamento instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.Codigo = database.update(
                DatabaseContract.PlanoPagamento.Tabela_Nome,
                PlanoPagamentoDataHandler.toContentValue(instancia),
                DatabaseContract.PlanoPagamento.Coluna_Codigo + " = ?",
                new String[]{ String.valueOf(instancia.Codigo)});
        this.Close(false);

        return instancia;
    }

    public boolean Excluir(long codigo)
    {
        this.OpenWrite();

        long rows = database.delete(
                DatabaseContract.PlanoPagamento.Tabela_Nome,
                DatabaseContract.PlanoPagamento.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(codigo)}
        );

        this.Close(false);
        return (rows > 0);
    }
}
