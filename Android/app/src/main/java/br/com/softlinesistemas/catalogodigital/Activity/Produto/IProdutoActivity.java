package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.support.v4.app.Fragment;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public interface IProdutoActivity {
    void onBackPressed();
    void AttachFragment(Fragment fragment, String tag);
}
