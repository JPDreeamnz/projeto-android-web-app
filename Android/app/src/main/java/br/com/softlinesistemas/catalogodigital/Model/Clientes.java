package br.com.softlinesistemas.catalogodigital.Model;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by João Pedro R. Carvalho on 14/03/2016.
 */
public class Clientes {
    public long Codigo;
    public String Nome;
    public int TipoDocumento;
    public String Documento;
    public String InscricaoEstadual;
    public BigDecimal LimiteCredito;

    public List<Endereco> Enderecos;
    public List<Contato> Contatos;
}
