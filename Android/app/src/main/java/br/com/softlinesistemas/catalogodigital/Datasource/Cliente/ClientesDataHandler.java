package br.com.softlinesistemas.catalogodigital.Datasource.Cliente;

import android.content.ContentValues;
import android.database.Cursor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;

/**
 * Created by João Pedro R. Carvalho on 17/03/2016.
 */
public class ClientesDataHandler {
    public static Clientes fromCursor(Cursor cursor)
    {
        Clientes cliente = null;

        if(cursor.moveToFirst())
        {
            cliente = BindCursorCliente(cursor);
        }

        cursor.close();
        return cliente;
    }

    public static List<Clientes> fromCursorToList(Cursor cursor)
    {
        List<Clientes> contatos = new ArrayList<Clientes>();

        if(cursor.moveToFirst())
        {
            do{
                contatos.add(BindCursorCliente(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return contatos;
    }

    public static ContentValues toContentValues(Clientes cliente)
    {
        ContentValues values = new ContentValues();

        if(cliente.Codigo > 0) values.put(DatabaseContract.Clientes.Coluna_Codigo, cliente.Codigo);
        values.put(DatabaseContract.Clientes.Coluna_Documento, cliente.Documento);
        values.put(DatabaseContract.Clientes.Coluna_InscricaoEstadual, cliente.InscricaoEstadual);
        values.put(DatabaseContract.Clientes.Coluna_LimiteCredito, cliente.LimiteCredito.doubleValue());
        values.put(DatabaseContract.Clientes.Coluna_Nome, cliente.Nome);
        values.put(DatabaseContract.Clientes.Coluna_TipoDocumento, cliente.TipoDocumento);

        return values;
    }

    public static int FromCursorToInt(Cursor cursor)
    {
        int retorno = 0;
        if(cursor.moveToFirst())
        {
            retorno = cursor.getInt(cursor.getColumnIndex("Quantidade"));
        }

        cursor.close();

        return retorno;
    }

    private static Clientes BindCursorCliente(Cursor cursor)
    {
        Clientes cliente = new Clientes();

        cliente.Codigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Clientes.Coluna_Codigo)
        );
        cliente.Documento = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Clientes.Coluna_Documento)
        );
        cliente.InscricaoEstadual = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Clientes.Coluna_InscricaoEstadual)
        );
        cliente.LimiteCredito = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.Clientes.Coluna_LimiteCredito)
        ));
        cliente.Nome = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Clientes.Coluna_Nome)
        );
        cliente.TipoDocumento = cursor.getInt(
                cursor.getColumnIndex(DatabaseContract.Clientes.Coluna_TipoDocumento)
        );

        return cliente;
    }
}
