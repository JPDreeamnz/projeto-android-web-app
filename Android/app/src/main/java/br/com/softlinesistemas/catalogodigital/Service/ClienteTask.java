package br.com.softlinesistemas.catalogodigital.Service;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONArray;

import java.math.BigDecimal;
import java.util.ArrayList;

import br.com.softlinesistemas.catalogodigital.Datasource.Cliente.ClientesDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;

/**
 * Created by João Pedro R. Carvalho on 15/09/2016.
 */

public class ClienteTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    ClientesDatasource _dtClientes;
    IIntegracaoService _integracao;
    String URL;

    public ClienteTask(String URL, ClientesDatasource dtClientes, IIntegracaoService integracao){
        this._dtClientes = dtClientes;
        this._integracao = integracao;
        this.URL = "http://" + URL + "api/Integracao/ObterClientes";
    }
    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        Pair<Integer, String> respota;
        try {
            HttpRequest request = HttpRequest.get(this.URL, true, "dataRegistro", params[0])
                    .accept("application/json")
                    .header("Authorization", "Bearer " + params[1]);

            respota = new Pair<>(request.code(), request.body());
        } catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, "Não foi possível obter os clientes.");
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        try{
            if(resposta.first == 200) {
                JSONArray json = new JSONArray(resposta.second);
                for(int i = 0; i < json.length(); i++) {
                    Clientes novoCliente = new Clientes();

                    novoCliente.Codigo = json.getJSONObject(i).getLong("Codigo");
                    novoCliente.Nome = json.getJSONObject(i).getString("Nome");
                    novoCliente.TipoDocumento = json.getJSONObject(i).isNull("TipoDocumento") ? 0 : json.getJSONObject(i).getInt("TipoDocumento");
                    novoCliente.Documento = json.getJSONObject(i).getString("Documento");
                    novoCliente.InscricaoEstadual = json.getJSONObject(i).getString("InscricaoEstadual");
                    Double limite = json.getJSONObject(i).isNull("LimiteCredito") ? new Double(0) : json.getJSONObject(i).getDouble("LimiteCredito");
                    novoCliente.LimiteCredito = new BigDecimal(limite);
                    novoCliente.Enderecos = new ArrayList<>();
                    novoCliente.Contatos = new ArrayList<>();

                    JSONArray endereco = json.getJSONObject(i).getJSONArray("Endereco");
                    for(int j = 0; j < endereco.length(); j++)
                    {
                        Endereco novoEndereco = new Endereco();

                        novoEndereco.EnderecoID = endereco.getJSONObject(j).getLong("EnderecoID");
                        novoEndereco.Endereco = endereco.getJSONObject(j).getString("Endereco");
                        novoEndereco.Numero = endereco.getJSONObject(j).getString("Numero");
                        novoEndereco.Complemento = endereco.getJSONObject(j).getString("Complemento");
                        novoEndereco.Bairro = endereco.getJSONObject(j).getString("Bairro");
                        novoEndereco.Cidade = endereco.getJSONObject(j).getString("Cidade");
                        novoEndereco.Estado = endereco.getJSONObject(j).getString("Estado");
                        novoEndereco.CEP = endereco.getJSONObject(j).isNull("CEP") ? 0 : endereco.getJSONObject(j).getLong("CEP");

                        novoCliente.Enderecos.add(novoEndereco);
                    }

                    JSONArray contatos = json.getJSONObject(i).getJSONArray("Contato");
                    for(int k = 0; k < contatos.length(); k++)
                    {
                        Contato novoContato = new Contato();

                        novoContato.ContatoID = contatos.getJSONObject(k).getLong("ContatoID");
                        novoContato.DDD = contatos.getJSONObject(k).getString("DDD");
                        novoContato.Telefone = contatos.getJSONObject(k).getString("Telefone");
                        novoContato.Email = contatos.getJSONObject(k).getString("Email");
                        novoContato.TipoContato = contatos.getJSONObject(k).getLong("TipoContato");

                        novoCliente.Contatos.add(novoContato);
                    }

                    if(this._dtClientes.ExiteCliente(novoCliente.Codigo)) {
                        this._dtClientes.Excluir(novoCliente);
                    }

                    this._dtClientes.Incluir(novoCliente);
                }

                this._integracao.AtualizarPush(true, "Tabela clientes atualizada com sucesso.");
            }else{
                this._integracao.AtualizarPush(true, resposta.first + " - Não foi possível atualizar os clientes");
            }
        } catch (Exception e){
            this._integracao.AtualizarPush(true, "Não foi possível atualizar os clientes.");
        }
    }
}
