package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import br.com.softlinesistemas.catalogodigital.Activity.BaseFragment;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 17/03/2017.
 */

public class EditarContatoFragment extends BaseFragment {
    private long _contatoCodigo;
    private boolean isCarregaEditar = false;
    private ClientesBusiness _clientesBusiness;

    private Spinner _spTipoContato;
    private EditText _txtContato,
            _txtDDD;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View editarContatoView = inflater.inflate(R.layout.clientes_criar_contato, null);
        this.setView(editarContatoView);
        this.setHasOptionsMenu(true);
        return editarContatoView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.contato_criar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.contato_salvar_button){
            Contato contato = new Contato();

            this._spTipoContato = (Spinner) this.getView().findViewById(R.id.contato_spTipoCOntato);
            this._txtContato = (EditText) this.getView().findViewById(R.id.contato_txtContato);

            if (this._txtContato.getText().toString() == null || this._txtContato.getText().toString().isEmpty()) {
                this._txtContato.setError("Campo Obrigatório.");
                return false;
            }

            if(this._spTipoContato.getSelectedItem().toString().equalsIgnoreCase("E-MAIL")){
                contato.TipoContato = 3;
                contato.Email = this._txtContato.getText().toString();
                if(!Patterns.EMAIL_ADDRESS.matcher(contato.Email).matches())
                {
                    this._txtContato.setError("Formato de E-mail inválido.");
                    return false;
                }
            } else {
                this._txtDDD = (EditText) this.getView().findViewById(R.id.contato_txtDDD);

                if (this._txtDDD.getText().toString() == null || this._txtDDD.getText().toString().isEmpty()) {
                    this._txtDDD.setError("Campo DDD Obrigatório.");
                    return false;
                }

                contato.TipoContato =
                        this._spTipoContato.getSelectedItem().toString().toUpperCase() == "FIXO" ?
                                1 : 2;
                contato.DDD = this._txtDDD.getText().toString();
                contato.Telefone = this._txtContato.getText().toString();
            }

            contato.ContatoID = this._contatoCodigo;
            this._clientesBusiness.EditarContato(contato);
            EscondeKeyboard(this.getView(), getActivity());

            getActivity().getFragmentManager().popBackStack();
        }

        return true;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public void setView(View view){
        this._spTipoContato = (Spinner) view.findViewById(R.id.contato_spTipoCOntato);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(),
                R.array.tipo_contato_spinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this._spTipoContato.setAdapter(adapter);

        for (Contato contato : this._clientesBusiness.getContatos())
        {
            if(contato.ContatoID == this._contatoCodigo)
            {
                this.isCarregaEditar = true;
                this._spTipoContato.setSelection(((int)contato.TipoContato - 1));
                switch ((int)contato.TipoContato)
                {
                    case 3:
                        _txtContato = (EditText) view.findViewById(R.id.contato_txtContato);
                        InputFilter[] FilterArray = new InputFilter[1];
                        FilterArray[0] = new InputFilter.LengthFilter(50);
                        _txtContato.setFilters(FilterArray);
                        _txtContato.setText(contato.Email, TextView.BufferType.EDITABLE);
                        break;
                    default:
                        _txtContato = (EditText) view.findViewById(R.id.contato_txtContato);
                        _txtDDD = (EditText) view.findViewById(R.id.contato_txtDDD);

                        _txtDDD.setText(String.valueOf(contato.DDD));
                        _txtContato.setText(String.valueOf(contato.Telefone), TextView.BufferType.EDITABLE);
                        break;
                }
            }
        }

        this._spTipoContato.setOnItemSelectedListener(this.TipoContatoClick());
    }

    public void setContatoCodigo(long contatoCodigo)
    {
        this._contatoCodigo = contatoCodigo;
    }

    public void setClientesBusiness(ClientesBusiness clientesBusiness){
        this._clientesBusiness = clientesBusiness;
    }

    public Spinner.OnItemSelectedListener TipoContatoClick(){
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] tipoContato = getResources().getStringArray(R.array.tipo_contato_spinner);

                TextView abreParenteses = (TextView) parent.getRootView().findViewById(R.id.contato_txtDDDAParenteses),
                        fechaParenteses = (TextView) parent.getRootView().findViewById(R.id.contato_txtDDDFParenteses);
                TextInputLayout labelDDD = (TextInputLayout) parent.getRootView().findViewById(R.id.contato_labelDDD),
                        labelContato = (TextInputLayout) parent.getRootView().findViewById(R.id.contato_labelContato);
                _txtContato = (EditText) parent.getRootView().findViewById(R.id.contato_txtContato);
                _txtContato.setError(null);

                if(!isCarregaEditar)
                    _txtContato.setText(null);
                else
                    isCarregaEditar = false;

                if(tipoContato[position].toUpperCase().equals("E-MAIL")){
                    abreParenteses.setVisibility(View.GONE);
                    fechaParenteses.setVisibility(View.GONE);
                    labelDDD.setVisibility(View.GONE);

                    labelContato.setHint(getResources().getString(R.string.Email));
                    _txtContato.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                    if(getResources().getBoolean(R.bool.ehTablet)) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(300, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.topMargin = 10;
                        _txtContato.setLayoutParams(params);
                    } else { }

                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(50);
                    _txtContato.setFilters(FilterArray);
                } else {
                    labelContato.setHint(getResources().getString(R.string.Telefone));
                    _txtContato.setInputType(InputType.TYPE_CLASS_PHONE);

                    if(getResources().getBoolean(R.bool.ehTablet)) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(150, ViewGroup.LayoutParams.MATCH_PARENT);
                        params.topMargin = 10;
                        _txtContato.setLayoutParams(params);
                    } else { }

                    InputFilter[] FilterArray = new InputFilter[1];
                    FilterArray[0] = new InputFilter.LengthFilter(9);
                    _txtContato.setFilters(FilterArray);

                    if(labelDDD.getVisibility() == View.GONE){
                        abreParenteses.setVisibility(View.VISIBLE);
                        fechaParenteses.setVisibility(View.VISIBLE);
                        labelDDD.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        };
    }

    public void EscondeKeyboard(View v, Context context)
    {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
