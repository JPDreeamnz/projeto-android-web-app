package br.com.softlinesistemas.catalogodigital.Business;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Datasource.Produto.ProdutoDataSource;
import br.com.softlinesistemas.catalogodigital.Model.Produto;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public class ProdutoBusiness {
    private ProdutoDataSource _dtProduto;

    public ProdutoBusiness(Context context){
        this._dtProduto = new ProdutoDataSource(context);
    }

    public Produto ObterProduto(long codigo)
    {
        try
        {
            return this._dtProduto.ObterUnico(codigo);
        }catch (SQLiteException sql){
            //TODO: Implementar Log
        }catch (Exception e){
            //TODO: Implementar Log
        }

        return null;
    }

    public List<Produto> ObterTodos(int pagina)
    {
        List<Produto> retorno = new ArrayList<Produto>();
        try
        {
            retorno = this._dtProduto.ObterTodos(pagina);
        }catch (SQLiteException sql){
            String erro = sql.getMessage();
            //TODO: Implementar Log
        }catch (Exception e) {
            //TODO: Implementar Log
        }

        return retorno;
    }

    public List<Produto> ObterTodos(String busca, int pagina)
    {
        List<Produto> retorno = new ArrayList<Produto>();
        try
        {
            retorno = this._dtProduto.ObterTodos(busca, pagina);
        }catch (SQLiteException sql){
            String erro = sql.getMessage();
            //TODO: Implementar Log
        }catch (Exception e) {
            //TODO: Implementar Log
        }

        return retorno;
    }

    public String ObterNomeProduto(long produtoCodigo)
    {
        String retorno = null;
        try
        {
            retorno = this._dtProduto.ObterNomeProduto(produtoCodigo);
        }catch (SQLiteException sql){
            String erro = sql.getMessage();
            //TODO: Implementar Log
        }catch (Exception e) {
            //TODO: Implementar Log
        }

        return retorno;
    }

    public int ObterQuantidadeProdutos(String busca)
    {
        int retorno = 0;
        try
        {
            retorno = this._dtProduto.ObterQuantidadeProdutos(busca);
        }catch (SQLiteException sql){
            String erro = sql.getMessage();
            //TODO: Implementar Log
        }catch (Exception e) {
            //TODO: Implementar Log
        }

        return retorno;
    }
}
