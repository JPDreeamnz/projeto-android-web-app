package br.com.softlinesistemas.catalogodigital.Datasource.Configuracao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.ParseException;

import java.util.Calendar;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.SincroniaTabelas;

/**
 * Created by João Pedro R. Carvalho on 12/04/2017.
 */

public class SincroniaTabelasDataSource extends BaseHelper {
    /**
     * BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     * do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     *
     * @param context
     */
    public SincroniaTabelasDataSource(Context context) {
        super(context);
    }

    public boolean ExisteRegistro() throws SQLiteException
    {
        this.OpenRead();
        Cursor cursor = database.query(
                DatabaseContract.SincroniaTabelas.Tabela_Nome,
                DatabaseContract.SincroniaTabelas.Tabela_TodasColunas,
                DatabaseContract.SincroniaTabelas.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(1)},
                null, null, null
        );

        boolean retorno = cursor.moveToFirst();
        cursor.close();
        this.Close(false);

        return retorno;
    }

    public boolean Editar(SincroniaTabelas instancia) throws SQLiteException, ParseException
    {
        if(!ExisteRegistro())
            instancia = CriarInstancia();

        this.OpenRead();
        int rows = database.update(
                DatabaseContract.SincroniaTabelas.Tabela_Nome,
                SincroniaTabelasDataHandler.toContentValues(instancia),
                DatabaseContract.SincroniaTabelas.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(1)}
        );
        this.Close(true);

        return (rows != 0);
    }

    public SincroniaTabelas Obter() throws SQLiteException, ParseException, java.text.ParseException {
        if(!ExisteRegistro())
        {
            CriarInstancia();
        }

        this.OpenRead();
        SincroniaTabelas retorno = SincroniaTabelasDataHandler.fromCursor(
                database.query(
                        DatabaseContract.SincroniaTabelas.Tabela_Nome,
                        DatabaseContract.SincroniaTabelas.Tabela_TodasColunas,
                        DatabaseContract.SincroniaTabelas.Coluna_Codigo + " = ?",
                        new String[] {String.valueOf(1)},
                        null, null, null
                ));
        this.Close(false);

        return retorno;
    }

    private SincroniaTabelas CriarInstancia() throws SQLiteException, ParseException
    {
        SincroniaTabelas obj = new SincroniaTabelas();

        obj.Codigo = 1;

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1990);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        obj.Data = cal.getTime();

        this.OpenWrite();
        database.insert(
                DatabaseContract.SincroniaTabelas.Tabela_Nome,
                null,
                SincroniaTabelasDataHandler.toContentValues(obj)
        );
        this.Close(false);

        return obj;
    }
}
