package br.com.softlinesistemas.catalogodigital.Datasource.Configuracao;

import android.content.Context;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by João Pedro R. Carvalho on 11/12/2016.
 */

public class ConfiguracaoDatasource {
    private Context _contexto;
    private File _diretorio;
    private static String _pasta;

    public ConfiguracaoDatasource(Context contexto){
        this._contexto = contexto;
        this._pasta = contexto.getFilesDir().getAbsolutePath();
        this._diretorio = new File(this._pasta + "/confg.txt");

        if(!this._diretorio.exists()){
            this.CriarDiretorio();
        }
    }

    public boolean CriarDiretorio(){
        this._diretorio.getParentFile().mkdirs();

        String repoServ = "52.67.101.75:83";
        String authServ = "52.67.101.75:81";

        CriarConfiguracao(repoServ + ";" + authServ);

        return this._diretorio.exists();
    }

    public boolean EditarConfiguracao(String authServ, String repoServ)
    {
        CriarConfiguracao(repoServ + ";" + authServ);
        return this._diretorio.exists();
    }

    public void CriarConfiguracao(String sBody) {
        try {
            FileWriter writer = new FileWriter(_diretorio);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String[] CarregarConfiguracao()
    {
        String linha = "";
        StringBuilder builder = new StringBuilder();

        try{
            BufferedReader br = new BufferedReader(new FileReader(_diretorio));

            while ((linha = br.readLine()) != null){
                builder.append(linha);
            }
            br.close();
        } catch (IOException e){
            e.printStackTrace();
        }
        return builder.toString().split(";");
    }
}
