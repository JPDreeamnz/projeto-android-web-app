package br.com.softlinesistemas.catalogodigital.Model;

/**
 * Created by João Pedro R. Carvalho on 18/02/2016.
 */
public class Contato {
    public long ContatoID;
    public String DDD;
    public String Telefone;
    public String Email;
    public long TipoContato;
    public long ClienteCodigo;

    public boolean CompararContatos(Contato contato)
    {
        if(this.TipoContato ==  1 || this.TipoContato == 2){
            return
                    this.ContatoID == contato.ContatoID &&
                    this.ClienteCodigo == contato.ClienteCodigo &&
                    this.TipoContato == contato.TipoContato &&
                    this.DDD.equalsIgnoreCase(contato.DDD) &&
                    this.Telefone.equalsIgnoreCase(contato.Telefone);
        } else {
            return
                    this.ContatoID == contato.ContatoID &&
                    this.ClienteCodigo == contato.ClienteCodigo &&
                    this.TipoContato == contato.TipoContato &&
                    this.Email.equalsIgnoreCase(contato.Email);
        }

    }
}
