package br.com.softlinesistemas.catalogodigital.Model.POJO;

/**
 * Created by João Pedro R. Carvalho on 10/04/2017.
 */

public class DadosImagem {
    public long CodigoImagem;
    public long CodigoProduto;
    public String CaminhoPasta;
    public String URL_IMAGEM;
}
