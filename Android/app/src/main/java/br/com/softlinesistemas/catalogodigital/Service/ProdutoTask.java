package br.com.softlinesistemas.catalogodigital.Service;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONArray;

import br.com.softlinesistemas.catalogodigital.Datasource.Produto.ProdutoDataSource;
import br.com.softlinesistemas.catalogodigital.Model.POJO.DadosImagem;
import br.com.softlinesistemas.catalogodigital.Model.Produto;

/**
 * Created by João Pedro R. Carvalho on 15/09/2016.
 */

public class ProdutoTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    Context _contexto;
    ProdutoDataSource _dtProduto;
    IIntegracaoService _integracao;
    ControleImagemDownload _controleImagemDownload;
    String TOKEN;
    String URL;
    String URL_IMAGEM;

    public ProdutoTask(
            String URL,
            ProdutoDataSource dtProduto,
            Context contexto,
            IIntegracaoService integracao,
            ControleImagemDownload controleImagemDownload){
        this._dtProduto = dtProduto;
        this._integracao = integracao;
        this._contexto = contexto;
        this._controleImagemDownload = controleImagemDownload;
        this.URL = "http://" + URL + "api/Integracao/ObterProdutosComPreco";
        this.URL_IMAGEM = "http://" + URL + "api/Integracao/ObterImagem";
    }
    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        Pair<Integer, String> respota;
        try {
            this.TOKEN = params[1];

            HttpRequest request = HttpRequest.get(this.URL, true, "dataRegistros", params[0])
                    .accept("application/json")
                    .header("Authorization", "Bearer " + params[1]);

            respota = new Pair<>(request.code(), request.body());
        } catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, "Não foi possível obter os produtos.");
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        try{
            if(resposta.first == 200) {
                JSONArray json = new JSONArray(resposta.second);
                for(int i = 0; i < json.length(); i++) {
                    Produto novoProduto = new Produto();

                    novoProduto.Codigo = json.getJSONObject(i).getLong("Codigo");
                    novoProduto.Nome = json.getJSONObject(i).getString("Nome");
                    novoProduto.Unidade = json.getJSONObject(i).getString("Unidade");
                    novoProduto.CodigoBarras = json.getJSONObject(i).getString("CodigoBarras");
                    novoProduto.CodigoFabrica = json.getJSONObject(i).getString("CodigoFabrica");
                    novoProduto.CodigoOriginal = json.getJSONObject(i).getString("CodigoOriginal");
                    novoProduto.Cor = CheckNull(json.getJSONObject(i).getString("Cor"));
                    novoProduto.Tamanho = CheckNull(json.getJSONObject(i).getString("Tamanho"));
                    novoProduto.Marca = json.getJSONObject(i).getString("Marca");
                    Double valorVenda = json.getJSONObject(i).isNull("ValorVenda") ? new Double(0) : json.getJSONObject(i).getDouble("ValorVenda");
                    novoProduto.ValorVenda = valorVenda.floatValue();
                    Double quantidadeEstoque = json.getJSONObject(i).isNull("QuantidadeEstoque") ? new Double(0) : json.getJSONObject(i).getDouble("QuantidadeEstoque");
                    novoProduto.QuantidadeEstoque = quantidadeEstoque == null ? 0 : quantidadeEstoque.floatValue();

                    JSONArray imagens = json.getJSONObject(i).getJSONArray("ImagensProduto");
                    for(int j = 0; j < imagens.length(); j++)
                    {
                        DadosImagem dadosImagem = new DadosImagem();
                        dadosImagem.CodigoImagem = imagens.getJSONObject(j).getInt("Sequencial");
                        dadosImagem.CaminhoPasta = imagens.getJSONObject(j).getString("CaminhoPasta");
                        dadosImagem.CodigoProduto = novoProduto.Codigo;
                        dadosImagem.URL_IMAGEM = this.URL_IMAGEM;

                        this._controleImagemDownload.AddDadosImagem(dadosImagem);
                    }

                    if(this._dtProduto.ExisteProduto(novoProduto.Codigo))
                        this._dtProduto.Editar(novoProduto);
                    else
                        this._dtProduto.Incluir(novoProduto);
                }

                this._integracao.AtualizarPush(true, "Tabela produtos atualizada com sucesso.");
                this._integracao.DownloadImagens();
            }else{
                this._integracao.AtualizarPush(true, resposta.first + " - Não foi possível atualizar os produtos");
            }
        } catch (Exception e){
            this._integracao.AtualizarPush(true, "Não foi possível atualizar os produtos.");
        }
    }

    private String CheckNull(String campo)
    {
        if(campo == "null")
        {
            return null;
        } else {
            return campo;
        }
    }
}
