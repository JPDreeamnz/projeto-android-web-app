package br.com.softlinesistemas.catalogodigital.Business;

import java.util.HashMap;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Model.Endereco;

/**
 * Created by João Pedro R. Carvalho on 20/03/2016.
 */
public class EnderecoBusiness {
    private HashMap<Long, Endereco> _enderecos;

    public EnderecoBusiness()
    {
        this._enderecos = new HashMap<Long, Endereco>();
    }

    public void Inserir(Endereco contato)
    {
        this._enderecos.put((long)(this._enderecos.size() + 1), contato);
    }

    public void Editar(long key, Endereco contato)
    {
        this._enderecos.put(key, contato);
    }

    public void Excluir(long key)
    {
        this._enderecos.remove(key);
    }

    public Endereco ObtemEndereco(long key)
    {
        return this._enderecos.get(key);
    }

    public HashMap<Long, Endereco> ObtemLista()
    {
        return this._enderecos;
    }

    public void InserirLista(List<Endereco> enderecos)
    {
        for (Endereco endereco : enderecos) {
            this.Inserir(endereco);
        }
    }
}
