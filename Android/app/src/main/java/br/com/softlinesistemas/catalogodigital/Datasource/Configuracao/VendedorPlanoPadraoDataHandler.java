package br.com.softlinesistemas.catalogodigital.Datasource.Configuracao;

import android.content.ContentValues;
import android.database.Cursor;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.VendedorPlanoPadrao;

/**
 * Created by João Pedro R. Carvalho on 01/03/2017.
 */

public class VendedorPlanoPadraoDataHandler {
    public static VendedorPlanoPadrao fromCursor(Cursor cursor)
    {
        VendedorPlanoPadrao vendedorPlanoPadrao = null;

        if(cursor.moveToFirst())
        {
            vendedorPlanoPadrao = BindCursorVendedorPlanoPadrao(cursor);
        }

        cursor.close();
        return vendedorPlanoPadrao;
    }

    public static ContentValues toContentValues(VendedorPlanoPadrao vendedorPlanoPadrao)
    {
        ContentValues values = new ContentValues();

        if(vendedorPlanoPadrao.Codigo > 0) values.put(DatabaseContract.VendedorPlanoPadrao.Coluna_Codigo, vendedorPlanoPadrao.Codigo);
        values.put(DatabaseContract.VendedorPlanoPadrao.Coluna_CodigoVendedor, vendedorPlanoPadrao.CodigoVendedor);
        values.put(DatabaseContract.VendedorPlanoPadrao.Coluna_CodigoPlanoPagamento, vendedorPlanoPadrao.CodigoPlanoPagamento);

        return values;
    }

    private static VendedorPlanoPadrao BindCursorVendedorPlanoPadrao(Cursor cursor)
    {
        VendedorPlanoPadrao vendedorPlanoPadrao = new VendedorPlanoPadrao();

        vendedorPlanoPadrao.Codigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.VendedorPlanoPadrao.Coluna_Codigo)
        );
        vendedorPlanoPadrao.CodigoVendedor = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.VendedorPlanoPadrao.Coluna_CodigoVendedor)
        );
        vendedorPlanoPadrao.CodigoPlanoPagamento = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.VendedorPlanoPadrao.Coluna_CodigoPlanoPagamento)
        );

        return vendedorPlanoPadrao;
    }
}
