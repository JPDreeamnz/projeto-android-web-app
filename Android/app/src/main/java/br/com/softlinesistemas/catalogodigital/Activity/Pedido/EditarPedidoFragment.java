package br.com.softlinesistemas.catalogodigital.Activity.Pedido;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;

/**
 * Created by João Pedro R. Carvalho on 25/07/2016.
 */

public class EditarPedidoFragment extends BaseV4Fragment {

    private PedidoBusiness _pedidoBusiness;
    private long _pedidoCodigo;

    public static EditarPedidoFragment novaInstancia(PedidoBusiness pedidoBusiness, long pedidoCodigo, int fragmentContainer)
    {
        EditarPedidoFragment instancia = new EditarPedidoFragment();
        instancia._pedidoBusiness = pedidoBusiness;
        instancia._pedidoCodigo = pedidoCodigo;
        instancia.setFragmentContainer(fragmentContainer);
        return instancia;
    }
}
