package br.com.softlinesistemas.catalogodigital.Datasource.Cliente;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Pair;

import java.text.ParseException;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Database.IDatabaseOperations;
import br.com.softlinesistemas.catalogodigital.Datasource.Contato.ContatoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Endereco.EnderecoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;

/**
 * Created by João Pedro R. Carvalho on 19/03/2016.
 */
public class ClientesDatasource extends BaseHelper implements IDatabaseOperations<Clientes> {
    private final EnderecoDatasource _dtEndereco;
    private final ContatoDatasource _dtContato;

    public ClientesDatasource(Context context, EnderecoDatasource dtEndereco, ContatoDatasource dtContato) {
        super(context);
        this._dtEndereco = dtEndereco;
        this._dtContato = dtContato;
    }

    /**
     * Método que efetua a inserção do Objeto especificado na base de dados
     *
     * @param instancia
     * @return instância do objeto contendo o ID.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Clientes Incluir(Clientes instancia) throws SQLiteException, ParseException {
        try
        {
            this.OpenWrite();
            this.database.beginTransaction();

            instancia.Codigo = database.insert(
                    DatabaseContract.Clientes.Tabela_Nome,
                    null,
                    ClientesDataHandler.toContentValues(instancia)
            );

            for (Endereco endereco : instancia.Enderecos) {
                endereco.ClienteCodigo = instancia.Codigo;
                endereco.EnderecoID = this._dtEndereco.Incluir(endereco).EnderecoID;
            }

            for (Contato contato : instancia.Contatos) {
                contato.ClienteCodigo = instancia.Codigo;
                contato.ContatoID = this._dtContato.Incluir(contato).ContatoID;
            }

            this.database.setTransactionSuccessful();
            this.database.endTransaction();

            return instancia;

        } catch (SQLiteException sql) {
            this.database.endTransaction();
            throw sql;
        } catch (ParseException pars) {
            this.database.endTransaction();
            throw pars;
        }
    }

    /**
     * Método que efetua a edição do objeto especificado na base de dados
     *
     * @param instancia
     * @return true se o objeto foi editado com sucesso no retorno de row = 1,
     * false se consulta foi corretamente executada mas não editou nenhuma row.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Editar(Clientes instancia) throws SQLiteException, ParseException {
        try
        {
            this.OpenWrite();
            this.database.beginTransaction();

            int rowsCliente = database.update(
                    DatabaseContract.Clientes.Tabela_Nome,
                    ClientesDataHandler.toContentValues(instancia),
                    DatabaseContract.Clientes.Coluna_Codigo + " = ?",
                    new String[] {String.valueOf(instancia.Codigo)}
            );

            for (Endereco endereco : instancia.Enderecos) {
                if(endereco.EnderecoID == 0) //Novo endereco
                {
                    endereco.EnderecoID = this._dtEndereco.Incluir(endereco).EnderecoID;
                    if(endereco.EnderecoID == -1)
                        throw new SQLiteException("Não foi possível incluir o endereço. (CEP: " + endereco.CEP + ")");
                }
                else if(endereco.EnderecoID == -1) //Endereço a deletar
                {
                    if(!this._dtEndereco.Excluir(endereco))
                        throw new SQLiteException("Não foi possível excluir o endereço. (CEP: " + endereco.CEP + ")");
                }
                else {
                    if(!this._dtEndereco.Editar(endereco))
                        throw new SQLiteException("Não foi possível editar o endereço. (CEP: " + endereco.CEP + ")");
                }
            }

            for (Contato contato : instancia.Contatos) {
                if(contato.ContatoID == 0) {
                    contato.ContatoID = this._dtContato.Incluir(contato).ContatoID;
                    if(contato.ContatoID == -1) {
                        String valorErro = !contato.Email.isEmpty() ?
                                contato.Email : contato.DDD + contato.Telefone;
                        throw new SQLiteException("Não foi possível incluir o contato. (contato: " + valorErro + ")");
                    }
                }
                else if(contato.ContatoID == -1) {
                    if(!this._dtContato.Excluir(contato)){
                        String valorErro = !contato.Email.isEmpty() ?
                                contato.Email : contato.DDD + contato.Telefone;
                        throw new SQLiteException("Não foi possível incluir o contato. (contato: " + valorErro + ")");
                    }
                }
                else {
                    if(!this._dtContato.Editar(contato))
                    {
                        String valorErro = !contato.Email.isEmpty() ?
                                contato.Email : contato.DDD + contato.Telefone;
                        throw new SQLiteException("Não foi possível incluir o contato. (contato: " + valorErro + ")");
                    }
                }
            }

            this.database.setTransactionSuccessful();
            this.database.endTransaction();

            return (rowsCliente != 0);

        } catch (SQLiteException sql) {
            this.database.endTransaction();
            throw sql;
        } catch (ParseException pars) {
            this.database.endTransaction();
            throw pars;
        }
    }

    /**
     * Método que efetua a exclusão do objeto especificado na base de dados
     *
     * @param instancia
     * @return true para objeto excluido corretamente da base de dados com retorno de row = 1,
     * false se a consulta foi corretamente executada mas não excluiu nenhuma row
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Excluir(Clientes instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        this.database.beginTransaction();

        database.delete(
                DatabaseContract.Endereco.Tabela_Nome,
                DatabaseContract.Endereco.Coluna_ClienteCodigo + " = ?",
                new String[] {String.valueOf(instancia.Codigo)}
        );

        database.delete(
                DatabaseContract.Contato.Tabela_Nome,
                DatabaseContract.Contato.Coluna_ClienteCodigo + " = ?",
                new String[] {String.valueOf(instancia.Codigo)}
        );

        int rowsCliente = database.delete(
                DatabaseContract.Clientes.Tabela_Nome,
                DatabaseContract.Clientes.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(instancia.Codigo)}
        );

        this.database.setTransactionSuccessful();
        this.database.endTransaction();

        return (rowsCliente != 0);
    }

    public boolean ExcluirContato(Contato contato) throws ParseException {
        return this._dtContato.Excluir(contato);
    }

    public boolean ExcluirEndereco(Endereco endereco) throws ParseException {
        return this._dtEndereco.Excluir(endereco);
    }

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     *
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Clientes ObterUnico(long id) throws SQLiteException, ParseException {
        Clientes cliente = null;

        this.OpenRead();
        cliente = ClientesDataHandler.fromCursor(
                database.query(
                        DatabaseContract.Clientes.Tabela_Nome,
                        DatabaseContract.Clientes.Tabela_TodasColunas,
                        DatabaseContract.Clientes.Coluna_Codigo + " = ?",
                        new String[] {String.valueOf(id)},
                        null, null, null
                )
        );

        cliente.Enderecos = this._dtEndereco.ObterTodos(cliente.Codigo);
        cliente.Contatos = this._dtContato.ObterTodos(cliente.Codigo);
        this.Close(true);

        return cliente;
    }

    public boolean ExiteCliente(long id)
    {
        this.OpenRead();
        Cursor cursor = database.query(
                DatabaseContract.Clientes.Tabela_Nome,
                DatabaseContract.Clientes.Tabela_TodasColunas,
                DatabaseContract.Clientes.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(id)},
                null, null, null
        );

        boolean retorno = cursor.moveToFirst();
        cursor.close();

        return retorno;
    }

    public Pair<Boolean, String> ExisteClientePorDocumento(long codigo, long tipoDocumento, String documento)
    {
        this.OpenRead();
        Cursor cursor = database.query(
                DatabaseContract.Clientes.Tabela_Nome,
                DatabaseContract.Clientes.Tabela_TodasColunas,
                DatabaseContract.Clientes.Coluna_TipoDocumento + " = ?" +
                " AND " + DatabaseContract.Clientes.Coluna_Documento + " = ?" +
                " AND " + DatabaseContract.Clientes.Coluna_Codigo + " <> ?",
                new String[] {String.valueOf(tipoDocumento), documento, String.valueOf(codigo)},
                null, null, null
        );

        boolean retorno = cursor.moveToFirst();
        String nomeCliente = retorno == true ? cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Clientes.Coluna_Nome)
        ) : "";
        cursor.close();

        return new Pair<>(retorno, nomeCliente);
    }


    public List<Clientes> ObterTodos()
    {
        this.OpenRead();
        List<Clientes> retorno =  ClientesDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Clientes.Tabela_Nome,
                        DatabaseContract.Clientes.Tabela_TodasColunas,
                        null,null,null, null, DatabaseContract.Clientes.Coluna_Nome + " ASC"
                )
        );

        for (Clientes cliente: retorno) {
            cliente.Enderecos = this._dtEndereco.ObterTodos(cliente.Codigo);
            cliente.Contatos = this._dtContato.ObterTodos(cliente.Codigo);
        }

        this.Close(true);

        return retorno;
    }


    public List<Clientes> ObterTodos(int pagina)
    {
        String paginacao = (pagina*50) + ", 50";

        this.OpenRead();
        List<Clientes> retorno =  ClientesDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Clientes.Tabela_Nome,
                        DatabaseContract.Clientes.Tabela_TodasColunas,
                        null,null,null, null, DatabaseContract.Clientes.Coluna_Nome + " ASC", paginacao
                )
        );

        for (Clientes cliente: retorno) {
            cliente.Enderecos = this._dtEndereco.ObterTodos(cliente.Codigo);
            cliente.Contatos = this._dtContato.ObterTodos(cliente.Codigo);
        }

        this.Close(true);

        return retorno;
    }

    public List<Clientes> ObterTodosFiltro(String busca, int pagina)
    {
        String paginacao = (pagina*50) + ", 50";

        this.OpenRead();

        busca = "%" + busca + "%";

        List<Clientes> retorno =  ClientesDataHandler.fromCursorToList(
                database.rawQuery("Select * from Clientes " +
                        "WHERE " +
                        "Nome like ? OR " +
                        "Documento like ? OR " +
                        "InscricaoEstadual like ? " +
                        " ORDER BY Nome " +
                        " LIMIT " + paginacao, new String[]{ busca, busca, busca })
        );

        for (Clientes cliente: retorno) {
            cliente.Enderecos = this._dtEndereco.ObterTodos(cliente.Codigo);
            cliente.Contatos = this._dtContato.ObterTodos(cliente.Codigo);
        }

        this.Close(true);

        return retorno;
    }

    public Cursor ObterClientesAutoComplete(String busca)
    {
        this.OpenRead();

        busca = "%" + busca + "%";

        Cursor retorno =  database.rawQuery("Select rowid _id, * from Clientes " +
                        "WHERE " +
                        "Nome like ? OR " +
                        "Codigo like ? ", new String[]{ busca, busca });

        return retorno;
    }

    public int ObterQuantidadeClientes()
    {
        this.OpenRead();
        int retorno = ClientesDataHandler.FromCursorToInt(database.rawQuery("SELECT COUNT(*) AS Quantidade FROM CLIENTES", null));
        this.Close(true);

        return retorno;
    }

    public List<Endereco> ObterEnderecos(long clienteCodigo)
    {
        this.OpenRead();
        List<Endereco> retorno = this._dtEndereco.ObterTodos(clienteCodigo);
        this.Close(true);

        return retorno;
    }

    public List<Contato> ObterContatos(long clienteCodigo)
    {
        this.OpenRead();
        List<Contato> retorno = this._dtContato.ObterTodos(clienteCodigo);
        this.Close(true);

        return retorno;
    }
}
