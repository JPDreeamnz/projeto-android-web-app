package br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jcarvalho on 18/07/2016.
 */

public class ImagemProdutoDatasource {

    private Context _contexto;
    private File _diretorio;
    private static String _pasta;

    public ImagemProdutoDatasource(long codigoProduto, Context contexto){
        this._contexto = contexto;
        this._pasta = contexto.getFilesDir().getAbsolutePath() + "/softimagens";
        this._diretorio = new File(this._pasta + "/" + String.valueOf(codigoProduto));

        if(!this._diretorio.exists()){
            this.CriarDiretorio();
        }
    }

    public ImagemProdutoDatasource(Context contexto){
        this._contexto = contexto;
        this._pasta = contexto.getFilesDir().getAbsolutePath() + "/softimagens";
        this._diretorio = new File(this._pasta);
    }

    public void MudarDiretorio(long codigoProduto)
    {
        this._diretorio = new File(this._pasta + "/" + String.valueOf(codigoProduto));
    }

    public void MudarDiretorioCriar(long codigoProduto){
        MudarDiretorio(codigoProduto);

        if(!this._diretorio.exists())
            CriarDiretorio();
    }

    public boolean CriarDiretorio(){
        this._diretorio.mkdirs();
        return this._diretorio.exists();
    }

    public boolean ExcluirDiretorio(){
        this.DeletarDiretorioRecursivo(this._diretorio);
        return !this._diretorio.exists();
    }

    public boolean SalvarImagemMemoriaInterna(Bitmap bitmap, String imagemCodigo) throws IOException {
        boolean salvo = true;

        ContextWrapper wrapper = new ContextWrapper(this._contexto);
        File arquivo = new File(_diretorio, imagemCodigo + ".png");

        FileOutputStream fileOutputStream = null;
        try{
            fileOutputStream = new FileOutputStream(arquivo);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        } catch (Exception e){
            e.printStackTrace();
            salvo = false;
        } finally {
            fileOutputStream.close();
        }

        return salvo;
    }

    public String[] ObterImagens(){
        int aux = 0;
        String[] retorno = new String[this._diretorio.list().length];
        for (String img : this._diretorio.list()) {
            retorno[aux] = this._diretorio.getPath() + "/" + img;
            aux++;
        }

        return retorno  ;
    }

    public String ObterPrimeiraImagem()
    {
        String[] arquivos = this._diretorio.list();
        if(arquivos != null && arquivos.length > 0)
            return arquivos[0];
        else
            return null;
    }

    private void DeletarDiretorioRecursivo(File arquivoOuPasta){
        if(arquivoOuPasta.isDirectory()){
            for (File filho : arquivoOuPasta.listFiles()) {
                this.DeletarDiretorioRecursivo(filho);
            }
        }

        arquivoOuPasta.delete();
    }
}
