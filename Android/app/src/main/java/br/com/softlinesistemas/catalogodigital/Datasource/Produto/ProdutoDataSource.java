package br.com.softlinesistemas.catalogodigital.Datasource.Produto;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Produto;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public class ProdutoDataSource extends BaseHelper {
    /**
     * BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     * do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     *
     * @param context
     */
    public ProdutoDataSource(Context context) {
        super(context);
    }

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     *
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    public Produto ObterUnico(long id) throws SQLiteException, ParseException {
        Produto produto = null;

        this.OpenRead();
        produto = ProdutoDataHandler.fromCursor(
                database.query(
                        DatabaseContract.Produto.Tabela_Nome,
                        DatabaseContract.Produto.Tabela_TodasColunas,
                        DatabaseContract.Produto.Coluna_Codigo + " = ?",
                        new String[] {String.valueOf(id)},
                        null, null, null
                )
        );
        this.Close(true);

        return produto;
    }

    public boolean ExisteProduto(long codigo)
    {
        this.OpenRead();
        Cursor cursor = database.query(
                DatabaseContract.Produto.Tabela_Nome,
                DatabaseContract.Produto.Tabela_TodasColunas,
                DatabaseContract.Produto.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(codigo)},
                null, null, null
        );

        boolean retorno = cursor.moveToFirst();
        cursor.close();
        this.Close(false);

        return retorno;
    }

    public List<Produto> ObterTodos() throws ParseException {
        this.OpenRead();
        List<Produto> retorno = ProdutoDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Produto.Tabela_Nome,
                        DatabaseContract.Produto.Tabela_TodasColunas,
                        null,null,null, null, null
                )
        );
        this.Close(false);

        return retorno;
    }

    public List<Produto> ObterTodos(int pagina) throws ParseException {
        String paginacao = (pagina*50) + ", 50";

        this.OpenRead();
        List<Produto> retorno = ProdutoDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Produto.Tabela_Nome,
                        DatabaseContract.Produto.Tabela_TodasColunas,
                        null,null,null, null, DatabaseContract.Produto.Coluna_Nome + " ASC", paginacao
                )
        );
        this.Close(false);

        return retorno;
    }

    public List<Produto> ObterTodos(String busca, int pagina) throws ParseException {
        String paginacao = (pagina*50) + ", 50";

        this.OpenRead();
        busca = "%" + busca + "%";
        List<Produto> retorno = ProdutoDataHandler.fromCursorToList(
                database.rawQuery("Select * from Produto P " +
                        "WHERE " +
                        "P.Nome like ? OR " +
                        "P.Unidade like ? OR " +
                        "P.CodigoBarras like ? OR " +
                        "P.CodigoFabrica like ? OR " +
                        "P.CodigoOriginal like ? OR " +
                        "P.Cor like ? OR " +
                        "P.Tamanho like ? OR " +
                        "P.Marca like ? " +
                        "ORDER BY P.Nome " +
                        "LIMIT " + paginacao, new String[]{ busca, busca, busca, busca, busca, busca, busca, busca }
                )
        );
        this.Close(false);

        return retorno;
    }

    public String ObterNomeProduto(long codigo)
    {
        String nome = null;

        this.OpenRead();
        Cursor cursor = database.rawQuery(
                "SELECT " + DatabaseContract.Produto.Coluna_Nome +
                        " FROM " + DatabaseContract.Produto.Tabela_Nome +
                        " WHERE " + DatabaseContract.Produto.Coluna_Codigo + " = " + String.valueOf(codigo), null);

        if(cursor.moveToFirst()){
            nome = cursor.getString(cursor.getColumnIndex(DatabaseContract.Produto.Coluna_Nome));
        }

        cursor.close();
        this.Close(true);

        return nome;
    }

    public Produto Incluir(Produto instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.Codigo = database.insert(DatabaseContract.Produto.Tabela_Nome, null, ProdutoDataHandler.toContentValue(instancia));
        this.Close(false);

        return instancia;
    }

    public Produto Editar(Produto instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.Codigo = database.update(
                DatabaseContract.Produto.Tabela_Nome,
                ProdutoDataHandler.toContentValue(instancia),
                DatabaseContract.Produto.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(instancia.Codigo)}
                );
        this.Close(false);

        return instancia;
    }

    public boolean Excluir(long codigo)
    {
        this.OpenWrite();

        long rows = database.delete(
                DatabaseContract.Produto.Tabela_Nome,
                DatabaseContract.Produto.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(codigo)}
        );

        this.Close(false);
        return (rows > 0);
    }

    public int ObterQuantidadeProdutos(String busca)
    {
        this.OpenRead();
        int retorno = 0;

        if(!busca.isEmpty()) {
            busca = "%" + busca + "%";
            retorno = ProdutoDataHandler.FromCursorToInt(
                    database.rawQuery("Select COUNT(*) AS Quantidade from Produto P " +
                            "WHERE " +
                            "P.Nome like ? OR " +
                            "P.Unidade like ? OR " +
                            "P.CodigoBarras like ? OR " +
                            "P.CodigoFabrica like ? OR " +
                            "P.CodigoOriginal like ? OR " +
                            "P.Cor like ? OR " +
                            "P.Tamanho like ? OR " +
                            "P.Marca like ?", new String[]{busca, busca, busca, busca, busca, busca, busca, busca}
                    )
            );
        } else {
            retorno = ProdutoDataHandler.FromCursorToInt(
                    database.rawQuery("Select COUNT(*) AS Quantidade from Produto P", null)
            );
        }
        this.Close(false);

        return retorno;
    }
}
