package br.com.softlinesistemas.catalogodigital.Datasource.Login;

import android.content.ContentValues;
import android.database.Cursor;
import java.text.ParseException;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Login;

/**
 * Created by João Pedro R. Carvalho on 15/12/2015.
 */
public final class LoginDataHandler {

    /**
     *  Executa a conversão do objeto Login para o tipo Cursor.
     * @param cursor
     * @return Objeto do tipo Cursor
     * @throws ParseException
     */
    public static Login fromCursor(Cursor cursor) throws ParseException {
        Login login = null;

        if(cursor.moveToFirst())
        {
            login = new Login();
            login.LoginID = cursor.getLong(
                    cursor.getColumnIndex(DatabaseContract.Login.Coluna_LoginID)
            );
            login.Token = cursor.getString(
                    cursor.getColumnIndex(DatabaseContract.Login.Coluna_Token)
            );
            login.setDataLogin(cursor.getString(
                    cursor.getColumnIndex(DatabaseContract.Login.Coluna_DataLogin)
            ));
            login.setDataExpiracao(cursor.getString(
                    cursor.getColumnIndex(DatabaseContract.Login.Coluna_DataExpiracao)
            ));
            login.Situacao = cursor.getInt(
                    cursor.getColumnIndex(DatabaseContract.Login.Coluna_Situacao)
            );
        }

        cursor.close();
        return login;
    }

    /**
     *  Converte objeto do tipo Login para um ContentValue
     * @param login
     * @return Objeto do tipo ContentValue
     */
    public static ContentValues toContentValue(Login login)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.Login.Coluna_Token, login.Token);
        values.put(DatabaseContract.Login.Coluna_DataLogin, login.getDataLogin());
        values.put(DatabaseContract.Login.Coluna_DataExpiracao, login.getDataExpiracao());
        values.put(DatabaseContract.Login.Coluna_Situacao, login.Situacao);

        return values;
    }

}
