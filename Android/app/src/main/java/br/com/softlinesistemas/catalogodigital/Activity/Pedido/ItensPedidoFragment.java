package br.com.softlinesistemas.catalogodigital.Activity.Pedido;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.view.CardViewNative;

/**
 * Created by João Pedro R. Carvalho on 26/07/2016.
 */

public class ItensPedidoFragment extends BaseV4Fragment {
    private boolean _editar;
    private IPedidoActivity _PedidoActivity;
    private PedidoBusiness _pedidoBusiness;
    private ProdutoBusiness _produtoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private ImagemProdutoDatasource _imagemProdutoDataSource;
    private Fragment criarPedidoFragment;

    private ListagemItensPedidoCard _listagemItensPedidoCard;

    public static ItensPedidoFragment novaInstancia(
            IPedidoActivity pedidoActivity,
            PedidoBusiness pedidoBusiness,
            ProdutoBusiness produtoBusiness,
            ItensPedidoBusiness itensPedidoBusiness,
            ImagemProdutoDatasource imagemProdutoDatasource,
            Fragment criarPedidoFragment,
            int fragmentContainer,
            boolean editar)
    {
        ItensPedidoFragment instancia = new ItensPedidoFragment();
        instancia._PedidoActivity = pedidoActivity;
        instancia._pedidoBusiness = pedidoBusiness;
        instancia._produtoBusiness = produtoBusiness;
        instancia._itensPedidoBusiness = itensPedidoBusiness;
        instancia._imagemProdutoDataSource = imagemProdutoDatasource;
        instancia.criarPedidoFragment = criarPedidoFragment;
        instancia.setFragmentContainer(fragmentContainer);
        instancia._editar = editar;
        return instancia;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View listagemView = inflater.inflate(R.layout.pedido_itens_fragment, container, false);
        this.setView(listagemView);
        return listagemView;
    }

    private void setView(View view)
    {
        //Iniciando a visualização da listagem de itens de pedido
        this._listagemItensPedidoCard = new ListagemItensPedidoCard(
                this.getActivity().getBaseContext(),
                this._produtoBusiness,
                this._itensPedidoBusiness,
                this._editar,
                this._imagemProdutoDataSource,
                this._PedidoActivity,
                this.criarPedidoFragment);
        this._listagemItensPedidoCard.init();
        CardViewNative cardView = (CardViewNative) view.findViewById(R.id.pedido_itens);
        cardView.setCard(_listagemItensPedidoCard);
    }
}
