package br.com.softlinesistemas.catalogodigital.Datasource.Pedido;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Database.IDatabaseOperations;
import br.com.softlinesistemas.catalogodigital.Datasource.Cliente.ClientesDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Contato.ContatoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Endereco.EnderecoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.ItensPedido.ItensPedidoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;

/**
 * Created by João Pedro R. Carvalho on 24/07/2016.
 */

public class PedidoDatasource extends BaseHelper implements IDatabaseOperations<Pedido> {
    private ItensPedidoDatasource _dtItensPedido;
    private ClientesDatasource _dtClientes;
    /**
     * BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     * do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     *
     * @param context
     */
    public PedidoDatasource(Context context) {
        super(context);
        _dtItensPedido = new ItensPedidoDatasource(context);
        _dtClientes = new ClientesDatasource(context, new EnderecoDatasource(context), new ContatoDatasource(context));
    }

    /**
     * Método que efetua a inserção do Objeto especificado na base de dados
     *
     * @param instancia
     * @return instância do objeto contendo o ID.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Pedido Incluir(Pedido instancia) throws SQLiteException, ParseException {
        try
        {
            this.OpenWrite();
            this.database.beginTransaction();

            instancia.Pedido = database.insert(
                    DatabaseContract.Pedido.Tabela_Nome,
                    null,
                    PedidoDataHandler.toContentValues(instancia)
            );

            for (ItensPedido item : instancia.ItensPedido) {
                item.Pedido = instancia.Pedido;
                item.Item = this._dtItensPedido.Incluir(item).Item;
            }

            this.database.setTransactionSuccessful();
            this.database.endTransaction();

            return instancia;

        } catch (SQLiteException sql) {
            this.database.endTransaction();
            throw sql;
        } catch (ParseException pars) {
            this.database.endTransaction();
            throw pars;
        }
    }

    /**
     * Método que efetua a edição do objeto especificado na base de dados
     *
     * @param instancia
     * @return true se o objeto foi editado com sucesso no retorno de row = 1,
     * false se consulta foi corretamente executada mas não editou nenhuma row.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Editar(Pedido instancia) throws SQLiteException, ParseException {return false;}


    public boolean Editar(Pedido instancia, List<ItensPedido> excluir) throws SQLiteException, ParseException {
        try
        {
            this.OpenWrite();
            this.database.beginTransaction();

            int rowsPedido = database.update(
                    DatabaseContract.Pedido.Tabela_Nome,
                    PedidoDataHandler.toContentValues(instancia),
                    DatabaseContract.Pedido.Coluna_Pedido + " = ?",
                    new String[] {String.valueOf(instancia.Pedido)}
            );

            for (ItensPedido item : instancia.ItensPedido) {
                if(item.Item < 0 && item.Item != -99) //Novo item
                {
                    item.Pedido = instancia.Pedido;
                    item.Item = this._dtItensPedido.Incluir(item).Item;
                    if(item.Item == -1)
                        throw new SQLiteException("Não foi possível incluir o produto. (Código: " + item.Produto + ")");
                }
                else {
                    this._dtItensPedido.Editar(item);
                        //throw new SQLiteException("Não foi possível editar o pedido. (Código: " + item.Produto + ")");
                }
            }

            for (ItensPedido itemExcluir : excluir) {
                if(!this._dtItensPedido.Excluir(itemExcluir))
                    throw new SQLiteException("Não foi possível excluir o produto. (Código: " + itemExcluir.Produto + ")");
            }

            this.database.setTransactionSuccessful();
            this.database.endTransaction();

            return (rowsPedido != 0);

        } catch (SQLiteException sql) {
            this.database.endTransaction();
            throw sql;
        } catch (ParseException pars) {
            this.database.endTransaction();
            throw pars;
        }
    }

    /**
     * Método que efetua a exclusão do objeto especificado na base de dados
     *
     * @param instancia
     * @return true para objeto excluido corretamente da base de dados com retorno de row = 1,
     * false se a consulta foi corretamente executada mas não excluiu nenhuma row
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Excluir(Pedido instancia) throws SQLiteException, ParseException {
        this.OpenWrite();

        int rowsPedido = database.delete(
                DatabaseContract.Pedido.Tabela_Nome,
                DatabaseContract.Pedido.Coluna_Pedido + " = ?",
                new String[] {String.valueOf(instancia.Pedido)}
        );

        this.Close(true);

        return (rowsPedido != 0);
    }

    public boolean ExcluirTodos() throws SQLiteException{
        this.OpenWrite();

        database.delete(
                DatabaseContract.Pedido.Tabela_Nome,
                null, null
        );

        this.Close(true);

        return true;
    }

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     *
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Pedido ObterUnico(long id) throws SQLiteException, ParseException {
        Pedido pedido = null;

        this.OpenRead();
        pedido = PedidoDataHandler.fromCursor(
                database.query(
                        DatabaseContract.Pedido.Tabela_Nome,
                        DatabaseContract.Pedido.Tabela_TodasColunas,
                        DatabaseContract.Pedido.Coluna_Pedido + " = ?",
                        new String[] {String.valueOf(id)},
                        null, null, null
                )
        );

        pedido.ItensPedido = this._dtItensPedido.ObterTodos(pedido.Pedido);
        this.Close(true);

        return pedido;
    }

    public List<Pedido> ObterTodos() throws ParseException {
        this.OpenRead();
        List<Pedido> retorno =  PedidoDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Pedido.Tabela_Nome,
                        DatabaseContract.Pedido.Tabela_TodasColunas,
                        null,null,null, null, null
                )
        );

        for (Pedido pedido: retorno) {
            pedido.ItensPedido = this._dtItensPedido.ObterTodos(pedido.Pedido);
            pedido.ClienteEntity = this._dtClientes.ObterUnico(pedido.Cliente);
        }

        this.Close(true);

        return retorno;
    }

    public List<Pedido> ObterTodos(String busca) throws ParseException {
        this.OpenRead();

        busca = "%" + busca + "%";
        List<Pedido> retorno =  PedidoDataHandler.fromCursorToList(
                database.rawQuery("SELECT * FROM Pedido P " +
                        "JOIN Clientes C ON P.Cliente = C.Codigo " +
                        "JOIN Vendedor V ON P.Vendedor = V.Codigo " +
                        "WHERE " +
                        "C.Nome like ? OR " +
                        "V.Nome like ?", new String[]{ busca, busca }
                )
        );

        for (Pedido pedido: retorno) {
            pedido.ItensPedido = this._dtItensPedido.ObterTodos(pedido.Pedido);
        }

        this.Close(true);

        return retorno;
    }
}
