package br.com.softlinesistemas.catalogodigital.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import br.com.softlinesistemas.catalogodigital.Activity.Login.LoginActivity;
import br.com.softlinesistemas.catalogodigital.Business.LoginBusiness;
import br.com.softlinesistemas.catalogodigital.R;

public class DashboardActivity extends BaseActivity {

    public DashboardActivity(){
        super(R.id.drawer_layout, R.layout.activity_dashboard);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
