package br.com.softlinesistemas.catalogodigital.Datasource.Vendedor;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Vendedor;

/**
 * Created by João Pedro R. Carvalho on 28/07/2016.
 */

public class VendedorDatasource extends BaseHelper {
    /**
     * BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     * do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     *
     * @param context
     */
    public VendedorDatasource(Context context) {
        super(context);
    }

    public ArrayList<Vendedor> ObtemLista()
    {
        ArrayList<Vendedor> entidade;
        this.OpenRead();
        entidade = VendedorDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Vendedor.Tabela_Nome,
                        DatabaseContract.Vendedor.Tabela_TodasColunas,
                        null,null,null, null, null
                )
        );
        this.Close(false);

        return entidade;
    }

    public Vendedor ObterVendedor(long codigo){
        this.OpenWrite();
        Vendedor vendedor = VendedorDataHandler.fromCursor(
                database.query(DatabaseContract.Vendedor.Tabela_Nome,
                    DatabaseContract.Vendedor.Tabela_TodasColunas,
                    DatabaseContract.Vendedor.Coluna_Codigo + " = ?",
                    new String[]{ String.valueOf(codigo)}, null, null, null));
        this.Close(false);

        return vendedor;
    }

    public boolean ExisteVendedor(long codigo)
    {
        this.OpenRead();
        Cursor cursor = database.query(DatabaseContract.Vendedor.Tabela_Nome,
                DatabaseContract.Vendedor.Tabela_TodasColunas,
                DatabaseContract.Vendedor.Coluna_Codigo + " = ?",
                new String[]{ String.valueOf(codigo)}, null, null, null);

        boolean retorno = cursor.moveToFirst();
        cursor.close();
        this.Close(false);

        return retorno;
    }

    public Vendedor Incluir(Vendedor instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.Codigo = database.insert(DatabaseContract.Vendedor.Tabela_Nome, null, VendedorDataHandler.toContentValue(instancia));
        this.Close(false);

        return instancia;
    }

    public Vendedor Editar(Vendedor instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.Codigo = database.update(
                DatabaseContract.Vendedor.Tabela_Nome,
                VendedorDataHandler.toContentValue(instancia),
                DatabaseContract.Vendedor.Coluna_Codigo + " = ?",
                new String[]{ String.valueOf(instancia.Codigo)});
        this.Close(false);

        return instancia;
    }

    public boolean Excluir(long codigo)
    {
        this.OpenWrite();

        long rows = database.delete(
                DatabaseContract.Vendedor.Tabela_Nome,
                DatabaseContract.Vendedor.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(codigo)}
        );

        this.Close(false);
        return (rows > 0);
    }
}
