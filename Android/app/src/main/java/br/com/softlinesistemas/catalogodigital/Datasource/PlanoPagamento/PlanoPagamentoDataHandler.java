package br.com.softlinesistemas.catalogodigital.Datasource.PlanoPagamento;

import android.content.ContentValues;
import android.database.Cursor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.PlanoPagamento;

/**
 * Created by João Pedro R. Carvalho on 27/07/2016.
 */

public class PlanoPagamentoDataHandler {
    public static PlanoPagamento fromCursor(Cursor cursor) throws ParseException
    {
        PlanoPagamento planoPagamento = null;

        if(cursor.moveToFirst())
        {
            planoPagamento = BindCursorPlanoPagamento(cursor);
        }

        cursor.close();
        return planoPagamento;
    }

    public static List<PlanoPagamento> fromCursorToList(Cursor cursor)
    {
        List<PlanoPagamento> planoPagamentos = new ArrayList<PlanoPagamento>();

        if(cursor.moveToFirst())
        {
            do{
                planoPagamentos.add(BindCursorPlanoPagamento(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return planoPagamentos;
    }

    public static ContentValues toContentValue(PlanoPagamento planoPagamento)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.PlanoPagamento.Coluna_Codigo, planoPagamento.Codigo);
        values.put(DatabaseContract.PlanoPagamento.Coluna_Nome, planoPagamento.Nome);

        return values;
    }

    private static PlanoPagamento BindCursorPlanoPagamento(Cursor cursor)
    {
        PlanoPagamento planoPagamento = new PlanoPagamento();

        planoPagamento.Codigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.PlanoPagamento.Coluna_Codigo)
        );
        planoPagamento.Nome = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.PlanoPagamento.Coluna_Nome)
        );

        return planoPagamento;
    }
}
