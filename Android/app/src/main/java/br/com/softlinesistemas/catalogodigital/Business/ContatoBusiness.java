package br.com.softlinesistemas.catalogodigital.Business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Model.Contato;

/**
 * Created by João Pedro R. Carvalho on 20/03/2016.
 */
public class ContatoBusiness {
    private HashMap<Long, Contato> _contatos;

    public ContatoBusiness()
    {
        this._contatos = new HashMap<Long, Contato>();
    }

    public void Incluir(Contato contato)
    {
        this._contatos.put((long)(this._contatos.size() + 1), contato);
    }

    public void Editar(long key, Contato contato)
    {
        this._contatos.put(key, contato);
    }

    public void Excluir(long key)
    {
        this._contatos.remove(key);
    }

    public Contato ObtemContato(long key)
    {
        return this._contatos.get(key);
    }

    public HashMap<Long, Contato> ObtemLista()
    {
        return this._contatos;
    }

    public void InserirLista(List<Contato> contatos)
    {
        for (Contato contato : contatos) {
            this.Incluir(contato);
        }
    }
}
