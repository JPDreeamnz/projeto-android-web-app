package br.com.softlinesistemas.catalogodigital.Model.Enum;

/**
 * Created by João Pedro R. Carvalho on 13/06/2016.
 */

public enum Operacoes {
    CRIADO,
    EDITADO,
    EXCLUIDO,
    VISUALIZADO;
}
