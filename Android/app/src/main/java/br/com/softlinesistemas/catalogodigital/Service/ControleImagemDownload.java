package br.com.softlinesistemas.catalogodigital.Service;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.POJO.DadosImagem;

/**
 * Created by João Pedro R. Carvalho on 10/04/2017.
 */

public class ControleImagemDownload {
    private String TOKEN;
    private int _threadsConcorrentes,
                _quantidadeImagens;

    private List<DadosImagem> _dadosImagem;
    private List<DadosImagem> _ImagensBaixadas;

    private Context _contexto;
    NotificationCompat.Builder _mBuilder;

    public ControleImagemDownload(String TOKEN, Context contexto)
    {
        this._threadsConcorrentes = 0;
        this.TOKEN = TOKEN;
        this._contexto = contexto;

        this._dadosImagem = new ArrayList<>();
        this._ImagensBaixadas = new ArrayList<>();

        this._mBuilder = new NotificationCompat.Builder(contexto)
                .setSmallIcon(android.support.design.R.drawable.notification_template_icon_bg);
        this._mBuilder.setContentTitle("Download de Imagens").setContentText("Integração iniciada!");
        NotificationManager manager = (NotificationManager) contexto.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(2, _mBuilder.build());
    }

    public void IniciarDownloadImagens()
    {
        try {
            if (this._threadsConcorrentes < 3) {
                do {
                    if (ExisteFilaImagens())
                        DownloadImagem();
                    else
                        return;
                } while (this._threadsConcorrentes <= 2);

                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void DownloadImagem() {
        DadosImagem imagem = _dadosImagem.get(0);

        this._ImagensBaixadas.add(imagem);
        this._dadosImagem.remove(imagem);
        this._threadsConcorrentes++;

        DownloadImagemTask downloadImagem = new DownloadImagemTask(
                imagem.URL_IMAGEM, this.TOKEN, imagem.CodigoProduto, String.valueOf(imagem.CodigoImagem), this._contexto, this);
        downloadImagem.execute(imagem.CaminhoPasta);
    }

    public void DownloadConcluido() {
        NotificationManager manager = (NotificationManager) _contexto.getSystemService(Context.NOTIFICATION_SERVICE);

        if(!this._dadosImagem.isEmpty()) {
            this._mBuilder.setContentTitle("Download de Imagens");
            this._mBuilder.setContentText(this._dadosImagem.size() + "/" + this._quantidadeImagens + " imagens baixadas.");
            manager.notify(2, _mBuilder.build());
        } else {
            this._mBuilder.setContentTitle("Download de Imagens");
            this._mBuilder.setContentText("Concluído!");
            manager.notify(2, _mBuilder.build());
        }

        this._threadsConcorrentes--;
        IniciarDownloadImagens();
    }

    public boolean ExisteFilaImagens() {
        return !_dadosImagem.isEmpty();
    }

    public void AddDadosImagem(DadosImagem objeto) {
        this._dadosImagem.add(objeto);
        this._quantidadeImagens++;
    }
}
