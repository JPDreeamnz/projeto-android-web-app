package br.com.softlinesistemas.catalogodigital.Service;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONArray;

import br.com.softlinesistemas.catalogodigital.Datasource.PlanoPagamento.PlanoPagamentoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.PlanoPagamento;

/**
 * Created by João Pedro R. Carvalho on 14/09/2016.
 */

public class PlanoPagamentoTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    PlanoPagamentoDatasource _dtPlano;
    IIntegracaoService _integracao;
    String URL;

    public PlanoPagamentoTask(String URL, PlanoPagamentoDatasource dtPlano, IIntegracaoService integracao){
        this._dtPlano = dtPlano;
        this._integracao = integracao;
        this.URL = "http://" + URL + "api/Integracao/ObterPlanoPagamento";
    }
    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        Pair<Integer, String> respota;
        try {
            HttpRequest request = HttpRequest.get(this.URL, true, "dataRegistros", params[0])
                    .accept("application/json")
                    .header("Authorization", "Bearer " + params[1]);

            respota = new Pair<>(request.code(), request.body());
        } catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, "Não foi possível obter os planos de pagamento.");
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        try{
            if(resposta.first == 200) {
                JSONArray json = new JSONArray(resposta.second);
                for(int i = 0; i < json.length(); i++) {
                    PlanoPagamento novoPlano = new PlanoPagamento();
                    novoPlano.Codigo = json.getJSONObject(i).getLong("Codigo");
                    novoPlano.Nome = json.getJSONObject(i).getString("Nome");

                    if(this._dtPlano.ExitePlano(novoPlano.Codigo))
                        this._dtPlano.Editar(novoPlano);
                    else
                        this._dtPlano.Incluir(novoPlano);
                }

                this._integracao.AtualizarPush(true, "Tabela plano de pagamento atualizada com sucesso.");
            }else{
                this._integracao.AtualizarPush(true, resposta.first + " - Não foi possível atualizar os planos de pagamento");
            }
        } catch (Exception e){
            this._integracao.AtualizarPush(true, "Não foi possível atualizar os planos de pagamento.");
        }
    }
}
