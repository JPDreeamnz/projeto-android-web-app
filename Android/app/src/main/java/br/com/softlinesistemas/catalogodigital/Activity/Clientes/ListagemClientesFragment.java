package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Activity.BaseFragment;
import br.com.softlinesistemas.catalogodigital.Activity.EndlessScroll;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardGridView;

/**
 * Created by João Pedro R. Carvalho on 26/03/2016.
 */
public class ListagemClientesFragment extends BaseFragment implements SearchView.OnQueryTextListener {

    private boolean _filtrado;
    private String _busca;
    private ClientesBusiness _clientesBusiness;
    private FloatingActionButton mCriarCliente;
    private CardGridArrayAdapter mCardArrayAdapter;
    private EndlessScroll _endlessScroll;
    private ArrayList<Card> _cards;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View listagemView = inflater.inflate(R.layout.listagem_clientes, null);
        this._clientesBusiness = new ClientesBusiness(inflater.getContext());

        mCriarCliente = (FloatingActionButton) listagemView.findViewById(R.id.clientes_criar_fab);
        mCriarCliente.setBackgroundTintList(
                ColorStateList.valueOf(
                        ContextCompat.getColor(this.getActivity().getBaseContext(), R.color.colorPrimaryRed)
                )
        );
        mCriarCliente.setOnClickListener(BotaoCriarCliente());

        this.setView(listagemView);
        setHasOptionsMenu(true);
        return listagemView;
    }

    private void setView(View view)
    {
        this._filtrado = false;
        this._cards = new ArrayList<>();
        final Context contexto = view.getContext();
        mCardArrayAdapter = new CardGridArrayAdapter(contexto,_cards);

        CardGridView gridView = (CardGridView) view.findViewById(R.id.grid_lista_clientes);
        if (gridView!=null){
            gridView.setAdapter(mCardArrayAdapter);

            _endlessScroll = new EndlessScroll(gridView, new EndlessScroll.RefreshList() {
                @Override
                public void onRefresh(int pageNumber) {
                    PreencheListCards(pageNumber, (_filtrado ? _busca : ""), contexto);
                    mCardArrayAdapter.notifyDataSetChanged();
                }
            });

            gridView.setOnScrollListener(_endlessScroll);
        }
    }

    private void PreencheListCards(int pagina, String filtro, Context contexto)
    {
        List<Clientes> clientes = filtro.isEmpty() ?
                this._clientesBusiness.ObterTodos(pagina) : this._clientesBusiness.ObterTodosFiltro(filtro, pagina);
        for (Clientes cliente : clientes)
        {
            ClienteCard card = new ClienteCard(contexto);
            card.setCliente(cliente);

            CardHeader header = new CardHeader(contexto);
            header.setTitle(cliente.Nome);
            card.addCardHeader(header);
            card.setId(String.valueOf(cliente.Codigo));
            card.setOnClickListener(this.clickCliente());

            _cards.add(card);
        }

        if((pagina * clientes.size()) < this._clientesBusiness.ObterQuantidadeClientes())
            _endlessScroll.notifyMorePages();
        else
            _endlessScroll.noMorePages();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_filtro, menu);

        MenuItem itemFiltro = menu.findItem(R.id.filtro);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(itemFiltro);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(this);
    }

    private View.OnClickListener BotaoCriarCliente()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CriarClienteFragment fragment = new CriarClienteFragment();
                fragment.setClienteBusiness(_clientesBusiness);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(
                        R.id.clientes_listagem_fragment_container,
                        fragment,
                        "CLIENTES_CRIAR_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(
                        android.R.animator.fade_in,
                        android.R.animator.fade_out,
                        android.R.animator.fade_in,
                        android.R.animator.fade_out);
                transaction.commit();
            }
        };
    }

    private Card.OnCardClickListener clickCliente()
    {
        return new Card.OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                int clienteCodigo = Integer.parseInt(card.getId());

                _clientesBusiness = null;
                _clientesBusiness = new ClientesBusiness(getActivity().getBaseContext(), clienteCodigo);

                EditarClienteFragment fragment = new EditarClienteFragment();
                fragment.setClienteBusiness(_clientesBusiness);
                fragment.setClienteCodigo(clienteCodigo);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(
                        R.id.clientes_listagem_fragment_container,
                        fragment,
                        "CLIENTES_EDITAR_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        };
    }

    /**
     * Called when the user submits the query. This could be due to a key press on the
     * keyboard or due to pressing a submit button.
     * The listener can override the standard behavior by returning true
     * to indicate that it has handled the submit request. Otherwise return false to
     * let the SearchView handle the submission by launching any associated intent.
     *
     * @param query the query text that is to be submitted
     * @return true if the query has been handled by the listener, false to let the
     * SearchView perform the default action.
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Called when the query text is changed by the user.
     *
     * @param newText the new content of the query text field.
     * @return false if the SearchView should perform the default action of showing any
     * suggestions if available, true if the action was handled by the listener.
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        this._cards.clear();
        this._filtrado = true;
        this._busca = newText;
        View view = getView();

        if(view != null) {
            Context contexto = this.getActivity();
            PreencheListCards(0, newText, contexto);
            mCardArrayAdapter.notifyDataSetChanged();
        }

        return true;
    }
}

class ClienteCard extends Card{

    private Clientes _cliente;
    private TextView mTipoFisicoJuridico,
            mEmail,
            mContatos,
            mEndereco;
    /**
     * Constructor with a base inner layout defined by R.layout.inner_base_main
     *
     * @param context context
     */
    public  ClienteCard(Context context) {
        super(context, R.layout.clientes_card);
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        this.mTipoFisicoJuridico = (TextView) view.findViewById(R.id.cliente_tipoFisicoJuridico);
        this.mEmail = (TextView) view.findViewById(R.id.cliente_email);
        this.mContatos = (TextView) view.findViewById(R.id.cliente_contatos);
        this.mEndereco = (TextView) view.findViewById(R.id.cliente_endereco);

        if(this._cliente.TipoDocumento == 2){
            mTipoFisicoJuridico.setText("Pessoa Física");
        } else {
            mTipoFisicoJuridico.setText("Pessoa Jurídica");
        }

        String contatos = "";
        String email = "";
        if(this._cliente.Contatos != null) {
            for (Contato contato : this._cliente.Contatos) {
                if (contato.TipoContato == 3 && email == "") {
                    this.mEmail.setText(contato.Email);
                } else if (!contatos.contains("/")) {
                    if (contatos != "")
                        contatos += " / ";

                    contatos += "(" + contato.DDD + ") " + contato.Telefone;
                }
            }
        }
        this.mEmail.setText(email);
        this.mContatos.setText(contatos);

        this.mEndereco.setText(" - ");
        if(this._cliente.Enderecos != null) {
            for (Endereco endereco : this._cliente.Enderecos) {
                this.mEndereco.setText(
                        endereco.Endereco + ", " +
                                endereco.Numero + " " +
                                endereco.Bairro + " - " +
                                endereco.Cidade + "/" +
                                endereco.Estado
                );
            }
        }
    }

    public void setCliente(Clientes _cliente) {
        this._cliente = _cliente;
    }
}
