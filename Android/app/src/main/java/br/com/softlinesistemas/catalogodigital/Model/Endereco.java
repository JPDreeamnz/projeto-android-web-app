package br.com.softlinesistemas.catalogodigital.Model;

/**
 * Created by João Pedro R. Carvalho on 18/02/2016.
 */
public class Endereco {
    public long EnderecoID;
    public long ClienteCodigo;
    public String Endereco;
    public String Numero;
    public String Complemento;
    public String Bairro;
    public String Cidade;
    public String Estado;
    public long CEP;

    public boolean CompararEnderecos(Endereco endereco)
    {
        return
            this.EnderecoID == endereco.EnderecoID &&
            this.Endereco.equalsIgnoreCase(endereco.Bairro) &&
            this.Numero.equalsIgnoreCase(endereco.Numero) &&
            this.Estado.equalsIgnoreCase(endereco.Estado) &&
            this.Bairro.equalsIgnoreCase(endereco.Bairro) &&
            this.CEP == endereco.CEP &&
            this.Cidade.equalsIgnoreCase(endereco.Cidade) &&
            this.ClienteCodigo == endereco.ClienteCodigo &&
            this.Complemento.equalsIgnoreCase(endereco.Complemento);
    }
}
