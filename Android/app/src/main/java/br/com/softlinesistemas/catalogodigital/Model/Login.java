package br.com.softlinesistemas.catalogodigital.Model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by João Pedro R. Carvalho on 14/12/2015.
 */
public class Login {
    public long LoginID;
    public String Token;
    private Date DataLogin;
    private Date DataExpiracao;
    public int Situacao;

    public String getDataLogin(){
        return dateToString(DataLogin);
    }

    public void setDataLogin(String dataLogin) throws ParseException {
        DataLogin = stringToDate(dataLogin);
    }

    public String getDataExpiracao() {
        return dateToString(DataExpiracao);
    }

    public void setDataExpiracao(String dataExpiracao) throws ParseException {
        DataExpiracao = stringToDate(dataExpiracao);
    }

    private Date stringToDate(String data) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.parse(data);
    }

    private String dateToString(Date data){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.format(data);
    }
}
