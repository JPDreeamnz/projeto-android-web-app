package br.com.softlinesistemas.catalogodigital.Activity;

import android.support.v4.app.Fragment;

/**
 * Created by João Pedro R. Carvalho on 15/08/2016.
 */

public class BaseV4Fragment extends Fragment {
    private static int _FragmentContainer;

    public static int getFragmentContainer() {
        return _FragmentContainer;
    }

    public static void setFragmentContainer(int fragmentContainer) {
        BaseV4Fragment._FragmentContainer = fragmentContainer;
    }
}
