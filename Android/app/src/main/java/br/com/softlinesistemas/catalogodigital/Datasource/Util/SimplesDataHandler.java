package br.com.softlinesistemas.catalogodigital.Datasource.Util;

import android.database.Cursor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by João Pedro R. Carvalho on 22/12/2015.
 */
public class SimplesDataHandler {

    public static Date fromCursorToDate(Cursor cursor, String indiceColuna) throws ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date data = null;

        if(cursor.moveToFirst()){
            data = dateFormat.parse(cursor.getString(cursor.getColumnIndex(indiceColuna)));
        }

        return data;
    }

    public static String ConverteData(Date data) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        return dateFormat.format(data);
    }
}
