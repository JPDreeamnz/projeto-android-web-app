package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import br.com.softlinesistemas.catalogodigital.Activity.BaseFragment;
import br.com.softlinesistemas.catalogodigital.Activity.MoneyTextWatcher;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.view.CardViewNative;

/**
 * Created by João Pedro R. Carvalho on 05/07/2016.
 */

public class EditarClienteFragment extends BaseFragment implements IClientesActivity {
    private int _clienteCodigo;
    private boolean _menuVisualizar = true;
    private ClientesBusiness _clienteBusiness;

    private ListagemEnderecoCard _listagemEnderecoCard;
    private ListagemContatoCard _listagemContatoCard;

    private Spinner _spTipoDocumento;

    private EditText _txtNome,
            _txtDocumento,
            _txtInscricaoEstadual,
            _txtLimiteCredito;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewCriar = inflater.inflate(R.layout.clientes_criar_fragment, null);
        this.setView(viewCriar);
        this.setHasOptionsMenu(true);

        return viewCriar;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(this._menuVisualizar)
            inflater.inflate(R.menu.clientes_editar_menu, menu);
        else
            inflater.inflate(R.menu.clientes_criar_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.clientes_salvar_button){
            View view = this.getView();
            Clientes cliente = new Clientes();

            this._txtDocumento = (EditText) view.findViewById(R.id.clientes_textDocumento);
            this._txtInscricaoEstadual = (EditText) view.findViewById(R.id.clientes_textInscricaoEstadual);
            this._txtLimiteCredito = (EditText) view.findViewById(R.id.clientes_textLimiteCredito);
            this._txtNome = (EditText) view.findViewById(R.id.clientes_textNome);
            this._spTipoDocumento = (Spinner) view.findViewById(R.id.clientes_spTipoDocumento);

            if (this._txtNome.getText().toString() == null || this._txtNome.getText().toString().isEmpty()) {
                this._txtNome.setError("Campo Nome Obrigatório.");
                return false;
            }

            cliente.Codigo = this._clienteCodigo;
            cliente.InscricaoEstadual = this._txtInscricaoEstadual.getText().toString();
            cliente.Documento = this._txtDocumento.getText().toString();

            if(!this._txtLimiteCredito.getText().toString().isEmpty())
                cliente.LimiteCredito = MoneyTextWatcher.unmaskMoneyBigDecimal(this._txtLimiteCredito.getText().toString());

            cliente.Nome = this._txtNome.getText().toString();

            if(this._spTipoDocumento.getSelectedItem().toString().toUpperCase().equals("CNPJ")){
                cliente.TipoDocumento = 1;
            } else {
                cliente.TipoDocumento = 2;
            }

            Pair<Boolean, String> existeCliente = this._clienteBusiness.ExisteCliente(cliente.Codigo, cliente.TipoDocumento, cliente.Documento);
            if(existeCliente != null && existeCliente.first)
            {
                this._txtDocumento.setError("O cliente " + existeCliente.second + " foi cadastrado com esse documento.");
                return false;
            }

            cliente.Enderecos = this._clienteBusiness.getEnderecosSalvar();
            for (Endereco endereco: cliente.Enderecos) {
                endereco.ClienteCodigo = this._clienteCodigo;
            }

            cliente.Contatos = this._clienteBusiness.getContatosSalvar();
            for (Contato contato: cliente.Contatos) {
                contato.ClienteCodigo = this._clienteCodigo;
            }

            this._clienteBusiness.Editar(cliente);

            EscondeKeyboard(view, view.getContext());

            getActivity().getFragmentManager().popBackStack();
        } else if(id == R.id.clientes_editar_button) {
            this._menuVisualizar = false;
            this.adicionaRemoveBloqueios();
            this.setCards(this.getView());
            this.getActivity().invalidateOptionsMenu();
        } else if(id == R.id.clientes_deletar_button) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this.getActivity());
            dialog.setMessage(R.string.confirmar_deletar_cliente);
            dialog.setCancelable(true);
            dialog.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    _clienteBusiness.Excluir(_clienteBusiness.ObterCliente(_clienteCodigo));
                    getActivity().getFragmentManager().popBackStack();
                }
            });
            dialog.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = dialog.create();
            alert.show();
        }

        return true;
    }

    public void setClienteBusiness(ClientesBusiness clientesBusiness){
        this._clienteBusiness = clientesBusiness;
    }

    public void setClienteCodigo(int clienteCodigo){
        this._clienteCodigo = clienteCodigo;
    }

    private void setView(View view)
    {
        //Iniciando o dropdown
        this._spTipoDocumento = (Spinner) view.findViewById(R.id.clientes_spTipoDocumento);
        ArrayAdapter<CharSequence> adaptador = ArrayAdapter.createFromResource(view.getContext(),
                R.array.tipo_documento_spinner, android.R.layout.simple_spinner_item);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this._spTipoDocumento.setAdapter(adaptador);

        if(this._menuVisualizar) {
            Clientes cliente = this._clienteBusiness.ObterCliente(this._clienteCodigo);

            this._spTipoDocumento.setSelection(cliente.TipoDocumento - 1);

            this._txtNome = (EditText) view.findViewById(R.id.clientes_textNome);
            this._txtNome.setText(cliente.Nome);

            this._txtDocumento = (EditText) view.findViewById(R.id.clientes_textDocumento);
            this._txtDocumento.setText(cliente.Documento);

            this._txtInscricaoEstadual = (EditText) view.findViewById(R.id.clientes_textInscricaoEstadual);
            this._txtInscricaoEstadual.setText(cliente.InscricaoEstadual);

            this._txtLimiteCredito = (EditText) view.findViewById(R.id.clientes_textLimiteCredito);
            this._txtLimiteCredito.addTextChangedListener(new MoneyTextWatcher(this._txtLimiteCredito));
            this._txtLimiteCredito.setText(String.format("%.2f", cliente.LimiteCredito));
        }

        this.setCards(view);
        this.adicionaRemoveBloqueios();
    }

    private void setCards(View view){
        // Iniciando a visualização da listagem de endereços
        this._listagemEnderecoCard = new ListagemEnderecoCard(
                this.getActivity().getBaseContext(),
                this._clienteBusiness,
                this,
                this._menuVisualizar);
        this._listagemEnderecoCard.init();
        CardViewNative cardViewEndereco = (CardViewNative) view.findViewById(R.id.clientes_enderecos);
        cardViewEndereco.setCard(_listagemEnderecoCard);

        // Iniciando a visualização da listagem de contatos
        this._listagemContatoCard = new ListagemContatoCard(this.getActivity().getBaseContext(),
                this._clienteBusiness, this, this._menuVisualizar);
        this._listagemContatoCard.init();
        CardViewNative cardViewContato = (CardViewNative) view.findViewById(R.id.clientes_contatos);
        cardViewContato.setCard(_listagemContatoCard);
    }

    private void adicionaRemoveBloqueios()
    {
        this._spTipoDocumento.setEnabled(!this._menuVisualizar);
        this._txtNome.setEnabled(!this._menuVisualizar);
        this._txtDocumento.setEnabled(!this._menuVisualizar);
        this._txtInscricaoEstadual.setEnabled(!this._menuVisualizar);
        this._txtLimiteCredito.setEnabled(!this._menuVisualizar);

        this._listagemContatoCard.getCardHeader().setOtherButtonVisible(!this._menuVisualizar);
        this._listagemEnderecoCard.init();
        this._listagemEnderecoCard.getCardHeader().setOtherButtonVisible(!this._menuVisualizar);
    }

    @Override
    public void onBackPressed() {}

    public void EscondeKeyboard(View v, Context context)
    {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
