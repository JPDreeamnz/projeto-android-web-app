package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;
import br.com.softlinesistemas.catalogodigital.Model.Enum.Operacoes;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;

/**
 * Created by João Pedro R. Carvalho on 13/06/2016.
 */

public class ListagemEnderecoCard extends CardWithList {
    private final String _title = "Endereços";
    private boolean _isVisualizar;
    private ClientesBusiness _clientesBusiness;
    private IClientesActivity _clientesActivity;

    /**
     * Constructor with a base inner layout defined by R.layout.inner_base_main_cardwithlist
     *
     * @param context context
     */
    public ListagemEnderecoCard(Context context,
                                ClientesBusiness clientesBusiness,
                                IClientesActivity clientesActivity,
                                boolean isVisualizar) {
        super(context);
        this._clientesBusiness = clientesBusiness;
        this._clientesActivity = clientesActivity;
        this._isVisualizar = isVisualizar;
    }

    /**
     * Implement this method to initialize your CardHeader.
     * <p/>
     * An example:
     * <pre><code>
     *     //Add Header
     *     CardHeader header = new CardHeader(getContext());
     *     //Add a popup menu. This method set OverFlow button to visible
     *     header.setPopupMenu(R.menu.popupmain, new CardHeader.OnClickCardHeaderPopupMenuListener() {
     *          @Override
     *          public void onMenuItemClick(BaseCard card, MenuItem item) {
     *              Toast.makeText(getContext(), "Click on " + item.getTitle(), Toast.LENGTH_SHORT).show();
     *          }
     *      });
     *      header.setTitle("Weather"); //should use R.string.
     *      return header;
     * <p/>
     * </code></pre>
     *
     * @return the {@link CardHeader}
     */
    @Override
    protected CardHeader initCardHeader() {
        CardHeader header = new CardHeader(getContext());
        header.setTitle(this._title);

        if(!this._isVisualizar) {
            header.setOtherButtonVisible(true);
            header.setOtherButtonClickListener(this.CriarEndereco());
        }

        return header;
    }

    /**
     * Implement this method to initialize your Card.
     * <p/>
     * An example:
     * <pre><code>
     *      setSwipeable(true);
     *      setOnSwipeListener(new OnSwipeListener() {
     *          @Override
     *              public void onSwipe(Card card) {
     *                  Toast.makeText(getContext(), "Swipe on " + card.getCardHeader().getTitle(), Toast.LENGTH_SHORT).show();
     *              }
     *      });
     *  </code></pre>
     */
    @Override
    protected void initCard() {

    }

    /**
     * Implement this method to initialize the list of objects
     * <p/>
     * An example:
     * <pre><code>
     * <p/>
     *      List<ListObject> mObjects = new ArrayList<ListObject>();
     * <p/>
     *      WeatherObject w1= new WeatherObject();
     *      mObjects.add(w1);
     * <p/>
     *      return mObjects;
     * </code></pre>
     *
     * @return the List of ListObject. Return <code>null</code> if the list is empty.
     */
    @Override
    protected List<ListObject> initChildren() {
        List<ListObject> lista = new ArrayList<ListObject>();

        List<Endereco> enderecos = this._clientesBusiness.getEnderecos();
        for (Endereco item : enderecos) {
            lista.add(EnderecoListObject.toEnderecoListObject(item));
        }

        return lista.size() > 0 ? lista : null;
    }

    /**
     * This method is called by the {@link LinearListAdapter} for each row.
     * You can provide your layout and setup your ui elements.
     *
     * @param childPosition position inside the list of objects
     * @param object        {@link ListObject}
     * @param convertView   view used by row
     * @param parent        parent view
     * @return
     */
    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent) {
        TextView txtEndereco = (TextView) convertView.findViewById(R.id.endereco_txtEndereco);
        ImageButton btnExcluir = (ImageButton) convertView.findViewById(R.id.endereco_botao_excluir);
        ImageButton btnEditar = (ImageButton) convertView.findViewById(R.id.endereco_botao_editar);

        if(this._isVisualizar) {
            btnExcluir.setImageResource(android.R.color.transparent);
            btnExcluir.setEnabled(false);

            btnEditar.setImageResource(android.R.color.transparent);
            btnEditar.setEnabled(false);
        }
        else {
            btnExcluir.setImageResource(R.drawable.ic_trash_dark);
            btnExcluir.setOnClickListener(this.BotaoExcluirEndereco(object));

            btnEditar.setImageResource(R.drawable.ic_editar_dark_certo);
            btnEditar.setOnClickListener(this.EditarEndereco(object));
        }

        Endereco endereco = (Endereco) object;
        txtEndereco.setText(endereco.Endereco + ", " +
                endereco.Numero + " " + endereco.Bairro + " - " + endereco.Cidade + "/" + endereco.Estado);

        return convertView;
    }

    /**
     * Implement this method to specify the layoutId used for each row in the list
     *
     * @return the layoutId
     */
    @Override
    public int getChildLayoutId() {
        return R.layout.clientes_listar_endereco_card;
    }

    private View.OnClickListener BotaoExcluirEndereco(final ListObject object)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Endereco endereco = (Endereco) object;
                _clientesBusiness.ExcluirEndereco(endereco);
                getLinearListAdapter().remove(object);
            }
        };
    }

    private View.OnClickListener EditarEndereco(final ListObject object)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Endereco endereco = (Endereco) object;
                EditarEnderecoFragment fragment = new EditarEnderecoFragment();
                fragment.setEnderecoID(endereco.EnderecoID);
                fragment.setClienteBusiness(_clientesBusiness);

                FragmentTransaction transaction = ListagemEnderecoCard.this._clientesActivity
                        .getFragmentManager().beginTransaction();
                transaction.replace(R.id.clientes_listagem_fragment_container,
                        fragment,
                        "CLIENTES_EDITAR_ENDERECO_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(
                        android.R.animator.fade_in,
                        android.R.animator.fade_out,
                        android.R.animator.fade_in,
                        android.R.animator.fade_out
                );
                transaction.commit();
            }
        };
    }

    private CardHeader.OnClickCardHeaderOtherButtonListener CriarEndereco(){
        return new CardHeader.OnClickCardHeaderOtherButtonListener() {
            @Override
            public void onButtonItemClick(Card card, View view) {
                CriarEnderecoFragment fragment = new CriarEnderecoFragment();
                fragment.setClienteBusiness(_clientesBusiness);

                FragmentTransaction transaction = ListagemEnderecoCard.this
                        ._clientesActivity.getFragmentManager().beginTransaction();
                transaction.replace(
                        R.id.clientes_listagem_fragment_container,
                        fragment,
                        "CLIENTE_CRIAR_ENDERECO_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(
                        android.R.animator.fade_in,
                        android.R.animator.fade_out,
                        android.R.animator.fade_in,
                        android.R.animator.fade_out);
                transaction.commit();
            }
        };
    }
}

class EnderecoListObject extends Endereco implements CardWithList.ListObject
{

    public static EnderecoListObject toEnderecoListObject(Endereco endereco)
    {
        EnderecoListObject enderecoListObject = new EnderecoListObject();

        enderecoListObject.Endereco = endereco.Endereco;
        enderecoListObject.Complemento = endereco.Complemento;
        enderecoListObject.Cidade = endereco.Cidade;
        enderecoListObject.CEP = endereco.CEP;
        enderecoListObject.ClienteCodigo = endereco.ClienteCodigo;
        enderecoListObject.Bairro = endereco.Bairro;
        enderecoListObject.EnderecoID = endereco.EnderecoID;
        enderecoListObject.Estado = endereco.Estado;
        enderecoListObject.Numero = endereco.Numero;

        return enderecoListObject;
    }

    /**
     * Returns the object id
     *
     * @return
     */
    @Override
    public String getObjectId() {
        return null;
    }

    /**
     * Returns the parent card
     */
    @Override
    public Card getParentCard() {
        return null;
    }

    /**
     * Register a callback to be invoked when an item in this LinearListView has
     * been clicked.
     *
     * @param onItemClickListener
     * @return The callback to be invoked with an item in this LinearListView has
     * been clicked, or null id no callback has been set.
     */
    @Override
    public void setOnItemClickListener(CardWithList.OnItemClickListener onItemClickListener) {

    }

    /**
     * @return The callback to be invoked with an item in this LinearListView has
     * been clicked, or null id no callback has been set.
     */
    @Override
    public CardWithList.OnItemClickListener getOnItemClickListener() {
        return null;
    }

    /**
     * Indicates if the item is swipeable
     */
    @Override
    public boolean isSwipeable() {
        return false;
    }

    /**
     * Set the item as swipeable
     *
     * @param isSwipeable
     */
    @Override
    public void setSwipeable(boolean isSwipeable) {

    }

    /**
     * Returns the callback to be invoked when item has been swiped
     *
     * @return listener
     */
    @Override
    public CardWithList.OnItemSwipeListener getOnItemSwipeListener() {
        return null;
    }

    /**
     * Register a callback to be invoked when an item in this LinearListView has
     * been swiped.
     *
     * @param onSwipeListener listener
     */
    @Override
    public void setOnItemSwipeListener(CardWithList.OnItemSwipeListener onSwipeListener) {

    }
}