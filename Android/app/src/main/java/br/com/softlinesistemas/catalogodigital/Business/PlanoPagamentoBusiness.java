package br.com.softlinesistemas.catalogodigital.Business;

import android.content.Context;

import java.util.ArrayList;

import br.com.softlinesistemas.catalogodigital.Datasource.PlanoPagamento.PlanoPagamentoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.POJO.PlanoPagamentoSpinner;
import br.com.softlinesistemas.catalogodigital.Model.PlanoPagamento;

/**
 * Created by João Pedro R. Carvalho on 27/07/2016.
 */

public class PlanoPagamentoBusiness {
    private PlanoPagamentoDatasource _dtPlanoPagamento;

    public PlanoPagamentoBusiness(Context context)
    {
        _dtPlanoPagamento = new PlanoPagamentoDatasource(context);
    }

    public ArrayList<PlanoPagamentoSpinner> ObtemListaSpinner()
    {
        ArrayList<PlanoPagamentoSpinner> planoPagamentoSP = new ArrayList<>();

        for (PlanoPagamento planoPagamento : this._dtPlanoPagamento.ObtemLista()) {
            planoPagamentoSP.add(new PlanoPagamentoSpinner(planoPagamento));
        }

        return planoPagamentoSP;
    }
}
