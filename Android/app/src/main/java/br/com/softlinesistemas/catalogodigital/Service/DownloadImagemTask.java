package br.com.softlinesistemas.catalogodigital.Service;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.Pair;
import android.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;

/**
 * Created by João Pedro R. Carvalho on 26/09/2016.
 */

public class DownloadImagemTask extends AsyncTask<String, Void, Pair<Integer, byte[]>> {
    private ImagemProdutoDatasource _dtImagemProduto;
    private ControleImagemDownload _controle;
    private long _codigoProduto;
    private String _codigoImagem;
    private String TOKEN;
    private String URL;

    public DownloadImagemTask(
            String URL, String TOKEN, long codigoProduto, String codigoImagem, Context contexto, ControleImagemDownload controle)
    {
        this._dtImagemProduto = new ImagemProdutoDatasource(contexto);
        this._controle = controle;
        this._codigoProduto = codigoProduto;
        this._codigoImagem = codigoImagem;
        this.TOKEN = TOKEN;
        this.URL = URL;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, byte[]> doInBackground(String... params) {
        Pair<Integer, byte[]> respota;
        try {
            HttpRequest request = HttpRequest.get(this.URL, true, "caminhoPasta", params[0])
                    .accept("application/json")
                    .header("Authorization", "Bearer " + this.TOKEN);

            int code = request.code();
            String base64 = request.body();

            respota = new Pair<>(code, Base64.decode(base64, Base64.DEFAULT));
        }
        catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, new byte[0]);
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, byte[]> retorno) {
            try {
                if(retorno.first == 200) {
                    this._dtImagemProduto.MudarDiretorioCriar(this._codigoProduto);
                    this._dtImagemProduto.SalvarImagemMemoriaInterna(
                            BitmapFactory.decodeByteArray(retorno.second, 0, retorno.second.length), this._codigoImagem);
                    this._dtImagemProduto = null;
                }

                    this._controle.DownloadConcluido();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
