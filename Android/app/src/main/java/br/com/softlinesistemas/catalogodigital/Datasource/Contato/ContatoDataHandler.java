package br.com.softlinesistemas.catalogodigital.Datasource.Contato;

import android.content.ContentValues;
import android.database.Cursor;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Contato;

/**
 * Created by João Pedro R. Carvalho on 14/03/2016.
 */
public class ContatoDataHandler {

    public static Contato fromCursor(Cursor cursor) throws ParseException
    {
        Contato contato = null;

        if(cursor.moveToFirst())
        {
            contato = BindCursorContato(cursor);
        }

        cursor.close();
        return contato;
    }

    public static List<Contato> fromCursorToList(Cursor cursor)
    {
        List<Contato> contatos = new ArrayList<Contato>();

        if(cursor.moveToFirst())
        {
            do{
                contatos.add(BindCursorContato(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return contatos;
    }

    public static ContentValues toContentValue(Contato contato)
    {
        ContentValues values = new ContentValues();

        if(contato.ContatoID > 0) values.put(DatabaseContract.Contato.Coluna_ContatoID, contato.ContatoID);
        values.put(DatabaseContract.Contato.Coluna_TipoContato, contato.TipoContato);
        values.put(DatabaseContract.Contato.Coluna_DDD, contato.DDD);
        values.put(DatabaseContract.Contato.Coluna_Telefone, contato.Telefone);
        values.put(DatabaseContract.Contato.Coluna_Email, contato.Email);
        values.put(DatabaseContract.Contato.Coluna_ClienteCodigo, contato.ClienteCodigo);

        return values;
    }

    private static Contato BindCursorContato(Cursor cursor)
    {
        Contato contato = new Contato();

        contato.ContatoID = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Contato.Coluna_ContatoID)
        );
        contato.TipoContato = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Contato.Coluna_TipoContato)
        );
        contato.DDD = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Contato.Coluna_DDD)
        );
        contato.Telefone = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Contato.Coluna_Telefone)
        );
        contato.Email = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Contato.Coluna_Email)
        );
        contato.ClienteCodigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Contato.Coluna_ClienteCodigo)
        );

        return contato;
    }
}
