package br.com.softlinesistemas.catalogodigital.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;

/**
 * Created by João Pedro R. Carvalho on 14/12/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DatabaseName = "DB_CatalogoDigital";
    private static final int DatabaseVersion = 4;
    private Context _context;

    /**
     *  DatabaseHelper segue a convenção da documentação android no auxilio à criação e atualização
     *  do banco de dados.
     * @param context
     */
    public DatabaseHelper(Context context)
    {
        super(context, DatabaseName, null, DatabaseVersion);
        this._context = context;
    }

    /**
     * Código a ser executado na criação do Banco de Dados.
     * @param db
     * @throws SQLiteException
     */
    @Override
    public void onCreate(SQLiteDatabase db) throws SQLiteException{
        db.execSQL(DatabaseContract.Login.CriarTabela);
        db.execSQL(DatabaseContract.Clientes.CriarTabela);
        db.execSQL(DatabaseContract.Contato.CriarTabela);
        db.execSQL(DatabaseContract.Endereco.CriarTabela);
        db.execSQL(DatabaseContract.Produto.CriarTabela);
        db.execSQL(DatabaseContract.Pedido.CriarTabela);
        db.execSQL(DatabaseContract.ItensPedido.CriarTabela);
        db.execSQL(DatabaseContract.Vendedor.CriarTabela);
        db.execSQL(DatabaseContract.PlanoPagamento.CriarTabela);
        db.execSQL(DatabaseContract.SincroniaTabelas.CriarTabela);
        db.execSQL(DatabaseContract.VendedorPlanoPadrao.CriarTabela);
        new ImagemProdutoDatasource(_context).CriarDiretorio();
    }

    /**
     * Código a ser executado na atualização do Banco de Dados.
     * @param db
     * @param oldVersion
     * @param newVersion
     * @throws SQLiteException
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) throws SQLiteException{
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.Login.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.Contato.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.Endereco.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.Clientes.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.Produto.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.Pedido.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.ItensPedido.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.Vendedor.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.PlanoPagamento.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.SincroniaTabelas.Tabela_Nome);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.VendedorPlanoPadrao.Tabela_Nome);
        new ImagemProdutoDatasource(_context).ExcluirDiretorio();
        onCreate(db);
    }
}
