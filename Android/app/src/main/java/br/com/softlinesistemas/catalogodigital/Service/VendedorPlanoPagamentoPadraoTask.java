package br.com.softlinesistemas.catalogodigital.Service;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONObject;

import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.VendedorPlanoPadraoDataSource;
import br.com.softlinesistemas.catalogodigital.Model.VendedorPlanoPadrao;

/**
 * Created by João Pedro R. Carvalho on 01/03/2017.
 */

public class VendedorPlanoPagamentoPadraoTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    VendedorPlanoPadraoDataSource _dtVendedor;
    IIntegracaoService _integracao;
    String URL;

    public VendedorPlanoPagamentoPadraoTask(String URL, VendedorPlanoPadraoDataSource dtVendedor, IIntegracaoService integracao){
        this._dtVendedor = dtVendedor;
        this._integracao = integracao;
        this.URL = "http://" + URL + "api/Integracao/ObterVendedorPlanoPadrao";
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        Pair<Integer, String> respota;
        try {
            HttpRequest request = HttpRequest.get(this.URL)
                    .accept("application/json")
                    .header("Authorization", "Bearer " + params[0]);

            respota = new Pair<>(request.code(), request.body());
        } catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, "Não foi possível obter o vendedor e o plano padrão.");
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        try{
            if(resposta.first == 200) {
                JSONObject obj = new JSONObject(resposta.second);

                VendedorPlanoPadrao novoVendedor = new VendedorPlanoPadrao();

                novoVendedor.Codigo = 1; //Sempre será 1 (existe apenas 1 registro)
                novoVendedor.CodigoVendedor = obj.getLong("VendedorDefault");
                novoVendedor.CodigoPlanoPagamento = obj.getLong("PlanoPagamentoDefault");

                this._dtVendedor.Editar(novoVendedor);

                this._integracao.AtualizarPush(true, "Tabela vendedor e plano de pagamento padrão atualizada com sucesso.");
            }else{
                this._integracao.AtualizarPush(true, resposta.first + " - Não foi possível atualizar o vendedor e o plano de pagamento padrão");
            }
        } catch (Exception e){
            this._integracao.AtualizarPush(true, "Não foi possível atualizar o vendedor e o plano de pagamento padrão.");
        }
    }
}
