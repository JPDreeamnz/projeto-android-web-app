package br.com.softlinesistemas.catalogodigital.Model;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public class Produto {
    public long Codigo;
    public String Nome;
    public String Unidade;
    public String CodigoBarras;
    public String CodigoFabrica;
    public String CodigoOriginal;
    public String Cor;
    public String Tamanho;
    public String Marca;
    public float ValorVenda;
    public float QuantidadeEstoque;
}
