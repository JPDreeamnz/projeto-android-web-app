package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.view.menu.MenuBuilder;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.math.BigDecimal;
import java.text.NumberFormat;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Activity.MoneyTextWatcher;
import br.com.softlinesistemas.catalogodigital.Activity.Pedido.CriarPedidoFragment;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;
import br.com.softlinesistemas.catalogodigital.Model.Produto;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by jcarvalho on 19/07/2016.
 */

public class ProdutoTabFragment extends BaseV4Fragment {
    private boolean _isPedido;
    private long _produtoCodigo;
    private ProdutoBusiness _produtoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private Fragment pedidoFragment;

    public static ProdutoTabFragment novaInstancia(long produtoCodigo, ProdutoBusiness produtoBusiness, ItensPedidoBusiness itensPedidoBusiness, Fragment pedidoFragment, boolean isPedido, int fragmentContainer){
        ProdutoTabFragment instancia = new ProdutoTabFragment();

        instancia._produtoCodigo = produtoCodigo;
        instancia._produtoBusiness = produtoBusiness;
        instancia._itensPedidoBusiness = itensPedidoBusiness;
        instancia._isPedido = isPedido;
        instancia.pedidoFragment = pedidoFragment;
        instancia.setFragmentContainer(fragmentContainer);
        return instancia;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.produto_tab_fragment, container, false);

        this.setHasOptionsMenu(true);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_produto);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.InformacoesGerais));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.imagens));

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setAdapter(
                new ProdutoPagerAdapter(
                        getChildFragmentManager(),
                        tabLayout.getTabCount(),
                        this._produtoBusiness,
                        this._produtoCodigo,
                        getFragmentContainer()));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(this.selectTabs(viewPager));

        return view;
    }

    private TabLayout.OnTabSelectedListener selectTabs(final ViewPager viewPager){
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(this._isPedido)
            inflater.inflate(R.menu.pedido_criar_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       int id = item.getItemId();
       if(id == R.id.pedido_salvar_button){
           View view = this.getView();
           final AlertDialog alertDialog;

           AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
           LayoutInflater inflater = getActivity().getLayoutInflater();
           builder.setTitle(R.string.informe_detalhes_produto);

           builder.setView(inflater.inflate(R.layout.pedido_finaliz_produto, null))
                   .setPositiveButton(R.string.OK, null)
                   .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                           dialog.cancel();
                       }
                   });

           alertDialog = builder.create();

           alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
               @Override
               public void onShow(DialogInterface dialog) {
                   Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                   b.setOnClickListener(setPositiveButton(alertDialog));
               }
           });



           alertDialog.show();

           Produto produto = _produtoBusiness.ObterProduto(_produtoCodigo);
           EditText txtValorDesconto = (EditText) alertDialog.findViewById(R.id.pedido_txtVlrDesconto);
           EditText txtValorUnitario = (EditText) alertDialog.findViewById(R.id.pedido_txtValorUnitario);

           //txtValorTotal.addTextChangedListener(new MoneyTextWatcher(txtValorTotal));
           //txtValorTotal.setEnabled(false);

           txtValorUnitario.addTextChangedListener(new MoneyTextWatcher(txtValorUnitario));
           txtValorDesconto.addTextChangedListener(new MoneyTextWatcher(txtValorDesconto));

           txtValorUnitario.setText(String.valueOf(NumberFormat.getCurrencyInstance().format(produto.ValorVenda)));
           txtValorUnitario.setEnabled(false);

       }

       return true;
    }

    private View.OnClickListener setPositiveButton(final AlertDialog alert)
    {
        return new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                boolean salvar = true;

                EditText txtQuantidade = (EditText) alert.findViewById(R.id.pedido_txtQuantidade);
                EditText txtValorDesconto = (EditText) alert.findViewById(R.id.pedido_txtVlrDesconto);
                EditText txtValorUnitario = (EditText) alert.findViewById(R.id.pedido_txtValorUnitario);

                if(txtQuantidade.getText().length() == 0)
                {
                    txtQuantidade.setError(getResources().getString(R.string.erro_campo_obrigatorio));
                    salvar = false;
                }

                if(txtValorUnitario.getText().length() == 0)
                {
                    txtValorUnitario.setError(getResources().getString(R.string.erro_campo_obrigatorio));
                    salvar = false;
                }

                if(salvar == true) {

                    ItensPedido item = new ItensPedido();

                    item.Produto = _produtoCodigo;
                    item.Quantidade = Float.parseFloat(String.valueOf(txtQuantidade.getText()));

                    if(txtValorDesconto.getText().length() == 0)
                        item.ValorDesconto = new BigDecimal(0);
                    else
                        item.ValorDesconto = BigDecimal.valueOf(MoneyTextWatcher.unmaskMoney(String.valueOf(txtValorDesconto.getText())));

                    item.ValorUnitario = BigDecimal.valueOf(MoneyTextWatcher.unmaskMoney(String.valueOf(txtValorUnitario.getText())));
                    BigDecimal qtd = new BigDecimal(item.Quantidade);
                    item.ValorTotal = item.ValorUnitario.multiply(qtd)
                            .subtract(item.ValorDesconto.multiply(qtd));

                    _itensPedidoBusiness.Inserir(item);
                    alert.dismiss();

                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(
                            R.id.pedido_fragment_container,
                            pedidoFragment,
                            "PEDIDO_CRIAR_FRAGMENT");
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        };
    }

    //Desabilitado até funcionar
    private TextWatcher setValorTotal(
            final EditText txtValorTotal,
            final EditText txtValorUnitario,
            final EditText txtValorDesconto,
            final EditText txtQuantitade)
    {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                float valorUnitario = MoneyTextWatcher.unmaskMoney(String.valueOf(txtValorUnitario.getText()));
                float valorDesconto = MoneyTextWatcher.unmaskMoney(String.valueOf(txtValorDesconto.getText()));
                int quantidade = txtQuantitade.getText().length() > 0 ? Integer.parseInt(String.valueOf(txtQuantitade.getText())) : 0;
                float valorTotal = (quantidade * valorUnitario) - (quantidade * valorDesconto);
                txtValorTotal.setText("");
                txtValorTotal.setText(String.valueOf(valorTotal));
            }
        };
    }
}
