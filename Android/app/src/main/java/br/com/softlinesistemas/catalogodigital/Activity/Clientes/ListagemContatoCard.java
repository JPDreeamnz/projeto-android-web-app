package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.Model.Enum.Operacoes;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;

/**
 * Created by João Pedro R. Carvalho on 13/06/2016.
 */

public class ListagemContatoCard extends CardWithList {
    private final String _title = "Contatos";
    private boolean _isVisualizar;
    private ClientesBusiness _clientesBusiness;
    private IClientesActivity _clientesActivity;

    /**
     * Constructor with a base inner layout defined by R.layout.inner_base_main_cardwithlist
     *
     * @param context context
     */
    public ListagemContatoCard(Context context,
                               ClientesBusiness clientesBusiness,
                               IClientesActivity clientesActivity,
                               boolean isVisualizar) {
        super(context);
        this._clientesBusiness = clientesBusiness;
        this._clientesActivity = clientesActivity;
        this._isVisualizar = isVisualizar;
    }

    /**
     * Implement this method to initialize your CardHeader.
     * <p/>
     * An example:
     * <pre><code>
     *     //Add Header
     *     CardHeader header = new CardHeader(getContext());
     *     //Add a popup menu. This method set OverFlow button to visible
     *     header.setPopupMenu(R.menu.popupmain, new CardHeader.OnClickCardHeaderPopupMenuListener() {
     *          @Override
     *          public void onMenuItemClick(BaseCard card, MenuItem item) {
     *              Toast.makeText(getContext(), "Click on " + item.getTitle(), Toast.LENGTH_SHORT).show();
     *          }
     *      });
     *      header.setTitle("Weather"); //should use R.string.
     *      return header;
     * <p/>
     * </code></pre>
     *
     * @return the {@link CardHeader}
     */
    @Override
    protected CardHeader initCardHeader() {
        CardHeader header = new CardHeader(getContext());
        header.setTitle(this._title);

        if(!this._isVisualizar) {
            header.setOtherButtonVisible(true);
            header.setOtherButtonClickListener(this.CriarContato());
        }

        return header;
    }

    /**
     * Implement this method to initialize your Card.
     * <p/>
     * An example:
     * <pre><code>
     *      setSwipeable(true);
     *      setOnSwipeListener(new OnSwipeListener() {
     *          @Override
     *              public void onSwipe(Card card) {
     *                  Toast.makeText(getContext(), "Swipe on " + card.getCardHeader().getTitle(), Toast.LENGTH_SHORT).show();
     *              }
     *      });
     *  </code></pre>
     */
    @Override
    protected void initCard() {

    }

    /**
     * Implement this method to initialize the list of objects
     * <p/>
     * An example:
     * <pre><code>
     * <p/>
     *      List<ListObject> mObjects = new ArrayList<ListObject>();
     * <p/>
     *      WeatherObject w1= new WeatherObject();
     *      mObjects.add(w1);
     * <p/>
     *      return mObjects;
     * </code></pre>
     *
     * @return the List of ListObject. Return <code>null</code> if the list is empty.
     */
    @Override
    protected List<ListObject> initChildren() {
        List<ListObject> lista = new ArrayList<ListObject>();

        List<Contato> contatos = this._clientesBusiness.getContatos();
        for (Contato item : contatos) {
            lista.add(ContatoListObject.toContatoListObject(item));
        }

        return lista.size() > 0 ? lista : null;
    }

    /**
     * This method is called by the {@link LinearListAdapter} for each row.
     * You can provide your layout and setup your ui elements.
     *
     * @param childPosition position inside the list of objects
     * @param object        {@link ListObject}
     * @param convertView   view used by row
     * @param parent        parent view
     * @return
     */
    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent) {
        TextView txtContato = (TextView) convertView.findViewById(R.id.contato_txtContato);
        ImageButton btnExcluir = (ImageButton) convertView.findViewById(R.id.contato_botao_excluir);
        ImageButton btnEditar = (ImageButton) convertView.findViewById(R.id.contato_botao_editar);

        if(this._isVisualizar) {
            btnExcluir.setImageResource(android.R.color.transparent);
            btnExcluir.setEnabled(false);

            btnEditar.setImageResource(android.R.color.transparent);
            btnEditar.setEnabled(false);
        }
        else {
            btnExcluir.setImageResource(R.drawable.ic_trash_dark);
            btnExcluir.setOnClickListener(this.BotaoExcluirContato(object));

            btnEditar.setImageResource(R.drawable.ic_editar_dark_certo);
            btnEditar.setOnClickListener(this.EditarContato(object));
        }
        Contato contato = (Contato) object;

        if(contato.TipoContato == 3) { //Email
            txtContato.setText(contato.Email);
        } else {
            txtContato.setText("(" + contato.DDD + ") " + contato.Telefone);
        }

        return convertView;
    }

    /**
     * Implement this method to specify the layoutId used for each row in the list
     *
     * @return the layoutId
     */
    @Override
    public int getChildLayoutId() {
        return R.layout.clientes_listar_contatos_card;
    }

    private View.OnClickListener BotaoExcluirContato(final ListObject object)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contato contato = (Contato) object;
                _clientesBusiness.ExcluirContato(contato);
                getLinearListAdapter().remove(object);
            }
        };
    }

    private View.OnClickListener EditarContato(final ListObject object)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contato contato = (Contato) object;
                EditarContatoFragment fragment = new EditarContatoFragment();
                fragment.setContatoCodigo(contato.ContatoID);
                fragment.setClientesBusiness(_clientesBusiness);

                FragmentTransaction transaction = ListagemContatoCard.this._clientesActivity
                        .getFragmentManager().beginTransaction();
                transaction.replace(R.id.clientes_listagem_fragment_container,
                        fragment,
                        "CLIENTES_EDITAR_CONTATO_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(
                        android.R.animator.fade_in,
                        android.R.animator.fade_out,
                        android.R.animator.fade_in,
                        android.R.animator.fade_out
                );
                transaction.commit();
            }
        };
    }

    private CardHeader.OnClickCardHeaderOtherButtonListener CriarContato(){
        return new CardHeader.OnClickCardHeaderOtherButtonListener() {
            @Override
            public void onButtonItemClick(Card card, View view) {
                CriarContatoFragment fragment = new CriarContatoFragment();
                fragment.setClientesBusiness(_clientesBusiness);

                FragmentTransaction transaction = ListagemContatoCard.this
                        ._clientesActivity.getFragmentManager().beginTransaction();
                transaction.replace(
                        R.id.clientes_listagem_fragment_container,
                        fragment,
                        "CLIENTE_CRIAR_CONTATO_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(
                        android.R.animator.fade_in,
                        android.R.animator.fade_out,
                        android.R.animator.fade_in,
                        android.R.animator.fade_out);
                transaction.commit();
            }
        };
    }
}

class ContatoListObject extends Contato implements CardWithList.ListObject
{

    public static ContatoListObject toContatoListObject(Contato contato)
    {
        ContatoListObject contatoListObject = new ContatoListObject();

        contatoListObject.DDD = contato.DDD;
        contatoListObject.Email = contato.Email;
        contatoListObject.Telefone = contato.Telefone;
        contatoListObject.TipoContato = contato.TipoContato;
        contatoListObject.ClienteCodigo = contato.ClienteCodigo;
        contatoListObject.ContatoID = contato.ContatoID;

        return contatoListObject;
    }

    /**
     * Returns the object id
     *
     * @return
     */
    @Override
    public String getObjectId() {
        return null;
    }

    /**
     * Returns the parent card
     */
    @Override
    public Card getParentCard() {
        return null;
    }

    /**
     * Register a callback to be invoked when an item in this LinearListView has
     * been clicked.
     *
     * @param onItemClickListener
     * @return The callback to be invoked with an item in this LinearListView has
     * been clicked, or null id no callback has been set.
     */
    @Override
    public void setOnItemClickListener(CardWithList.OnItemClickListener onItemClickListener) {

    }

    /**
     * @return The callback to be invoked with an item in this LinearListView has
     * been clicked, or null id no callback has been set.
     */
    @Override
    public CardWithList.OnItemClickListener getOnItemClickListener() {
        return null;
    }

    /**
     * Indicates if the item is swipeable
     */
    @Override
    public boolean isSwipeable() {
        return false;
    }

    /**
     * Set the item as swipeable
     *
     * @param isSwipeable
     */
    @Override
    public void setSwipeable(boolean isSwipeable) {

    }

    /**
     * Returns the callback to be invoked when item has been swiped
     *
     * @return listener
     */
    @Override
    public CardWithList.OnItemSwipeListener getOnItemSwipeListener() {
        return null;
    }

    /**
     * Register a callback to be invoked when an item in this LinearListView has
     * been swiped.
     *
     * @param onSwipeListener listener
     */
    @Override
    public void setOnItemSwipeListener(CardWithList.OnItemSwipeListener onSwipeListener) {

    }
}