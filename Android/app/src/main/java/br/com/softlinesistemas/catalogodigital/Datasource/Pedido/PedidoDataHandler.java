package br.com.softlinesistemas.catalogodigital.Datasource.Pedido;

import android.content.ContentValues;
import android.database.Cursor;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;

/**
 * Created by João Pedro R. Carvalho on 24/07/2016.
 */

public class PedidoDataHandler {
    public static Pedido fromCursor(Cursor cursor) throws ParseException {
        Pedido pedido = null;

        if(cursor.moveToFirst())
        {
            pedido = BindCursorPedido(cursor);
        }

        cursor.close();
        return pedido;
    }

    public static List<Pedido> fromCursorToList(Cursor cursor) throws ParseException {
        List<Pedido> pedidos = new ArrayList<Pedido>();

        if(cursor.moveToFirst())
        {
            do{
                pedidos.add(BindCursorPedido(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return pedidos;
    }

    public static ContentValues toContentValues(Pedido pedido)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.Pedido.Coluna_Cliente, pedido.Cliente);
        values.put(DatabaseContract.Pedido.Coluna_Data, pedido.getData());
        values.put(DatabaseContract.Pedido.Coluna_Observacao, pedido.Observacao);
        values.put(DatabaseContract.Pedido.Coluna_PlanoPagamento, pedido.PlanoPagamento);
        values.put(DatabaseContract.Pedido.Coluna_StatusIntegracao, pedido.StatusIntegracao);
        values.put(DatabaseContract.Pedido.Coluna_Valor, pedido.Valor.doubleValue());
        values.put(DatabaseContract.Pedido.Coluna_ValorAcrescimo, pedido.ValorAcrescimo.doubleValue());
        values.put(DatabaseContract.Pedido.Coluna_ValorDesconto, pedido.ValorDesconto.doubleValue());
        values.put(DatabaseContract.Pedido.Coluna_ValorProduto, pedido.ValorProduto.doubleValue());
        values.put(DatabaseContract.Pedido.Coluna_Vendedor, pedido.Vendedor);

        return values;
    }

    private static Pedido BindCursorPedido(Cursor cursor) throws ParseException {
        Pedido pedido = new Pedido();

        pedido.Cliente = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_Cliente)
        );
        pedido.setData(cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_Data)
        ));
        pedido.Observacao = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_Observacao)
        );
        pedido.Pedido = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_Pedido)
        );
        pedido.PlanoPagamento = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_PlanoPagamento)
        );
        pedido.StatusIntegracao = cursor.getInt(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_StatusIntegracao)
        );
        pedido.Valor = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_Valor)
        ));
        pedido.ValorAcrescimo = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_ValorAcrescimo)
        ));
        pedido.ValorDesconto = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_ValorDesconto)
        ));
        pedido.ValorProduto = new BigDecimal(cursor.getDouble(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_ValorProduto)
        ));
        pedido.Observacao = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_Observacao)
        );
        pedido.Vendedor = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Pedido.Coluna_Vendedor)
        );

        return pedido;
    }
}
