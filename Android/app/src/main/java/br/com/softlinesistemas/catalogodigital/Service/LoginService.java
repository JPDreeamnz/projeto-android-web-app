package br.com.softlinesistemas.catalogodigital.Service;


import android.os.AsyncTask;
import android.util.Pair;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.softlinesistemas.catalogodigital.Business.ILoginTaskListener;
import br.com.softlinesistemas.catalogodigital.Model.POJO.RetornoLoginPOJO;

/**
 * Created by João Pedro R. Carvalho on 16/12/2015.
 */
public class LoginService extends AsyncTask<String, Void, Pair<Integer, String>> {

    private static HttpURLConnection sConexao;
    private final ILoginTaskListener mTaskListener;
    private static final String URL_LOGIN = "/Api/Auth/Post";
    //private static final String URL_LOGIN = "http://192.168.0.15/Api/Api/Login/v1/Authentication";

    private static final class RETORNO_LOGIN{
        public static final String TOKEN = "Token";
        public static final String STATUS = "Status";
        public static final String EXPIRES = "ExpiresTimeForAndroid";
        public static final String REMAINING = "Remaning";
    }
    private static final class ENVIO_LOGIN{
        public static final String LOGIN = "Username";
        public static final String SENHA = "Password";
    }

    public LoginService(ILoginTaskListener taskListener) {
        this.mTaskListener = taskListener;
    }


    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p/>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        int respostaConexao = 0;
        StringBuilder respostaMensagem = new StringBuilder();

        try{
            URL url = new URL("http://" + params[2] + this.URL_LOGIN); //Temporário
            //URL url = new URL(this.URL_LOGIN);
            sConexao = (HttpURLConnection) url.openConnection();
            sConexao.setDoOutput(true);
            sConexao.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            sConexao.setRequestMethod("POST");

            JSONObject parametros = new JSONObject();
            parametros.put(ENVIO_LOGIN.LOGIN, params[0]);
            parametros.put(ENVIO_LOGIN.SENHA, params[1]);

            OutputStream outputStream = sConexao.getOutputStream();
            outputStream.write(parametros.toString().getBytes("UTF-8"));
            outputStream.close();

            InputStream inputStream = new BufferedInputStream(sConexao.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
            respostaConexao = sConexao.getResponseCode();

            String linha;
            while ((linha = bufferedReader.readLine()) != null){
                respostaMensagem.append(linha);
            }

        }catch (Exception e){
            respostaMensagem.append(e.getMessage());
        }finally {
            sConexao.disconnect();
        }

        return new Pair<>(respostaConexao, respostaMensagem.toString());
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resultado) {
        try {
            if(resultado.first == HttpURLConnection.HTTP_OK) {
                JSONObject json = new JSONObject(resultado.second);

                RetornoLoginPOJO retornoLogin = new RetornoLoginPOJO(
                        json.getString(RETORNO_LOGIN.TOKEN).isEmpty() ? -1 : 200,
                        json.getString(RETORNO_LOGIN.TOKEN),
                        json.getString(RETORNO_LOGIN.EXPIRES),
                        json.getString(RETORNO_LOGIN.STATUS),
                        json.getString(RETORNO_LOGIN.REMAINING)
                );

                if (mTaskListener != null) {
                    mTaskListener.OnTaskCompleted(retornoLogin);
                }
            }else{
                if(mTaskListener != null){
                    mTaskListener.onTaskError(resultado.second);
                }
            }
        } catch (JSONException e) {
            mTaskListener.onTaskError(e.getMessage()); //trabalhar um grava log
        }
    }
}
