package br.com.softlinesistemas.catalogodigital.Model.POJO;

/**
 * Created by João Pedro R. Carvalho on 21/12/2015.
 */
public class RetornoLoginPOJO {
    private int RetornoHttp;
    private String Token;
    private String Expires;
    private String Remaning;
    private String Mensagem;


    public RetornoLoginPOJO(int RetornoHttp, String Token, String Expires, String Mensagem, String Remaning){
        this.RetornoHttp = RetornoHttp;
        this.Token = Token;
        this.Expires = Expires;
        this.Mensagem = Mensagem;
        this.Remaning = Remaning;
    }

    public int getRetornoHttp(){
        return RetornoHttp;
    }

    public String getToken() {
        return Token;
    }

    public String getExpires() {
        return Expires;
    }

    public void setMensagem(String Mensagem){
        this.Mensagem = Mensagem;
    }

    public String getMessagem() {
        return Mensagem;
    }

    public String getRemaning(){
        return Remaning;
    }
}
