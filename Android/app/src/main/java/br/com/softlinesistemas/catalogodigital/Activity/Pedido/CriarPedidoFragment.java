package br.com.softlinesistemas.catalogodigital.Activity.Pedido;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PlanoPagamentoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.VendedorBusiness;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.view.CardViewNative;

/**
 * Created by João Pedro R. Carvalho on 25/07/2016.
 */

public class CriarPedidoFragment extends BaseV4Fragment {
    private IPedidoActivity _pedidoActivity;
    private Fragment _infoGeraisFragment;
    private Fragment _itensPedidoFragment;
    private PedidoBusiness _pedidoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private ProdutoBusiness _produtoBusiness;
    private VendedorBusiness _vendedorBusiness;
    private PlanoPagamentoBusiness _planoPagamentoBusiness;
    private ImagemProdutoDatasource _imagemProdutoDataSource;
    private ClientesBusiness _clientesBusiness;
    private long pedidoCodigo;
    private boolean menuVisualizar, salvarEditar;

    public static CriarPedidoFragment novaInstancia(
            IPedidoActivity pedidoActivity,
            PedidoBusiness pedidoBusiness,
            ItensPedidoBusiness itensPedidoBusiness,
            ProdutoBusiness produtoBusiness,
            PlanoPagamentoBusiness planoPagamentoBusiness,
            VendedorBusiness vendedorBusiness,
            ImagemProdutoDatasource imagemProdutoDatasource,
            ClientesBusiness clientesBusiness,
            boolean menuVisualizar,
            long pedidoCodigo,
            int fragmentContainer)
    {
        CriarPedidoFragment instancia = new CriarPedidoFragment();
        instancia._pedidoActivity = pedidoActivity;
        instancia._pedidoBusiness = pedidoBusiness;
        instancia._itensPedidoBusiness = itensPedidoBusiness;
        instancia._produtoBusiness = produtoBusiness;
        instancia._vendedorBusiness = vendedorBusiness;
        instancia._planoPagamentoBusiness = planoPagamentoBusiness;
        instancia._imagemProdutoDataSource = imagemProdutoDatasource;
        instancia._clientesBusiness = clientesBusiness;
        instancia.menuVisualizar = menuVisualizar;
        instancia.pedidoCodigo = pedidoCodigo;
        instancia.setFragmentContainer(fragmentContainer);
        return instancia;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewCriar = inflater.inflate(R.layout.pedido_criar_fragment, container, false);

        InformacoesGeraisPedidoFragment infoGeraisFragment =
                InformacoesGeraisPedidoFragment.novaInstancia(
                        _pedidoActivity,
                        _pedidoBusiness,
                        _itensPedidoBusiness,
                        _produtoBusiness,
                        _planoPagamentoBusiness,
                        _vendedorBusiness,
                        _imagemProdutoDataSource,
                        _clientesBusiness,
                        this.menuVisualizar,
                        getFragmentContainer());
        this._infoGeraisFragment = infoGeraisFragment;
        ItensPedidoFragment itensFragment = ItensPedidoFragment.novaInstancia(
                _pedidoActivity,
                _pedidoBusiness,
                _produtoBusiness,
                _itensPedidoBusiness,
                _imagemProdutoDataSource,
                this,
                getFragmentContainer(),
                this.menuVisualizar);
        this._itensPedidoFragment = itensFragment;

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(
                R.id.pedido_info_gerais_fragment_container, //Não usa o container geral pois é um filho do container geral
                infoGeraisFragment,
                "INFORMACOES_GERAIS_PEDIDO_FRAGMENT");
        transaction.add(
                R.id.pedido_itens_fragment_container, //Não usa o container geral pois é um filho do container geral
                itensFragment,
                "ITENS_PEDIDO_FRAGMENT");
        transaction.commit();

        this.setHasOptionsMenu(true);

        return viewCriar;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if(this.menuVisualizar){
            inflater.inflate(R.menu.pedido_editar_menu, menu);
        } else if(this.salvarEditar){
            inflater.inflate(R.menu.pedido_editar_salvar_menu, menu);
        } else {
            inflater.inflate(R.menu.pedido_criar_menu, menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.pedido_salvar_button){
            boolean salvar = true;

            View infoGeraisView = this._infoGeraisFragment.getView();

            EditText txtCliente = (EditText) infoGeraisView.findViewById(R.id.pedido_txtCliente);
            String cliente = String.valueOf(txtCliente.getText());
            Pedido pedido = this._itensPedidoBusiness.getPedidoMemoria();

            if(cliente.isEmpty()){
                //TODO: trocar para R.string
                txtCliente.setError("Campo Obrigatório");
                salvar = false;
            } else {
                long clienteCodigo = 0;
                if(cliente.contains("-")){
                    clienteCodigo = Integer.parseInt(cliente.split("-")[0].trim());
                } else if(this.tryParse(cliente)) {
                    clienteCodigo = Integer.parseInt(cliente);
                } else {
                    txtCliente.setError("Cliente Inválido");
                    salvar = false;
                }

                pedido.Cliente = clienteCodigo;
            }

            Spinner vendedor = (Spinner) infoGeraisView.findViewById(R.id.pedido_spnVendedor);
            String vendedorSpinner = String.valueOf(vendedor.getSelectedItem());
            if(!vendedorSpinner.isEmpty())
                pedido.Vendedor = Integer.parseInt(vendedorSpinner.split("-")[0].trim());

            Spinner plano = (Spinner) infoGeraisView.findViewById(R.id.pedido_spnPlanoPagamento);
            String planoPagamentoSpinner = String.valueOf(plano.getSelectedItem());
            if(!planoPagamentoSpinner.isEmpty())
                pedido.PlanoPagamento = Integer.parseInt(planoPagamentoSpinner.split("-")[0].trim());

            EditText observacao = (EditText) infoGeraisView.findViewById(R.id.pedido_txtObservacao);
            pedido.Observacao = String.valueOf(observacao.getText());

            if(_itensPedidoBusiness.ObterLista().isEmpty()){
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                        "Pedido precisa possuir ao menos 1 produto.", Toast.LENGTH_LONG);
                toast.show();

                salvar = false;
            }

            if(salvar) {
                this._pedidoBusiness.Incluir(pedido);
                this._itensPedidoBusiness.DisposeMemoria();
                this.ChamarListagem(getActivity().getSupportFragmentManager());
            }
        } else if(id == R.id.pedido_editar_button) {
            this.menuVisualizar = false;
            this.salvarEditar = true;
            this.AdicionarRemoverBloqueio(true);
            this.AdicionaRemoveBloqueioCard(true);
            this.getActivity().invalidateOptionsMenu();
        } else if(id == R.id.pedido_deletar_button){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this.getActivity());
            dialog.setMessage(R.string.confirmar_deletar_cliente);
            dialog.setCancelable(true);
            dialog.setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    _pedidoBusiness.Excluir(_pedidoBusiness.ObterUnico(pedidoCodigo));
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                }
            });
            dialog.setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = dialog.create();
            alert.show();
        } else if(id == R.id.pedido_editar_salvar_button){
            boolean salvar = true;

            View infoGeraisView = this._infoGeraisFragment.getView();
            Pedido pedido = this._itensPedidoBusiness.getPedidoMemoria();

            EditText txtCliente = (EditText) infoGeraisView.findViewById(R.id.pedido_txtCliente);
            String cliente = String.valueOf(txtCliente.getText());
            if(cliente.isEmpty()){
                //TODO: trocar para R.string
                txtCliente.setError("Campo Obrigatório");
                salvar = false;
            } else {
                long clienteCodigo = 0;
                if(cliente.contains("-")){
                    clienteCodigo = Integer.parseInt(cliente.split("-")[0].trim());
                } else if(this.tryParse(cliente)) {
                    clienteCodigo = Integer.parseInt(cliente);
                } else {
                    txtCliente.setError("Cliente Inválido");
                    salvar = false;
                }

                pedido.Cliente = clienteCodigo;
            }

            Spinner vendedor = (Spinner) infoGeraisView.findViewById(R.id.pedido_spnVendedor);
            String vendedorSpinner = String.valueOf(vendedor.getSelectedItem());
            if(!vendedorSpinner.isEmpty())
                pedido.Vendedor = Integer.parseInt(vendedorSpinner.split("-")[0].trim());

            Spinner plano = (Spinner) infoGeraisView.findViewById(R.id.pedido_spnPlanoPagamento);
            String planoPagamentoSpinner = String.valueOf(plano.getSelectedItem());
            if(!planoPagamentoSpinner.isEmpty())
                pedido.PlanoPagamento = Integer.parseInt(planoPagamentoSpinner.split("-")[0].trim());

            EditText observacao = (EditText) infoGeraisView.findViewById(R.id.pedido_txtObservacao);
            pedido.Observacao = String.valueOf(observacao.getText());

            if(_itensPedidoBusiness.ObterLista().isEmpty()){
                Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                        "Pedido precisa possuir ao menos 1 produto.", Toast.LENGTH_LONG);
                toast.show();

                salvar = false;
            }

            if(salvar) {
                this._pedidoBusiness.Editar(pedido, this._itensPedidoBusiness.getItensExcluir());
                this._itensPedidoBusiness.DisposeMemoria();
                this.ChamarListagem(getActivity().getSupportFragmentManager());
            }
        }

        return true;
    }

    private boolean tryParse(String valor){
        try {
            Integer.parseInt(valor);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void AdicionaRemoveBloqueioCard(boolean removerBloqueio){
        View viewPedidos = this._itensPedidoFragment.getView();

        if(removerBloqueio){
            ListagemItensPedidoCard _listagemItensPedidoCard = new ListagemItensPedidoCard(
                    this.getActivity().getBaseContext(),
                    this._produtoBusiness,
                    this._itensPedidoBusiness,
                    false,
                    this._imagemProdutoDataSource,
                    this._pedidoActivity,
                    this);
            _listagemItensPedidoCard.init();
            CardViewNative cardView = (CardViewNative) viewPedidos.findViewById(R.id.pedido_itens);
            cardView.replaceCard(_listagemItensPedidoCard);
        } else {

            ListagemItensPedidoCard _listagemItensPedidoCard = new ListagemItensPedidoCard(
                    this.getActivity().getBaseContext(),
                    this._produtoBusiness,
                    this._itensPedidoBusiness,
                    true,
                    this._imagemProdutoDataSource,
                    this._pedidoActivity,
                    this);
            _listagemItensPedidoCard.init();
            CardViewNative cardView = (CardViewNative) viewPedidos.findViewById(R.id.pedido_itens);
            cardView.replaceCard(_listagemItensPedidoCard);
        }
    }

    private void AdicionarRemoverBloqueio(boolean removerBloqueio)
    {
        View viewInfo = this._infoGeraisFragment.getView();

        EditText cliente = (EditText) viewInfo.findViewById(R.id.pedido_txtCliente);
        EditText observacao = (EditText) viewInfo.findViewById(R.id.pedido_txtObservacao);
        Spinner vendedor = (Spinner) viewInfo.findViewById(R.id.pedido_spnVendedor);
        Spinner plano = (Spinner) viewInfo.findViewById(R.id.pedido_spnPlanoPagamento);

        if(removerBloqueio){
            cliente.setEnabled(true);
            observacao.setEnabled(true);
            vendedor.setEnabled(true);
            plano.setEnabled(true);

        } else {
            cliente.setEnabled(false);
            observacao.setEnabled(false);
            vendedor.setEnabled(false);
            plano.setEnabled(false);
        }
    }

    private void ChamarListagem(FragmentManager suportFragment){
        FragmentTransaction transaction = suportFragment.beginTransaction();
        transaction.replace(
                R.id.pedido_fragment_container,
                ListagemPedidoFragment.novaInstancia(
                        _pedidoActivity,
                        _pedidoBusiness,
                        _itensPedidoBusiness,
                        _produtoBusiness,
                        _vendedorBusiness,
                        _planoPagamentoBusiness,
                        _imagemProdutoDataSource,
                        _clientesBusiness,
                        R.id.pedido_fragment_container),
                "LISTAGEM_PEDIDO_FRAGMENT");
        transaction.setCustomAnimations(R.anim.zoom_in, R.anim.zoom_out);
        transaction.addToBackStack(null);
        transaction.commit();
       ;
    }
}
