package br.com.softlinesistemas.catalogodigital.Activity.Pedido;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Activity.MoneyTextWatcher;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PlanoPagamentoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.VendedorBusiness;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.VendedorPlanoPadraoDataSource;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.POJO.PlanoPagamentoSpinner;
import br.com.softlinesistemas.catalogodigital.Model.POJO.VendedorSpinner;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;
import br.com.softlinesistemas.catalogodigital.Model.PlanoPagamento;
import br.com.softlinesistemas.catalogodigital.Model.Vendedor;
import br.com.softlinesistemas.catalogodigital.Model.VendedorPlanoPadrao;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 26/07/2016.
 */

public class InformacoesGeraisPedidoFragment extends BaseV4Fragment{
    private boolean _editar;
    private PedidoBusiness _pedidoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private ProdutoBusiness _produtoBusiness;
    private PlanoPagamentoBusiness _planoPagamentoBusiness;
    private VendedorBusiness _vendedorBusiness;
    private ClientesBusiness _clientesBusiness;

    private ImagemProdutoDatasource _imagemProdutoDataSource;

    private AutoCompleteTextView mCliente;
    private EditText mValorTotalProdutos,
                     mValorTotalDesconto,
                     mValorTotalAcrescimo,
                     mValorTotalPedido,
                     mObservacao;

    private Spinner mVendedor,
                    mPlanoPagamento;

    private IPedidoActivity _PedidoActivity;
    private ListagemItensPedidoCard _listagemItensPedidoCard;



    public static InformacoesGeraisPedidoFragment novaInstancia(
            IPedidoActivity pedidoActivity,
            PedidoBusiness pedidoBusiness,
            ItensPedidoBusiness itensPedidoBusiness,
            ProdutoBusiness produtoBusiness,
            PlanoPagamentoBusiness planoPagamentoBusiness,
            VendedorBusiness vendedorBusiness,
            ImagemProdutoDatasource imagemProdutoDatasource,
            ClientesBusiness clientesBusiness,
            boolean editar,
            int fragmentContainer)
    {
        InformacoesGeraisPedidoFragment instancia = new InformacoesGeraisPedidoFragment();
        instancia._PedidoActivity = pedidoActivity;
        instancia._pedidoBusiness = pedidoBusiness;
        instancia._itensPedidoBusiness = itensPedidoBusiness;
        instancia._produtoBusiness = produtoBusiness;
        instancia._planoPagamentoBusiness = planoPagamentoBusiness;
        instancia._vendedorBusiness = vendedorBusiness;
        instancia._imagemProdutoDataSource = imagemProdutoDatasource;
        instancia._clientesBusiness = clientesBusiness;
        instancia._editar = editar;
        instancia.setFragmentContainer(fragmentContainer);
        return instancia;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View listagemView = inflater.inflate(R.layout.pedido_informacoes_gerais_fragment, container, false);
        this.setView(listagemView, _itensPedidoBusiness.getPedidoMemoria());
        if(_editar) AdicionarBloqueio(listagemView);
        return listagemView;
    }

    private void setView(View view, Pedido pedido)
    {
        this.mCliente = (AutoCompleteTextView) view.findViewById(R.id.pedido_txtCliente);
        //String[] clientes = this._clientesBusiness.ObterClientesAutoComplete();

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                this.getContext(),
                android.R.layout.simple_list_item_1,
                null,
                new String[]{ DatabaseContract.Clientes.Coluna_Nome },
                new int[]{ android.R.id.text1 }, 0
        );

        adapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                final int colCodigo = cursor.getColumnIndexOrThrow(DatabaseContract.Clientes.Coluna_Codigo);
                final int colNome = cursor.getColumnIndexOrThrow(DatabaseContract.Clientes.Coluna_Nome);

                CharSequence retorno = String.valueOf(cursor.getInt(colCodigo)) + " - " + cursor.getString(colNome);

                if(cursor.isAfterLast()) {
                    cursor.close();
                    _clientesBusiness.ClosePool();
                }

                return  retorno;
            }
        });

        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence constraint) {
                    return (constraint != null) ? _clientesBusiness.ObterClientesAutoComplete(constraint.toString()) : null;
            }
        });


        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_list_item_1, clientes);
        this.mCliente.setAdapter(adapter);


        this.mCliente.setOnFocusChangeListener(this.AtualizaPedido());
        this.mCliente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager inputManager = (InputMethodManager) view.getContext()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);

                View v = ((Activity) view.getContext()).getCurrentFocus();

                inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });
        this.mCliente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                InputMethodManager mgr = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if(pedido.Cliente != 0) {
            Clientes cliente = _clientesBusiness.ObterCliente(pedido.Cliente);
            this.mCliente.setText(cliente.Codigo + " - " + cliente.Nome);
        }

        this.mValorTotalProdutos = (EditText) view.findViewById(R.id.pedido_txtValorTotalProd);
        this.mValorTotalProdutos.setEnabled(false);
        this.mValorTotalProdutos.addTextChangedListener(new MoneyTextWatcher(this.mValorTotalProdutos));
        if(pedido.ValorProduto != null) {
            this.mValorTotalProdutos.setText(String.valueOf(pedido.ValorProduto.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            this.mValorTotalProdutos.setText("0");
        }

        this.mValorTotalDesconto = (EditText) view.findViewById(R.id.pedido_txtValorTotalDesconto);
        this.mValorTotalDesconto.setEnabled(false);
        this.mValorTotalDesconto.addTextChangedListener(new MoneyTextWatcher(this.mValorTotalDesconto));
        if(pedido.ValorDesconto != null) {
            this.mValorTotalDesconto.setText(String.valueOf(pedido.ValorDesconto.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            this.mValorTotalDesconto.setText("0");
        }

        this.mValorTotalAcrescimo = (EditText) view.findViewById(R.id.pedido_txtValorTotalAcrescimo);
        this.mValorTotalAcrescimo.setEnabled(false);
        this.mValorTotalAcrescimo.addTextChangedListener(new MoneyTextWatcher(this.mValorTotalAcrescimo));
        if(pedido.ValorAcrescimo != null) {
            this.mValorTotalAcrescimo.setText(String.valueOf(pedido.ValorAcrescimo.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            this.mValorTotalAcrescimo.setText("0");
        }

        this.mValorTotalPedido = (EditText) view.findViewById(R.id.pedido_txtValorTotalPedido);
        this.mValorTotalPedido.setEnabled(false);
        this.mValorTotalPedido.addTextChangedListener(new MoneyTextWatcher(this.mValorTotalPedido));
        if(pedido.Valor != null) {
            this.mValorTotalPedido.setText(String.valueOf(pedido.Valor.setScale(2, BigDecimal.ROUND_UP)));
        } else {
            this.mValorTotalPedido.setText("0");
        }

        VendedorPlanoPadrao padrao = null;
        try {
            padrao = new VendedorPlanoPadraoDataSource(this.getContext()).Obter();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        this.mVendedor = (Spinner) view.findViewById(R.id.pedido_spnVendedor);
        ArrayList<VendedorSpinner> vendedores = _vendedorBusiness.ObtemListaSpinner();
        ArrayAdapter<VendedorSpinner> adapterVendedor = new ArrayAdapter<>(
                view.getContext(), android.R.layout.simple_spinner_item, vendedores);
        adapterVendedor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mVendedor.setAdapter(adapterVendedor);
        this.mVendedor.setOnFocusChangeListener(this.AtualizaPedido());
        if(pedido.Vendedor != 0){
            for (VendedorSpinner vendedor: vendedores) {
                if(vendedor.Codigo == pedido.Vendedor){
                    this.mVendedor.setSelection(adapterVendedor.getPosition(vendedor));
                }
            }
        } else {
            for (VendedorSpinner vendedor: vendedores) {
                if(vendedor.Codigo == padrao.CodigoVendedor){
                    this.mVendedor.setSelection(adapterVendedor.getPosition(vendedor));
                }
            }
        }

        this.mPlanoPagamento = (Spinner) view.findViewById(R.id.pedido_spnPlanoPagamento);
        ArrayList<PlanoPagamentoSpinner> planos = _planoPagamentoBusiness.ObtemListaSpinner();
        ArrayAdapter<PlanoPagamentoSpinner> adapterPlano = new ArrayAdapter<>(
                view.getContext(), android.R.layout.simple_spinner_item, planos);
        adapterPlano.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.mPlanoPagamento.setAdapter(adapterPlano);
        this.mPlanoPagamento.setOnFocusChangeListener(this.AtualizaPedido());
        if(pedido.PlanoPagamento != 0){
            for (PlanoPagamentoSpinner plano: planos) {
                if(plano.Codigo == pedido.PlanoPagamento){
                    this.mPlanoPagamento.setSelection(adapterPlano.getPosition(plano));
                }
            }
        } else {
            for (PlanoPagamentoSpinner plano: planos) {
                if(plano.Codigo == padrao.CodigoPlanoPagamento){
                    this.mPlanoPagamento.setSelection(adapterPlano.getPosition(plano));
                }
            }
        }

        this.mObservacao = (EditText) view.findViewById(R.id.pedido_txtObservacao);
        this.mObservacao.setOnFocusChangeListener(this.AtualizaPedido());
        if(pedido.Observacao != null && !pedido.Observacao.isEmpty())
            this.mObservacao.setText(pedido.Observacao);
    }

    private void AdicionarBloqueio(View viewInfo)
    {
        EditText cliente = (EditText) viewInfo.findViewById(R.id.pedido_txtCliente);
        EditText observacao = (EditText) viewInfo.findViewById(R.id.pedido_txtObservacao);
        Spinner vendedor = (Spinner) viewInfo.findViewById(R.id.pedido_spnVendedor);
        Spinner plano = (Spinner) viewInfo.findViewById(R.id.pedido_spnPlanoPagamento);

        cliente.setEnabled(false);
        observacao.setEnabled(false);
        vendedor.setEnabled(false);
        plano.setEnabled(false);
    }

    //Salva informações do pedido para futuro uso em tela
    private View.OnFocusChangeListener AtualizaPedido()
    {
        return new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!v.hasFocus())
                {
                    Pedido pedido = new Pedido();

                    String clienteAutoComplete = String.valueOf(mCliente.getText());
                    if(!clienteAutoComplete.isEmpty() && TryParse(clienteAutoComplete.split("-")[0].trim()))
                        pedido.Cliente = Integer.parseInt(clienteAutoComplete.split("-")[0].trim());

                    String vendedorSpinner = String.valueOf(mVendedor.getSelectedItem());
                    if(!vendedorSpinner.isEmpty())
                        pedido.Vendedor = Integer.parseInt(vendedorSpinner.split("-")[0].trim());

                    String planoPagamentoSpinner = String.valueOf(mPlanoPagamento.getSelectedItem());
                    if(!planoPagamentoSpinner.isEmpty())
                        pedido.PlanoPagamento = Integer.parseInt(planoPagamentoSpinner.split("-")[0].trim());

                    pedido.Observacao = String.valueOf(mObservacao.getText());

                    _itensPedidoBusiness.setPedidoMemoria(pedido);
                }
            }
        };
    }

    private boolean TryParse(String valor){
        try{
            int val = Integer.parseInt(valor);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
