package br.com.softlinesistemas.catalogodigital.Activity.Login;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.softlinesistemas.catalogodigital.Activity.ConfiguracaoSingleton;
import br.com.softlinesistemas.catalogodigital.Activity.DashboardActivity;
import br.com.softlinesistemas.catalogodigital.Business.DatabaseBusiness;
import br.com.softlinesistemas.catalogodigital.Business.LoginBusiness;
import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.ConfiguracaoDatasource;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 18/12/2015.
 */
public class LoginActivity extends AppCompatActivity implements ILoginActivity {

    private EditText txtLogin,
                     txtSenha,
                     mAuthSrv,
                     mRepoSrv;
    private Button btnLogin;
    private TextView txtSL;

    private Boolean logoPressionado;
    private ConfiguracaoDatasource dtConfiguracao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtLogin = (EditText) findViewById(R.id.txtLogin);
        txtLogin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    txtLogin.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorPrimaryBlack));
                } else {
                    txtLogin.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorPrimaryRed));
                }
            }
        });
        if(txtLogin.getText() != null)
            txtLogin.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimaryBlack));

        txtSenha = (EditText) findViewById(R.id.txtSenha);
        txtSenha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    txtSenha.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorPrimaryBlack));
                }else{
                    txtSenha.setTextColor(ContextCompat.getColor(v.getContext(), R.color.colorPrimaryRed));
                }
            }
        });
        if(txtSenha.getText() != null)
            txtSenha.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.colorPrimaryBlack));

        btnLogin = (Button) findViewById(R.id.loginButton);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtLogin = (EditText) findViewById(R.id.txtLogin);
                txtSenha = (EditText) findViewById(R.id.txtSenha);

                //Esconde o virtual keyboard se estiver aparecendo
                InputMethodManager imm = (InputMethodManager) LoginActivity.this.getSystemService(Service.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                //Checa se os campos foram preenchidos
                boolean modelIsValid = true;
                if (txtLogin.getText().toString() == null || txtLogin.getText().toString().isEmpty()) {

                    txtLogin.setError("Favor inserir um login válido.");
                    modelIsValid = false;
                }
                if (txtSenha.getText().toString() == null || txtSenha.getText().toString().isEmpty()) {
                    txtSenha.setError("Favor inserir uma senha válida.");
                    modelIsValid = false;
                }
                if (!modelIsValid)
                    return;

                //Executa o processo de login
                LoginBusiness login = new LoginBusiness(LoginActivity.this, LoginActivity.this);
                //login.HttpLogin(String.valueOf(txtLogin.getText()), String.valueOf(txtSenha.getText()));
                String servidor = ConfiguracaoSingleton.getInstancia(new ConfiguracaoDatasource(getBaseContext())).getAUTH_SERVER();
                login.HttpLogin(String.valueOf(txtLogin.getText()),
                        String.valueOf(txtSenha.getText()),
                        servidor);
            }
        });

        btnLogin.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        v.getBackground().setColorFilter(0x778C2326, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });

        this.dtConfiguracao = new ConfiguracaoDatasource(this);
        this.txtSL = (TextView) findViewById(R.id.textLoginSL);
        this.txtSL.setOnTouchListener(new View.OnTouchListener() {
            private final Handler handler = new Handler();
            private final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    if(logoPressionado)
                    {
                        final AlertDialog alertDialog;

                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();

                        View viewIP = inflater.inflate(R.layout.login_ip_servidores, null);

                        String[] conteudo = dtConfiguracao.CarregarConfiguracao();

                        mAuthSrv = (EditText) viewIP.findViewById(R.id.txtAutenticadorSrv);
                        mRepoSrv = (EditText) viewIP.findViewById(R.id.txtRepositorioSrv);

                        if(conteudo.length > 0)
                        {
                            mAuthSrv.setText(conteudo[1]);
                            mRepoSrv.setText(conteudo[0]);
                        }

                        builder.setTitle(R.string.informe_servidores);
                        builder.setView(viewIP)
                                .setPositiveButton(R.string.OK, null)
                                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        alertDialog = builder.create();

                        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                                b.setOnClickListener(setPositiveButton(alertDialog));
                            }
                        });

                        alertDialog.show();
                    }
                }
            };

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    handler.postDelayed(runnable, 5000);
                    logoPressionado = true;
                }

                if(event.getAction() == MotionEvent.ACTION_UP)
                {
                    if(logoPressionado)
                    {
                        logoPressionado = false;
                        handler.removeCallbacks(runnable);
                    }
                }
                return false;
            }
        });
    }

    private View.OnClickListener setPositiveButton(final AlertDialog alert) {
        return new View.OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                mAuthSrv = (EditText) alert.findViewById(R.id.txtAutenticadorSrv);
                mRepoSrv = (EditText) alert.findViewById(R.id.txtRepositorioSrv);

                String authSrv = String.valueOf(mAuthSrv.getText());
                String repoSrv = String.valueOf(mRepoSrv.getText());

                dtConfiguracao.EditarConfiguracao(authSrv, repoSrv);
                ConfiguracaoSingleton.resetInstancia(dtConfiguracao);

                alert.dismiss();
            }
        };
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent( event );
    }

    @Override
    public void LoginSucesso() {
        CharSequence mensagem = "Login efetuado com sucesso";
        int duracao = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(this.getApplicationContext(), mensagem, duracao);
        toast.show();

        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

    @Override
    public void LoginErro(String erro) {
        CharSequence mensagem = erro;
        int duracao = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(LoginActivity.this.getApplicationContext(), mensagem, duracao);
        toast.show();
    }
}
