package br.com.softlinesistemas.catalogodigital.Business;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.util.Pair;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import br.com.softlinesistemas.catalogodigital.Activity.Login.ILoginActivity;
import br.com.softlinesistemas.catalogodigital.Datasource.Login.LoginDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Login;
import br.com.softlinesistemas.catalogodigital.Model.POJO.RetornoLoginPOJO;
import br.com.softlinesistemas.catalogodigital.Service.LoginService;

/**
 * Created by João Pedro R. Carvalho on 17/12/2015.
 */
public class LoginBusiness implements ILoginTaskListener {
    private ILoginActivity mLoginActivity;
    private LoginDatasource dtLogin;
    private static Boolean estaLogando = null;

    public LoginBusiness(Context context){
        this.dtLogin = new LoginDatasource(context);
        this.mLoginActivity = null;

        if(estaLogando == null)
            estaLogando = false;
    }

    public LoginBusiness(ILoginActivity loginActivity, Context context){
        this.dtLogin = new LoginDatasource(context);
        this.mLoginActivity = loginActivity;
    }

    //TODO: REMOVER O IPServidor ao final do desenvolvimento
    public void HttpLogin(String Login, String Senha, String IPServidor){
        if(!estaLogando) {
            estaLogando = true;
            LoginService servicoLogin = new LoginService(this);
            servicoLogin.execute(Login, Senha, IPServidor);
        }
    }

    public Pair<Integer, String> Logout(){
        Pair<Integer, String> retorno;

        try{
            Login login = dtLogin.ObterUltimoLogin();
            if(login != null){
                login.Situacao = 0;
                if(!dtLogin.Editar(login)){
                    throw new Exception("Não foi possível efetuar o Logout. Tente novamente em alguns minutos");
                }else{
                    retorno = new Pair(0, "Logout efetuado com sucesso.");
                }
            }else {
                retorno = new Pair(1, "Não existe usuário logado.");
            }

        }catch (ParseException pe){
            //TODO: Aplicar o log aqui
            retorno = new Pair(2, pe.getMessage());

        }catch (SQLiteException sql){
            //TODO: Aplicar o log aqui
            retorno = new Pair(2, sql.getMessage());
        }catch (Exception e){
           //TODO: Aplicar o log aqui
            retorno = new Pair(2, e.getMessage());
        }

        return retorno;
    }

    public boolean VerificaToken(){
        boolean retorno = true; //false

        /*try{
            retorno = true;
           *//* Date dataExpiracao = dtLogin.ObterDataExpiracao();
            if(dataExpiracao != null)
                retorno = new Date().compareTo(dataExpiracao) <= 0;*//*
        }catch (ParseException e){
            //TODO: Implementar log.
        }catch (SQLiteException sql){
            //TODO: Implementar log.
        }*/

        return retorno;
    }

    @Override
    public void OnTaskCompleted(RetornoLoginPOJO retornoLogin) {
        try{
            estaLogando = false;

            if(retornoLogin.getRetornoHttp() == 200) {
                Login login = new Login();
                login.Token = retornoLogin.getToken();
                login.setDataLogin(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
                login.setDataExpiracao(retornoLogin.getExpires());
                login.Situacao = 1;

                dtLogin.Incluir(login);
                mLoginActivity.LoginSucesso();

                return;
            }
        }catch (ParseException e){
            //TODO: Implementar log.
           retornoLogin.setMensagem(e.getMessage());
        }catch (SQLiteException sql){
            //TODO: Implementar log.
           retornoLogin.setMensagem(sql.getMessage());
        }

        mLoginActivity.LoginErro(retornoLogin.getMessagem());
    }

    @Override
    public void onTaskError(String erros) {
        estaLogando = false;
        mLoginActivity.LoginErro(erros);
    }
}
