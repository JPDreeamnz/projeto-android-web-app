package br.com.softlinesistemas.catalogodigital.Datasource.Endereco;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Database.IDatabaseOperations;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;

/**
 * Created by João Pedro R. Carvalho on 19/03/2016.
 */
public class EnderecoDatasource extends BaseHelper implements IDatabaseOperations<Endereco> {
    public EnderecoDatasource(Context context) {
        super(context);
    }

    /**
     * Método que efetua a inserção do Objeto especificado na base de dados
     *
     * @param instancia
     * @return instância do objeto contendo o ID.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Endereco Incluir(Endereco instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.EnderecoID = database.insert(DatabaseContract.Endereco.Tabela_Nome, null, EnderecoDataHandler.toContentValues(instancia));
        this.Close(false);

        return instancia;
    }

    /**
     * Método que efetua a edição do objeto especificado na base de dados
     *
     * @param instancia
     * @return true se o objeto foi editado com sucesso no retorno de row = 1,
     * false se consulta foi corretamente executada mas não editou nenhuma row.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Editar(Endereco instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        int rows = database.update(
                DatabaseContract.Endereco.Tabela_Nome,
                EnderecoDataHandler.toContentValues(instancia),
                DatabaseContract.Endereco.Coluna_EnderecoID + " = ?",
                new String[] {String.valueOf(instancia.EnderecoID)}
        );
        this.Close(false);

        return (rows != 0);
    }

    /**
     * Método que efetua a exclusão do objeto especificado na base de dados
     *
     * @param instancia
     * @return true para objeto excluido corretamente da base de dados com retorno de row = 1,
     * false se a consulta foi corretamente executada mas não excluiu nenhuma row
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Excluir(Endereco instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        int rows = database.delete(
                DatabaseContract.Endereco.Tabela_Nome,
                DatabaseContract.Endereco.Coluna_EnderecoID + " = ?",
                new String[]{String.valueOf(instancia.EnderecoID)}
        );
        this.Close(false);

        return (rows != 0);
    }

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     *
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Endereco ObterUnico(long id) throws SQLiteException, ParseException {
        Endereco endereco = null;

        this.OpenRead();
        endereco = EnderecoDataHandler.fromCursor(
                database.query(
                        DatabaseContract.Endereco.Tabela_Nome,
                        DatabaseContract.Endereco.Tabela_TodasColunas,
                        DatabaseContract.Endereco.Coluna_EnderecoID + " = ?",
                        new String[] {String.valueOf(id)},
                        null, null, null
                )
        );
        this.Close(true);

        return endereco;
    }

    public List<Endereco> ObterTodos(long clienteCodigo)
    {
        this.OpenRead();
        List<Endereco> retorno =  EnderecoDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Endereco.Tabela_Nome,
                        DatabaseContract.Endereco.Tabela_TodasColunas,
                        DatabaseContract.Endereco.Coluna_ClienteCodigo + " = ?",
                        new String[] {String.valueOf(clienteCodigo)},
                        null, null, null
                )
        );
        this.Close(false);

        return retorno;
    }
}
