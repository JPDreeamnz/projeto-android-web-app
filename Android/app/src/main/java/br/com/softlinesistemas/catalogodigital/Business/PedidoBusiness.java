package br.com.softlinesistemas.catalogodigital.Business;

import android.content.Context;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Datasource.Pedido.PedidoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;

/**
 * Created by João Pedro R. Carvalho on 25/07/2016.
 */

public class PedidoBusiness {
    private PedidoDatasource _dtPedido;

    public PedidoBusiness(Context context)
    {
        this._dtPedido = new PedidoDatasource(context);
    }

    public boolean Incluir(Pedido pedido){
        try {
            this._dtPedido.Incluir(pedido);
            return true;
        } catch (ParseException e) {}

        return false;
    }

    public boolean Editar(Pedido pedido, List<ItensPedido> excluir){
        try {
            this._dtPedido.Editar(pedido, excluir);
            return true;
        } catch (ParseException e) {}

        return false;
    }

    public boolean Excluir(Pedido pedido){
        try {
            this._dtPedido.Excluir(pedido);
            return true;
        } catch (ParseException e) {}

        return false;
    }

    public List<Pedido> ObterTodos()
    {
        try {
            return _dtPedido.ObterTodos();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    public List<Pedido> ObterTodos(String busca)
    {
        try {
            return _dtPedido.ObterTodos(busca);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    public Pedido ObterUnico(long codigo){
        try {
            return _dtPedido.ObterUnico(codigo);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
