package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import br.com.softlinesistemas.catalogodigital.Activity.BaseActivity;
import br.com.softlinesistemas.catalogodigital.R;

public class ClientesActivity extends BaseActivity implements IClientesActivity{

    public ClientesActivity(){
        super(R.id.drawer_layout_clientes, R.layout.activity_clientes);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
        {
            this.AttachFragment(new ListagemClientesFragment(), "LISTAGEM_CLIENTES_FRAGMENT");
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = this.getFragmentManager();
        if(fragmentManager.getBackStackEntryCount() > 0){
            fragmentManager.popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    public void AttachFragment(Fragment fragment, String tag){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(
                R.id.clientes_listagem_fragment_container,
                fragment,
                tag);
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
        transaction.commit();
    }
}
