package br.com.softlinesistemas.catalogodigital.Service;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Datasource.Pedido.PedidoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Util.SimplesDataHandler;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;
import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;

/**
 * Created by João Pedro R. Carvalho on 15/09/2016.
 */

public class UploadPedidosTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    PedidoDatasource _dtPedido;
    IIntegracaoService _integracao;
    String URL;

    public UploadPedidosTask(String URL, PedidoDatasource dtPedido, IIntegracaoService integracao){
        this._dtPedido = dtPedido;
        this._integracao = integracao;
        this.URL = "http://" + URL + "api/Integracao/BulkInsertPedidos";
    }
    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        Pair<Integer, String> respota = null;
        try {
            List<Pedido> pedidos = _dtPedido.ObterTodos();
            JSONArray jsonArray = new JSONArray();
            for (int i=0; i < pedidos.size(); i++) {
                jsonArray.put(getJSONObjectPedido(pedidos.get(i)));
            }
            HttpRequest request = HttpRequest.post(this.URL)
                    .header("content-type", HttpRequest.CONTENT_TYPE_JSON)
                    .header("Authorization", "Bearer " + params[0])
                    .send(jsonArray.toString());

            respota = new Pair<>(request.code(), request.body());
        } catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, "Não foi possível enviar os pedidos.");
        } catch (ParseException e) {
            respota = new Pair<>(500, "Não foi possível enviar os pedidos.");
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        try{
            if(resposta.first == 200) {
                this._integracao.AtualizarPush("Pedidos enviados com sucesso.");
                this._dtPedido.ExcluirTodos();
            }else{
                this._integracao.AtualizarPush(resposta.first + " - Não foi possível enviar os pedidos.");
            }
        } catch (Exception e){
            this._integracao.AtualizarPush("Não foi possível enviar os pedidos.");
        }
    }

    public JSONObject getJSONObjectPedido(Pedido pedido) throws ParseException {
        JSONObject obj = new JSONObject();
        try {
            obj.put("PedidoCodigo", pedido.Pedido);
            obj.put("ClienteCodigo", pedido.Cliente);
            obj.put("VendedorCodigo", pedido.Vendedor);
            obj.put("PlanoPagamentoCodigo", pedido.PlanoPagamento);
            obj.put("DataPedido", SimplesDataHandler.ConverteData(pedido.Data));
            obj.put("ValorProduto", pedido.ValorProduto.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            obj.put("ValorDesconto", pedido.ValorDesconto.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            obj.put("ValorAcrescimo", pedido.ValorAcrescimo.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            obj.put("Valor", pedido.Valor.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
            obj.put("Observacao", pedido.Observacao);
            obj.put("DataRegistro", null);
            obj.put("StatusIntegracao", pedido.StatusIntegracao);
            obj.put("Cliente", getJSONObjectCliente(pedido.ClienteEntity));
            obj.put("Vendedor", null);
            obj.put("PlanoPagamento", null);

            JSONArray jsonArray = new JSONArray();
            for (ItensPedido item : pedido.ItensPedido) {
                jsonArray.put(getJSONObjectItemPedido(item));
            }

            obj.put("ItensPedido", jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public JSONObject getJSONObjectCliente(Clientes item) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("Codigo", item.Codigo);
            obj.put("Documento", item.Documento);
            obj.put("InscricaoEstadual", item.InscricaoEstadual);
            obj.put("LimiteCredito", item.LimiteCredito.setScale(2, BigDecimal.ROUND_HALF_UP));
            obj.put("Nome", item.Nome);
            obj.put("TipoDocumento", item.TipoDocumento);

            JSONArray jsonArrayEndereco = new JSONArray();
            for (Endereco endereco : item.Enderecos) {
                jsonArrayEndereco.put(getJSONObjectEndereco(endereco));
            }
            obj.put("Endereco", jsonArrayEndereco);

            JSONArray jsonArrayContato = new JSONArray();
            for (Contato contato : item.Contatos) {
                jsonArrayContato.put(getJSONObjectContato(contato));
            }
            obj.put("Contato", jsonArrayContato);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public JSONObject getJSONObjectEndereco(Endereco item) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("EnderecoID", item.EnderecoID);
            obj.put("Endereco", (item.Endereco == null || item.Endereco.isEmpty()) ? JSONObject.NULL : item.Endereco);
            obj.put("Bairro", (item.Bairro == null || item.Bairro.isEmpty()) ? JSONObject.NULL : item.Bairro);
            obj.put("Quantidade", item.CEP);
            obj.put("Cidade", (item.Cidade == null || item.Cidade.isEmpty()) ? JSONObject.NULL : item.Cidade);
            obj.put("ClienteCodigo", item.ClienteCodigo);
            obj.put("Complemento", (item.Complemento == null || item.Complemento.isEmpty()) ? JSONObject.NULL : item.Complemento);
            obj.put("Estado", (item.Estado == null || item.Estado.isEmpty()) ? JSONObject.NULL : item.Estado);
            obj.put("Numero", (item.Numero == null || item.Numero.isEmpty()) ? JSONObject.NULL : item.Numero);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public JSONObject getJSONObjectContato(Contato item) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("ContatoID", item.ContatoID);
            obj.put("ClienteCodigo", item.ClienteCodigo);
            obj.put("DDD", (item.DDD == null || item.DDD.isEmpty()) ? JSONObject.NULL : item.DDD);
            obj.put("Email", (item.Email == null || item.Email.isEmpty()) ? JSONObject.NULL : item.Email);
            obj.put("Telefone", (item.Telefone == null || item.Telefone.isEmpty()) ? JSONObject.NULL : item.Telefone);
            obj.put("TipoContato", item.TipoContato);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public JSONObject getJSONObjectItemPedido(ItensPedido item) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("PedidoCodigo", 0);
            obj.put("ItemCodigo", item.Item);
            obj.put("ProdutoCodigo", item.Produto);
            obj.put("Quantidade", item.Quantidade);
            obj.put("ValorUnitario", item.ValorUnitario.setScale(2, BigDecimal.ROUND_HALF_UP));
            obj.put("ValorDesconto", item.ValorDesconto.setScale(2, BigDecimal.ROUND_HALF_UP));
            obj.put("ValorTotal", item.ValorTotal.setScale(2, BigDecimal.ROUND_HALF_UP));
            obj.put("Produto", null);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }
}
