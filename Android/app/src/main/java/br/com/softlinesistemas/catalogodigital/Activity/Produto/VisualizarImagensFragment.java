package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by jcarvalho on 19/07/2016.
 */

public class VisualizarImagensFragment extends BaseV4Fragment {
    private long _produtoCodigo;
    private ImagemProdutoDatasource _dtImagem;

    public static VisualizarImagensFragment novaInstancia(long produtoCodigo, int fragmentContainer){
        VisualizarImagensFragment instancia = new VisualizarImagensFragment();

        instancia._produtoCodigo = produtoCodigo;
        instancia.setFragmentContainer(fragmentContainer);

        return instancia;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.imagem_prod_gvfragment, container, false);
        GridView grid = (GridView)view.findViewById(R.id.imagem_prod_gridView);

        this._dtImagem = new ImagemProdutoDatasource(this._produtoCodigo, this.getContext());
        String[] items = this._dtImagem.ObterImagens();

        if(items != null)
            grid.setAdapter(new ImagensProdutoAdapter(getActivity(), this._produtoCodigo, items));
        else{
            TextView vazio = (TextView) view.findViewById(R.id.imagem_prod_txtEmpty);
            vazio.setText(R.string.nao_existe_imagens_produto);
            grid.setEmptyView(vazio);
            grid.setAdapter(new ImagensProdutoAdapter(getActivity(), this._produtoCodigo, new String[]{}));
        }

        return view;
    }
}
