package br.com.softlinesistemas.catalogodigital.Model;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;

/**
 * Created by João Pedro R. Carvalho on 24/07/2016.
 */

public class Pedido {
    public long Pedido;
    public long Cliente;
    public long Vendedor;
    public long PlanoPagamento;
    public Date Data;
    public BigDecimal ValorProduto;
    public BigDecimal ValorDesconto;
    public BigDecimal ValorAcrescimo;
    public BigDecimal Valor;
    public String Observacao;
    public int StatusIntegracao;

    public List<ItensPedido> ItensPedido;

    public Vendedor VendedorEntity;
    public Clientes ClienteEntity;
    public PlanoPagamento PlanoPagamentoEntity;

    public String getData() {
        return dateToString(this.Data);
    }

    public void setData(String data) throws ParseException {
        if(this.Data == null)
            this.Data = new Date();
        this.Data = stringToDate(data);
    }

    private Date stringToDate(String data) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.parse(data);
    }

    private String dateToString(Date data){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return dateFormat.format(data);
    }
}
