package br.com.softlinesistemas.catalogodigital.Database;

/**
 * Created by João Pedro R. Carvalho on 14/12/2015.
 *
 * Contrato de Database, toda aplicação android precisa de um contrato estático.
 * Dessa forma toda tabela com suas colunas ficam centralizadas em um documento único
 * caso seja necessária alguma alteração, basta fazer aqui.
 */
public class DatabaseContract {

    /**
     *  Tabela de Login
     */
    public static abstract  class Login
    {
        public static final String Tabela_Nome = "Login";
        public static final String Coluna_LoginID = "LoginID";
        public static final String Coluna_Token = "Token";
        public static final String Coluna_DataLogin = "DataLogin";
        public static final String Coluna_DataExpiracao = "DataExpiracao";
        public static final String Coluna_Situacao = "Situacao";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_LoginID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        Coluna_Token + " TEXT NOT NULL," +
                        Coluna_DataLogin + " NUMERIC NOT NULL," +
                        Coluna_DataExpiracao + " NUMERIC NOT NULL," +
                        Coluna_Situacao + " INTEGER NOT NULL)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_LoginID,
                Coluna_Token,
                Coluna_DataLogin,
                Coluna_DataExpiracao,
                Coluna_Situacao
        };
    }

    public static abstract class Endereco
    {
        public static final String Tabela_Nome = "Endereco";
        public static final String Coluna_EnderecoID = "EnderecoID";
        public static final String Coluna_Endereco = "Endereco";
        public static final String Coluna_Numero = "Numero";
        public static final String Coluna_Complemento = "Complemento";
        public static final String Coluna_Bairro = "Bairro";
        public static final String Coluna_Cidade = "Cidade";
        public static final String Coluna_Estado = "Estado";
        public static final String Coluna_CEP = "CEP";
        public static final String Coluna_ClienteCodigo = "ClienteCodigo";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_EnderecoID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        Coluna_Endereco + " TEXT NULL," +
                        Coluna_Numero + " TEXT NULL," +
                        Coluna_Complemento + " TEXT NULL," +
                        Coluna_Bairro + " TEXT NULL," +
                        Coluna_Cidade + " TEXT NULL," +
                        Coluna_Estado + " TEXT NULL," +
                        Coluna_CEP + " INTEGER NULL, " +
                        Coluna_ClienteCodigo + " INTEGER NOT NULL, " +
                        "FOREIGN KEY(" + Coluna_ClienteCodigo + ") " +
                            "REFERENCES " + Clientes.Tabela_Nome + "(" + Clientes.Coluna_Codigo + ") ON DELETE CASCADE)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_EnderecoID,
                Coluna_Endereco,
                Coluna_Numero,
                Coluna_Complemento,
                Coluna_Bairro,
                Coluna_Cidade,
                Coluna_Estado,
                Coluna_CEP,
                Coluna_ClienteCodigo
        };
    }

    public static abstract class Contato
    {
        public static final String Tabela_Nome = "Contato";
        public static final String Coluna_ContatoID = "ContatoID";
        public static final String Coluna_DDD = "DDD";
        public static final String Coluna_Telefone = "Telefone";
        public static final String Coluna_Email = "Email";
        public static final String Coluna_TipoContato = "TipoContato";
        public static final String Coluna_ClienteCodigo = "ClienteCodigo";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_ContatoID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        Coluna_DDD + " TEXT NULL," +
                        Coluna_Telefone + " TEXT NULL," +
                        Coluna_Email + " TEXT NULL," +
                        Coluna_TipoContato + " INTEGER NULL, " +
                        Coluna_ClienteCodigo + " INTEGER NOT NULL, " +
                        "FOREIGN KEY(" + Coluna_ClienteCodigo + ") " +
                            "REFERENCES " + Clientes.Tabela_Nome + "(" + Clientes.Coluna_Codigo + ") ON DELETE CASCADE)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_ContatoID,
                Coluna_DDD,
                Coluna_Telefone,
                Coluna_Email,
                Coluna_TipoContato,
                Coluna_ClienteCodigo
        };
    }

    public static abstract class Clientes
    {
        public static final String Tabela_Nome = "Clientes";
        public static final String Coluna_Codigo = "Codigo";
        public static final String Coluna_Nome = "Nome";
        public static final String Coluna_TipoDocumento = "TipoDocumento";
        public static final String Coluna_Documento = "Documento";
        public static final String Coluna_InscricaoEstadual = "InscricaoEstadual";
        public static final String Coluna_LimiteCredito = "LimiteCredito";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Codigo + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        Coluna_Nome + " TEXT NOT NULL, " +
                        Coluna_TipoDocumento + " INTEGER NULL, " +
                        Coluna_Documento + " TEXT NULL, " +
                        Coluna_InscricaoEstadual + " TEXT NULL, " +
                        Coluna_LimiteCredito + " DECIMAL(12,2) NULL)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Codigo,
                Coluna_Nome,
                Coluna_TipoDocumento,
                Coluna_Documento,
                Coluna_InscricaoEstadual,
                Coluna_LimiteCredito
        };
    }

    public static abstract class Produto{
        public static final String Tabela_Nome = "Produto";
        public static final String Coluna_Codigo = "Codigo";
        public static final String Coluna_Nome = "Nome";
        public static final String Coluna_Unidade = "Unidade";
        public static final String Coluna_CodigoBarras = "CodigoBarras";
        public static final String Coluna_CodigoFabrica = "CodigoFabrica";
        public static final String Coluna_CodigoOriginal = "CodigoOriginal";
        public static final String Coluna_Cor = "Cor";
        public static final String Coluna_Tamanho = "Tamanho";
        public static final String Coluna_Marca = "Marca";
        public static final String Coluna_ValorVenda = "ValorVenda";
        public static final String Coluna_QuantidadeEstoque = "QuantidadeEstoque";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Codigo + " INTEGER PRIMARY KEY, " +
                        Coluna_Nome + " TEXT NOT NULL, " +
                        Coluna_Unidade + " TEXT NOT NULL, " +
                        Coluna_CodigoBarras + " TEXT NULL, " +
                        Coluna_CodigoFabrica + " TEXT NULL, " +
                        Coluna_CodigoOriginal + " TEXT NULL, " +
                        Coluna_Cor + " TEXT NULL, " +
                        Coluna_Tamanho + " TEXT NULL, " +
                        Coluna_Marca + " TEXT NULL, " +
                        Coluna_ValorVenda + " DECIMAL(12,2) NOT NULL, " +
                        Coluna_QuantidadeEstoque + " DECIMAL(12,3) NULL)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Codigo,
                Coluna_Nome,
                Coluna_Unidade,
                Coluna_CodigoBarras,
                Coluna_CodigoFabrica,
                Coluna_CodigoOriginal,
                Coluna_Cor,
                Coluna_Tamanho,
                Coluna_Marca,
                Coluna_ValorVenda,
                Coluna_QuantidadeEstoque
        };
    }

    public static abstract class Pedido{
        public static final String Tabela_Nome = "Pedido";
        public static final String Coluna_Pedido = "Pedido";
        public static final String Coluna_Cliente = "Cliente";
        public static final String Coluna_Vendedor = "Vendedor";
        public static final String Coluna_PlanoPagamento = "PlanoPagamento";
        public static final String Coluna_Data = "Data";
        public static final String Coluna_ValorProduto = "ValorProduto";
        public static final String Coluna_ValorDesconto = "ValorDesconto";
        public static final String Coluna_ValorAcrescimo = "ValorAcrescimo";
        public static final String Coluna_Valor = "Valor";
        public static final String Coluna_Observacao = "Observacao";
        public static final String Coluna_StatusIntegracao = "StatusIntegracao";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Pedido + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        Coluna_Cliente + " INTEGER NOT NULL, " +
                        Coluna_Vendedor + " INTEGER NOT NULL, " +
                        Coluna_PlanoPagamento + " INTEGER NOT NULL, " +
                        Coluna_Data + " NUMERIC NOT NULL, " +
                        Coluna_ValorProduto + " DECIMAL(12,2) NOT NULL, " +
                        Coluna_ValorDesconto + " DECIMAL(12,2) NOT NULL, " +
                        Coluna_ValorAcrescimo + " DECIMAL(12,2) NOT NULL, " +
                        Coluna_Valor + " DECIMAL(12,2) NOT NULL, " +
                        Coluna_Observacao + " TEXT NOT NULL, " +
                        Coluna_StatusIntegracao + " INTEGER NULL, " +
                        "FOREIGN KEY(" + Coluna_Cliente + ") " +
                        "REFERENCES " + Clientes.Tabela_Nome + "(" + Clientes.Coluna_Codigo + ") ON DELETE CASCADE, " +
                        "FOREIGN KEY(" + Coluna_Vendedor + ") " +
                        "REFERENCES " + Vendedor.Tabela_Nome + "(" + Vendedor.Coluna_Codigo + ") ON DELETE CASCADE, " +
                        "FOREIGN KEY(" + Coluna_PlanoPagamento + ") " +
                        "REFERENCES " + PlanoPagamento.Tabela_Nome + "(" + PlanoPagamento.Coluna_Codigo + ") ON DELETE CASCADE)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Pedido,
                Coluna_Cliente,
                Coluna_Vendedor,
                Coluna_PlanoPagamento,
                Coluna_Data,
                Coluna_ValorProduto,
                Coluna_ValorDesconto,
                Coluna_ValorAcrescimo,
                Coluna_Valor,
                Coluna_Observacao,
                Coluna_StatusIntegracao
        };
    }

    public static abstract class ItensPedido{
        public static final String Tabela_Nome = "ItensPedido";
        public static final String Coluna_Pedido = "Pedido";
        public static final String Coluna_Item = "Item";
        public static final String Coluna_Produto = "Produto";
        public static final String Coluna_Quantidade = "Quantidade";
        public static final String Coluna_ValorUnitario = "ValorUnitario";
        public static final String Coluna_ValorDesconto = "ValorDesconto";
        public static final String Coluna_ValorTotal = "ValorTotal";


        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Pedido + " INTEGER, " +
                        Coluna_Item + " INTEGER, " +
                        Coluna_Produto + " INTEGER NOT NULL, " +
                        Coluna_Quantidade + " DECIMAL(12,3) NOT NULL, " +
                        Coluna_ValorUnitario + " DECIMAL(12,2) NOT NULL, " +
                        Coluna_ValorDesconto + " DECIMAL(12,2) NOT NULL, " +
                        Coluna_ValorTotal + " DECIMAL(12,2) NOT NULL, " +
                        "PRIMARY KEY (" + Coluna_Pedido + ", " + Coluna_Item + "), " +
                        "FOREIGN KEY(" + Coluna_Pedido + ") " +
                        "REFERENCES " + Pedido.Tabela_Nome + "(" + Pedido.Coluna_Pedido + ") ON DELETE CASCADE)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Pedido,
                Coluna_Item,
                Coluna_Produto,
                Coluna_Quantidade,
                Coluna_ValorUnitario,
                Coluna_ValorDesconto,
                Coluna_ValorTotal
        };
    }

    public static abstract class SincroniaTabelas{
        public static final String Tabela_Nome = "SincroniaTabelas";
        public static final String Coluna_Codigo = "Codigo";
        public static final String Coluna_Data = "Data";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Codigo + " INTEGER PRIMARY KEY, " +
                        Coluna_Data + " NUMERIC NOT NULL)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Codigo,
                Coluna_Data
        };
    }

    public static abstract class Vendedor{
        public static final String Tabela_Nome = "Vendedor";
        public static final String Coluna_Codigo = "Codigo";
        public static final String Coluna_Nome = "Nome";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Codigo + " INTEGER PRIMARY KEY, " +
                        Coluna_Nome + " TEXT NOT NULL)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Codigo,
                Coluna_Nome
        };
    }

    public static abstract class PlanoPagamento{
        public static final String Tabela_Nome = "PlanoPagamento";
        public static final String Coluna_Codigo = "Codigo";
        public static final String Coluna_Nome = "Cliente";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Codigo + " INTEGER PRIMARY KEY, " +
                        Coluna_Nome + " TEXT NOT NULL)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Codigo,
                Coluna_Nome
        };
    }

    public static abstract class VendedorPlanoPadrao{
        public static final String Tabela_Nome = "VendedorPlanoPadrao";
        public static final String Coluna_Codigo = "Codigo";
        public static final String Coluna_CodigoVendedor = "CodigoVendedor";
        public static final String Coluna_CodigoPlanoPagamento = "CodigoPlanoPagamento";

        public static final String CriarTabela =
                "CREATE TABLE " + Tabela_Nome + "(" +
                        Coluna_Codigo + " INTEGER PRIMARY KEY, " +
                        Coluna_CodigoVendedor + " INTEGER NOT NULL, " +
                        Coluna_CodigoPlanoPagamento + " INTEGER NOT NULL)";

        public static final String[] Tabela_TodasColunas = new String[]{
                Coluna_Codigo, Coluna_CodigoVendedor, Coluna_CodigoPlanoPagamento
        };
    }
}
