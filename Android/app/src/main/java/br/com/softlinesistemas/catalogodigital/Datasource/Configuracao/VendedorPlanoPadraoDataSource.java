package br.com.softlinesistemas.catalogodigital.Datasource.Configuracao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.VendedorPlanoPadrao;

/**
 * Created by João Pedro R. Carvalho on 01/03/2017.
 */

public class VendedorPlanoPadraoDataSource extends BaseHelper {
    /**
     * BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     * do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     *
     * @param context
     */
    public VendedorPlanoPadraoDataSource(Context context) {
        super(context);
    }

    public boolean ExistePadrao() throws SQLiteException
    {
        this.OpenRead();
        Cursor cursor = database.query(
                DatabaseContract.VendedorPlanoPadrao.Tabela_Nome,
                DatabaseContract.VendedorPlanoPadrao.Tabela_TodasColunas,
                DatabaseContract.VendedorPlanoPadrao.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(1)},
                null, null, null
        );

        boolean retorno = cursor.moveToFirst();
        cursor.close();
        this.Close(false);

        return retorno;
    }

    public boolean Editar(VendedorPlanoPadrao instancia) throws SQLiteException, ParseException
    {
        if(!ExistePadrao())
            instancia = CriarInstancia();

        this.OpenRead();
        int rows = database.update(
                DatabaseContract.VendedorPlanoPadrao.Tabela_Nome,
                VendedorPlanoPadraoDataHandler.toContentValues(instancia),
                DatabaseContract.VendedorPlanoPadrao.Coluna_Codigo + " = ?",
                new String[] {String.valueOf(1)}
        );
        this.Close(true);

        return (rows != 0);
    }

    public VendedorPlanoPadrao Obter() throws SQLiteException, ParseException
    {
        if(!ExistePadrao())
        {
            CriarInstancia();
        }

        this.OpenRead();
        VendedorPlanoPadrao retorno = VendedorPlanoPadraoDataHandler.fromCursor(
                database.query(
                        DatabaseContract.VendedorPlanoPadrao.Tabela_Nome,
                        DatabaseContract.VendedorPlanoPadrao.Tabela_TodasColunas,
                        DatabaseContract.VendedorPlanoPadrao.Coluna_Codigo + " = ?",
                        new String[] {String.valueOf(1)},
                        null, null, null
                ));
        this.Close(false);

        return retorno;
    }

    private VendedorPlanoPadrao CriarInstancia() throws SQLiteException, ParseException
    {
        VendedorPlanoPadrao obj = new VendedorPlanoPadrao();

        obj.Codigo = 1;
        obj.CodigoVendedor = 1;
        obj.CodigoPlanoPagamento = 1;

        this.OpenWrite();
        database.insert(
                DatabaseContract.VendedorPlanoPadrao.Tabela_Nome,
                null,
                VendedorPlanoPadraoDataHandler.toContentValues(obj)
        );
        this.Close(false);

        return obj;
    }
}
