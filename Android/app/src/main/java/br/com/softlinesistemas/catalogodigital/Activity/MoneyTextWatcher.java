package br.com.softlinesistemas.catalogodigital.Activity;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Classe retirada do stackoverflow
 * Créditos para ToddH
 *
 * Obs: Ela pode ser alterada conforme o uso, mas a base foi retirada do link abaixo
 * http://stackoverflow.com/questions/5107901/better-way-to-format-currency-input-edittext
 */

public class MoneyTextWatcher implements TextWatcher {
    private final WeakReference<EditText> editTextWeakReference;

    public MoneyTextWatcher(EditText editText) {
        editTextWeakReference = new WeakReference<EditText>(editText);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        EditText editText = editTextWeakReference.get();
        if (editText == null) return;
        String s = editable.toString();
        editText.removeTextChangedListener(this);
        String cleanString = s.toString().replaceAll("[R$.,]", "");
        if(cleanString.length() > 12)
            cleanString = cleanString.substring(0, (cleanString.length() - 1));

        if(!cleanString.isEmpty()) {
            BigDecimal parsed = new BigDecimal(cleanString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
            String formatted = NumberFormat.getCurrencyInstance().format(parsed);
            editText.setText(formatted);
            editText.setSelection(formatted.length());
        }
        editText.addTextChangedListener(this);
    }

    public static float unmaskMoney(String valor){
        if(valor.isEmpty())
            return 0;

        String cleanString = valor.toString().replaceAll("[R$.]", "").replaceAll("[,]", ".");
        return new Float(cleanString);
    }

    public static BigDecimal unmaskMoneyBigDecimal(String valor){
        if(valor.isEmpty())
            return BigDecimal.ZERO;

        String cleanString = valor.toString().replaceAll("[R$.]", "").replaceAll("[,]", ".");
        return new BigDecimal(cleanString);
    }

    public static double unmaskMoneyDouble(String valor){
        if(valor.isEmpty())
            return 0;

        String cleanString = valor.toString().replaceAll("[R$.]", "").replaceAll("[,]", ".");
        return new Double(cleanString);
    }
}
