package br.com.softlinesistemas.catalogodigital.Datasource.Contato;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Database.IDatabaseOperations;
import br.com.softlinesistemas.catalogodigital.Model.Contato;

/**
 * Created by João Pedro R. Carvalho on 17/03/2016.
 */
public class ContatoDatasource extends BaseHelper implements IDatabaseOperations<Contato> {
    public ContatoDatasource(Context context) {
        super(context);
    }

    /**
     * Método que efetua a inserção do Objeto especificado na base de dados
     *
     * @param instancia
     * @return instância do objeto contendo o ID.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Contato Incluir(Contato instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.ContatoID = database.insert(DatabaseContract.Contato.Tabela_Nome, null, ContatoDataHandler.toContentValue(instancia));
        this.Close(false);

        return instancia;
    }

    /**
     * Método que efetua a edição do objeto especificado na base de dados
     *
     * @param instancia
     * @return true se o objeto foi editado com sucesso no retorno de row = 1,
     * false se consulta foi corretamente executada mas não editou nenhuma row.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Editar(Contato instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        int rows = database.update(
                DatabaseContract.Contato.Tabela_Nome,
                ContatoDataHandler.toContentValue(instancia),
                DatabaseContract.Contato.Coluna_ContatoID + "= ?",
                new String[]{String.valueOf(instancia.ContatoID)}
        );
        this.Close(false);

        return (rows != 0);
    }

    /**
     * Método que efetua a exclusão do objeto especificado na base de dados
     *
     * @param instancia
     * @return true para objeto excluido corretamente da base de dados com retorno de row = 1,
     * false se a consulta foi corretamente executada mas não excluiu nenhuma row
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Excluir(Contato instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        int rows = database.delete(
                DatabaseContract.Contato.Tabela_Nome,
                DatabaseContract.Contato.Coluna_ContatoID + "= ?",
                new String[]{String.valueOf(instancia.ContatoID)}
        );
        this.Close(false);

        return (rows != 0);
    }

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     *
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Contato ObterUnico(long id) throws SQLiteException, ParseException {
        Contato contato = null;

        this.OpenRead();
        contato = ContatoDataHandler.fromCursor(
                database.query(
                        DatabaseContract.Contato.Tabela_Nome,
                        DatabaseContract.Contato.Tabela_TodasColunas,
                        DatabaseContract.Contato.Coluna_ContatoID + " = ?",
                        new String[] {String.valueOf(id)},
                        null, null, null
                )
        );
        this.Close(true);

        return contato;
    }

    public List<Contato> ObterTodos(long clienteCodigo)
    {
        this.OpenRead();
        List<Contato> retorno = ContatoDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.Contato.Tabela_Nome,
                        DatabaseContract.Contato.Tabela_TodasColunas,
                        DatabaseContract.Contato.Coluna_ClienteCodigo + " = ?",
                        new String[] {String.valueOf(clienteCodigo)},
                        null, null, null
                )
        );
        this.Close(false);

        return retorno;
    }
}
