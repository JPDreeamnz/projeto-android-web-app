package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;

/**
 * Created by jcarvalho on 19/07/2016.
 */

public class ProdutoPagerAdapter extends FragmentPagerAdapter {
    private int _numeroTabs;
    private long _produtoCodigo;
    private int _fragmentContainer;
    private ProdutoBusiness _produtoBusiness;

    public ProdutoPagerAdapter(
            FragmentManager fragmentManager,
            int numeroTabs,
            ProdutoBusiness produtoBusiness,
            long produtoCodigo,
            int fragmentContainer){
        super(fragmentManager);
        this._numeroTabs = numeroTabs;
        this._produtoBusiness = produtoBusiness;
        this._produtoCodigo = produtoCodigo;
        this._fragmentContainer = fragmentContainer;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                VisualizarProdutoFragment tabProduto = VisualizarProdutoFragment.novaInstancia(this._produtoBusiness, this._produtoCodigo, this._fragmentContainer);
                return tabProduto;
            case 1:
                VisualizarImagensFragment tabImagens = VisualizarImagensFragment.novaInstancia(this._produtoCodigo, this._fragmentContainer);
                return tabImagens;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return this._numeroTabs;
    }
}
