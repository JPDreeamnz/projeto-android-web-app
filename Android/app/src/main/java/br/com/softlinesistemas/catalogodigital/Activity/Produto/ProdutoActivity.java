package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import br.com.softlinesistemas.catalogodigital.Activity.BaseActivity;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public class ProdutoActivity extends BaseActivity implements IProdutoActivity {
    private ProdutoBusiness _produtoBusiness;

    public ProdutoActivity(){
        super(R.id.produto_drawer_layout, R.layout.produto_activity);
        this._produtoBusiness = new ProdutoBusiness(ProdutoActivity.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null)
        {
            this.AttachFragment(ListagemProdutoFragment.novaInstancia(this._produtoBusiness, null, null, R.id.produto_fragment_container, false), "LISTAGEM_PRODUTO_FRAGMENT");
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if(fragmentManager.getBackStackEntryCount() > 0){
            fragmentManager.popBackStackImmediate();
        } else {
            super.onBackPressed();
        }
    }

    public void AttachFragment(Fragment fragment, String tag){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(
                R.id.produto_fragment_container,
                fragment,
                tag);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.commit();
    }
}
