package br.com.softlinesistemas.catalogodigital.Model;

import java.math.BigDecimal;

/**
 * Created by João Pedro R. Carvalho on 24/07/2016.
 */

public class ItensPedido {
    public long Pedido;
    public long Item;
    public long Produto;
    public float Quantidade;
    public BigDecimal ValorUnitario;
    public BigDecimal ValorDesconto;
    public BigDecimal ValorTotal;
}
