package br.com.softlinesistemas.catalogodigital.Business;

import android.content.Context;

import java.util.ArrayList;

import br.com.softlinesistemas.catalogodigital.Datasource.Vendedor.VendedorDatasource;
import br.com.softlinesistemas.catalogodigital.Model.POJO.VendedorSpinner;
import br.com.softlinesistemas.catalogodigital.Model.Vendedor;

/**
 * Created by João Pedro R. Carvalho on 28/07/2016.
 */

public class VendedorBusiness {
    private VendedorDatasource _dtVendedor;

    public VendedorBusiness(Context context)
    {
        _dtVendedor = new VendedorDatasource(context);
    }

    public ArrayList<VendedorSpinner> ObtemListaSpinner()
    {
        ArrayList<VendedorSpinner> vendedorSP = new ArrayList<>();

        for (Vendedor vendedor : this._dtVendedor.ObtemLista()) {
            vendedorSP.add(new VendedorSpinner(vendedor));
        }

        return vendedorSP;
    }

    public Vendedor ObterVendedor(long codigo){
        return this._dtVendedor.ObterVendedor(codigo);
    }
}
