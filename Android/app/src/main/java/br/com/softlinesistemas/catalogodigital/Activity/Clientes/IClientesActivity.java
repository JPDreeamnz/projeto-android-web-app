package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.app.FragmentManager;

/**
 * Created by João Pedro R. Carvalho on 19/06/2016.
 */

public interface IClientesActivity {
    FragmentManager getFragmentManager();
    void onBackPressed();
}
