package br.com.softlinesistemas.catalogodigital.Model.POJO;

import br.com.softlinesistemas.catalogodigital.Model.Vendedor;

/**
 * Created by João Pedro R. Carvalho on 31/08/2016.
 */

public class VendedorSpinner extends Vendedor {

    public VendedorSpinner(Vendedor vendedor)
    {
        this.Codigo = vendedor.Codigo;
        this.Nome = vendedor.Nome;
    }

    public String toString(){
        return this.Codigo + " - " + this.Nome;
    }
}
