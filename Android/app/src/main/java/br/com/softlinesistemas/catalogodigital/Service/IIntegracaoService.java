package br.com.softlinesistemas.catalogodigital.Service;

/**
 * Created by João Pedro R. Carvalho on 12/09/2016.
 */

public interface IIntegracaoService {
    void Login(TipoIntegracaoEnum tipoIntegracao);
    void DownloadTabelas(String token);
    void UploadPedidos(String token);
    void DownloadImagens();
    void AtualizarPush(boolean sincronia, String mensagem);
    void AtualizarPush(String mensagem);
    void Dispose();
}
