package br.com.softlinesistemas.catalogodigital.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by João Pedro R. Carvalho on 17/12/2015.
 */
public enum LoginRetornoEnum {
    LOGIN_NAOCADASTRADO(-1),
    LOGIN_SUCESSO(0),
    LOGIN_INVALIDO(1),
    LOGIN_ERRO(2);

    private final int valor;
    private static final Map<Integer, LoginRetornoEnum> intToEnum = new HashMap<Integer, LoginRetornoEnum>();
    static {
        for(LoginRetornoEnum loginEnum : LoginRetornoEnum.values()){
            intToEnum.put(loginEnum.valor, loginEnum);
        }
    }

    LoginRetornoEnum(int valor) {
        this.valor = valor;
    }

    public static LoginRetornoEnum fromInt(int valor){
        LoginRetornoEnum loginEnum = intToEnum.get(Integer.valueOf(valor));
        if(loginEnum == null)
            loginEnum = LOGIN_NAOCADASTRADO;

        return loginEnum;
    }
}
