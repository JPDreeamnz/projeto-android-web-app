package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;

import java.io.File;

import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by jcarvalho on 19/07/2016.
 */

public class ImagensProdutoAdapter extends ArrayAdapter {
    private Context _context;
    private LayoutInflater _layout;
    private String[] _diretorioImagens;

    public ImagensProdutoAdapter(Context context, long codigoProduto, String[] diretorioImagens) {
        super(context, R.layout.imagem_prod_imageview, diretorioImagens);

        this._context = context;
        this._diretorioImagens = diretorioImagens;

        this._layout = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = _layout.inflate(R.layout.imagem_prod_imageview, parent, false);

        Picasso.with(this._context)
                .load(new File(this._diretorioImagens[position]))
                .fit()
                .centerInside()
                .into((ImageView) convertView.findViewById(R.id.imagem_prod_imageView));

        return convertView;
    }

    @Override
    public int getCount() {
        return this._diretorioImagens.length;
    }
}
