package br.com.softlinesistemas.catalogodigital.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by João Pedro R. Carvalho on 14/12/2015.
 */
public class BaseHelper {
    private DatabaseHelper dbHelper;
    public static SQLiteDatabase database;

    /**
     *  BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     *  do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     * @param context
     */
    public BaseHelper(Context context) {
        if(this.dbHelper == null)
            dbHelper = new DatabaseHelper(context);
    }

    /**
     * Método que abre conexão de escrita com o banco de dados.
     */
    public void OpenWrite()
    {
        if(database == null || !database.isOpen())
            database = dbHelper.getWritableDatabase();
    }

    /**
     * Método que abre conexão de leitura com o banco de dados.
     */
    public void OpenRead()
    {
        if(database == null || !database.isOpen())
            database = dbHelper.getReadableDatabase();
    }

    /**
     * Método que limpa a database, deve ser excluido ao fim do desenvolvimento
     */
    public void LimpaDatabase(){
        OpenWrite();
        dbHelper.onUpgrade(database, 0, 1);
        Close(true);
    }

    /**
     * Método que fecha a conexão com o banco de dados.
     * Sempre que utilizar algum método Open é obrigatório chamar o método close após finalizar
     * a operação.
     */
    public void Close(boolean disposeDb)
    {
        dbHelper.close();

        if(disposeDb)
            database.close();
    }
}
