package br.com.softlinesistemas.catalogodigital.Service;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONArray;

import br.com.softlinesistemas.catalogodigital.Datasource.Vendedor.VendedorDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Vendedor;

/**
 * Created by João Pedro R. Carvalho on 12/09/2016.
 */

public class VendedorTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    VendedorDatasource _dtVendedor;
    IIntegracaoService _integracao;
    String URL;

    public VendedorTask(String URL, VendedorDatasource dtVendedor, IIntegracaoService integracao){
        this._dtVendedor = dtVendedor;
        this._integracao = integracao;
        this.URL = "http://" + URL + "api/Integracao/ObterVendedor";
    }
    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        Pair<Integer, String> respota;
        try {
            HttpRequest request = HttpRequest.get(this.URL, true, "dataRegistros", params[0])
                    .accept("application/json")
                    .header("Authorization", "Bearer " + params[1]);

            respota = new Pair<>(request.code(), request.body());
        } catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, "Não foi possível obter os vendedores.");
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        try{
            if(resposta.first == 200) {
                JSONArray json = new JSONArray(resposta.second);
                for(int i = 0; i < json.length(); i++) {
                    Vendedor novoVendedor = new Vendedor();
                    novoVendedor.Codigo = json.getJSONObject(i).getLong("Codigo");
                    novoVendedor.Nome = json.getJSONObject(i).getString("Nome");

                    if(this._dtVendedor.ExisteVendedor(novoVendedor.Codigo))
                        this._dtVendedor.Editar(novoVendedor);
                    else
                        this._dtVendedor.Incluir(novoVendedor);
                }

                this._integracao.AtualizarPush(true, "Tabela vendedores atualizada com sucesso.");
            }else{
                this._integracao.AtualizarPush(true, resposta.first + " - Não foi possível atualizar os vendedores");
            }
        } catch (Exception e){
            this._integracao.AtualizarPush(true, "Não foi possível atualizar os vendedores.");
        }
    }
}
