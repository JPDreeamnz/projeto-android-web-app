package br.com.softlinesistemas.catalogodigital.Datasource.Login;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.Date;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Database.IDatabaseOperations;
import br.com.softlinesistemas.catalogodigital.Datasource.Util.SimplesDataHandler;
import br.com.softlinesistemas.catalogodigital.Model.Login;

/**
 * Created by João Pedro R. Carvalho on 14/12/2015.
 */
public class LoginDatasource extends BaseHelper implements IDatabaseOperations<Login>{
    public LoginDatasource(Context context) {
        super(context);
    }


    /**
     * Método que efetua a inserção do Login especificado na base de dados
     *
     * @param instancia
     * @return instância do Login contendo o ID.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Login Incluir(Login instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        instancia.LoginID = database.insert(DatabaseContract.Login.Tabela_Nome, null, LoginDataHandler.toContentValue(instancia));
        this.Close(true);
        return instancia;
    }

    /**
     * Método que efetua a edição do Login especificado na base de dados
     *
     * @param instancia
     * @return true se o Login foi editado com sucesso no retorno de row = 1,
     * false se consulta foi corretamente executada mas não editou nenhuma row.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Editar(Login instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        int rows = database.update(
                DatabaseContract.Login.Tabela_Nome,
                LoginDataHandler.toContentValue(instancia),
                DatabaseContract.Login.Coluna_LoginID + " = ?",
                new String[]{String.valueOf(instancia.LoginID)}
        );
        this.Close(true);
        return (rows != 0);
    }

    /**
     * Método que efetua a exclusão do objeto especificado na base de dados
     *
     * @param instancia
     * @return true para objeto excluido corretamente da base de dados com retorno de row = 1,
     * false se a consulta foi corretamente executada mas não excluiu nenhuma row
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Excluir(Login instancia) throws SQLiteException, ParseException {
        this.OpenWrite();
        int rows = database.delete(
                DatabaseContract.Login.Tabela_Nome,
                DatabaseContract.Login.Coluna_LoginID + " = ?",
                new String[]{String.valueOf(instancia.LoginID)}
        );
        this.Close(true);
        return (rows != 0);
    }

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     *
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public Login ObterUnico(long id) throws SQLiteException, ParseException {
        Login login = null;

        this.OpenRead();
        login = LoginDataHandler.fromCursor(
                database.query(
                        DatabaseContract.Login.Tabela_Nome,
                        DatabaseContract.Login.Tabela_TodasColunas,
                        DatabaseContract.Login.Coluna_LoginID + " = ?",
                        new String[] { String.valueOf(id) },
                        null, null, null
                )
        );
        this.Close(true);

        return login;
    }

    public Login ObterUltimoLogin() throws ParseException {
        Login login = null;

        this.OpenRead();
        login = LoginDataHandler.fromCursor(
                database.query(
                        DatabaseContract.Login.Tabela_Nome,
                        DatabaseContract.Login.Tabela_TodasColunas,
                        DatabaseContract.Login.Coluna_Situacao + " = ?",
                        new String[] { "1" },
                        null, null, null, "1"
                )
        );
        this.Close(true);

        return login;
    }

    public Date ObterDataExpiracao() throws SQLiteException, ParseException {
        Date data = null;

        //retornar data válida para o token do último login
        this.OpenRead();
        data = SimplesDataHandler.fromCursorToDate(
                database.query(
                        DatabaseContract.Login.Tabela_Nome,
                        new String[]{DatabaseContract.Login.Coluna_DataExpiracao},
                        DatabaseContract.Login.Coluna_Situacao + " = ?",
                        new String[]{"1"},
                        null, null, null, "1"
                ),
                DatabaseContract.Login.Coluna_DataExpiracao
        );
        this.Close(true);

        return data;
    }
}
