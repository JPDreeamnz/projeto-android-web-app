package br.com.softlinesistemas.catalogodigital.Activity;

import android.app.Fragment;

/**
 * Created by João Pedro R. Carvalho on 15/08/2016.
 */

public class BaseFragment extends Fragment {
    private static int _FragmentContainer;

    public static int getFragmentContainer() {
        return _FragmentContainer;
    }

    public static void setFragmentContainer(int fragmentContainer) {
        BaseFragment._FragmentContainer = fragmentContainer;
    }
}
