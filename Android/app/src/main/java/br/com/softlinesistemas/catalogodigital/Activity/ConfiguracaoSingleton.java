package br.com.softlinesistemas.catalogodigital.Activity;

import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.ConfiguracaoDatasource;

/**
 * Created by João Pedro R. Carvalho on 05/12/2016.
 */

public class ConfiguracaoSingleton {
    private static ConfiguracaoSingleton _instancia;

    private String AUTH_SERVER;
    private String REPOSITORY_SERVER;

    public ConfiguracaoSingleton(ConfiguracaoDatasource source)
    {
        String[] config = source.CarregarConfiguracao();
        REPOSITORY_SERVER = config[0];
        AUTH_SERVER = config[1];
    }

    public static ConfiguracaoSingleton getInstancia(ConfiguracaoDatasource source)
    {
        if(_instancia == null)
        {
            _instancia = new ConfiguracaoSingleton(source);
        }

        return _instancia;
    }

    public static ConfiguracaoSingleton resetInstancia(ConfiguracaoDatasource source)
    {
        _instancia = new ConfiguracaoSingleton(source);
        return _instancia;
    }


    public String getAUTH_SERVER() {
        return AUTH_SERVER;
    }

    public void setAUTH_SERVER(String AUTH_SERVER) {
        this.AUTH_SERVER = AUTH_SERVER;
    }

    public String getREPOSITORY_SERVER() {
        return REPOSITORY_SERVER;
    }

    public void setREPOSITORY_SERVER(String REPOSITORY_SERVER) {
        this.REPOSITORY_SERVER = REPOSITORY_SERVER;
    }
}
