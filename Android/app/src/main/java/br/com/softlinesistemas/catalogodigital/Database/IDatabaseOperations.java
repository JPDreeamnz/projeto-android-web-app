package br.com.softlinesistemas.catalogodigital.Database;

import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.List;

/**
 * Created by João Pedro R. Carvalho on 14/12/2015.
 */
public interface IDatabaseOperations<Object> {
    /**
     *  Método que efetua a inserção do Objeto especificado na base de dados
     * @param instancia
     * @return instância do objeto contendo o ID.
     * @throws SQLiteException
     * @throws ParseException
     */
    public Object Incluir(Object instancia) throws SQLiteException, ParseException;

    /**
     *  Método que efetua a edição do objeto especificado na base de dados
     * @param instancia
     * @return true se o objeto foi editado com sucesso no retorno de row = 1,
     *  false se consulta foi corretamente executada mas não editou nenhuma row.
     * @throws SQLiteException
     * @throws ParseException
     */
    public boolean Editar(Object instancia) throws SQLiteException, ParseException;

    /**
     *  Método que efetua a exclusão do objeto especificado na base de dados
     * @param instancia
     * @return true para objeto excluido corretamente da base de dados com retorno de row = 1,
     *  false se a consulta foi corretamente executada mas não excluiu nenhuma row
     * @throws SQLiteException
     * @throws ParseException
     */
    public boolean Excluir(Object instancia) throws SQLiteException, ParseException;

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    public Object ObterUnico(long id) throws SQLiteException, ParseException;
}
