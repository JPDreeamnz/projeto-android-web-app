package br.com.softlinesistemas.catalogodigital.Activity.Pedido;


import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ItensPedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PedidoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.PlanoPagamentoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Business.VendedorBusiness;
import br.com.softlinesistemas.catalogodigital.Datasource.ImagemProduto.ImagemProdutoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;
import br.com.softlinesistemas.catalogodigital.R;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardGridArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardGridView;

/**
 * Created by João Pedro R. Carvalho on 25/07/2016.
 */

public class ListagemPedidoFragment extends BaseV4Fragment implements SearchView.OnQueryTextListener{
    private IPedidoActivity _pedidoActivity;
    private PedidoBusiness _pedidoBusiness;
    private ItensPedidoBusiness _itensPedidoBusiness;
    private ProdutoBusiness _produtoBusiness;
    private VendedorBusiness _vendedorBusiness;
    private PlanoPagamentoBusiness _planoPagamentoBusiness;
    private ImagemProdutoDatasource _imagemProdutoDataSource;
    private ClientesBusiness _clientesBusiness;
    private FloatingActionButton mNovoPedido;

    public static ListagemPedidoFragment novaInstancia(
            IPedidoActivity pedidoActivity,
            PedidoBusiness pedidoBusiness,
            ItensPedidoBusiness itensPedidoBusiness,
            ProdutoBusiness produtoBusiness,
            VendedorBusiness vendedorBusiness,
            PlanoPagamentoBusiness planoPagamentoBusiness,
            ImagemProdutoDatasource imagemProdutoDatasource,
            ClientesBusiness clientesBusiness,
            int fragmentContainer)
    {
        ListagemPedidoFragment instancia = new ListagemPedidoFragment();
        instancia._pedidoActivity = pedidoActivity;
        instancia._pedidoBusiness = pedidoBusiness;
        instancia._itensPedidoBusiness = itensPedidoBusiness;
        instancia._produtoBusiness = produtoBusiness;
        instancia._vendedorBusiness = vendedorBusiness;
        instancia._planoPagamentoBusiness = planoPagamentoBusiness;
        instancia._imagemProdutoDataSource = imagemProdutoDatasource;
        instancia._clientesBusiness = clientesBusiness;
        instancia.setFragmentContainer(fragmentContainer);
        return instancia;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View listagemView = inflater.inflate(R.layout.pedido_listagem_fragment, null);

        mNovoPedido = (FloatingActionButton) listagemView.findViewById(R.id.pedido_novo_fab);
        mNovoPedido.setBackgroundTintList(
                ColorStateList.valueOf(
                        ContextCompat.getColor(this.getActivity().getBaseContext(), R.color.colorPrimaryRed)
                )
        );
        mNovoPedido.setOnClickListener(BotaoNovoPedido());

        this.setView(listagemView);
        setHasOptionsMenu(true);
        return listagemView;
    }

    private void setView(View view)
    {
        Context contexto = view.getContext();
        ArrayList<Card> cards = new ArrayList<>();

        for (Pedido pedido : this._pedidoBusiness.ObterTodos())
        {
            //Card card = new Card(contexto);
            PedidoCard card = new PedidoCard(contexto);

            pedido.VendedorEntity = this._vendedorBusiness.ObterVendedor(pedido.Vendedor);
            card.setPedido(pedido);

            CardHeader header = new CardHeader(contexto);
            header.setTitle(_clientesBusiness.ObterCliente(pedido.Cliente).Nome);
            card.addCardHeader(header);
            card.setId(String.valueOf(pedido.Pedido));
            card.setOnClickListener(this.clickPedido());

            cards.add(card);
        }

        CardGridArrayAdapter mCardArrayAdapter = new CardGridArrayAdapter(contexto,cards);

        CardGridView gridView = (CardGridView) view.findViewById(R.id.grid_lista_pedidos);
        if (gridView!=null){
            gridView.setAdapter(mCardArrayAdapter);
        }
    }

    private View.OnClickListener BotaoNovoPedido()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _itensPedidoBusiness.DisposeMemoria();
                CriarPedidoFragment fragment = CriarPedidoFragment
                        .novaInstancia(
                                _pedidoActivity,
                                _pedidoBusiness,
                                _itensPedidoBusiness,
                                _produtoBusiness,
                                _planoPagamentoBusiness,
                                _vendedorBusiness,
                                _imagemProdutoDataSource,
                                _clientesBusiness,
                                false,
                                0,
                                getFragmentContainer());

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(
                        getFragmentContainer(),
                        fragment,
                        "PEDIDO_CRIAR_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(R.anim.zoom_out, R.anim.zoom_in);
                transaction.commit();
            }
        };
    }

    private Card.OnCardClickListener clickPedido()
    {
        return new Card.OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                int pedidoCodigo = Integer.parseInt(card.getId());
                Pedido pedido = _pedidoBusiness.ObterUnico(pedidoCodigo);
                _itensPedidoBusiness.setPedidoMemoria(pedido);
                _itensPedidoBusiness.Inserir(pedido.ItensPedido);

                CriarPedidoFragment fragment = CriarPedidoFragment
                        .novaInstancia(
                                _pedidoActivity,
                                _pedidoBusiness,
                                _itensPedidoBusiness,
                                _produtoBusiness,
                                _planoPagamentoBusiness,
                                _vendedorBusiness,
                                _imagemProdutoDataSource,
                                _clientesBusiness,
                                true,
                                pedidoCodigo,
                                getFragmentContainer());

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(
                        getFragmentContainer(),
                        fragment,
                        "PEDIDO_CRIAR_FRAGMENT");
                transaction.addToBackStack(null);
                transaction.setCustomAnimations(R.anim.zoom_out, R.anim.zoom_in);
                transaction.commit();
            }
        };
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menu_filtro, menu);

        MenuItem itemFiltro = menu.findItem(R.id.filtro);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(itemFiltro);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(this);
    }

    /**
     * Called when the user submits the query. This could be due to a key press on the
     * keyboard or due to pressing a submit button.
     * The listener can override the standard behavior by returning true
     * to indicate that it has handled the submit request. Otherwise return false to
     * let the SearchView handle the submission by launching any associated intent.
     *
     * @param query the query text that is to be submitted
     * @return true if the query has been handled by the listener, false to let the
     * SearchView perform the default action.
     */
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    /**
     * Called when the query text is changed by the user.
     *
     * @param newText the new content of the query text field.
     * @return false if the SearchView should perform the default action of showing any
     * suggestions if available, true if the action was handled by the listener.
     */
    @Override
    public boolean onQueryTextChange(String newText) {
        View view = getView();

        if(view != null) {
            Context contexto = view.getContext();
            ArrayList<Card> cards = new ArrayList<>();
            List<Pedido> pedidos = newText.isEmpty() ? this._pedidoBusiness.ObterTodos() : this._pedidoBusiness.ObterTodos(newText);

            for (Pedido pedido : pedidos) {

                PedidoCard card = new PedidoCard(contexto);

                pedido.VendedorEntity = this._vendedorBusiness.ObterVendedor(pedido.Vendedor);
                card.setPedido(pedido);

                CardHeader header = new CardHeader(contexto);
                header.setTitle(_clientesBusiness.ObterCliente(pedido.Cliente).Nome);
                card.addCardHeader(header);
                card.setId(String.valueOf(pedido.Pedido));
                card.setOnClickListener(this.clickPedido());

                cards.add(card);
            }

            CardGridArrayAdapter mCardArrayAdapter = new CardGridArrayAdapter(contexto, cards);

            CardGridView gridView = (CardGridView) view.findViewById(R.id.grid_lista_pedidos);
            if (gridView != null) {
                gridView.setAdapter(mCardArrayAdapter);
            }
        }

        return true;
    }
}

class PedidoCard extends Card{

    private Pedido _pedido;
    private TextView mVendedor,
            mData,
            mQtdItens,
            mValorTotal;
    /**
     * Constructor with a base inner layout defined by R.layout.inner_base_main
     *
     * @param context context
     */
    public PedidoCard(Context context) {
        super(context, R.layout.pedido_card);
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        this.mVendedor = (TextView) view.findViewById(R.id.pedido_card_txtVendedor);
        this.mData = (TextView) view.findViewById(R.id.pedido_card_txtData);
        this.mQtdItens = (TextView) view.findViewById(R.id.pedido_card_txtQtdItens);
        this.mValorTotal = (TextView) view.findViewById(R.id.pedido_card_txtValorTotal);

        this.mVendedor.setText(this._pedido.VendedorEntity.Nome);
        this.mData.setText(this._pedido.getData());
        this.mQtdItens.setText(String.valueOf(this._pedido.ItensPedido.size()));

        this.mValorTotal.setText(NumberFormat.getCurrencyInstance().format(this._pedido.Valor));

    }

    public void setPedido(Pedido pedido) {
        this._pedido = pedido;
    }
}
