package br.com.softlinesistemas.catalogodigital.Business;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Datasource.Cliente.ClientesDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Contato.ContatoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Endereco.EnderecoDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.Contato;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;
import br.com.softlinesistemas.catalogodigital.Model.Enum.Operacoes;

/**
 * Created by João Pedro R. Carvalho on 20/03/2016.
 */
public class ClientesBusiness {
    private ClientesDatasource _dtClientes;

    private long _enderecoIndex; //Mantem o ultimo index gerado para endereços criados em tempo de execução.
    private List<Endereco> _enderecosNovos; //Mantem os end criados em tempo de execução.
    private List<Endereco> _enderecosMemoria; //Mantem os end que estão no banco de dados.
    private List<Endereco> _enderecosExcluir; //Mantem os end excluidos.

    private long _contatoIndex; //Mantem o ultimo index gerado para contatos criados em tempo de execução.
    private List<Contato> _contatosNovos; //Mantem os cttos criados em tempo de execução.
    private List<Contato> _contatosMemoria; //Mantem os cttos que estão no banco de dados.
    private List<Contato> _contatosExcluir; //Mantem os cttos excluidos.

    public ClientesBusiness(Context contexto, long clienteCodigo)
    {
        this._dtClientes = new ClientesDatasource(
                contexto,
                new EnderecoDatasource(contexto),
                new ContatoDatasource(contexto)
        );

        this._enderecosNovos = new ArrayList<>();
        this._enderecosMemoria =  new ArrayList<>();
        this._enderecosExcluir = new ArrayList<>();

        this._contatosNovos = new ArrayList<>();
        this._contatosMemoria = new ArrayList<>();
        this._contatosExcluir = new ArrayList<>();

        for (Endereco endereco : this._dtClientes.ObterEnderecos(clienteCodigo)) {
            if(this._enderecoIndex < endereco.EnderecoID)
                this._enderecoIndex = endereco.EnderecoID;

            this._enderecosMemoria.add(endereco);
        }

        for (Contato contato : this._dtClientes.ObterContatos(clienteCodigo)) {
            if(this._contatoIndex < contato.ContatoID)
                this._contatoIndex = contato.ContatoID;

            this._contatosMemoria.add(contato);
        }
    }

    public ClientesBusiness(Context contexto)
    {
        this._dtClientes = new ClientesDatasource(
                contexto,
                new EnderecoDatasource(contexto),
                new ContatoDatasource(contexto)
        );

        this._enderecoIndex = 0;
        this._enderecosNovos = new ArrayList<>();
        this._enderecosMemoria =  new ArrayList<>();
        this._enderecosExcluir = new ArrayList<>();

        this._contatoIndex = 0;
        this._contatosNovos = new ArrayList<>();
        this._contatosMemoria = new ArrayList<>();
        this._contatosExcluir = new ArrayList<>();
    }

    public boolean Incluir(Clientes cliente)
    {
        try
        {
            this._dtClientes.Incluir(cliente);
            return true;
        }catch (SQLiteException sql){
            //TODO: Implementar Log
            return false;
        }catch (Exception e){
            //TODO: Implementar Log
            return false;
        }
    }
    
    public boolean Editar(Clientes cliente)
    {
        try
        {
            this._dtClientes.Editar(cliente);
            if(!_contatosExcluir.isEmpty())
            {
                for (Contato ctto :this._contatosExcluir) {
                    this._dtClientes.ExcluirContato(ctto);
                }
            }

            if(!_enderecosExcluir.isEmpty())
            {
                for (Endereco end : this._enderecosExcluir)
                {
                    this._dtClientes.ExcluirEndereco(end);
                }
            }

            return true;
        }catch (SQLiteException sql){
            //TODO: Implementar Log
        }catch (Exception e){
            //TODO: Implementar Log
        }

        return false;
    }

    public boolean Excluir(Clientes cliente)
    {
        try
        {
            this._dtClientes.Excluir(cliente);
            return true;
        }catch (SQLiteException sql){
            //TODO: Implementar Log
        }catch (Exception e){
            //TODO: Implementar Log
        }finally {
            return false;
        }
    }

    public Clientes ObterCliente(long clienteCodigo)
    {
        try
        {
            return this._dtClientes.ObterUnico(clienteCodigo);
        }catch (SQLiteException sql){
            //TODO: Implementar Log
        }catch (Exception e){
            //TODO: Implementar Log
        }

        return null;
    }

    public Pair<Boolean, String> ExisteCliente(long codigo, long tipoDocumento, String documento)
    {
        Pair<Boolean, String> retorno = null;

        try
        {
            return this._dtClientes.ExisteClientePorDocumento(codigo, tipoDocumento, documento);
        }catch (SQLiteException sql){
            //TODO: Implementar Log
        }catch (Exception e){
            //TODO: Implementar Log
        }

        return retorno;
    }

    public List<Clientes> ObterTodos(int pagina)
    {
        List<Clientes> retorno = new ArrayList<Clientes>();
        try
        {
            retorno = this._dtClientes.ObterTodos(pagina);
        }catch (SQLiteException sql){
            String erro = sql.getMessage();
            //TODO: Implementar Log
        }catch (Exception e) {
            //TODO: Implementar Log
        }

        return retorno;
    }

    public List<Clientes> ObterTodosFiltro(String busca, int pagina)
    {
        List<Clientes> retorno = new ArrayList<Clientes>();
        try
        {
            retorno = this._dtClientes.ObterTodosFiltro(busca, pagina);
        }catch (SQLiteException sql){
            String erro = sql.getMessage();
            //TODO: Implementar Log
        }catch (Exception e) {
            //TODO: Implementar Log
            String erro = e.getMessage();
        }

        return retorno;
    }

    public int ObterQuantidadeClientes()
    {
        int retorno = 0;
        try
        {
            retorno = this._dtClientes.ObterQuantidadeClientes();
        }catch (SQLiteException sql){
            String erro = sql.getMessage();
            //TODO: Implementar Log
        }catch (Exception e) {
            //TODO: Implementar Log
        }

        return retorno;
    }

    public boolean AdicionarEndereco(Endereco endereco){
        this._enderecoIndex++;
        endereco.EnderecoID = this._enderecoIndex;
        return this._enderecosNovos.add(endereco);
    }

    public boolean EditarEndereco (Endereco endereco)
    {
        for (Endereco end : this._enderecosNovos) {
            if(end.EnderecoID == endereco.EnderecoID)
            {
                end.Endereco = endereco.Endereco;
                end.Bairro = endereco.Bairro;
                end.CEP = endereco.CEP;
                end.Cidade = endereco.Cidade;
                end.Complemento = endereco.Complemento;
                end.Estado = endereco.Estado;
                end.Numero = endereco.Numero;

                return true;
            }
        }

        for (Endereco end : this._enderecosMemoria) {
            if(end.EnderecoID == endereco.EnderecoID)
            {
                end.Endereco = endereco.Endereco;
                end.Bairro = endereco.Bairro;
                end.CEP = endereco.CEP;
                end.Cidade = endereco.Cidade;
                end.Complemento = endereco.Complemento;
                end.Estado = endereco.Estado;
                end.Numero = endereco.Numero;

                return true;
            }
        }

        return false;
    }

    public boolean ExcluirEndereco (Endereco endereco){
        boolean remover = false;
        Endereco enderecoRemover = null;
        for (Endereco end :this._enderecosMemoria) {
            if(end.EnderecoID == endereco.EnderecoID){
                 remover = true;
                 enderecoRemover = end;
            }
        }

        Endereco removerNovo = null;
        for (Endereco end : this._enderecosNovos) {
            if(end.EnderecoID == endereco.EnderecoID){
                removerNovo = end;
            }
        }
        if(removerNovo != null)
            this._enderecosNovos.remove(removerNovo);

        if(remover) {
            this._enderecosExcluir.add(enderecoRemover);
            this._enderecosMemoria.remove(enderecoRemover);
        }

        return true;
    }

    public List<Endereco> getEnderecos()
    {
        List<Endereco> retorno = new ArrayList<>();

        retorno.addAll(this._enderecosMemoria);
        retorno.addAll(this._enderecosNovos);

        return retorno;
    }

    public List<Endereco> getEnderecosSalvar()
    {
        List<Endereco> retorno = new ArrayList<>();

        for (Endereco end : this._enderecosNovos) {
            end.EnderecoID = 0;
            retorno.add(end);
        }

        retorno.addAll(this._enderecosMemoria);
        return retorno;
    }


    public boolean AdicionarContato(Contato contato){
        this._contatoIndex++;
        contato.ContatoID = this._contatoIndex;
        return this._contatosNovos.add(contato);
    }

    public boolean EditarContato(Contato contato){
        for (Contato ctto : this._contatosNovos) {
            if(ctto.ContatoID == contato.ContatoID)
            {
                ctto.Email = contato.Email;
                ctto.DDD = contato.DDD;
                ctto.Telefone = contato.Telefone;
                ctto.TipoContato = contato.TipoContato;

                return true;
            }
        }

        for (Contato ctto : this._contatosMemoria) {
            if(ctto.ContatoID == contato.ContatoID)
            {
                ctto.Email = contato.Email;
                ctto.DDD = contato.DDD;
                ctto.Telefone = contato.Telefone;
                ctto.TipoContato = contato.TipoContato;

                return true;
            }
        }

        return false;
    }

    public boolean ExcluirContato (Contato contato){
        boolean remover = false;
        Contato contatoRemover = null;
        for (Contato ctto :this._contatosMemoria) {
            if(ctto.ContatoID == contato.ContatoID){
                remover = true;
                contatoRemover = ctto;
            }
        }

        Contato removerNovo = null;
        for (Contato ctto : this._contatosNovos) {
            if(ctto.ContatoID == contato.ContatoID){
                removerNovo = ctto;
            }
        }

        if(removerNovo != null)
            this._contatosNovos.remove(removerNovo);

        if(remover) {
            this._contatosExcluir.add(contatoRemover);
            this._contatosMemoria.remove(contatoRemover);
        }

        return true;
    }

    public List<Contato> getContatos()
    {
        List<Contato> retorno = new ArrayList<>();

        retorno.addAll(this._contatosMemoria);
        retorno.addAll(this._contatosNovos);

        return retorno;
    }

    public List<Contato> getContatosSalvar()
    {
        List<Contato> retorno = new ArrayList<>();

        for (Contato ctto : this._contatosNovos) {
            ctto.ContatoID = 0;
            retorno.add(ctto);
        }

        retorno.addAll(this._contatosMemoria);
        return retorno;
    }

    public Cursor ObterClientesAutoComplete(String busca)
    {
        return _dtClientes.ObterClientesAutoComplete(busca);
    }

    //Método Criado para que pudesse fechar a consulta do autocomplete.
    public void ClosePool()
    {
        _dtClientes.Close(true);
    }
}
