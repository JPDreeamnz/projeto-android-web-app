package br.com.softlinesistemas.catalogodigital.Activity.Pedido;

import android.support.v4.app.FragmentManager;

/**
 * Created by João Pedro R. Carvalho on 28/07/2016.
 */

public interface IPedidoActivity {
    FragmentManager getSupportFragmentManager();
}
