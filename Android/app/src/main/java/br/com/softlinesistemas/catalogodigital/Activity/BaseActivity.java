package br.com.softlinesistemas.catalogodigital.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.MenuItem;
import android.widget.Toast;

import br.com.softlinesistemas.catalogodigital.Activity.Clientes.ClientesActivity;
import br.com.softlinesistemas.catalogodigital.Activity.Integracao.IntegracaoActivity;
import br.com.softlinesistemas.catalogodigital.Activity.Login.LoginActivity;
import br.com.softlinesistemas.catalogodigital.Activity.Pedido.PedidoActivity;
import br.com.softlinesistemas.catalogodigital.Activity.Produto.ProdutoActivity;
import br.com.softlinesistemas.catalogodigital.Business.LoginBusiness;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 24/03/2016.
 */
public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private int _idRes,
                _idView;

    public BaseActivity(int idRes, int idView){
        this._idRes = idRes;
        this._idView = idView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.setContentView(this._idView);

        if(this.getSupportActionBar() == null) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            this.setSupportActionBar(toolbar);

            DrawerLayout drawer = (DrawerLayout) this.findViewById(this._idRes);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) this.findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id){
            case R.id.nav_dashboard:
            {
                this.startActivity(new Intent(BaseActivity.this, DashboardActivity.class));
            }break;
            case R.id.nav_clientes:
            {
                this.startActivity(new Intent(BaseActivity.this, ClientesActivity.class));
            }break;
            case R.id.nav_produtos:
            {
                this.startActivity(new Intent(BaseActivity.this, ProdutoActivity.class));
            }break;
            case R.id.nav_pedidos:
            {
                this.startActivity(new Intent(BaseActivity.this, PedidoActivity.class));
            }break;
            case R.id.nav_integracao:
            {
                this.startActivity(new Intent(BaseActivity.this, IntegracaoActivity.class));
            }break;
            /*case R.id.nav_configuracao:
            { }break;*/
            case R.id.nav_logout:
            {
                LoginBusiness logout = new LoginBusiness(BaseActivity.this);
                Pair<Integer, String> retorno = logout.Logout();

                Toast.makeText(BaseActivity.this, retorno.second, Toast.LENGTH_SHORT).show();

                if(retorno.first == 1 || retorno.first == 0){
                    this.startActivity(new Intent(BaseActivity.this, LoginActivity.class));
                }
            }break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(this._idRes);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
