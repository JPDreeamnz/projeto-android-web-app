package br.com.softlinesistemas.catalogodigital.Datasource.Vendedor;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.ParseException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Vendedor;

/**
 * Created by João Pedro R. Carvalho on 28/07/2016.
 */

public class VendedorDataHandler {
    public static Vendedor fromCursor(Cursor cursor) throws ParseException
    {
        Vendedor vendedor = null;

        if(cursor.moveToFirst())
        {
            vendedor = BindCursorVendedor(cursor);
        }

        cursor.close();
        return vendedor;
    }

    public static ArrayList<Vendedor> fromCursorToList(Cursor cursor)
    {
        ArrayList<Vendedor> vendedores = new ArrayList<Vendedor>();

        if(cursor.moveToFirst())
        {
            do{
                vendedores.add(BindCursorVendedor(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return vendedores;
    }

    public static ContentValues toContentValue(Vendedor vendedor)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.Vendedor.Coluna_Codigo, vendedor.Codigo);
        values.put(DatabaseContract.Vendedor.Coluna_Nome, vendedor.Nome);

        return values;
    }

    private static Vendedor BindCursorVendedor(Cursor cursor)
    {
        Vendedor vendedor = new Vendedor();

        vendedor.Codigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Vendedor.Coluna_Codigo)
        );
        vendedor.Nome = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Vendedor.Coluna_Nome)
        );

        return vendedor;
    }
}
