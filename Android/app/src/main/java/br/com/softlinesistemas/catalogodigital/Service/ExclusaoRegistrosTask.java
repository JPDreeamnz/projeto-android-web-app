package br.com.softlinesistemas.catalogodigital.Service;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import org.json.JSONArray;

import java.text.ParseException;

import br.com.softlinesistemas.catalogodigital.Datasource.Cliente.ClientesDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.SincroniaTabelasDataSource;
import br.com.softlinesistemas.catalogodigital.Datasource.PlanoPagamento.PlanoPagamentoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Produto.ProdutoDataSource;
import br.com.softlinesistemas.catalogodigital.Datasource.Vendedor.VendedorDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Clientes;
import br.com.softlinesistemas.catalogodigital.Model.POJO.RetornoItensExcluidos;

/**
 * Created by João Pedro R. Carvalho on 12/04/2017.
 */

public class ExclusaoRegistrosTask extends AsyncTask<String, Void, Pair<Integer, String>> {
    private String _URL;

    private ClientesDatasource _dtClientes;
    private PlanoPagamentoDatasource _dtPlanoPagamento;
    private ProdutoDataSource _dtProduto;
    private VendedorDatasource _dtVendedor;

    public ExclusaoRegistrosTask(
            String URL,
            ClientesDatasource clientesDatasource,
            PlanoPagamentoDatasource planoPagamentoDatasource,
            ProdutoDataSource produtoDataSource,
            VendedorDatasource vendedorDatasource)
    {
        this._URL = "http://" + URL + "api/Integracao/ObterRegistrosExcluidos";;

        this._dtClientes = clientesDatasource;
        this._dtPlanoPagamento = planoPagamentoDatasource;
        this._dtProduto = produtoDataSource;
        this._dtVendedor = vendedorDatasource;
    }

    /**
     * Override this method to perform a computation on a background thread. The
     * specified parameters are the parameters passed to {@link #execute}
     * by the caller of this task.
     * <p>
     * This method can call {@link #publishProgress} to publish updates
     * on the UI thread.
     *
     * @param params The parameters of the task.
     * @return A result, defined by the subclass of this task.
     * @see #onPreExecute()
     * @see #onPostExecute
     * @see #publishProgress
     */
    @Override
    protected Pair<Integer, String> doInBackground(String... params) {
        android.support.v4.util.Pair<Integer, String> respota;
        try {
            HttpRequest request = HttpRequest.get(this._URL, true, "dataRegistros", params[0])
                    .accept("application/json")
                    .header("Authorization", "Bearer " + params[1]);

            respota = new Pair<>(request.code(), request.body());
        } catch (HttpRequest.HttpRequestException e) {
            respota = new Pair<>(500, "Não foi possível obter os registros excluídos.");
        }

        return respota;
    }

    @Override
    protected void onPostExecute(Pair<Integer, String> resposta) {
        try{
            if(resposta.first == 200) {
                JSONArray json = new JSONArray(resposta.second);
                for (int i = 0; i < json.length(); i++) {
                    RetornoItensExcluidos item = new RetornoItensExcluidos();
                    item.Tabela = json.getJSONObject(i).getString("Tabela");
                    item.ID = Long.parseLong(json.getJSONObject(i).getString("ID"));

                    ExcluirItem(item);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void ExcluirItem(RetornoItensExcluidos item)
    {
        try {
            switch (item.Tabela) {
                case "Clientes": {
                    Clientes cliente = _dtClientes.ObterUnico(item.ID);
                    if(cliente != null)
                        _dtClientes.Excluir(cliente);
                    break;
                }
                case "PlanoPagamento": {
                    _dtPlanoPagamento.Excluir(item.ID);
                    break;
                }
                case "Produto": {
                    _dtProduto.Excluir(item.ID);
                    break;
                }
                case "Vendedor": {
                    _dtVendedor.Excluir(item.ID);
                    break;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
