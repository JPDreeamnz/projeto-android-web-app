package br.com.softlinesistemas.catalogodigital.Datasource.ItensPedido;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import java.text.ParseException;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.BaseHelper;
import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Database.IDatabaseOperations;
import br.com.softlinesistemas.catalogodigital.Datasource.Pedido.PedidoDataHandler;
import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;

/**
 * Created by João Pedro R. Carvalho on 21/07/2016.
 */

public class ItensPedidoDatasource extends BaseHelper implements IDatabaseOperations<ItensPedido> {
    /**
     * BaseHelper é a classe pai dos DataSource(DS) pois ela encapsula as informações
     * do DatabaseHelper e permite que os DS possuam acesso à suas respectivas tabelas.
     *
     * @param context
     */
    public ItensPedidoDatasource(Context context) {
        super(context);
    }

    /**
     * Método que efetua a inserção do Objeto especificado na base de dados
     *
     * @param instancia
     * @return instância do objeto contendo o ID.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public ItensPedido Incluir(ItensPedido instancia) throws SQLiteException, ParseException {
        try
        {
            this.OpenWrite();

            instancia.Item = database.insert(
                    DatabaseContract.ItensPedido.Tabela_Nome,
                    null,
                    ItensPedidoDataHandler.toContentValues(instancia)
            );

            this.Close(false);

            return instancia;

        } catch (SQLiteException sql) {
            throw sql;
        }
    }

    /**
     * Método que efetua a edição do objeto especificado na base de dados
     *
     * @param instancia
     * @return true se o objeto foi editado com sucesso no retorno de row = 1,
     * false se consulta foi corretamente executada mas não editou nenhuma row.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Editar(ItensPedido instancia) throws SQLiteException, ParseException {
        try
        {
            this.OpenWrite();

            int rowsPedido = database.update(
                    DatabaseContract.ItensPedido.Tabela_Nome,
                    ItensPedidoDataHandler.toContentValues(instancia),
                    DatabaseContract.ItensPedido.Coluna_Pedido + " = ? AND " +
                    DatabaseContract.ItensPedido.Coluna_Item + " = ?",
                    new String[] {String.valueOf(instancia.Pedido), String.valueOf(instancia.Item)}
            );

            this.Close(false);

            return (rowsPedido != 0);

        } catch (SQLiteException sql) {
            throw sql;
        }
    }

    /**
     * Método que efetua a exclusão do objeto especificado na base de dados
     *
     * @param instancia
     * @return true para objeto excluido corretamente da base de dados com retorno de row = 1,
     * false se a consulta foi corretamente executada mas não excluiu nenhuma row
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public boolean Excluir(ItensPedido instancia) throws SQLiteException, ParseException {
        this.OpenWrite();

        int rowsPedido = database.delete(
                DatabaseContract.ItensPedido.Tabela_Nome,
                DatabaseContract.ItensPedido.Coluna_Pedido + " = ? AND " +
                DatabaseContract.ItensPedido.Coluna_Item + " = ?",
                new String[] {String.valueOf(instancia.Pedido), String.valueOf(instancia.Item)}
        );

        this.Close(true);

        return (rowsPedido != 0);
    }

    /**
     * Obtém um objeto da tabela de acordo com o ID especificado.
     *
     * @param id
     * @return instância do objeto encontrado.
     * @throws SQLiteException
     * @throws ParseException
     */
    @Override
    public ItensPedido ObterUnico(long id) throws SQLiteException, ParseException {
        return null;
    }

    public List<ItensPedido> ObterTodos(long pedido) throws ParseException {
        this.OpenRead();
        List<ItensPedido> retorno =  ItensPedidoDataHandler.fromCursorToList(
                database.query(
                        DatabaseContract.ItensPedido.Tabela_Nome,
                        DatabaseContract.ItensPedido.Tabela_TodasColunas,
                        DatabaseContract.ItensPedido.Coluna_Pedido + " = ?",new String[] { String.valueOf(pedido) },null, null, null
                )
        );

        this.Close(true);

        return retorno;
    }
}
