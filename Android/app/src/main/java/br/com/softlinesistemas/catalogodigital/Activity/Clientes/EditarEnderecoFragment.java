package br.com.softlinesistemas.catalogodigital.Activity.Clientes;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import br.com.softlinesistemas.catalogodigital.Activity.BaseFragment;
import br.com.softlinesistemas.catalogodigital.Business.ClientesBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 17/03/2017.
 */

public class EditarEnderecoFragment extends BaseFragment {
    private ClientesBusiness _clienteBusiness;
    private long _enderecoID;

    private Spinner _spEstados;

    private EditText _txtNumero,
            _txtCidade,
            _txtEndereco,
            _txtBairro,
            _txtCEP,
            _txtComplemento;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View criarEnderecoView = inflater.inflate(R.layout.clientes_criar_endereco, null);
        this.setView(criarEnderecoView);
        this.setHasOptionsMenu(true);
        return criarEnderecoView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.endereco_criar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.endereco_salvar_button){
            View view = this.getView();
            Endereco endereco = new Endereco();

            this._txtBairro = (EditText) view.findViewById(R.id.endereco_txtBairro);
            this._txtCEP = (EditText) view.findViewById(R.id.endereco_txtCEP);
            this._txtCidade = (EditText) view.findViewById(R.id.endereco_txtCidade);
            this._txtComplemento = (EditText) view.findViewById(R.id.endereco_txtComplemento);
            this._txtEndereco = (EditText) view.findViewById(R.id.endereco_textEndereco);
            this._txtNumero = (EditText) view.findViewById(R.id.endereco_textNumero);

            if (this._txtEndereco.getText().toString() == null || this._txtEndereco.getText().toString().isEmpty()) {
                this._txtEndereco.setError("Campo Endereço Obrigatório.");
                return false;
            }

            if (this._txtNumero.getText().toString() == null || this._txtNumero.getText().toString().isEmpty()) {
                this._txtNumero.setError("Campo Número Obrigatório.");
                return false;
            }

            endereco.Bairro = this._txtBairro.getText().toString();
            endereco.Estado = this._spEstados.getSelectedItem().toString();
            endereco.Numero = this._txtNumero.getText().toString();

            String cep = this._txtCEP.getText().toString();
            if(!cep.isEmpty())
                endereco.CEP = Integer.parseInt(cep);

            endereco.Cidade = this._txtCidade.getText().toString();
            endereco.Complemento = this._txtComplemento.getText().toString();
            endereco.Endereco = this._txtEndereco.getText().toString();

            endereco.EnderecoID = this._enderecoID;

            this._clienteBusiness.EditarEndereco(endereco);

            getActivity().getFragmentManager().popBackStack();
        }

        return true;
    }

    public void setEnderecoID(long enderecoID)
    {
        this._enderecoID = enderecoID;
    }

    private void setView(View view){
        this._spEstados = (Spinner) view.findViewById(R.id.endereco_spEstado);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(),
                R.array.estados, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this._spEstados.setAdapter(adapter);

        for (Endereco endereco : _clienteBusiness.getEnderecos()) {
            if(endereco.EnderecoID == this._enderecoID)
            {
                this._txtBairro = (EditText) view.findViewById(R.id.endereco_txtBairro);
                this._txtCEP = (EditText) view.findViewById(R.id.endereco_txtCEP);
                this._txtCidade = (EditText) view.findViewById(R.id.endereco_txtCidade);
                this._txtComplemento = (EditText) view.findViewById(R.id.endereco_txtComplemento);
                this._txtEndereco = (EditText) view.findViewById(R.id.endereco_textEndereco);
                this._txtNumero = (EditText) view.findViewById(R.id.endereco_textNumero);

                this._txtBairro.setText(endereco.Bairro);
                this._txtCEP.setText(String.valueOf(endereco.CEP));
                this._txtCidade.setText(endereco.Cidade);
                this._txtComplemento.setText(endereco.Complemento);
                this._txtEndereco.setText(endereco.Endereco);
                this._txtNumero.setText(String.valueOf(endereco.Numero));

                this._spEstados.setSelection(adapter.getPosition(endereco.Estado));
            }
        }
    }

    public void setClienteBusiness(ClientesBusiness clienteBusiness) {
        this._clienteBusiness = clienteBusiness;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }
}
