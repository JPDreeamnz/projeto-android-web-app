package br.com.softlinesistemas.catalogodigital.Activity;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.GridView;

/**
 * Created by João Pedro R. Carvalho on 30/10/2016.
 */

public class EndlessScroll implements OnScrollListener {

    private GridView _gridView;
    private boolean isLoading;
    private boolean hasMorePages;
    private int pageNumber = 0;
    private RefreshList refreshList;
    private boolean isRefreshing;

    public EndlessScroll(GridView gridView,RefreshList refreshList) {
        this._gridView = gridView;
        this.isLoading = false;
        this.hasMorePages = true;
        this.refreshList=refreshList;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (_gridView.getLastVisiblePosition() + 1 >= (totalItemCount - 10) && !isLoading) {
            isLoading = true;
            if (hasMorePages&&!isRefreshing) {
                isRefreshing=true;
                refreshList.onRefresh(pageNumber);
            }
        } else {
            isLoading = false;
        }

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) { }

    public void noMorePages() {
        this.hasMorePages = false;
    }

    public void notifyMorePages(){
        isRefreshing=false;
        pageNumber=pageNumber+1;
    }

    public interface RefreshList {
        void onRefresh(int pageNumber);
    }

}
