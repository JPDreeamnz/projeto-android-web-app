package br.com.softlinesistemas.catalogodigital.Business;

import android.content.Context;
import br.com.softlinesistemas.catalogodigital.Datasource.Login.LoginDatasource;

/**
 * Created by João Pedro R. Carvalho on 25/12/2015.
 *
 * Criado apenas para limpar a database, deletar ao fim do desenvolvimento
 */
public class DatabaseBusiness {
    LoginDatasource dtLogin;

    public DatabaseBusiness(Context context){
        this.dtLogin = new LoginDatasource(context);
    }

    public void LimpaDatabase(){
        dtLogin.LimpaDatabase();
    }
}
