package br.com.softlinesistemas.catalogodigital.Service;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import br.com.softlinesistemas.catalogodigital.Activity.ConfiguracaoSingleton;
import br.com.softlinesistemas.catalogodigital.Datasource.Cliente.ClientesDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.ConfiguracaoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.SincroniaTabelasDataSource;
import br.com.softlinesistemas.catalogodigital.Datasource.Configuracao.VendedorPlanoPadraoDataSource;
import br.com.softlinesistemas.catalogodigital.Datasource.Contato.ContatoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Endereco.EnderecoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Login.LoginDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Pedido.PedidoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.PlanoPagamento.PlanoPagamentoDatasource;
import br.com.softlinesistemas.catalogodigital.Datasource.Produto.ProdutoDataSource;
import br.com.softlinesistemas.catalogodigital.Datasource.Vendedor.VendedorDatasource;
import br.com.softlinesistemas.catalogodigital.Model.Login;
import br.com.softlinesistemas.catalogodigital.Model.SincroniaTabelas;

/**
 * Created by João Pedro R. Carvalho on 12/09/2016.
 */

public class IntegracaoService implements IIntegracaoService {
    private LoginDatasource _dtLogin;
    private ControleImagemDownload _DownloadImagem;
    private String URL_LOGIN = "";
    private String URL = "";
    private Date _dataSinc;
    private int quantidadeTabelas = 0;
    Context _contexto;
    NotificationCompat.Builder _mBuilder;

    public IntegracaoService(Context contexto, int tipoUpload) //1 = integração, 2 = pedidos
    {
        this._contexto = contexto;
        this._dtLogin = new LoginDatasource(contexto);
        this._dataSinc = new Date();
        ConfiguracaoDatasource configDS = new ConfiguracaoDatasource(contexto);
        ConfiguracaoSingleton config = ConfiguracaoSingleton.getInstancia(configDS);
        this.URL = config.getREPOSITORY_SERVER();
        this.URL_LOGIN = config.getAUTH_SERVER();

        this._mBuilder = new NotificationCompat.Builder(contexto)
                .setSmallIcon(android.support.design.R.drawable.notification_template_icon_bg);

        if(tipoUpload == 1)
            this._mBuilder.setContentTitle("0/5 Tabelas Sicronizadas").setContentText("Integração iniciada!");
        else
            this._mBuilder.setContentTitle("Enviando Pedidos").setContentText("Aguarde enquanto o envio é executado.");

        NotificationManager manager = (NotificationManager) _contexto.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, _mBuilder.build());
    }

    public void Execute(TipoIntegracaoEnum tipoIntegracao){
        try {
            Login login = this._dtLogin.ObterUltimoLogin();
            if(login == null) {
                login = new Login();
                login.Token = "";
            }

            CheckLoginTask loginTask = new CheckLoginTask(this.URL_LOGIN + "/", this, tipoIntegracao);
            loginTask.execute(login.Token);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Login(TipoIntegracaoEnum tipoIntegracao){
        LoginTask login = new LoginTask(this.URL_LOGIN + "/", this, tipoIntegracao);
        login.execute();
    }

    @Override
    public void DownloadTabelas(String token){
        try {
            SimpleDateFormat smp = new SimpleDateFormat("yyyy-MM-dd");
            Date ultimoSinc = new SincroniaTabelasDataSource(this._contexto).Obter().Data;

            VendedorDatasource dtVendedor = new VendedorDatasource(this._contexto);
            VendedorTask attVendedores = new VendedorTask(this.URL + "/", dtVendedor, this);
            attVendedores.execute(smp.format(ultimoSinc), token);

            PlanoPagamentoDatasource dtPlanoPagamento = new PlanoPagamentoDatasource(this._contexto);
            PlanoPagamentoTask attPlano = new PlanoPagamentoTask(this.URL + "/", dtPlanoPagamento, this);
            attPlano.execute(smp.format(ultimoSinc), token);

            ClientesDatasource dtClientes = new ClientesDatasource(this._contexto, new EnderecoDatasource(this._contexto), new ContatoDatasource(this._contexto));
            ClienteTask attClientes = new ClienteTask(
                    this.URL + "/",
                    dtClientes,
                    this);
            attClientes.execute(smp.format(ultimoSinc), token);

            ProdutoDataSource dtProduto = new ProdutoDataSource(this._contexto);
            _DownloadImagem = new ControleImagemDownload(
                    token, this._contexto);
            ProdutoTask attProduto = new ProdutoTask(this.URL + "/", dtProduto, _contexto, this, _DownloadImagem);
            attProduto.execute(smp.format(ultimoSinc), token);

            VendedorPlanoPagamentoPadraoTask attVendedorPlanoPadrao = new VendedorPlanoPagamentoPadraoTask(
                    this.URL + "/",
                    new VendedorPlanoPadraoDataSource(this._contexto),
                    this
            );
            attVendedorPlanoPadrao.execute(token);

            ExclusaoRegistrosTask attExclusaoRegistros = new ExclusaoRegistrosTask(
                    this.URL + "/",
                    dtClientes,
                    dtPlanoPagamento,
                    dtProduto,
                    dtVendedor
            );
            attExclusaoRegistros.execute(smp.format(ultimoSinc), token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void UploadPedidos(String token){
        UploadPedidosTask uploadPedidos = new UploadPedidosTask(this.URL + "/", new PedidoDatasource(this._contexto), this);
        uploadPedidos.execute(token);
    }

    @Override
    public void DownloadImagens(){
        _DownloadImagem.IniciarDownloadImagens();
    }

    public void AtualizarDataSincronia(){
        try {
            SincroniaTabelasDataSource dtSincronia = new SincroniaTabelasDataSource(this._contexto);
            SincroniaTabelas sinc = dtSincronia.Obter();
            sinc.Data = this._dataSinc;
            dtSincronia.Editar(sinc);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Map<String, Object> fromJson(JSONObject jsonObject) {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keyIterator = jsonObject.keys();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            try {
                Object obj = jsonObject.get(key);

                if (obj instanceof JSONObject) {
                    map.put(key, fromJson((JSONObject) obj));
                }
                else if (obj instanceof JSONArray) {
                    map.put(key, fromJson((JSONArray) obj));
                }
                else {
                    map.put(key, obj);
                }
            }
            catch (JSONException jsone) {
                Log.wtf("RequestManager", "Failed to get value for " + key + " from JSONObject.", jsone);
            }
        }

        return map;
    }

    public static List<Object> fromJson(JSONArray jsonArray) {
        List<Object> list = new ArrayList<Object>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                Object obj = jsonArray.get(i);

                if (obj instanceof JSONObject) {
                    list.add(fromJson((JSONObject) obj));
                }
                else if (obj instanceof JSONArray) {
                    list.add(fromJson((JSONArray) obj));
                }
                else {
                    list.add(obj);
                }
            }
            catch (JSONException jsone) {
                Log.wtf("RequestManager", "Failed to get value at index " + i + " from JSONArray.", jsone);
            }
        }

        return list;
    }

    @Override
    public void AtualizarPush(boolean sincronia, String mensagem) {
        if(sincronia)
            this.quantidadeTabelas = this.quantidadeTabelas + 1;

        NotificationManager manager = (NotificationManager) _contexto.getSystemService(Context.NOTIFICATION_SERVICE);

        if(quantidadeTabelas < 4) {
            this._mBuilder.setContentTitle(this.quantidadeTabelas + "/4 Tabelas Sicronizadas");
            this._mBuilder.setContentText(mensagem);
            manager.notify(1, _mBuilder.build());
        } else {
            this._mBuilder.setContentTitle(this.quantidadeTabelas + "/4 Tabelas Sicronizadas");
            this._mBuilder.setContentTitle("Sincronia Conclída");
            manager.notify(1, _mBuilder.build());
            this.Dispose();
        }
    }

    @Override
    public void AtualizarPush(String mensagem)
    {
        NotificationManager manager = (NotificationManager) _contexto.getSystemService(Context.NOTIFICATION_SERVICE);
        this._mBuilder.setContentText(mensagem);
        manager.notify(1, _mBuilder.build());
    }

    @Override
    public void Dispose(){
        AtualizarDataSincronia();
    }
}
