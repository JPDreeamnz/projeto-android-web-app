package br.com.softlinesistemas.catalogodigital.Model.POJO;

import br.com.softlinesistemas.catalogodigital.Model.PlanoPagamento;

/**
 * Created by João Pedro R. Carvalho on 31/08/2016.
 */

public class PlanoPagamentoSpinner extends PlanoPagamento{

    public PlanoPagamentoSpinner(PlanoPagamento planoPagamento){
        this.Codigo = planoPagamento.Codigo;
        this.Nome = planoPagamento.Nome;
    }

    public String toString(){
        return this.Codigo + " - " + this.Nome;
    }
}
