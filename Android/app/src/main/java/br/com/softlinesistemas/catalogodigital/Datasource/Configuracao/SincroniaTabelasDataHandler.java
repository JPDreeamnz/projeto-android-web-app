package br.com.softlinesistemas.catalogodigital.Datasource.Configuracao;

import android.content.ContentValues;
import android.database.Cursor;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.SincroniaTabelas;

/**
 * Created by João Pedro R. Carvalho on 12/04/2017.
 */

public class SincroniaTabelasDataHandler {
    public static SincroniaTabelas fromCursor(Cursor cursor) throws ParseException {
        SincroniaTabelas sincroniaTabelas = null;

        if(cursor.moveToFirst())
        {
            sincroniaTabelas = BindCursorSincroniaTabelas(cursor);
        }

        cursor.close();
        return sincroniaTabelas;
    }

    public static ContentValues toContentValues(SincroniaTabelas sincroniaTabelas)
    {
        ContentValues values = new ContentValues();

        values.put(DatabaseContract.SincroniaTabelas.Coluna_Codigo, sincroniaTabelas.Codigo);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        values.put(DatabaseContract.SincroniaTabelas.Coluna_Data, dateFormat.format(sincroniaTabelas.Data));

        return values;
    }

    private static SincroniaTabelas BindCursorSincroniaTabelas(Cursor cursor) throws ParseException {
        SincroniaTabelas sincroniaTabelas = new SincroniaTabelas();

        sincroniaTabelas.Codigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.SincroniaTabelas.Coluna_Codigo)
        );

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        sincroniaTabelas.Data = dateFormat.parse(cursor.getString(
                cursor.getColumnIndex(DatabaseContract.SincroniaTabelas.Coluna_Data)
        ));

        return sincroniaTabelas;
    }
}
