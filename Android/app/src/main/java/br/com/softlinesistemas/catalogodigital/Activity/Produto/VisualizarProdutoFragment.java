package br.com.softlinesistemas.catalogodigital.Activity.Produto;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.lang.ref.WeakReference;
import java.text.NumberFormat;

import br.com.softlinesistemas.catalogodigital.Activity.BaseV4Fragment;
import br.com.softlinesistemas.catalogodigital.Business.ProdutoBusiness;
import br.com.softlinesistemas.catalogodigital.Model.Produto;
import br.com.softlinesistemas.catalogodigital.R;

/**
 * Created by João Pedro R. Carvalho on 12/07/2016.
 */

public class VisualizarProdutoFragment extends BaseV4Fragment {
    private long _produtoCodigo;
    private ProdutoBusiness _produtoBusiness;

    private EditText mNome,
                     mUnidade,
                     mCodigoBarras,
                     mCodigoFabrica,
                     mCodigoOriginal,
                     mCor,
                     mTamanho,
                     mMarca,
                     mValorVenda,
                     mQtdEstoque;

    public static VisualizarProdutoFragment novaInstancia(ProdutoBusiness produtoBusiness, long produtoCodigo, int fragmentContainer){
        VisualizarProdutoFragment instancia = new VisualizarProdutoFragment();
        instancia._produtoBusiness = produtoBusiness;
        instancia._produtoCodigo = produtoCodigo;
        instancia.setFragmentContainer(fragmentContainer);
        return instancia;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View viewCriar = inflater.inflate(R.layout.produto_visualizar_fragment, null);
        this.setView(viewCriar);
        return viewCriar;
    }

    private void setView(View view)
    {
        this.mCodigoBarras = (EditText) view.findViewById(R.id.produto_textCodigoBarras);
        this.mCodigoFabrica = (EditText) view.findViewById(R.id.produto_textCodigoFabrica);
        this.mCodigoOriginal = (EditText) view.findViewById(R.id.produto_textCodigoOriginal);
        this.mCor = (EditText) view.findViewById(R.id.produto_textCor);
        this.mMarca = (EditText) view.findViewById(R.id.produto_textMarca);
        this.mNome = (EditText) view.findViewById(R.id.produto_textNome);
        this.mQtdEstoque = (EditText) view.findViewById(R.id.produto_textQuantidadeEstoque);
        this.mTamanho = (EditText) view.findViewById(R.id.produto_textTamanho);
        this.mUnidade = (EditText) view.findViewById(R.id.produto_textUnidade);
        this.mValorVenda = (EditText) view.findViewById(R.id.produto_textValorVenda);

        Produto produto = this._produtoBusiness.ObterProduto(this._produtoCodigo);

        this.mCodigoBarras.setText(produto.CodigoBarras);
        this.mCodigoBarras.setEnabled(false);

        this.mCodigoFabrica.setText(produto.CodigoFabrica);
        this.mCodigoFabrica.setEnabled(false);

        this.mCodigoOriginal.setText(produto.CodigoOriginal);
        this.mCodigoOriginal.setEnabled(false);

        this.mCor.setText(produto.Cor);
        this.mCor.setEnabled(false);

        this.mMarca.setText(produto.Marca);
        this.mMarca.setEnabled(false);

        this.mNome.setText(produto.Nome);
        this.mNome.setEnabled(false);

        this.mQtdEstoque.setText(String.valueOf(produto.QuantidadeEstoque));
        this.mQtdEstoque.setEnabled(false);

        this.mTamanho.setText(produto.Tamanho);
        this.mTamanho.setEnabled(false);

        this.mUnidade.setText(produto.Unidade);
        this.mUnidade.setEnabled(false);

        this.mValorVenda.setText(NumberFormat.getCurrencyInstance().format(produto.ValorVenda));
        this.mValorVenda.setEnabled(false);
    }
}
