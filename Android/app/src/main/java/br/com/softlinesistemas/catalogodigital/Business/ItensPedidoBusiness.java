package br.com.softlinesistemas.catalogodigital.Business;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Model.ItensPedido;
import br.com.softlinesistemas.catalogodigital.Model.Pedido;

/**
 * Created by João Pedro R. Carvalho on 28/07/2016.
 */

public class ItensPedidoBusiness {
    private long _menorIndiceItem;
    private Pedido _pedidoMemoria;
    private List<ItensPedido> _itens;
    private List<ItensPedido> _itensExcluir;

    public ItensPedidoBusiness()
    {
        if(_pedidoMemoria == null)
            _pedidoMemoria = new Pedido();

        _menorIndiceItem = 0;
        _itensExcluir = new ArrayList<>();
        _itens = new ArrayList<>();
    }

    public void Inserir(ItensPedido item){
        if(item.Item == 0)
            item.Item = _menorIndiceItem = _menorIndiceItem - 1;

        this._itens.add(item);
    }

    public void Inserir(List<ItensPedido> lista)
    {
        this._itens = lista;
    }

    public void Editar(ItensPedido item){
        for (ItensPedido itemPedido : this._itens) {
            if(itemPedido.Item == item.Item){
                itemPedido.Produto = item.Produto;
                itemPedido.ValorTotal = item.ValorTotal;
                itemPedido.ValorDesconto = item.ValorDesconto;
                itemPedido.ValorUnitario = item.ValorUnitario;
                itemPedido.Quantidade = item.Quantidade;
            }
        }
    }

    public void Excluir(ItensPedido item)
    {
        for (ItensPedido itemPedido : this._itens) {
            if(itemPedido.Item == item.Item){
                this._itensExcluir.add(item);
                itemPedido.Item = -99;
            }
        }
    }

    public void Excluir()
    {
        this._itensExcluir = new ArrayList<>();
        this._itens = new ArrayList<>();
    }

    public List<ItensPedido> ObterLista()
    {
        List<ItensPedido> lista = new ArrayList<>();

        for (ItensPedido itemPedido : this._itens) {
            if(itemPedido.Item != -99){
                lista.add(itemPedido);
            }
        }

        return lista;
    }

    public ItensPedido ObterItem(long id)
    {
        for (ItensPedido item : this._itens) {
            if(item.Pedido == id)
                return item;
        }

        return null;
    }

    public Pedido getPedidoMemoria() {
        BigDecimal valorProdutos = new BigDecimal(0),
                valorDescontos = new BigDecimal(0),
                valorTotal = new BigDecimal(0);

        for (ItensPedido item : this._itens) {
            if(item.Item != -99){
                valorProdutos = valorProdutos.add(item.ValorUnitario.multiply(new BigDecimal(item.Quantidade)));
                valorDescontos = valorDescontos.add(item.ValorDesconto.multiply(new BigDecimal(item.Quantidade)));
                valorTotal = valorTotal.add(item.ValorTotal);
            }
        }

        this._pedidoMemoria.ValorProduto = valorProdutos;
        this._pedidoMemoria.ValorDesconto = valorDescontos;
        this._pedidoMemoria.Valor = valorTotal;

        this._pedidoMemoria.ItensPedido = _itens;
        this._pedidoMemoria.ValorAcrescimo = new BigDecimal(0);
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            this._pedidoMemoria.setData(dateFormat.format(new Date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return _pedidoMemoria;
    }

    public void setPedidoMemoria(Pedido pedidoMemoria) {
        this._pedidoMemoria = pedidoMemoria;
    }

    public List<ItensPedido> getItensExcluir() {
        return _itensExcluir;
    }

    public void DisposeMemoria()
    {
        this._pedidoMemoria = null;
        this._itens = null;
        this._itensExcluir = null;

        this._pedidoMemoria = new Pedido();
        this._itens = new ArrayList<>();
        this._itensExcluir = new ArrayList<>();
    }
}
