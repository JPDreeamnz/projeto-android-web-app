package br.com.softlinesistemas.catalogodigital.Datasource.Endereco;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import br.com.softlinesistemas.catalogodigital.Database.DatabaseContract;
import br.com.softlinesistemas.catalogodigital.Model.Endereco;

/**
 * Created by João Pedro R. Carvalho on 14/03/2016.
 */
public final class EnderecoDataHandler {
    public static Endereco fromCursor(Cursor cursor)
    {
        Endereco endereco = null;

        if(cursor.moveToFirst())
        {
            endereco = BindCursorEndereco(cursor);
        }

        cursor.close();
        return endereco;
    }

    public static List<Endereco> fromCursorToList(Cursor cursor)
    {
        List<Endereco> enderecos = new ArrayList<Endereco>();

        if(cursor.moveToFirst())
        {
            do{
                enderecos.add(BindCursorEndereco(cursor));
            }while (cursor.moveToNext());
        }

        cursor.close();
        return enderecos;
    }

    public static ContentValues toContentValues(Endereco endereco)
    {
        ContentValues values = new ContentValues();

        if(endereco.EnderecoID > 0) values.put(DatabaseContract.Endereco.Coluna_EnderecoID, endereco.EnderecoID);
        values.put(DatabaseContract.Endereco.Coluna_Endereco, endereco.Endereco);
        values.put(DatabaseContract.Endereco.Coluna_Bairro, endereco.Bairro);
        values.put(DatabaseContract.Endereco.Coluna_CEP, endereco.CEP);
        values.put(DatabaseContract.Endereco.Coluna_Cidade, endereco.Cidade);
        values.put(DatabaseContract.Endereco.Coluna_Complemento, endereco.Complemento);
        values.put(DatabaseContract.Endereco.Coluna_Estado, endereco.Estado);
        values.put(DatabaseContract.Endereco.Coluna_Numero, endereco.Numero);
        values.put(DatabaseContract.Endereco.Coluna_ClienteCodigo, endereco.ClienteCodigo);

        return values;
    }

    private static Endereco BindCursorEndereco(Cursor cursor)
    {
        Endereco endereco = new Endereco();

        endereco.EnderecoID = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_EnderecoID)
        );
        endereco.ClienteCodigo = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_ClienteCodigo)
        );
        endereco.Endereco = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_Endereco)
        );
        endereco.Bairro = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_Bairro)
        );
        endereco.CEP = cursor.getLong(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_CEP)
        );
        endereco.Cidade = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_Cidade)
        );
        endereco.Complemento = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_Complemento)
        );
        endereco.Estado = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_Estado)
        );
        endereco.Numero = cursor.getString(
                cursor.getColumnIndex(DatabaseContract.Endereco.Coluna_Numero)
        );

        return endereco;
    }
}
