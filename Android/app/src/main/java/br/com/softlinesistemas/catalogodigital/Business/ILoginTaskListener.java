package br.com.softlinesistemas.catalogodigital.Business;

import br.com.softlinesistemas.catalogodigital.Model.POJO.RetornoLoginPOJO;

/**
 * Created by João Pedro R. Carvalho on 16/12/2015.
 */
public interface ILoginTaskListener {
    void OnTaskCompleted(RetornoLoginPOJO retornoLogin);
    void onTaskError(String erros);
}
