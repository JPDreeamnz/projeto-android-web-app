package br.com.softlinesistemas.catalogodigital.Activity.Integracao;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.com.softlinesistemas.catalogodigital.Activity.BaseActivity;
import br.com.softlinesistemas.catalogodigital.R;
import br.com.softlinesistemas.catalogodigital.Service.IntegracaoService;
import br.com.softlinesistemas.catalogodigital.Service.TipoIntegracaoEnum;

/**
 * Created by João Pedro R. Carvalho on 14/09/2016.
 */

public class IntegracaoActivity extends BaseActivity {

    private IntegracaoService _integracao;
    private Button mBaixarRegistros,
                   mUploadPedidos;

    public IntegracaoActivity(){
        super(R.id.integracao_drawer_layout, R.layout.integracao_activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.mBaixarRegistros = (Button) this.findViewById(R.id.integracao_dowload_tabelasbtn);
        this.mBaixarRegistros.setOnClickListener(this.BaixarRegistros(this));

        this.mUploadPedidos = (Button) this.findViewById(R.id.integracao_pedidosbtn);
        this.mUploadPedidos.setOnClickListener(this.UploadPedidos(this));
    }

    public View.OnClickListener BaixarRegistros(final IntegracaoActivity integracaoActivity)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _integracao = new IntegracaoService(integracaoActivity, 1);
                _integracao.Execute(TipoIntegracaoEnum.DOWNLOAD_TABELAS);
            }
        };
    }

    public View.OnClickListener UploadPedidos(final IntegracaoActivity integracaoActivity)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _integracao = new IntegracaoService(integracaoActivity, 2);
                _integracao.Execute(TipoIntegracaoEnum.UPLOAD_TODOS_PEDIDOS);
            }
        };
    }
}
