﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Administrator.Models
{
    public class LoginAuth
    {
        public string Token { get; set; }
        public string Status { get; set; }
        public UserModel Usuario { get; set; }
    }
}