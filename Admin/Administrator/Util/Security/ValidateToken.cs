﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Administrator.Util.Security
{
    public class ValidateToken
    {
        private WebClient web;

        public ValidateToken()
        {
            web = new WebClient();
        }

        public bool isValid(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
                return false;

            string validationReturn = web.DownloadString(ConfigurationApiManager.GetValidationUrl(token));

            Boolean status;
            Boolean.TryParse(validationReturn, out status);
            return status;
        }

        public void removeToken(string token)
        {
            if(!string.IsNullOrWhiteSpace(token)){
                web.DownloadString(ConfigurationApiManager.GetRemoveUrl(token));
            }
        }
    }
}