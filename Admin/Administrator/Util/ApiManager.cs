﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Administrator.Util
{
    public class ApiManager
    {
        WebClient web;

        public ApiManager(string token)
        {
            ApiStartup(token);
        }

        public ApiManager(string token, string baseUrl)
        {
            ApiStartup(token);
            web.BaseAddress = baseUrl;
        }

        private void ApiStartup(string token)
        {
            web = new WebClient();

            web.BaseAddress = ConfigurationApiManager.BASE_API_SERVER; // server url
            web.Headers.Add(HttpRequestHeader.Accept, "application/json"); // only accept json return
            web.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + token); // always send token
            web.Encoding = Encoding.UTF8;
        }

        public T Get<T>(string url) where T : class
        {
            try
            {
                string data = web.DownloadString(url);
                T getReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);

                return getReturn;
            }
            catch (WebException wex)
            {
                if (((System.Net.HttpWebResponse)wex.Response).StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException("Login timeout");
                }

                return null;
            }
        }

        public T Post<T>(string url, object data) where T : class
        {
            web.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            string postReturn = web.UploadString(url, JsonConvert.SerializeObject(data));
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(postReturn);
        }

        public T Put<T>(string url, object data) where T : class
        {
            web.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
            string putReturn = web.UploadString(url, "PUT", data.ToQueryStringSerialized());
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(putReturn);
        }

        public T Delete<T>(string url, object data) where T : class
        {
            string deleteReturn = web.DownloadString($"{url}{data}");
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(deleteReturn);
        }
    }
}