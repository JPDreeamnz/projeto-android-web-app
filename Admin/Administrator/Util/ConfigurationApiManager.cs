﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Administrator.Util
{
    public static class ConfigurationApiManager
    {
        public static string BASE_AUTH_WEB_SITE;
        public static string BASE_API_SERVER;

        public static string AUTH_LOGIN
        {
            get
            {
                return BASE_AUTH_WEB_SITE + "API/AUTH/POST";
            }
            private set { }
        }

        private static string AUTH_VALIDATE
        {
            get
            {
                return BASE_AUTH_WEB_SITE + "API/AUTH/GET";
            }
            set { }
        }

        private static string AUTH_REMOVE
        {
            get
            {
                return BASE_AUTH_WEB_SITE + "API/AUTH/DELETE";
            }
            set { }
        }

        public static string TOKEN = "TOKEN_KEY";

        public static string GetValidationUrl(string token)
        {
            return AUTH_VALIDATE + "/?Token=" + token;
        }

        public static string GetRemoveUrl(string token)
        {
            return AUTH_REMOVE + "/?Token=" + token;
        }
    }
}