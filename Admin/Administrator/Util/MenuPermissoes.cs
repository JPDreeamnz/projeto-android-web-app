﻿using Api.Enums;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Administrator.Util
{
    public class MenuPermissoes
    {
        private static Dictionary<int, string> _menuUsuarioOrdem;
        private static string _menuUsuario
        {
            get
            {
                if (HttpContext.Current.Session["MenuUsuario"] == null)
                {
                    return string.Empty;
                }
                else
                    return (string)HttpContext.Current.Session["MenuUsuario"];
            }

            set
            {
                HttpContext.Current.Session["MenuUsuario"] = value;
            }
        }

        private static Dictionary<int, string> _menuLateralOrdem;
        private static string _menuLateral
        {
            get
            {
                if (HttpContext.Current.Session["MenuLateral"] == null)
                {
                    return string.Empty;
                }
                else
                    return (string)HttpContext.Current.Session["MenuLateral"];
            }

            set
            {
                HttpContext.Current.Session["MenuLateral"] = value;
            }
        }

        #region VariaveisMenu
        private const string _Auditoria = "<li>" +
                                            "<a href=\"\\Auditoria\">" +
                                                "<i class=\"material-icons\" style=\"position: absolute; right: auto; font-size: 22px;\">assignment</i>" +
                                                "<span style=\"position: relative; left: 30px;\">Auditoria</span>" +
                                            "</a>" +
                                         "</li>",
                            _Clientes = "<li>" +
                                            "<a href=\"\\Clientes\" class=\"waves-effect waves-red\">" +
                                                "<div>" +
                                                    "<i class=\"material-icons\">&#xE87C;</i>" +
                                                    "<span class=\"menu_title\">Clientes</span>" +
                                                "</div>" +
                                            "</a>" +
                                        "</li>",
                            _Financeiro = "<li>" +
                                            "<a href=\"\\Financeiro\" class=\"waves-effect waves-red\">" +
                                                "<div>" +
                                                    "<i class=\"material-icons\">payment</i>" +
                                                    "<span class=\"menu_title\">Financeiro</span>" +
                                                "</div>" +
                                            "</a>" +
                                        "</li>",
                            _Home = "<li>" +
                                        "<a href=\"\\Home\" class=\"waves-effect waves-red\">" +
                                            "<div>" +
                                                "<i class=\"material-icons\">dashboard</i>" +
                                                "<span class=\"menu_title\"> Dashboard</span>" +
                                            "</div>" +
                                        "</a>" +
                                    "</li>",
                            _Parametros = "<li>" +
                                            "<a href=\"\\Parametros\">" +
                                                "<i class=\"material-icons\" style=\"position: absolute; right: auto; font-size: 22px;\">build</i>" +
                                                "<span style=\"position: relative; left: 30px;\">Sistema</span>" +
                                            "</a>" +
                                        "</li>",
                            _PedidoVenda = "<li>" +
                                            "<a href=\"\\PedidoVenda\" class=\"waves-effect waves-red\">" +
                                                "<div>" + 
                                                    "<i class=\"material-icons\">local_grocery_store</i>" +
                                                    "<span class=\"menu_title\">Pedido de Venda</span>" +
                                                "</div>" +
                                            "</a>" +
                                        "</li>",
                            _PlanoPagamento = "<li>" +
                                                "<a href=\"\\PlanoPagamento\" class=\"waves-effect waves-red\">" +
                                                    "<div>" +
                                                        "<i class=\"material-icons\">list</i>" +
                                                        "<span class=\"menu_title\">Plano de Pagamento</span>" +
                                                    "</div>" +
                                                "</a>" +
                                            "</li>",
                            _Produto = "<li>" +
                                            "<a href=\"\\Produto\" class=\"waves-effect waves-red\">" +
                                                "<div>" +
                                                    "<i class=\"material-icons\">local_offer</i>" +
                                                    "<span class=\"menu_title\">Produto</span>" +
                                                "</div>" +
                                            "</a>" +
                                        "</li>",
                            _Usuarios ="<li>" +
                                        "<a href=\"\\Users\" class=\"waves-effect waves-red\">" +
                                            "<div>" +
                                                "<i class=\"material-icons\">&#xE851;</i>" +
                                                "<span class=\"menu_title\">Usuários</span>" +
                                            "</div>" +
                                        "</a>" +
                                    "</li>",
                            _Vendedor = "<li>" +
                                            "<a href=\"\\Vendedor\" class=\"waves-effect waves-red\">" +
                                                "<div>" +
                                                    "<i class=\"material-icons\">group</i>" +
                                                    "<span class=\"menu_title\">Vendedor</span>" +
                                                "</div>" +
                                            "</a>" +
                                        "</li>",
                            _GrupoUsuarios = "<li>" +
                                                "<a href=\"\\GrupoUsuarios\">" +
                                                    "<i class=\"material-icons\" style=\"position: absolute; right: auto; font-size: 22px;\">group</i>" +
                                                    "<span style=\"position: relative; left: 30px; font-size: 12px;\">Grupo de Usuários</span>" +
                                                "</a>" +
                                            "</li>",
                            _Logout = "<li>" +
                                        "<a href=\"\\Account\\Logout\">" +
                                             "<i class=\"material-icons\" style=\"position: absolute; right: auto; font-size: 22px;\">power_settings_new</i>" + 
                                             "<span style=\"position: relative; left: 30px;\">Logout</span>" +
                                        "</a>" +
                                    "</li>";
        #endregion

        public static void MontaMenu(List<PermissionamentoModel> permissionamento)
        {
            _menuUsuarioOrdem = new Dictionary<int, string>();
            _menuLateralOrdem = new Dictionary<int, string>();

            foreach (var permissao in permissionamento.Where(x => x.TipoPermissaoCodigo == TipoPermissionamento.Listar))
            {
                switch (permissao.ControllerCodigo)
                {
                    case PermissionamentoController.Auditoria:
                        SetMenuUsuario((int)permissao.ControllerCodigo, _Auditoria);
                        break;
                    case PermissionamentoController.Clientes:
                        SetMenuLateral((int)permissao.ControllerCodigo, _Clientes);
                        break;
                    case PermissionamentoController.Financeiro:
                        SetMenuLateral((int)permissao.ControllerCodigo, _Financeiro);
                        break;
                    case PermissionamentoController.Home:
                        SetMenuLateral((int)permissao.ControllerCodigo, _Home);
                        break;
                    case PermissionamentoController.Parametros:
                        SetMenuUsuario((int)permissao.ControllerCodigo, _Parametros);
                        break;
                    case PermissionamentoController.PedidoVenda:
                        SetMenuLateral((int)permissao.ControllerCodigo, _PedidoVenda);
                        break;
                    case PermissionamentoController.PlanoPagamento:
                        SetMenuLateral((int)permissao.ControllerCodigo, _PlanoPagamento);
                        break;
                    case PermissionamentoController.Produto:
                        SetMenuLateral((int)permissao.ControllerCodigo, _Produto);
                        break;
                    case PermissionamentoController.Usuarios:
                        SetMenuLateral((int)permissao.ControllerCodigo, _Usuarios);
                        break;
                    case PermissionamentoController.Vendedor:
                        SetMenuLateral((int)permissao.ControllerCodigo, _Vendedor);
                        break;
                    case PermissionamentoController.GrupoUsuarios:
                        SetMenuUsuario((int)permissao.ControllerCodigo, _GrupoUsuarios);
                        break;
                    case PermissionamentoController.Nulo:
                    default:
                        break;
                }
            }

            string menuLateral = string.Empty;
            foreach (var opcoes in _menuLateralOrdem.OrderBy(x => x.Key))
            {
                menuLateral += opcoes.Value;
            }
            _menuLateral = menuLateral;

            string menuUsuario = string.Empty;
            foreach (var opcoes in _menuUsuarioOrdem.OrderBy(x => x.Key))
            {
                if (string.IsNullOrEmpty(menuUsuario))
                    menuUsuario += opcoes.Value;
                else
                    menuUsuario += "<li class=\"divider\"></li>" + opcoes.Value;
            }
            menuUsuario += "<li class=\"divider\"></li>" + _Logout;
            _menuUsuario = menuUsuario;
        }

        private static void SetMenuUsuario(int ordem, string menuOpcao)
        {
            _menuUsuarioOrdem.Add(ordem, menuOpcao);
        }

        private static void SetMenuLateral(int ordem, string menuOpcao)
        {
            _menuLateralOrdem.Add(ordem, menuOpcao);
        }
    }
}