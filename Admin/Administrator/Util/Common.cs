﻿using Api.Models.POCO;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace System
{
    public static class Common
    {
        internal static NameValueCollection ToQueryString(this object Obj)
        {
            NameValueCollection query = new NameValueCollection();

            foreach (var property in Obj.GetType().GetProperties())
            {
                string key = property.Name;
                string value = property.GetValue(Obj).ToString();

                query.Add(key, value);
            }

            return query;
        }

        internal static string ToQueryStringSerialized(this object Obj)
        {
            string queryString = string.Empty;

            foreach (var property in Obj.GetType().GetProperties())
            {
                string key = property.Name;
                string value = (property.GetValue(Obj) ?? string.Empty).ToString();

                queryString += string.Format("{0}={1}&", key,value);
            }

            return queryString.Substring(0, (queryString.Length - 1));
        }

        internal static JQueryDataTableParam RequestToJQueryDataTableParam(this NameValueCollection param)
        {
            return new JQueryDataTableParam()
            {
                sEcho = param["draw"],
                iDisplayStart = Convert.ToInt32(param["start"]),
                iDisplayLength = Convert.ToInt32(param["length"]),
                sSearch = param["search[value]"],
                oSearch = param["order[0][dir]"],
                iSortingCols = Convert.ToInt32(param["order[0][column]"])
            };
        }
    }
}