﻿using Api.Enums;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Api.Models;
using Api.Models.POCO;

namespace Administrator.Util
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
    public class Permissao : ActionFilterAttribute
    {
        private UserModel _permissao;
        private PermissionamentoController _acessoController;
        private TipoPermissionamento _acessoOperacao;

        public Permissao(PermissionamentoController acessoController, TipoPermissionamento acessoOperacao)
        {
            this._acessoController = acessoController;
            this._acessoOperacao = acessoOperacao;
            this._permissao = (UserModel)HttpContext.Current.Session["UsuarioLogado"];
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool retorno = true;

            if(this._permissao == null)
                this._permissao = (UserModel)HttpContext.Current.Session["UsuarioLogado"];

            if (_acessoOperacao != TipoPermissionamento.Nulo && _acessoOperacao != TipoPermissionamento.Livre)
            {
                ApiManager cweb = new ApiManager((string)HttpContext.Current.Session[ConfigurationApiManager.TOKEN], ConfigurationApiManager.BASE_AUTH_WEB_SITE);
                retorno = (bool)cweb.Post<object>("Api/Auth/AutorizarAcesso", new AutorizacaoAcesso()
                {
                    GrupoUsuariosCodigo = _permissao.GrupoUsuariosCodigo,
                    Controller = _acessoController,
                    TipoPermissionamento = _acessoOperacao
                });
            }

            if (retorno)
            {
                base.OnActionExecuting(filterContext);
            }
            else
                filterContext.Result = new RedirectResult("~/Error/401");
        }
    }
}