﻿/*
    Script com funções para usos em classes da aplicação.
    As funções aqui são utilizadas em classes de TODA a aplicação, 
    caso execute alguma alteração por favor se certificar que nenhuma funcionalidade foi comprometida.
*/

// O método a seguir é usado em classes formata-currency 
// Ele garante que apenas valores numéricos entrem no input e coloca a máscara numérica 
$('input.formata-currency').on('keydown', function (e) {
    // tab, esc, enter
    if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
        // Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40) || 
        ($.inArray(e.keyCode, [8, 46]) !== -1)) { //||
        //(e.keyCode >= 40 && e.keyCode <= 50 && e.altKey === true && e.shiftKey === true)) {
        return;
    }

    e.preventDefault();

    var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
    var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

    var maxlength = $(this).attr('maxlength');
    var casasDecimais = typeof $(this).attr('casasdecimais') == 'undefined' ? 2 : $(this).attr('casasdecimais');
    var value = $(this).val();
    var clean = value.replace(/\./g, '').replace(/,/g, '').replace(/^0+/, '');

    var charCode = String.fromCharCode(e.keyCode);
    var p = $.inArray(charCode, a);
    var t = $.inArray(charCode, n);

    if (p !== -1 || t !== -1) {
        if (n[p])
            value = clean + n[p];
        else
            value = clean + n[t];

        if (value.length == 2) value = '0' + value;
        if (value.length == 1) value = '00' + value;

        var formatted = '';
        for (var i = 0; i < value.length; i++) {
            var sep = '';
            if (i == casasDecimais) sep = ',';
            if (i > (3 + (casasDecimais - 2)) && (i + 1 - (casasDecimais - 2)) % 3 == 0) sep = '.';
            formatted = value.substring(value.length - 1 - i, value.length - i) + sep + formatted;
        }

        $(this).val(formatted);
    }
    return;
});

//Abstração do método acima permitir apenas valores numéricos dentro do máximo permitido pelo campo
$('input.apenas-numeros').on('keydown', function (e) {
    // tab, esc, enter
    if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
        // Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40) ||
        ($.inArray(e.keyCode, [8, 46]) !== -1)) { //||
        //(e.keyCode >= 40 && e.keyCode <= 50 && e.altKey === true && e.shiftKey === true)) {
        return;
    }

    e.preventDefault();

    var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
    var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

    var maxlength = $(this).attr('maxlength');
    var value = $(this).val();
    var clean = value.replace(/\-/g, '').replace(/^0+/, '');

    var charCode = String.fromCharCode(e.keyCode);
    var p = $.inArray(charCode, a);
    var t = $.inArray(charCode, n);

    if (p !== -1 || t !== -1) {
        if (n[p])
            charCode = n[p];
        else
            charCode = n[t];
            
        if (maxlength !== '') {
            if (value.length <= (maxlength - 1)) {
                value = value + charCode;
            }
        } else {
            value = value + charCode;
        }

        $(this).val(value);
    }
    return;

});

// Configura todos os datepickers do sistema
$('.datepicker').pickadate({
    selectMonths: true,
    selectYears: 15,
    container: 'body',

    //Traduções
    // Strings and translations
    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    weekdaysShort: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
    today: 'Hoje',
    clear: 'Limpar',
    close: 'Fechar',
    closeOnSelect: true,
    autoClose: true,
    labelMonthNext: 'Próximo mês',
    labelMonthPrev: 'Mês Anterior',
    labelMonthSelect: 'Selecione um mês',
    labelYearSelect: 'Selecione um Ano',
    format: 'dd/mm/yyyy',
    editable: true,
    onRender: function () {
        $("th[class='picker__weekday'][title='Domingo']").html('Dom');
        $("th[class='picker__weekday'][title='Segunda']").html('Seg');
        $("th[class='picker__weekday'][title='Terça']").html('Ter');
        $("th[class='picker__weekday'][title='Quarta']").html('Qua');
        $("th[class='picker__weekday'][title='Quinta']").html('Qui');
        $("th[class='picker__weekday'][title='Sexta']").html('Sex');
        $("th[class='picker__weekday'][title='Sábado']").html('Sab');
    },
});

$('.datepicker').on('keydown', function (e) {
    var value = $(this).val();
    var clean = value.replace('/', '');

    // tab, esc, enter
    if ($.inArray(e.keyCode, [9, 27, 13]) !== -1 ||
        // Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40) ||
        ($.inArray(e.keyCode, [8, 46]) !== -1) ||
        (clean.length < 2 || clean.length > 2 && clean.length < 4 || clean.length > 4)) { //||
        //(e.keyCode >= 40 && e.keyCode <= 50 && e.altKey === true && e.shiftKey === true)) {
        return;
    }

    e.preventDefault();

    var a = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "`"];
    var n = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

    var charCode = String.fromCharCode(e.keyCode);
    var p = $.inArray(charCode, a);
    var t = $.inArray(charCode, n);

    if (p !== -1 || t !== -1) {
        if (n[p])
            value = clean + n[p];
        else
            value = clean + n[t];

        var formatted = '';
        switch(value.length)
        {
            case 3:
                {
                    formatted = clean.substring(0, 2) + '/' + charCode;
                } break;
            case 5:
                {
                    formatted = clean.substring(0, 2) + '/' + clean.substring(2, 4) + '/' + charCode;
                } break;
            default:
                {
                    formatted = $(this).val() + charCode;
                } break;
        }
        
        $(this).val(formatted);
    }
    return;
});

//Comando para quando o botão search for clicado, automaticamente seleciona o campo de pesquisa
$(".search-toggle").on('click', function () {
    if ($(".hiddensearch").css('display') !== 'block')
    {
        setTimeout(function () {
            $('input[type="search"]').get(0).focus();
        }, 1);
    }
});