﻿/*
    Script com funções para usos Gerais.
    As funções aqui são utilizadas em classes de TODA a aplicação, 
    caso execute alguma alteração por favor se certificar que nenhuma funcionalidade foi comprometida.
*/

function ValidarEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}