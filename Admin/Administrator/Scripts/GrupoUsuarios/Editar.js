var grupoUsuarios;
$(document).ready(function () {
    grupoUsuarios = new GrupoUsuarios();
    grupoUsuarios.AdicionarGrupoUsuarios(JSON.parse($("#Model").val()));
    $("#formGrupo").submit(function (event) {
        event.preventDefault();
        grupoUsuarios.SubmitGrupo("Editar", $("#Nome").val());
    });
    $('input[type="checkbox"]').on('change', function () {
        var controllerCodigo = this.id.split('_')[0];
        var tipoPermissionamento = this.id.split('_')[1];
        if ($(this).prop('checked')) {
            if (+controllerCodigo === 0) {
                if (+tipoPermissionamento === 0) {
                    MarcaCheckBoxes($('input[type="checkbox"]'));
                }
                else {
                    MarcaCheckBoxes($('input[type="checkbox"][id$="' + tipoPermissionamento + '"]'));
                }
            }
            else if (+tipoPermissionamento === 0) {
                MarcaCheckBoxes($('input[type="checkbox"][id^="' + controllerCodigo + '"]'));
            }
            else {
                MarcaCheckBoxes($(this));
            }
        }
        else {
            if (+controllerCodigo === 0) {
                if (+tipoPermissionamento === 0) {
                    DesmarcaCheckBoxes($('input[type="checkbox"]'));
                }
                else {
                    DesmarcaCheckBoxes($('input[type="checkbox"][id$="' + tipoPermissionamento + '"]'));
                }
            }
            else if (+tipoPermissionamento === 0) {
                DesmarcaCheckBoxes($('input[type="checkbox"][id^="' + controllerCodigo + '"]'));
            }
            else {
                DesmarcaCheckBoxes($(this));
            }
        }
    });
});
window.document.addEventListener('DOMContentLoaded', function () {
    grupoUsuarios.ObterPermissionamentos().forEach(function (permissionamento) {
        $("#" + permissionamento.ControllerCodigo + "_" + permissionamento.TipoPermissaoCodigo).attr('checked', 'checked');
    });
});
//# sourceMappingURL=Editar.js.map