﻿function MarcaCheckBoxes(selector: JQuery): void {
    selector.each(function (index: number, element: Element) {
        if (element.hasAttribute('checked')) {
            return;
        }

        element.setAttribute('checked', 'checked');
        grupoUsuarios.AdicionarPermissionamento(+element.id.split('_')[0], +element.id.split('_')[1]);
    });
}

function DesmarcaCheckBoxes(selector: JQuery): void {
    selector.each(function (index: number, element: Element) {
        if (element.hasAttribute('checked')) {
            element.removeAttribute('checked');
            grupoUsuarios.ExcluirPermissionamento(+element.id.split('_')[0], +element.id.split('_')[1]);
        }
    });
}