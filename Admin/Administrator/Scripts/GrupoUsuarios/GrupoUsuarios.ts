﻿class GrupoUsuariosModel {
    Codigo: number;
    Nome: string;
    Permissionamento: Array<PermissionamentoModel>;
}

interface IGrupoUsuarios {
    AdicionarGrupoUsuarios(grupo: GrupoUsuariosModel): void;
    AdicionarPermissionamento(controllerCodigo: number, tipoPermissaoCodigo: number): void;
    ExcluirPermissionamento(controllerCodigo: number, tipoPermissaoCodigo: number): void;
    ObterPermissionamentos(): Array<PermissionamentoModel>;
    SubmitGrupo(action: string, nomeGrupo: string): void;
}

class GrupoUsuarios implements IGrupoUsuarios {
    private grupo: GrupoUsuariosModel;

    constructor()
    {
        this.grupo = new GrupoUsuariosModel();
        this.grupo.Permissionamento = new Array<PermissionamentoModel>();
    }

    public AdicionarPermissionamento(controllerCodigo: number, tipoPermissaoCodigo: number): void {
        if (controllerCodigo !== 0 && tipoPermissaoCodigo !== 0) {
            var permissionamento = new PermissionamentoModel();
            permissionamento.GrupoUsuarioCodigo = typeof (this.grupo.Codigo) === "undefined" ? 0 : this.grupo.Codigo;
            permissionamento.PermissionamentoCodigo = 0;
            permissionamento.ControllerCodigo = controllerCodigo;
            permissionamento.TipoPermissaoCodigo = tipoPermissaoCodigo;

            this.grupo.Permissionamento.push(permissionamento);
        }
    }

    public ExcluirPermissionamento(controllerCodigo: number, tipoPermissaoCodigo: number): void {
        var permissao = this.grupo.Permissionamento.filter(function (permissionamento: PermissionamentoModel) {
            return permissionamento.ControllerCodigo === controllerCodigo && permissionamento.TipoPermissaoCodigo === tipoPermissaoCodigo;
        });

        var permissionamentoAtualizado = this.grupo.Permissionamento.filter(function (permissionamento: PermissionamentoModel) {
            return permissionamento !== permissao[0];
        });

        this.grupo.Permissionamento = [];
        this.grupo.Permissionamento = permissionamentoAtualizado;
    }

    public AdicionarGrupoUsuarios(grupo: GrupoUsuariosModel): void {
        this.grupo = grupo;
    }

    public ObterPermissionamentos(): Array<PermissionamentoModel> {
        return this.grupo.Permissionamento;
    }

    public SubmitGrupo(action: string, nomeGrupo: string): void {
        this.grupo.Nome = nomeGrupo;

        $.post(window.location.origin + '/GrupoUsuarios/' + action, this.grupo)
            .done(function (retorno) {
                if (retorno.Status) {
                    Materialize.toast(retorno.Mensagem, 3000, "rounded");
                    window.location.href = window.location.origin + '/GrupoUsuarios';
                }
            })
            .fail(function () {
                Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
            });
    }

    private ObterPermissionamento(controllerCodigo: number, tipoPermissaoCodigo: number): PermissionamentoModel {
        var perm = $.grep(this.grupo.Permissionamento, function (permissionamento) {
            return permissionamento.ControllerCodigo === controllerCodigo && permissionamento.TipoPermissaoCodigo === tipoPermissaoCodigo;
        });

        return perm[0];
    }
}