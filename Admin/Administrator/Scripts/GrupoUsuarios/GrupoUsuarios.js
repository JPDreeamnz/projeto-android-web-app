var GrupoUsuariosModel = (function () {
    function GrupoUsuariosModel() {
    }
    return GrupoUsuariosModel;
}());
var GrupoUsuarios = (function () {
    function GrupoUsuarios() {
        this.grupo = new GrupoUsuariosModel();
        this.grupo.Permissionamento = new Array();
    }
    GrupoUsuarios.prototype.AdicionarPermissionamento = function (controllerCodigo, tipoPermissaoCodigo) {
        if (controllerCodigo !== 0 && tipoPermissaoCodigo !== 0) {
            var permissionamento = new PermissionamentoModel();
            permissionamento.GrupoUsuarioCodigo = typeof (this.grupo.Codigo) === "undefined" ? 0 : this.grupo.Codigo;
            permissionamento.PermissionamentoCodigo = 0;
            permissionamento.ControllerCodigo = controllerCodigo;
            permissionamento.TipoPermissaoCodigo = tipoPermissaoCodigo;
            this.grupo.Permissionamento.push(permissionamento);
        }
    };
    GrupoUsuarios.prototype.ExcluirPermissionamento = function (controllerCodigo, tipoPermissaoCodigo) {
        var permissao = this.grupo.Permissionamento.filter(function (permissionamento) {
            return permissionamento.ControllerCodigo === controllerCodigo && permissionamento.TipoPermissaoCodigo === tipoPermissaoCodigo;
        });
        var permissionamentoAtualizado = this.grupo.Permissionamento.filter(function (permissionamento) {
            return permissionamento !== permissao[0];
        });
        this.grupo.Permissionamento = [];
        this.grupo.Permissionamento = permissionamentoAtualizado;
    };
    GrupoUsuarios.prototype.AdicionarGrupoUsuarios = function (grupo) {
        this.grupo = grupo;
    };
    GrupoUsuarios.prototype.ObterPermissionamentos = function () {
        return this.grupo.Permissionamento;
    };
    GrupoUsuarios.prototype.SubmitGrupo = function (action, nomeGrupo) {
        this.grupo.Nome = nomeGrupo;
        $.post(window.location.origin + '/GrupoUsuarios/' + action, this.grupo)
            .done(function (retorno) {
            if (retorno.Status) {
                Materialize.toast(retorno.Mensagem, 3000, "rounded");
                window.location.href = window.location.origin + '/GrupoUsuarios';
            }
        })
            .fail(function () {
            Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
        });
    };
    GrupoUsuarios.prototype.ObterPermissionamento = function (controllerCodigo, tipoPermissaoCodigo) {
        var perm = $.grep(this.grupo.Permissionamento, function (permissionamento) {
            return permissionamento.ControllerCodigo === controllerCodigo && permissionamento.TipoPermissaoCodigo === tipoPermissaoCodigo;
        });
        return perm[0];
    };
    return GrupoUsuarios;
}());
//# sourceMappingURL=GrupoUsuarios.js.map