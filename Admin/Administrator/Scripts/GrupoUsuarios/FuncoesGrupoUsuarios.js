function MarcaCheckBoxes(selector) {
    selector.each(function (index, element) {
        if (element.hasAttribute('checked')) {
            return;
        }
        element.setAttribute('checked', 'checked');
        grupoUsuarios.AdicionarPermissionamento(+element.id.split('_')[0], +element.id.split('_')[1]);
    });
}
function DesmarcaCheckBoxes(selector) {
    selector.each(function (index, element) {
        if (element.hasAttribute('checked')) {
            element.removeAttribute('checked');
            grupoUsuarios.ExcluirPermissionamento(+element.id.split('_')[0], +element.id.split('_')[1]);
        }
    });
}
//# sourceMappingURL=FuncoesGrupoUsuarios.js.map