﻿jQuery(document).ready(function () {
    InitTableAjax(
        "#tabelaPedido",
        "../PedidoVenda/ObterListagem",
        "GET",
        [
            { "data": "Codigo" },
            { "data": "Nome" },
            { "data": "DataPedido" },
            { "data": "Valor" },
            { "data": "Botoes" }
        ]
    );
});