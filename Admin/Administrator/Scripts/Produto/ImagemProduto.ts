﻿class ImagemProdutoModel {
    CodigoProduto: number;
    Sequencial: number;
    CaminhoPasta: string;
    Extensao: string;
    Situacao: EstadoEntidade;
}

//POCO de imagens
class Imagens {
    Sequencial: number;
    Imagem: string;
}

enum Estado {
    COM_IMAGENS,
    SEM_IMAGENS
}

enum EstadoEntidade {
    INSERIDO = 1,
    EDITADO,
    EXCLUIDO,
    VISUALIZADO
}

interface IOperacoesImagemProduto {
    RemoveImagensDropzone(selector: JQuery): void;
    RemoveCarousel(selector: JQuery): void;
    RemoveImagemCarousel(carouselSelector: JQuery, botoesCarouselSelector: JQuery, codigoProduto: number, sequencial: number): void;
    AtualizaImagensCarousel(carouselSelector: JQuery, botoesCarouselSelector: JQuery): void;
    AdicionarImagem(imagem: ImagemProdutoModel): void;
    ObtemListaImagens(): Array<ImagemProdutoModel>;
    AdicionarListaImagens(imagens: Array<ImagemProdutoModel>): void;
    ObtemImagem(codigoProduto: number, sequencial: number): ImagemProdutoModel;
    ObtemUrl(): string;
    ObtemToken(): string;
}

class ImagemProduto implements IOperacoesImagemProduto {
    private url: string;
    private token: string;
    private imagensProduto: Array<ImagemProdutoModel>;

    constructor(url: string, token: string) {
        this.imagensProduto = new Array<ImagemProdutoModel>();
        this.url = url;
        this.token = token;
    }

    public RemoveImagensDropzone(selector: JQuery): void {
        selector.find('.dz-complete').remove();
        selector.removeClass('dz-started');
    }

    public RemoveCarousel(selector: JQuery): void {
        selector.html('');
        selector.removeClass('initialized');
    }

    public RemoveImagemCarousel(carouselSelector: JQuery, botoesCarouselSelector: JQuery, codigoProduto: number, sequencial: number): void {
        //Este bloco remove as imagens da listagem

        var index = this.imagensProduto.indexOf(this.ObtemImagem(codigoProduto, sequencial));
        this.imagensProduto[index].Situacao = EstadoEntidade.EXCLUIDO;
        this.AtualizaImagensCarousel(carouselSelector, botoesCarouselSelector)
    }

    public AtualizaImagensCarousel(carouselSelector: JQuery, botoesCarouselSelector: JQuery): void {
        if (this.imagensProduto.length > 0) {
            var request = {
                imagens: JSON.stringify(this.imagensProduto),
                token: this.token
            };
            $.post(this.url + 'api/ImagemProduto/ListaImagens', request)
                .done(function (data: Array<Imagens>) {
                    carouselSelector.html('');

                    if (data.length > 0) {
                        var htmlCarousel = '';
                        $.each(data, function (index: number, objeto: Imagens) {
                            htmlCarousel += MontaImagem(objeto.Sequencial, objeto.Imagem);
                        });

                        carouselSelector.html(htmlCarousel);
                        if (htmlCarousel !== '' && $("li .active").attr('href') === "#Imagens") {
                            carouselSelector.carousel({
                                dist: 0,
                                shift: 0,
                                padding: 0,
                                full_width: 1 //true
                            });

                            MostraEscondeBotoesCarousel(botoesCarouselSelector, Estado.COM_IMAGENS);
                        }
                    }
                    else {
                        carouselSelector.html(LimpaCarousel(carouselSelector));
                        MostraEscondeBotoesCarousel(botoesCarouselSelector, Estado.SEM_IMAGENS);
                    }
                })
                .fail(function () {
                    Materialize.toast("Não foi possivel fazer a requisição para obter as imagens.", 3000);
                });
        } else {
            carouselSelector.html(LimpaCarousel(carouselSelector));
            MostraEscondeBotoesCarousel(botoesCarouselSelector, Estado.SEM_IMAGENS);
        }
    }

    public AdicionarImagem(imagem: ImagemProdutoModel): void {
        this.imagensProduto.push(imagem);
    }

    public ObtemListaImagens(): Array<ImagemProdutoModel> {
        return this.imagensProduto;
    }

    public AdicionarListaImagens(imagens: Array<ImagemProdutoModel>): void {
        this.imagensProduto = imagens;
    }

    public ObtemImagem(codigoProduto: number, sequencial: number): ImagemProdutoModel {
        var img = $.grep(this.imagensProduto, function (imagem) {
            return imagem.CodigoProduto === codigoProduto && imagem.Sequencial === sequencial;
        });

        return img[0];
    }

    public ObtemUrl(): string {
        return this.url;
    }

    public ObtemToken(): string {
        return this.token;
    }
}

function LimpaCarousel(carouselSelector: JQuery): string {
    return '<div class="centro-pagina" style="text-align: center; vertical-align: middle;line-height: 600px;">' +
        'Não existe imagens anexadas.' +
        '</div>';
}

function MontaImagem(sequencial: number, imagem: string): string {
    return '<a class="carousel-item" href="#' + sequencial + '!" id="' + sequencial + '" style="margin: 0 auto;"><img class="responsive-img" src="data:image;base64,' + imagem + '"></a>';
}

function MostraEscondeBotoesCarousel(selector: JQuery, tipoCarousel: Estado): void {
    if (tipoCarousel === Estado.COM_IMAGENS) {
        selector.attr('style', 'display: normal');
    } else {
        selector.attr('style', 'display: none');
    }

}