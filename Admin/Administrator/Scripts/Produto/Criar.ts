﻿var imagemProduto: ImagemProduto;
var minImageWidth: number;
var minImageHeight: number;

$(document).ready(function () {
    imagemProduto = new ImagemProduto($("#url").val(), $("#TK").val());
    minImageHeight = 600;
    minImageWidth = 800;

    Dropzone.options.UploadImagemDrop = {
        maxFileSize: 5,
        url: imagemProduto.ObtemUrl() + 'api/ImagemProduto/SalvarImagem?codigoProduto=' + $("#Codigo").val() + '&token=' + imagemProduto.ObtemToken(),
        method: 'POST',
        dictDefaultMessage: "Arraste imagens aqui ou clique para incluir. <spam style='font-size: 70%'><br /><i>(Resolução de imagem mínimo de 800x600. Tamanho máximo de 5mb.)</i>",
        dictInvalidFileType: "O arquivo selecionado não é uma imagem.",
        acceptedFiles: "image/*",
        init: function () {
            this.on("thumbnail", function (file) {
                if (file.width < minImageWidth || file.height < minImageHeight) {
                    file.rejectDimensions();
                } else {
                    file.acceptDimensions();
                }
            });
        },
        accept: function (file, done) {
            file.acceptDimensions = done;
            file.rejectDimensions = function () { done("Imagem muito pequena."); }
        },
        success: function (retorno: any) {
            imagemProduto.AdicionarImagem(JSON.parse(retorno.xhr.response));
        },
        queuecomplete: function () {
            var carousel = $("#carousel-produto");
            var botoesCarousel = $("#botoesCarousel");
            var codigoProduto = $("#Codigo").val();

            imagemProduto.RemoveCarousel(carousel);
            imagemProduto.AtualizaImagensCarousel(carousel, botoesCarousel);
        }
    };

    $("#Situacao").material_select();

    $("#carousel-retorna").on('click', function () {
        $("#carousel-produto").carousel('prev');
    });

    $("#carousel-avanca").on('click', function () {
        $("#carousel-produto").carousel('next');
    });

    $("#carousel-remove").on('click', function () {
        $('[class="carousel-item"]').each(function () {
            if ($(this).css('z-index') === '0') {
                imagemProduto.RemoveCarousel($("#carousel-produto"));
                imagemProduto.RemoveImagemCarousel($("#carousel-produto"), $("#botoesCarousel"), +$("#Codigo").val(), +$(this).attr('id'))
            }
        })
    });

    $("#ImagensTab").on('click', function () {
        if ($("li .active").attr('href') === "#InformacoesGerais" &&
            $("#carousel-produto").hasClass('initialized') === false &&
            $("#carousel-produto").children().hasClass('centro-pagina') === false &&
            $("#carousel-produto").children().length > 0)
        {
            //A transição de tabs buga o uso do carousel. 
            //Para a animação do tab não interromper é aguardado 10ms e disparado o carousel.
            setTimeout(function () {
                $("#carousel-produto").carousel({
                    dist: 0,
                    shift: 0,
                    padding: 0,
                    full_width: 1 //true
                });

                MostraEscondeBotoesCarousel($("#botoesCarousel"), Estado.COM_IMAGENS);
            }, 10);
        }
    });
    
    $("#formProduto").on('submit', function (event) {
        event.preventDefault();
        SubmitProduto('Criar', imagemProduto.ObtemListaImagens());
    });
});

function ModalAnexarImagem() {
    imagemProduto.RemoveImagensDropzone($("#UploadImagemDrop"));
    $("#modalUploadImagem").openModal();
}