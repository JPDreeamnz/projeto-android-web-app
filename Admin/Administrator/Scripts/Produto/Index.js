jQuery(document).ready(function () {
    InitTableAjax("#tabelaProduto", "../Produto/ObterListagem", "GET", [
        { "data": "Codigo" },
        { "data": "Nome" },
        { "data": "Marca" },
        { "data": "ValorVenda" },
        { "data": "QuantidadeEstoque" },
        { "data": "Status" },
        { "data": "Botoes" }
    ]);
});
function ExcluirProduto(codigo) {
    $.blockUI({ message: 'Removendo registro...aguarde!' });
    $.ajax({
        type: "POST",
        url: window.location.origin + '/Produto/Excluir',
        data: { codigo: codigo },
        success: function (data) {
            if (data.Status) {
                Materialize.toast(data.Mensagem, 3000, "rounded");
                setTimeout(function () {
                    window.location.reload();
                }, 1500);
            }
        },
        dataType: "json"
    });
}
//# sourceMappingURL=Index.js.map