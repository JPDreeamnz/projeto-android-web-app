﻿class ProdutoModel {
    Codigo: number;
    Nome: string;
    Unidade: string;
    CodigoBarras: string;
    CodigoFabrica: string;
    CodigoOriginal: string;
    Cor: string;
    Tamanho: string;
    Marca: string;
    ValorVenda: number;
    QuantidadeEstoque: number;
    Status: number;
    ImagensProduto: Array<ImagemProdutoModel>;
}

function SubmitProduto(action: string, imagens: Array<ImagemProdutoModel>) {
    var obj = new ProdutoModel();
    obj.Codigo = $("#Codigo").val();
    obj.CodigoBarras = $("#CodigoBarras").val();
    obj.CodigoFabrica = $("#CodigoFabrica").val();
    obj.CodigoOriginal = $("#CodigoOriginal").val();
    obj.Cor = $("#Cor").val();
    obj.Marca = $("#Marca").val();
    obj.Nome = $("#Nome").val();
    obj.QuantidadeEstoque = $("#QuantidadeEstoque").val().replace(/\./g, '');
    obj.Status = $("#Situacao").val();
    obj.Tamanho = $("#Tamanho").val();
    obj.Unidade = $("#Unidade").val();
    obj.ValorVenda = $("#ValorVenda").val().replace(/\./g, '');

    obj.ImagensProduto = new Array<ImagemProdutoModel>();
    obj.ImagensProduto = imagens;

    $.post(window.location.origin + '/Produto/' + action, obj)
        .done(function (retorno) {
            if (retorno.Status) {
                Materialize.toast(retorno.Mensagem, 3000, "rounded");
                window.location.href = window.location.origin + '/Produto';
            }
        })
        .fail(function () {
            Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
        });
}