var ImagemProdutoModel = (function () {
    function ImagemProdutoModel() {
    }
    return ImagemProdutoModel;
}());
//POCO de imagens
var Imagens = (function () {
    function Imagens() {
    }
    return Imagens;
}());
var Estado;
(function (Estado) {
    Estado[Estado["COM_IMAGENS"] = 0] = "COM_IMAGENS";
    Estado[Estado["SEM_IMAGENS"] = 1] = "SEM_IMAGENS";
})(Estado || (Estado = {}));
var EstadoEntidade;
(function (EstadoEntidade) {
    EstadoEntidade[EstadoEntidade["INSERIDO"] = 1] = "INSERIDO";
    EstadoEntidade[EstadoEntidade["EDITADO"] = 2] = "EDITADO";
    EstadoEntidade[EstadoEntidade["EXCLUIDO"] = 3] = "EXCLUIDO";
    EstadoEntidade[EstadoEntidade["VISUALIZADO"] = 4] = "VISUALIZADO";
})(EstadoEntidade || (EstadoEntidade = {}));
var ImagemProduto = (function () {
    function ImagemProduto(url, token) {
        this.imagensProduto = new Array();
        this.url = url;
        this.token = token;
    }
    ImagemProduto.prototype.RemoveImagensDropzone = function (selector) {
        selector.find('.dz-complete').remove();
        selector.removeClass('dz-started');
    };
    ImagemProduto.prototype.RemoveCarousel = function (selector) {
        selector.html('');
        selector.removeClass('initialized');
    };
    ImagemProduto.prototype.RemoveImagemCarousel = function (carouselSelector, botoesCarouselSelector, codigoProduto, sequencial) {
        //Este bloco remove as imagens da listagem
        var index = this.imagensProduto.indexOf(this.ObtemImagem(codigoProduto, sequencial));
        this.imagensProduto[index].Situacao = EstadoEntidade.EXCLUIDO;
        this.AtualizaImagensCarousel(carouselSelector, botoesCarouselSelector);
    };
    ImagemProduto.prototype.AtualizaImagensCarousel = function (carouselSelector, botoesCarouselSelector) {
        if (this.imagensProduto.length > 0) {
            var request = {
                imagens: JSON.stringify(this.imagensProduto),
                token: this.token
            };
            $.post(this.url + 'api/ImagemProduto/ListaImagens', request)
                .done(function (data) {
                carouselSelector.html('');
                if (data.length > 0) {
                    var htmlCarousel = '';
                    $.each(data, function (index, objeto) {
                        htmlCarousel += MontaImagem(objeto.Sequencial, objeto.Imagem);
                    });
                    carouselSelector.html(htmlCarousel);
                    if (htmlCarousel !== '' && $("li .active").attr('href') === "#Imagens") {
                        carouselSelector.carousel({
                            dist: 0,
                            shift: 0,
                            padding: 0,
                            full_width: 1 //true
                        });
                        MostraEscondeBotoesCarousel(botoesCarouselSelector, Estado.COM_IMAGENS);
                    }
                }
                else {
                    carouselSelector.html(LimpaCarousel(carouselSelector));
                    MostraEscondeBotoesCarousel(botoesCarouselSelector, Estado.SEM_IMAGENS);
                }
            })
                .fail(function () {
                Materialize.toast("Não foi possivel fazer a requisição para obter as imagens.", 3000);
            });
        }
        else {
            carouselSelector.html(LimpaCarousel(carouselSelector));
            MostraEscondeBotoesCarousel(botoesCarouselSelector, Estado.SEM_IMAGENS);
        }
    };
    ImagemProduto.prototype.AdicionarImagem = function (imagem) {
        this.imagensProduto.push(imagem);
    };
    ImagemProduto.prototype.ObtemListaImagens = function () {
        return this.imagensProduto;
    };
    ImagemProduto.prototype.AdicionarListaImagens = function (imagens) {
        this.imagensProduto = imagens;
    };
    ImagemProduto.prototype.ObtemImagem = function (codigoProduto, sequencial) {
        var img = $.grep(this.imagensProduto, function (imagem) {
            return imagem.CodigoProduto === codigoProduto && imagem.Sequencial === sequencial;
        });
        return img[0];
    };
    ImagemProduto.prototype.ObtemUrl = function () {
        return this.url;
    };
    ImagemProduto.prototype.ObtemToken = function () {
        return this.token;
    };
    return ImagemProduto;
}());
function LimpaCarousel(carouselSelector) {
    return '<div class="centro-pagina" style="text-align: center; vertical-align: middle;line-height: 600px;">' +
        'Não existe imagens anexadas.' +
        '</div>';
}
function MontaImagem(sequencial, imagem) {
    return '<a class="carousel-item" href="#' + sequencial + '!" id="' + sequencial + '" style="margin: 0 auto;"><img class="responsive-img" src="data:image;base64,' + imagem + '"></a>';
}
function MostraEscondeBotoesCarousel(selector, tipoCarousel) {
    if (tipoCarousel === Estado.COM_IMAGENS) {
        selector.attr('style', 'display: normal');
    }
    else {
        selector.attr('style', 'display: none');
    }
}
//# sourceMappingURL=ImagemProduto.js.map