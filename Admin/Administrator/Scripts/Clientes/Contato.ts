﻿class Contato {
    ContatoID: number;
    TipoContato: number;
    DDD: number;
    Telefone: string;
    Email: string;
    NovoContato: boolean;
}