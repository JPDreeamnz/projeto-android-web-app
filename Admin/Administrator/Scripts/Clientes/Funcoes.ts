﻿function CriarEndereco() {
    LimparModalEndereco();
    $("#subtituloModalEndereco").html('');
    $("#subtituloModalEndereco").html('Criar Endereço');
    $("#modalEndereco").openModal();
}

function EditarEndereco(EnderecoID) {
    LimparModalEndereco();
    $("#subtituloModalEndereco").html('');
    $("#subtituloModalEndereco").html('Editar Endereço');

    var end = cliente.ObtemEndereco(EnderecoID);

    $("#EnderecoID").val(end.EnderecoID);
    $("#CEP").val(end.CEP);
    $("#Endereco").val(end.Endereco);
    $("#Numero").val(end.Numero);
    $("#Bairro").val(end.Bairro);
    $("#Cidade").val(end.Cidade);
    $("#UF").val(end.Estado);
    $("#Complemento").val(end.Complemento);
    
    //Conjunto de comandos necessários para evitar bug de labels
    $("#CEP").siblings('label').addClass('active');
    $("#Endereco").siblings('label').addClass('active');
    $("#Numero").siblings('label').addClass('active');
    $("#Bairro").siblings('label').addClass('active');
    $("#Cidade").siblings('label').addClass('active');
    $("#UF").siblings('label').addClass('active');
    $("#Complemento").siblings('label').addClass('active');

    //Executa o trigger do dropdown
    $("#UF").material_select();
    $("#UF").trigger('change');
    
    $("#modalEndereco").openModal();
}

function SalvarEndereco() {
    if ($("#EnderecoID").val() === null || $("#EnderecoID").val() === "") {

        cliente.CriarEndereco(
            0,
            $("#CEP").val(),
            $("#Endereco").val(),
            $("#Numero").val(),
            $("#Bairro").val(),
            $("#Cidade").val(),
            $("#UF").val(),
            $("#Complemento").val()
        );

    } else {
        cliente.EditarEndereco(
            $("#EnderecoID").val(),
            $("#CEP").val(),
            $("#Endereco").val(),
            $("#Numero").val(),
            $("#Bairro").val(),
            $("#Cidade").val(),
            $("#UF").val(),
            $("#Complemento").val()
        );
    }

    LimparModalEndereco();
    AtualizaTabelaEndereco();
    $("#modalEndereco").closeModal();
}

function ExcluirEndereco(EnderecoID) {
    cliente.ExcluirEndereco(EnderecoID);
    AtualizaTabelaEndereco();
}

function LimparModalEndereco() {
    $("#EnderecoID").val('');
    $("#CEP").val('');
    $("#Endereco").val('');
    $("#Numero").val('');
    $("#Bairro").val('');
    $("#Cidade").val('');
    $("#UF").val('');
    $("#Complemento").val('');
}

function CriarContato() {
    LimparModalContato();
    $("#subtituloModalContato").html('');
    $("#subtituloModalContato").html('Criar Contato');
    $("#modalContato").openModal();
}

function EditarContato(ContatoID) {
    LimparModalContato();

    var ctto = cliente.ObtemContato(ContatoID);

    $("#ContatoID").val(ctto.ContatoID);
    $("#DDD").val(ctto.DDD);
    $("#Email").val(ctto.Email);
    $("#Telefone").val(ctto.Telefone);
    $("#TipoContato").val(ctto.TipoContato);

    //Conjunto de comandos necessários para evitar bug de labels
    $("#labelDDD").addClass('active');
    $("#labelEmail").addClass('active');
    $("#labelTelefone").addClass('active');
    $("#labelTipoContato").addClass('active');

    //Executa o trigger do dropdown
    $("#TipoContato").material_select();
    $("#TipoContato").trigger('change');
        
    $("#subtituloModalContato").html('');
    $("#subtituloModalContato").html('Editar Contato');
    $("#modalContato").openModal();
}

function SalvarContato() {
    if ($("#TipoContato").val() === "3" && $("#Email").hasClass('invalid') === true) {
        Materialize.toast("O E-mail é inválido.", 2000);
        return;
    }

    if ($("#ContatoID").val() === null || $("#ContatoID").val() === "") {
        cliente.CriarContato(
            0,
            $("#TipoContato").val(),
            $("#DDD").val(),
            $("#Telefone").val(),
            $("#Email").val()
        );
    } else {
        cliente.EditarContato(
            $("#ContatoID").val(),
            $("#TipoContato").val(),
            $("#DDD").val(),
            $("#Telefone").val(),
            $("#Email").val()
        );
    }

    LimparModalContato();
    AtualizaTabelaContato();
    $("#modalContato").closeModal();
}

function ExcluirContato(ContatoID) {
    cliente.ExcluirContato(ContatoID);
    AtualizaTabelaContato();
}

function LimparModalContato() {
    $("#ContatoID").val('');
    $("#DDD").val('');
    $("#Email").val('');
    $("#Telefone").val('');
    $("#TipoContato").val('1');
}

function AtualizaTabelaContato() {
    var html = '';
    if (cliente.Contato.length > 0) {
        for (var index in cliente.Contato) {
            var contato = cliente.Contato[index];
            var tipoContato = (contato.TipoContato == 1) ? "Telefone Fixo" : (contato.TipoContato == 2) ? "Telefone Celular" : "E-mail";
            html += '<tr>' +
                '<td>' + tipoContato + '</td>' +
                '<td>' + (contato.DDD !== null ? contato.DDD : "") + '</td>' +
                '<td>' + (contato.Telefone !== null ? contato.Telefone : "") + '</td>' +
                '<td>' + (contato.Email !== null ? contato.Email : "") + '</td>' +
                '<td>' +
                '<a id="ExcluirContato" class="icone-clicavel" value="' + contato.ContatoID + '" onclick="ExcluirContato(' + contato.ContatoID + ')"> <i class="material-icons" >&#xE872; </i></a>' +
                '<a id="EditarContato" class="icone-clicavel" value="' + contato.ContatoID + '" onclick="EditarContato(' + contato.ContatoID + ')" > <i class="material-icons" >&#xE254; </i></a>' +
                '</td>'
            '</tr>';
        }
    } else {
        html += '<tr>' +
            '<td>' +
            'Nenhum Contato Cadastrado' +
            '</td>' +
            '<td> </td>' +
            '<td> </td>' +
            '<td> </td>' +
            '<td> </td>' +
            '< /tr>';
    }

    $("#innerTableContato").html('');
    $("#innerTableContato").html(html);
}

function AtualizaTabelaEndereco() {
    var html = '';
    if (cliente.Endereco.length > 0) {
        for (var index in cliente.Endereco) {
            var endereco = cliente.Endereco[index];
            html += '<tr>' +
                '<td>' + endereco.Endereco + '</td>' +
                '<td>' + endereco.Numero + '</td>' +
                '<td>' + endereco.Bairro + '</td>' +
                '<td>' + endereco.Cidade + '</td>' +
                '<td>' + endereco.Estado + '</td>' +
                '<td>' +
                '<a id="ExcluirEndereco" class="icone-clicavel" value="' + endereco.EnderecoID + '" onclick="ExcluirEndereco(' + endereco.EnderecoID + ')"> <i class="material-icons" >&#xE872; </i></a>' +
                '<a id="EditarEndereco" class="icone-clicavel" value="' + endereco.EnderecoID + '" onclick="EditarEndereco(' + endereco.EnderecoID + ')"> <i class="material-icons" >&#xE254; </i></a>' +
                '</td>' +
                '</tr>';
        }
    } else {
        html += '<tr>' +
            '<td>' +
            'Nenhum Endereço Cadastrado' +
            '</td>' +
            '<td > </td>' +
            '<td > </td>' +
            '<td > </td>' +
            '<td > </td>' +
            '<td > </td>' +
            '< /tr>';
    }

    $("#innerTableEndereco").html('');
    $("#innerTableEndereco").html(html);
}