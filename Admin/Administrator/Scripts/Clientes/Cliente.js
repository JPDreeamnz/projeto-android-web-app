var Cliente = (function () {
    function Cliente() {
    }
    Cliente.prototype.ObtemCliente = function () {
        var model = new ClienteModel();
        model.Codigo = this.Codigo;
        model.Documento = +this.Documento;
        model.InscricaoEstadual = this.InscricaoEstadual;
        model.LimiteCredito = this.LimiteCredito;
        model.Nome = this.Nome;
        model.Observacao = this.Observacao;
        model.StatusCliente = +this.Situacao;
        model.TipoDocumento = this.TipoDocumento;
        model.Endereco = [];
        for (var index in this.Endereco) {
            model.Endereco.push(this.Endereco[index]);
            if (model.Endereco[index].NovoEndereco == true) {
                model.Endereco[index].EnderecoID = 0;
            }
        }
        model.Contato = [];
        for (var index in this.Contato) {
            model.Contato.push(this.Contato[index]);
            if (model.Contato[index].NovoContato == true) {
                model.Contato[index].ContatoID = 0;
            }
        }
        return model;
    };
    Cliente.prototype.NovoEnderecoID = function () {
        var maior = 0;
        for (var index in this.Endereco) {
            if (this.Endereco[index].EnderecoID > maior) {
                maior = this.Endereco[index].EnderecoID;
            }
        }
        return (maior + 1);
    };
    Cliente.prototype.NovoContatoID = function () {
        var maior = 0;
        for (var index in this.Contato) {
            if (this.Contato[index].ContatoID > maior) {
                maior = this.Contato[index].ContatoID;
            }
        }
        return (maior + 1);
    };
    //Região Endereço
    Cliente.prototype.ObtemEndereco = function (enderecoID) {
        var end = $.grep(cliente.Endereco, function (endereco) {
            return endereco.EnderecoID === +enderecoID;
        });
        return end[0];
    };
    Cliente.prototype.CriarEndereco = function (enderecoID, cep, endereco, numero, bairro, cidade, estado, complemento) {
        var obj = new Endereco();
        obj.EnderecoID = enderecoID !== 0 ? enderecoID : this.NovoEnderecoID();
        obj.CEP = cep;
        obj.Endereco = endereco;
        obj.Numero = numero;
        obj.Bairro = bairro;
        obj.Cidade = cidade;
        obj.Estado = estado;
        obj.Complemento = complemento;
        obj.NovoEndereco = true;
        this.Endereco.push(obj);
    };
    Cliente.prototype.EditarEndereco = function (enderecoID, cep, endereco, numero, bairro, cidade, estado, complemento) {
        var index = cliente.Endereco.indexOf(this.ObtemEndereco(enderecoID));
        cliente.Endereco[index].CEP = cep;
        cliente.Endereco[index].Endereco = endereco;
        cliente.Endereco[index].Numero = numero;
        cliente.Endereco[index].Bairro = bairro;
        cliente.Endereco[index].Cidade = cidade;
        cliente.Endereco[index].Estado = estado;
        cliente.Endereco[index].Complemento = complemento;
    };
    Cliente.prototype.ExcluirEndereco = function (EnderecoID) {
        var enderecosAtualizados = cliente.Endereco.filter(function (endereco) {
            return endereco.EnderecoID !== +EnderecoID;
        });
        this.Endereco = [];
        this.Endereco = enderecosAtualizados;
    };
    //Fim Região Endereço
    //Inicio Região Contato
    Cliente.prototype.ObtemContato = function (contatoID) {
        var ctto = $.grep(cliente.Contato, function (contato) {
            return contato.ContatoID === +contatoID;
        });
        return ctto[0];
    };
    Cliente.prototype.CriarContato = function (contatoID, tipoContato, ddd, telefone, email) {
        var ctto = new Contato();
        ctto.ContatoID = contatoID !== 0 ? contatoID : this.NovoContatoID();
        ctto.TipoContato = tipoContato;
        ctto.DDD = ddd;
        ctto.Telefone = telefone;
        ctto.Email = email;
        ctto.NovoContato = true;
        this.Contato.push(ctto);
    };
    Cliente.prototype.EditarContato = function (contatoID, tipoContato, ddd, telefone, email) {
        var index = this.Contato.indexOf(this.ObtemContato(contatoID));
        cliente.Contato[index].TipoContato = tipoContato;
        cliente.Contato[index].DDD = ddd;
        cliente.Contato[index].Telefone = telefone;
        cliente.Contato[index].Email = email;
    };
    Cliente.prototype.ExcluirContato = function (contatoID) {
        var contatosAtualizados = cliente.Contato.filter(function (contato) {
            return contato.ContatoID !== +contatoID;
        });
        cliente.Contato = [];
        cliente.Contato = contatosAtualizados;
    };
    return Cliente;
}());
var ClienteModel = (function () {
    function ClienteModel() {
    }
    return ClienteModel;
}());
//# sourceMappingURL=Cliente.js.map