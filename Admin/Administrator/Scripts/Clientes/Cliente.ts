﻿class Cliente {
    Codigo: number;
    Nome: string;
    TipoDocumento: number;
    Documento: number;
    InscricaoEstadual: string;
    LimiteCredito: number;
    Observacao: string;
    Situacao: number;

    Endereco: Array<Endereco>;
    Contato: Array<Contato>;

    ObtemCliente(): ClienteModel {
        var model = new ClienteModel();
        model.Codigo = this.Codigo;
        model.Documento = +this.Documento;
        model.InscricaoEstadual = this.InscricaoEstadual;
        model.LimiteCredito = this.LimiteCredito;
        model.Nome = this.Nome;
        model.Observacao = this.Observacao;
        model.StatusCliente = +this.Situacao;
        model.TipoDocumento = this.TipoDocumento;

        model.Endereco = [];
        for (var index in this.Endereco)
        {
            model.Endereco.push(this.Endereco[index]);
            if (model.Endereco[index].NovoEndereco == true)
            {
                model.Endereco[index].EnderecoID = 0;
            }
        } 

        model.Contato = [];
        for (var index in this.Contato)
        {
            model.Contato.push(this.Contato[index]);
            if (model.Contato[index].NovoContato == true)
            {
                model.Contato[index].ContatoID = 0;
            }
        }

        return model;
    }

    NovoEnderecoID(): number {
        var maior = 0;
        for (var index in this.Endereco)
        {
            if (this.Endereco[index].EnderecoID > maior) {
                maior = this.Endereco[index].EnderecoID;
            }
        }

        return (maior + 1);
    }

    NovoContatoID(): number {
        var maior = 0;
        for (var index in this.Contato)
        {
            if (this.Contato[index].ContatoID > maior) {
                maior = this.Contato[index].ContatoID;
            }
        }
        
        return (maior + 1);
    }

    //Região Endereço
    ObtemEndereco(enderecoID: number): Endereco
    {
        var end = $.grep(cliente.Endereco, function (endereco) {
            return endereco.EnderecoID === +enderecoID;
        });

        return end[0];
    }

    CriarEndereco(enderecoID: number, cep: number, endereco: string, numero: number, bairro: string, cidade: string, estado: string, complemento: string): void
    {
        var obj = new Endereco();

        obj.EnderecoID = enderecoID !== 0 ? enderecoID : this.NovoEnderecoID();
        obj.CEP = cep;
        obj.Endereco = endereco;
        obj.Numero = numero;
        obj.Bairro = bairro;
        obj.Cidade = cidade;
        obj.Estado = estado;
        obj.Complemento = complemento;
        obj.NovoEndereco = true;

        this.Endereco.push(obj);
    }
    
    EditarEndereco(enderecoID: number, cep: number, endereco: string, numero: number, bairro: string,
        cidade: string, estado: string, complemento: string)
    {
        var index = cliente.Endereco.indexOf(this.ObtemEndereco(enderecoID));

        cliente.Endereco[index].CEP = cep;
        cliente.Endereco[index].Endereco = endereco;
        cliente.Endereco[index].Numero = numero;
        cliente.Endereco[index].Bairro = bairro;
        cliente.Endereco[index].Cidade = cidade;
        cliente.Endereco[index].Estado = estado;
        cliente.Endereco[index].Complemento = complemento;
    }

    ExcluirEndereco(EnderecoID: number): void {
        var enderecosAtualizados = cliente.Endereco.filter(function (endereco) {
            return endereco.EnderecoID !== +EnderecoID;
        });

        this.Endereco = [];
        this.Endereco = enderecosAtualizados;
    }
    //Fim Região Endereço

    //Inicio Região Contato
    ObtemContato(contatoID: number): Contato
    {
        var ctto = $.grep(cliente.Contato, function (contato) {
            return contato.ContatoID === +contatoID;
        });

        return ctto[0];
    }

    CriarContato(contatoID: number, tipoContato: number, ddd: number, telefone: string, email: string): void
    {
        var ctto = new Contato();

        ctto.ContatoID = contatoID !== 0 ? contatoID : this.NovoContatoID();
        ctto.TipoContato = tipoContato;
        ctto.DDD = ddd;
        ctto.Telefone = telefone;
        ctto.Email = email;
        ctto.NovoContato = true;

        this.Contato.push(ctto);
    }

    EditarContato(contatoID: number, tipoContato: number, ddd: number, telefone: string, email: string): void
    {
        var index = this.Contato.indexOf(this.ObtemContato(contatoID));

        cliente.Contato[index].TipoContato = tipoContato;
        cliente.Contato[index].DDD = ddd;
        cliente.Contato[index].Telefone = telefone;
        cliente.Contato[index].Email = email;
    }

    ExcluirContato(contatoID: number): void
    {
        var contatosAtualizados = cliente.Contato.filter(function (contato) {
            return contato.ContatoID !== +contatoID;
        });

        cliente.Contato = [];
        cliente.Contato = contatosAtualizados;
    }
    //Fim Região Contatos
}

class ClienteModel {
    Codigo: number;
    Nome: string;
    TipoDocumento: number;
    Documento: number;
    InscricaoEstadual: string;
    LimiteCredito: number;
    Observacao: string;
    StatusCliente: number;

    Endereco: Array<Endereco>;
    Contato: Array<Contato>;
}