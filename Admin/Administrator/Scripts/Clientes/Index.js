jQuery(document).ready(function () {
    InitTableAjax("#tabelaClientes", "../Clientes/ObterListagem", "GET", [
        { "data": "Codigo" },
        { "data": "Nome" },
        { "data": "TipoDocumento" },
        { "data": "Documento" },
        { "data": "InscricaoEstadual" },
        { "data": "StatusCliente" },
        { "data": "Botoes" }
    ]);
});
function ExcluirClientes(codigo) {
    $.blockUI({ message: 'Removendo registro...aguarde!' });
    $.ajax({
        type: "POST",
        url: window.location.origin + '/Clientes/Deletar',
        data: { id: codigo },
        success: function (data) {
            if (data.Status) {
                Materialize.toast(data.Mensagem, 3000, "rounded");
                setTimeout(function () {
                    window.location.reload();
                }, 1500);
            }
        },
        dataType: "json"
    });
}
//# sourceMappingURL=Index.js.map