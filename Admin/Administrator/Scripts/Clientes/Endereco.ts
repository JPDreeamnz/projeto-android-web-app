﻿class Endereco {
    EnderecoID: number;
    CEP: number;
    Endereco: string;
    Numero: number;
    Bairro: string;
    Cidade: string;
    Estado: string;
    Complemento: string;
    NovoEndereco: boolean;
}
