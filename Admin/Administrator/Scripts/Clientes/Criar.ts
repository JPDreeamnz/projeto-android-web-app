﻿//Inicialização de objetos
var cliente = new Cliente();
cliente.Endereco = [];
cliente.Contato = [];

$(document).ready(function () {
    //Inicialização de componentes
    $("#TipoDocumento, #Situacao, #TipoContato, #UF").material_select();
    $('.modal-trigger').leanModal();
    $("#modalContato").width('25%');
    $("#modalContato").height('60%');

    $("#Salvar").on('click', function (event) {
       
        cliente.Nome = $("#Nome").val();
        cliente.TipoDocumento = $("#TipoDocumento").val();
        cliente.Documento = $("#Documento").val();
        cliente.InscricaoEstadual = $("#InscricaoEstadual").val();
        cliente.LimiteCredito = $("#LimiteCredito").val().replace('.', '');
        cliente.Situacao = $("#Situacao").val();
        cliente.Observacao = $("#Observacao").val();

        $.post(window.location.origin + '/Clientes/Criar', cliente.ObtemCliente())
            .done(function (retorno) {
                if (retorno.Status) {
                    Materialize.toast(retorno.Mensagem, 3000, "rounded");
                    window.location.href = window.location.origin + '/Clientes';
                }
            })
            .fail(function () {
                Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
            });
    });

    $("#TipoContato").on('change', function () {
        var novoValor = $("#TipoContato").val();
        if (novoValor === '1' || novoValor === '2') {
            $(".campo-telefone").attr('style', 'display: normal;');
            $(".campo-email").attr('style', 'display: none;');
        } else {
            $(".campo-telefone").attr('style', 'display: none;');
            $(".campo-email").attr('style', 'display: normal;');
        }
    });
});