jQuery(document).ready(function () {
    $("#botaoLogin").on('click', function () {
        event.preventDefault();
        $.post(window.location.origin + '/Account/AnaliseCredencial', $("#accountForm").serialize())
            .done(function (retorno) {
            if (retorno.Status === "OK") {
                if (retorno.Mensagem === "USUARIO_LOGADO") {
                    $("#modalLogado").openModal();
                }
                else {
                    $("#accountForm").submit();
                }
            }
            else {
                Materialize.toast(retorno.Mensagem, 3000, "rounded");
            }
        })
            .fail(function () {
            Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
        });
    });
});
//# sourceMappingURL=Account.js.map