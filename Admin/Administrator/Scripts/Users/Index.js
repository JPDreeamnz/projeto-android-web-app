jQuery(document).ready(function () {
    InitTable("#tabelaUsers");
});
function DeleteRecord(codigo) {
    $.blockUI({ message: 'Removendo registro...aguarde!' });
    $.ajax({
        type: "POST",
        url: window.location.origin + '/Users/Delete',
        data: { id: codigo },
        success: function (data) {
            setTimeout(function () {
                window.location.reload();
            }, 2000);
        },
        dataType: "json"
    });
}
//# sourceMappingURL=Index.js.map