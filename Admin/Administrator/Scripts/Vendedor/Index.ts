﻿jQuery(document).ready(function () {
    InitTable("#tabelaVendedores");
});

function ExcluirVendedor(codigo) {
    $.blockUI({ message: 'Removendo registro...aguarde!' });

    $.ajax({
        type: "POST",
        url: window.location.origin + "/Vendedor/Excluir",
        data: { id: codigo },
        success: function (data) {
            if (data.Status) {
                Materialize.toast(data.Mensagem, 2000);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        },
        dataType: "json"
    });
}