var Vendedor = (function () {
    function Vendedor() {
    }
    return Vendedor;
}());
function SubmitVendedor(action) {
    var obj = new Vendedor();
    obj.Codigo = $("#Codigo").val();
    obj.Nome = $("#Nome").val();
    obj.Observacao = $("#Observacao").val();
    obj.StatusVendedor = $("#Situacao").val();
    $.post(window.location.origin + '/Vendedor/' + action, obj)
        .done(function (retorno) {
        if (retorno.Status) {
            Materialize.toast(retorno.Mensagem, 3000, "rounded");
            window.location.href = window.location.origin + '/Vendedor';
        }
    })
        .fail(function () {
        Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
    });
}
//# sourceMappingURL=Vendedor.js.map