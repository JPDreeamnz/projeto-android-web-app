﻿// Interface typescript datatables criada para o materialize-datatables do Azamat Mukhiddinov
// Criada para que não seja inicializada a table e feitas funções uteis no incio do processamento do datatables

declare function InitTable(tagTabela: string): void;
declare function ObtemDataTablesSettings(): string;
declare function InitTable(tagTabela: string): void;
declare function InitTableAjax(tagTabela: string, caminhoAjax: string, tipoAjax: string, arrayColunas: any[]): void;