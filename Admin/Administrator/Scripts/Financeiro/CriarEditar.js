jQuery(document).ready(function () { });
function Submit() {
    var obj = new Financeiro();
    obj.ClienteCodigo = $("#ClienteCodigo").val();
    obj.Documento = $("#Documento").val();
    obj.Emissao = $("#Emissao").val();
    obj.Vencimento = $("#Vencimento").val();
    obj.Pagamento = $("#Pagamento").val();
    obj.Valor = $("#Valor").val().replace(/\./g, '');
    $.post(window.location.origin + '/Administrator/Financeiro/Criar', obj)
        .done(function (retorno) {
        if (retorno.Status) {
            Materialize.toast(retorno.Mensagem, 3000, "rounded");
            window.location.href = window.location.origin + '/Administrator/Financeiro';
        }
    })
        .fail(function () {
        Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
    });
}
