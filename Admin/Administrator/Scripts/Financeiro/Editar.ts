﻿jQuery(document).ready(function () {
    $("#EnviarFinanceiro").on('submit', function (event) {
        event.preventDefault();
        $("#ClienteCodigo").prop("disabled", false);
        Submit('Editar');
    });

    $("#ClienteCodigo").on('change', function () {
        $("#Emissao").pickadate('picker').open();
    });

    $("#Emissao, #Vencimento, #Pagamento").on('change', function (event) {
        $("#" + this.id).pickadate('picker').close();
    });
});

