﻿class Financeiro
{
    ClienteCodigo: number;
    Documento: string;
    Emissao: any;
    Vencimento: any;
    Pagamento: any;
    Valor: number;
}

function Submit(action) {
    var obj = new Financeiro();
    obj.ClienteCodigo = $("#ClienteCodigo").val();
    obj.Documento = $("#Documento").val();
    obj.Emissao = $("#Emissao").val();
    obj.Vencimento = $("#Vencimento").val();
    obj.Pagamento = $("#Pagamento").val();
    obj.Valor = $("#Valor").val().replace(/\./g, '');

    $.post(window.location.origin + '/Financeiro/' + action, obj)
        .done(function (retorno) {
            if (retorno.Status) {
                Materialize.toast(retorno.Mensagem, 3000, "rounded");
                window.location.href = window.location.origin + '/Financeiro';
            }
        })
        .fail(function () {
            Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
        });
}