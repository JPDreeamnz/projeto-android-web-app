jQuery(document).ready(function () {
    $('#ClienteCodigo').material_select();
    $("#Emissao").click(function (event) {
        event.stopPropagation();
        $("#Emissao").pickadate("picker").open();
    });
    $("#Vencimento").click(function (event) {
        event.stopPropagation();
        $("#Vencimento").pickadate("picker").open();
    });
    $("#Pagamento").click(function (event) {
        event.stopPropagation();
        $("#Pagamento").pickadate("picker").open();
    });
    $("#EnviarFinanceiro").on('submit', function (event) {
        event.preventDefault();
        Submit('Criar');
    });
    $("#ClienteCodigo").on('change', function () {
        $("#Emissao").pickadate('picker').open();
    });
    $("#Emissao, #Vencimento, #Pagamento").on('change', function (event) {
        $("#" + this.id).pickadate('picker').close();
    });
});
window.addEventListener("DOMContentLoaded", function () {
    $(".select-dropdown").focus();
});
//# sourceMappingURL=Criar.js.map