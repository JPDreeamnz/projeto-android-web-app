﻿/// <reference path="../DataTables/materialize-datatables.d.ts" />

jQuery(document).ready(function () {
    InitTable("#tabelaFinanceiro");
});

function ExcluirFinanceiro(clienteCodigo, documento) {
    $.blockUI({ message: 'Removendo registro...aguarde!' });

    $.ajax({
        type: "POST",
        url: window.location.origin + "/Financeiro/Excluir",
        data: { clienteCodigo: clienteCodigo, documento: documento },
        success: function (data) {
            if (data.Status) {
                Materialize.toast(data.Mensagem, 2000);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        },
        dataType: "json"
    });
}