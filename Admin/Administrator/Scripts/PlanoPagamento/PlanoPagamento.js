var PlanoPagamento = (function () {
    function PlanoPagamento() {
    }
    return PlanoPagamento;
}());
function SubmitPlano(action) {
    var obj = new PlanoPagamento();
    obj.Codigo = $("#Codigo").val();
    obj.Nome = $("#Nome").val();
    obj.Observacao = $("#Observacao").val();
    obj.StatusPlano = $("#Situacao").val();
    $.post(window.location.origin + '/PlanoPagamento/' + action, obj)
        .done(function (retorno) {
        if (retorno.Status) {
            Materialize.toast(retorno.Mensagem, 3000, "rounded");
            window.location.href = window.location.origin + '/PlanoPagamento';
        }
    })
        .fail(function () {
        Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
    });
}
//# sourceMappingURL=PlanoPagamento.js.map