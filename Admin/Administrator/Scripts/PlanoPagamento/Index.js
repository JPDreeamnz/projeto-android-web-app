jQuery(document).ready(function () {
    InitTable("#tabelaPlano");
});
function ExcluirPlano(codigo) {
    $.blockUI({ message: 'Removendo registro...aguarde!' });
    $.ajax({
        type: "POST",
        url: window.location.origin + "/PlanoPagamento/Excluir",
        data: { id: codigo },
        success: function (data) {
            if (data.Status) {
                Materialize.toast(data.Mensagem, 2000);
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
        },
        dataType: "json"
    });
}
//# sourceMappingURL=Index.js.map