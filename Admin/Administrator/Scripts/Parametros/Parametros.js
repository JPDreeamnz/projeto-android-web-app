var Parametros = (function () {
    function Parametros() {
    }
    return Parametros;
}());
function SubmitParametros(action) {
    var obj = new Parametros();
    obj.Codigo = $("#Codigo").val();
    obj.ClienteDefault = $("#ClienteDefault").val();
    obj.VendedorDefault = $("#VendedorDefault").val();
    obj.PlanoPagamentoDefault = $("#PlanoPagamentoDefault").val();
    obj.TipoIntegracao = $("#TipoIntegracaoCheck").prop('checked') ? 1 : 2;
    obj.ValidaEstoque = $("#ValidaEstoqueCheck").prop('checked') ? 1 : 2;
    $.post(window.location.origin + '/Parametros/' + action, obj)
        .done(function (retorno) {
        if (retorno.Status) {
            Materialize.toast(retorno.Mensagem, 3000, "rounded");
        }
    })
        .fail(function () {
        Materialize.toast("Não foi possivel fazer a requisição no serviço. Tente novamente em alguns minutos.", 3000);
    });
}
//# sourceMappingURL=Parametros.js.map