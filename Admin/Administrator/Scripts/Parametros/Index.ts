﻿jQuery(document).ready(function () {
    $("#formParametros").on('submit', function () {
        event.preventDefault();
        SubmitParametros('Editar');
    });
});

window.addEventListener('DOMContentLoaded', function () {
    if ($("#TipoIntegracao").val() === "ATIVO") {
        $("#TipoIntegracaoCheck").attr('checked', 'checked');
    }
    if ($("#ValidaEstoque").val() === "ATIVO") {
        $("#ValidaEstoqueCheck").attr('checked', 'checked');
    }
});