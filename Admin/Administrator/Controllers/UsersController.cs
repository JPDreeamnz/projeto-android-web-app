﻿using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class UsersController : AuthController
    {
        [Permissao(PermissionamentoController.Usuarios, TipoPermissionamento.Listar)]
        // GET: Usuarios
        public ActionResult Index() 
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            List<UserModel> users = api.Get<List<UserModel>>("api/users/v1/getall");
            if (users == null)
                users = new List<UserModel>();

            return View(users);
        }

        [Permissao(PermissionamentoController.Usuarios, TipoPermissionamento.Criar)]
        public ActionResult Create()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            List<GrupoUsuariosModel> grupos = api.Get<List<GrupoUsuariosModel>>("api/GrupoUsuarios/ListarGrupos");
            TempData["GrupoUsuarios"] = grupos.Select(gru => new SelectListItem
            {
                Value = gru.Codigo.ToString(),
                Text = gru.Nome
            });

            return View();
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Usuarios, TipoPermissionamento.Criar)]
        public ActionResult Create(UserModel model, string PasswordCheck)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest createResult = api.Post<RetornoRequest>("api/Users/v1/Post", model);
            
            TempData["messages"] = createResult.Mensagem;

            if (!createResult.Retorno)
                return View(model);

            return RedirectToAction("Index");
        }

        [Permissao(PermissionamentoController.Usuarios, TipoPermissionamento.Editar)]
        public ActionResult Edit(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            List<GrupoUsuariosModel> grupos = api.Get<List<GrupoUsuariosModel>>("api/GrupoUsuarios/ListarGrupos");
            TempData["GrupoUsuarios"] = grupos.Select(gru => new SelectListItem
            {
                Value = gru.Codigo.ToString(),
                Text = gru.Nome
            });


            UserModel model = api.Get<UserModel>($"api/Users/v1/Get?UserID={id}");
            if (model == null)
                model = new UserModel();

            return View(model);
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Usuarios, TipoPermissionamento.Editar)]
        public ActionResult Edit(UserModel model, string PasswordCheck)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest updateResult = api.Post<RetornoRequest>("api/Users/v1/Put", model);
            TempData["messages"] = updateResult.Mensagem;

            if (!updateResult.Retorno)
            {
                List<GrupoUsuariosModel> grupos = api.Get<List<GrupoUsuariosModel>>("api/GrupoUsuarios/ListarGrupos");
                TempData["GrupoUsuarios"] = grupos.Select(gru => new SelectListItem
                {
                    Value = gru.Codigo.ToString(),
                    Text = gru.Nome
                });
                return View(model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Usuarios, TipoPermissionamento.Excluir)]
        public JsonResult Delete(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            Boolean updateResult = (Boolean)api.Delete<Object>("api/Users/v1/Delete/?UserID=", id);

            if (!updateResult)
                TempData["messages"] = "Não foi possível remover o usuário.";
            else
                TempData["messages"] = "Usuário Removido com sucesso.";

            return Json(new { status = "OK" });
        }
    }
}