﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    /// <summary>
    /// Responsável por mostrar as mensagens amigáveis ao usuário.
    /// </summary>
    public class ErrorController : Controller
    {
        [ActionName("500")]
        public ActionResult InternalError()
        {
            return View();
        }

        [ActionName("404")]
        public ActionResult NotFound()
        {
            return View();
        }

        [ActionName("401")]
        public ActionResult NotAuthorized()
        {
            return View();
        }
    }
}