﻿using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class VendedorController : AuthController
    {
        // GET: Vendedor
        [Permissao(PermissionamentoController.Vendedor, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            
            List<VendedorModel> vendedor = api.Get<List<VendedorModel>>("api/vendedor/obterlista");
            if (vendedor == null)
                vendedor = new List<VendedorModel>();

            return View(vendedor);
        }

        [Permissao(PermissionamentoController.Vendedor, TipoPermissionamento.Criar)]
        public ActionResult Criar()
        {
            return View(new VendedorModel());
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Vendedor, TipoPermissionamento.Criar)]
        public ActionResult Criar(VendedorModel novoVendedor)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/Vendedor/Incluir", novoVendedor);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.Vendedor, TipoPermissionamento.Editar)]
        public ActionResult Editar(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            VendedorModel vendedor = api.Get<VendedorModel>($"api/Vendedor/ObterVendedor?codigo={id}");
            if (vendedor == null)
            {
                TempData["Mensagem"] = "O vendedor não foi encontrado. Por favor tente novamente.";
                return RedirectToAction("Index");
            }

            return View(vendedor);
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Vendedor, TipoPermissionamento.Editar)]
        public ActionResult Editar(VendedorModel vendedor)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/Vendedor/Editar", vendedor);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem });
        }
        
        [HttpPost]
        [Permissao(PermissionamentoController.Vendedor, TipoPermissionamento.Excluir)]
        public JsonResult Excluir(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            Boolean retorno = (Boolean)api.Delete<Object>("api/Vendedor/Excluir?codigo=", id);
            string mensagem = string.Empty;

            if (!retorno)
                mensagem = "Não foi possível remover o vendedor. Por favor tente novamente.";
            else
                mensagem = "Vendedor removido com sucesso.";

            return Json(new { Status = retorno, Mensagem = mensagem });
        }
    }
}