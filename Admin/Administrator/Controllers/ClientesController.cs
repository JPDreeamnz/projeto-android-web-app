﻿using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.DataTables;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Administrator.Controllers
{
    public class ClientesController : AuthController
    {
        // GET: Clientes
        [Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            return View();
        }

        [Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Criar)]
        public ActionResult Criar()
        {
            return View(new ClienteModel());
        }

        [HttpPost][Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Criar)]
        public ActionResult Criar(ClienteModel novoCliente)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/Clientes/Incluir", novoCliente);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Editar)]
        public ActionResult Editar(int id)
        {
            return Visualizar(id);
        }

        [HttpPost][Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Editar)]
        public ActionResult Editar(ClienteModel cliente)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/Clientes/Editar", cliente);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem });
        }

        [Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Listar)]
        public ActionResult Visualizar(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            ClienteModel cliente = api.Get<ClienteModel>($"api/Clientes/ObterCliente?codigo={id}");
            if (cliente == null)
            {
                TempData["Mensagem"] = "O cliente não foi encontrado. Por favor tente novamente.";
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        [HttpPost][Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Excluir)]
        public JsonResult Deletar(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            Boolean retorno = (Boolean)api.Delete<Object>("api/Clientes/Excluir?codigo=", id);
            string mensagem = string.Empty;

            if (!retorno)
                mensagem = "Não foi possível remover o Cliente. Por favor tente novamente.";
            else
                mensagem = "Cliente removido com sucesso.";

            return Json(new { Status = retorno, Mensagem = mensagem });
        }

        [Permissao(PermissionamentoController.Clientes, TipoPermissionamento.Listar)]
        public JsonResult ObterListagem()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            var param = HttpUtility.ParseQueryString(Request.Url.Query).RequestToJQueryDataTableParam();
            var clientes = api.Post<TableClientesRetorno>("api/Clientes/ObterDataTable", param);
            if (clientes.Clientes == null)
                clientes.Clientes = new List<ClienteTabela>();

            InsereBotoesListagem(clientes.Clientes, "Clientes");
            
            return Json(new
            {
                draw = param.sEcho,
                recordsTotal = clientes.QuantidadeRegistros,
                recordsFiltered = clientes.QuantidadeRegistros,
                data = clientes.Clientes
            }, JsonRequestBehavior.AllowGet);
        }
    }
}