﻿using Administrator.Util;
using Administrator.Util.Security;
using Api.Models;
using Api.Models.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Administrator.Controllers
{
    public class AuthController : BaseController
    {
        private UserModel _UsuarioLogado
        {
            get
            {
                if(Session["UsuarioLogado"] == null)
                {
                    return null;
                }
                else
                    return (UserModel)Session["UsuarioLogado"];
            }

            set
            {
                Session["UsuarioLogado"] = value;
            }
        }

        /// <summary>
        /// Verificar se o token atual é válido.
        /// Caso negativo, redireciona para a página de acesso negado.
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            ValidateToken valid = new ValidateToken();

            //Seta os menus
            TempData["MenuUsuario"] = this.Session["MenuUsuario"];
            TempData["MenuLateral"] = this.Session["MenuLateral"];

            if (!valid.isValid((string)Session[ConfigurationApiManager.TOKEN]))
            {
                valid.removeToken((string)Session[ConfigurationApiManager.TOKEN]);
                Session[ConfigurationApiManager.TOKEN] = null;
                Session["ImagemProdutoSessao"] = null;
                Session["UsuarioLogado"] = null;
                filterContext.Result = Redirect("~/Error/401");
            }

            if (this._UsuarioLogado == null)
            {
                ApiManager cweb = new ApiManager((string)Session[ConfigurationApiManager.TOKEN], ConfigurationApiManager.BASE_AUTH_WEB_SITE);
                this._UsuarioLogado = cweb.Get<UserModel>($"Api/Auth/ObterUsuario?token={(string)Session[ConfigurationApiManager.TOKEN]}");
            }

            base.OnActionExecuting(filterContext);
        }

        /// <summary>
        /// Insere para cada item do registro a ser enviado para o DataTables os botões de Editar e Excluir
        /// </summary>
        /// <param name="registros">Objeto do tipo TabelaPadrao que preencherá os botões</param>
        /// <param name="referencia">A controller de referência. Precisa da primeira letra maiúscula EX: ClientesController é enviado apenas o Clientes</param>
        public static void InsereBotoesListagem<T>(List<T> registros, string referencia) where T : TabelaPadrao
        {
            foreach (var linha in registros)
            {
                linha.Botoes = "<a class=\"waves-effect btn-flat no-padding\" href=\"\\" + referencia + "\\Editar\\" + linha.Codigo + "\"><i class=\"material-icons\">&#xE254;</i></a>" +
                                            "<a class=\"waves-effect btn-flat no-padding\" onclick=\"Excluir" + referencia + "('" + linha.Codigo + "')\"><i class=\"material-icons\">&#xE872;</i></a>";
            }
        }

        /// <summary>
        /// Insere para cada item do registro a ser enviado para o DataTables o botão de Visualizar
        /// </summary>
        /// <param name="registros">Objeto do tipo TabelaPadrao que preencherá os botões</param>
        /// <param name="referencia">A controller de referência. Precisa da primeira letra maiúscula EX: ClientesController é enviado apenas o Clientes</param>
        public static void InsereBotaoVisualizacao<T>(List<T> registros, string referencia) where T : TabelaPadrao
        {
            foreach (var linha in registros)
            {
                linha.Botoes = "<a class=\"icone-clicavel\" href=\"\\" + referencia + "\\Visualizar\\" + linha.Codigo + "\"><i class=\"material-icons\">find_in_page</i></a>";
            }
        }
    }
}