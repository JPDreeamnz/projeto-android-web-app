﻿
using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class PlanoPagamentoController : AuthController
    {
        // GET: Vendedor
        [Permissao(PermissionamentoController.PlanoPagamento, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            List<PlanoPagamentoModel> planoPagamento = api.Get<List<PlanoPagamentoModel>>("api/PlanoPagamento/obterlista");
            if (planoPagamento == null)
                planoPagamento = new List<PlanoPagamentoModel>();

            return View(planoPagamento);
        }

        [Permissao(PermissionamentoController.PlanoPagamento, TipoPermissionamento.Criar)]
        public ActionResult Criar()
        {
            return View(new PlanoPagamentoModel());
        }

        [HttpPost]
        [Permissao(PermissionamentoController.PlanoPagamento, TipoPermissionamento.Criar)]
        public ActionResult Criar(PlanoPagamentoModel novoPlanoPagamento)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/PlanoPagamento/Incluir", novoPlanoPagamento);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.PlanoPagamento, TipoPermissionamento.Editar)]
        public ActionResult Editar(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            PlanoPagamentoModel planoPagamento = api.Get<PlanoPagamentoModel>($"api/PlanoPagamento/ObterPlano?codigo={id}");
            if (planoPagamento == null)
            {
                TempData["Mensagem"] = "O plano de pagamento não foi encontrado. Por favor tente novamente.";
                return RedirectToAction("Index");
            }

            return View(planoPagamento);
        }

        [HttpPost]
        [Permissao(PermissionamentoController.PlanoPagamento, TipoPermissionamento.Editar)]
        public ActionResult Editar(PlanoPagamentoModel planoPagamento)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/PlanoPagamento/Editar", planoPagamento);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem });
        }

        [HttpPost]
        [Permissao(PermissionamentoController.PlanoPagamento, TipoPermissionamento.Excluir)]
        public JsonResult Excluir(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            Boolean retorno = (Boolean)api.Delete<Object>("api/PlanoPagamento/Excluir?codigo=", id);
            string mensagem = string.Empty;

            if (!retorno)
                mensagem = "Não foi possível remover o plano de pagamento. Por favor tente novamente.";
            else
                mensagem = "Plano de pagamento removido com sucesso.";

            return Json(new { Status = retorno, Mensagem = mensagem });
        }
    }
}