﻿using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class FinanceiroController : AuthController
    {
        // GET: Financeiro
        [Permissao(PermissionamentoController.Financeiro, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            List<FinanceiroModel> financeiro = api.Get<List<FinanceiroModel>>("api/financeiro/ListarCadastrosFinanceiros");
            if (financeiro == null)
                financeiro = new List<FinanceiroModel>();

            return View(financeiro);
        }

        [Permissao(PermissionamentoController.Financeiro, TipoPermissionamento.Criar)]
        public ActionResult Criar()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            List<ClienteModel> clientes = api.Get<List<ClienteModel>>("api/Clientes/ObterLista");
            if(clientes.Count == 0)
            {
                //Emitir Mensagem
                return RedirectToAction("Index");
            }

            ParametrosModel parametro = api.Get<ParametrosModel>("api/Parametros/ObterParametros");
            if (parametro != null)
            {
                clientes = clientes.Where(x => x.Codigo != parametro.ClienteDefault).ToList();
            }

            TempData["Clientes"] = clientes.Where(x => x.StatusCliente == Situacao.ATIVO).Select(cli => new SelectListItem {
                Value = cli.Codigo.ToString(),
                Text = cli.Nome
            });

            return View();
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Financeiro, TipoPermissionamento.Criar)]
        public ActionResult Criar(FinanceiroModel model)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/Financeiro/Incluir", model);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.Financeiro, TipoPermissionamento.Editar)]
        public ActionResult Editar(int clienteCodigo, string documento)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            FinanceiroModel model = api.Get<FinanceiroModel>($"api/financeiro/obtercadastrofinanceiro?clienteCodigo={clienteCodigo}&documento={documento}");
            List<ClienteModel> clientes = api.Get<List<ClienteModel>>("api/Clientes/ObterLista");
            TempData["Clientes"] = clientes.Select(cli => new SelectListItem
            {
                Value = cli.Codigo.ToString(),
                Text = cli.Nome
            });

            if (model == null)
            {
                TempData["Mensagem"] = "Não foi encontrado o cadastro financeiro selecionado.";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Financeiro, TipoPermissionamento.Editar)]
        public ActionResult Editar(FinanceiroModel model)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/financeiro/editar", model);
            TempData["Mensagem"] = retorno.Mensagem;
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.Financeiro, TipoPermissionamento.Excluir)]
        public JsonResult Excluir(int clienteCodigo, string documento)
        {
            string Mensagem = string.Empty;
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            Boolean retorno = (Boolean)api.Get<Object>($"api/financeiro/Excluir?clienteCodigo={clienteCodigo}&documento={documento}");

            if (!retorno)
                Mensagem = "Não foi possível remover o cadastro financeiro";
            else
                Mensagem = "Cadastro financeiro removido com sucesso.";

            return Json(new { Status = retorno, Mensagem = Mensagem }, JsonRequestBehavior.AllowGet);
        }
    }
}