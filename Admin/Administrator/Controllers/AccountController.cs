﻿using Administrator.Models;
using Administrator.Util;
using Api.Models;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class AccountController : Controller
    {
        public AccountController()
        {
            Administrator.Util.
            ConfigurationApiManager.BASE_API_SERVER =
            System.Configuration.ConfigurationManager.AppSettings["BASE_API_SERVER"];

            Administrator.Util.
            ConfigurationApiManager.BASE_AUTH_WEB_SITE =
            System.Configuration.ConfigurationManager.AppSettings["BASE_AUTH_WEB_SITE"];
        }

        /// <summary>
        /// Mostrar página com formulário de login.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (Session[ConfigurationApiManager.TOKEN] != null)
            {
                string token = (string)Session[ConfigurationApiManager.TOKEN];

                ApiManager cweb = new ApiManager((string)Session[ConfigurationApiManager.TOKEN], ConfigurationApiManager.BASE_AUTH_WEB_SITE);
                var retorno = (Boolean)cweb.Get<object>($"Api/Auth/Get?Token={token}");
                if (retorno)
                {
                    return RedirectToAction("Index", "Home");
                }
            }

            return View();
        }

        /// <summary>
        /// Responsável por autenticar o usuário.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Login(Models.LoginModel model)
        {
            ApiManager cweb = new ApiManager((string)Session[ConfigurationApiManager.TOKEN], ConfigurationApiManager.BASE_AUTH_WEB_SITE);
            var authReturn = cweb.Post<LoginAuth>("Api/Auth/Post", model);
            MenuPermissoes.MontaMenu(cweb.Get<List<PermissionamentoModel>>($"Api/Auth/ObterPermissoes?GrupoUsuariosCodigo={authReturn.Usuario.GrupoUsuariosCodigo}"));
            TempData["MenuUsuario"] = this.Session["MenuUsuario"];
            TempData["MenuLateral"] = this.Session["MenuLateral"];

            if (authReturn.Status == "Sucesso")
            {
                Session[ConfigurationApiManager.TOKEN] = authReturn.Token;
                return RedirectToAction("index", "home");
            }
            else
            {
                return RedirectToAction("index");
            }
        }

        /// <summary>
        /// Cancelar autenticação do usuário.
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            ApiManager cweb = new ApiManager(string.Empty, ConfigurationApiManager.BASE_AUTH_WEB_SITE);
            var authReturn = (Boolean)cweb.Delete<Object>("Api/Auth/Delete/?Token=", (string)Session[ConfigurationApiManager.TOKEN]);

            Session.Abandon();
            return RedirectToAction("index");
        }

        //TODO: Sumarizar
        public JsonResult AnaliseCredencial(Models.LoginModel model)
        {
            string status = "OK",
                   mensagem = string.Empty;

            ApiManager cweb = new ApiManager((string)Session[ConfigurationApiManager.TOKEN], ConfigurationApiManager.BASE_AUTH_WEB_SITE);
            var retorno = cweb.Post<RetornoRequest>("Api/Auth/CredentialIsValid", model);
            if (!retorno.Retorno)
            {
                status = "ERRO";
                mensagem = retorno.Mensagem;
            }

            return Json(new { Status = status, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }
    }
}