﻿using Administrator.Util;
using Api.Enums;
using Api.Models.DataTables;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class PedidoVendaController : AuthController
    {
        [Permissao(PermissionamentoController.PedidoVenda, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            return View();
        }

        [Permissao(PermissionamentoController.PedidoVenda, TipoPermissionamento.Listar)]
        public ActionResult Visualizar(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            PedidoVendaModel pedidoVenda = api.Get<PedidoVendaModel>($"api/PedidoVenda/ObterPedido?codigo={id}");
            if (pedidoVenda == null)
                pedidoVenda = new PedidoVendaModel();

            return View(pedidoVenda);
        }

        [Permissao(PermissionamentoController.PedidoVenda, TipoPermissionamento.Listar)]
        public JsonResult ObterListagem()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            var param = HttpUtility.ParseQueryString(Request.Url.Query).RequestToJQueryDataTableParam();
            var clientes = api.Post<TablePedidoVenda>("api/PedidoVenda/ObterDataTable", param);
            if (clientes.Pedidos == null)
                clientes.Pedidos = new List<PedidoVendaTabela>();

            InsereBotaoVisualizacao(clientes.Pedidos, "PedidoVenda");

            return Json(new
            {
                draw = param.sEcho,
                recordsTotal = clientes.QuantidadeRegistros,
                recordsFiltered = clientes.QuantidadeRegistros,
                data = clientes.Pedidos
            }, JsonRequestBehavior.AllowGet);
        }
    }
}