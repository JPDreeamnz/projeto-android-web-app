﻿
using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class ParametrosController : AuthController
    {

        //TODO: Sumarizar
        [Permissao(PermissionamentoController.Parametros, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            ParametrosModel parametro = api.Get<ParametrosModel>("api/Parametros/ObterParametros");
            if (parametro == null)
                parametro = new ParametrosModel();

            return View(parametro);
        }

        //TODO: Sumarizar
        [HttpPost]
        [Permissao(PermissionamentoController.Parametros, TipoPermissionamento.Editar)]
        public JsonResult Editar(ParametrosModel objeto)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest resposta = api.Post<RetornoRequest>("api/Parametros/Editar", objeto);
            return Json(new { Status = resposta.Retorno, Mensagem = resposta.Mensagem }, JsonRequestBehavior.AllowGet);
        }
    }
}