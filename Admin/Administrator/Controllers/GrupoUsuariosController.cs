﻿using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class GrupoUsuariosController : AuthController
    {
        [Permissao(PermissionamentoController.GrupoUsuarios, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            List<GrupoUsuariosModel> grupos = api.Get<List<GrupoUsuariosModel>>("api/GrupoUsuarios/ListarGrupos");
            if (grupos == null)
                grupos = new List<GrupoUsuariosModel>();

            return View(grupos);
        }

        [Permissao(PermissionamentoController.GrupoUsuarios, TipoPermissionamento.Criar)]
        public ActionResult Criar()
        {
            return View();
        }

        [HttpPost]
        [Permissao(PermissionamentoController.GrupoUsuarios, TipoPermissionamento.Criar)]
        public ActionResult Criar(GrupoUsuariosModel model)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/GrupoUsuarios/Incluir", model);
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.GrupoUsuarios, TipoPermissionamento.Editar)]
        public ActionResult Editar(int codigo)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            GrupoUsuariosModel model = api.Get<GrupoUsuariosModel>($"api/GrupoUsuarios/ObterGrupo?codigo={codigo}");
            if (model == null)
            {
                TempData["Mensagem"] = "Não foi encontrado o Grupo de Usuários selecionado.";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        [Permissao(PermissionamentoController.GrupoUsuarios, TipoPermissionamento.Editar)]
        public ActionResult Editar(GrupoUsuariosModel model)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/GrupoUsuarios/editar", model);
            TempData["Mensagem"] = retorno.Mensagem;
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.GrupoUsuarios, TipoPermissionamento.Excluir)]
        public JsonResult Excluir(int codigo)
        {
            string Mensagem = string.Empty;
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            Boolean retorno = (Boolean)api.Get<Object>($"api/GrupoUsuarios/Excluir?codigo={codigo}");

            if (!retorno)
                Mensagem = "Não foi possível remover o cadastro financeiro";
            else
                Mensagem = "Cadastro financeiro removido com sucesso.";

            return Json(new { Status = retorno, Mensagem = Mensagem }, JsonRequestBehavior.AllowGet);
        }
    }
}