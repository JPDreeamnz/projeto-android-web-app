﻿using Administrator.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class HomeController : AuthController
    {
        /// <summary>
        /// Tela inícial do sistema.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TestPage()
        {
            return View();
        }

        public ActionResult Ping()
        {
            WebClient web = new WebClient();
            web.BaseAddress = ConfigurationApiManager.BASE_API_SERVER;
            web.Headers.Add("Authorization: Bearer " + Session[ConfigurationApiManager.TOKEN]);

            return Content(web.DownloadString("/api/serverinfo/v1/ping"));
        }
    }
}