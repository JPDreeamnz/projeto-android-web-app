﻿using Administrator.Util;
using Api.Enums;
using Api.Models;
using Api.Models.DataTables;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class ProdutoController : AuthController
    {
        // GET: Produto
        [Permissao(PermissionamentoController.Produto, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            return View();
        }

        [Permissao(PermissionamentoController.Produto, TipoPermissionamento.Criar)]
        public ActionResult Criar()
        {
            TempData["TK"] = (string)Session[ConfigurationApiManager.TOKEN];
            TempData["url"] = ConfigurationManager.AppSettings["BASE_API_SERVER"];

            return View(new ProdutoModel());
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Produto, TipoPermissionamento.Criar)]
        public ActionResult Criar(ProdutoModel model)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/Produto/Incluir", model);

            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.Produto, TipoPermissionamento.Editar)]
        public ActionResult Editar(int id)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            ProdutoModel model = api.Get<ProdutoModel>($"api/Produto/ObterProduto?codigo={id}");
            model.ImagensProduto = api.Get<List<ImagemProdutoModel>>($"api/ImagemProduto/ObterListaImagens?codigoProduto={id}");

            TempData["TK"] = (string)Session[ConfigurationApiManager.TOKEN];
            TempData["url"] = ConfigurationManager.AppSettings["BASE_API_SERVER"];

            if (model == null)
            {
                TempData["Mensagem"] = "Não foi encontrado o produto selecionado.";
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        [Permissao(PermissionamentoController.Produto, TipoPermissionamento.Editar)]
        public ActionResult Editar(ProdutoModel model)
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            RetornoRequest retorno = api.Post<RetornoRequest>("api/Produto/Editar", model);
            
            return Json(new { Status = retorno.Retorno, Mensagem = retorno.Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.Produto, TipoPermissionamento.Excluir)]
        public JsonResult Excluir(int codigo)
        {
            string Mensagem = string.Empty;
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);
            Boolean retorno = (Boolean)api.Get<Object>($"api/Produto/Excluir?codigo={codigo}");

            if (!retorno)
                Mensagem = "Não foi possível remover o produto";
            else
                Mensagem = "Produto removido com sucesso.";

            return Json(new { Status = retorno, Mensagem = Mensagem }, JsonRequestBehavior.AllowGet);
        }

        [Permissao(PermissionamentoController.Produto, TipoPermissionamento.Listar)]
        public JsonResult ObterListagem()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            var param = HttpUtility.ParseQueryString(Request.Url.Query).RequestToJQueryDataTableParam();
            var produtos = api.Post<TableProduto>("api/Produto/ObterDataTable", param);
            if (produtos.Produtos == null)
                produtos.Produtos = new List<ProdutoTabela>();

            InsereBotoesListagem(produtos.Produtos, "Produto");

            return Json(new
            {
                draw = param.sEcho,
                recordsTotal = produtos.QuantidadeRegistros,
                recordsFiltered = produtos.QuantidadeRegistros,
                data = produtos.Produtos
            }, JsonRequestBehavior.AllowGet);
        }
    }
}