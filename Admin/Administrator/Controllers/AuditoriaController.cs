﻿
using Administrator.Util;
using Api.Enums;
using Api.Models.DataTables;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Administrator.Controllers
{
    public class AuditoriaController : AuthController
    {
        [Permissao(PermissionamentoController.Auditoria, TipoPermissionamento.Listar)]
        public ActionResult Index()
        {
            return View();
        }

        [Permissao(PermissionamentoController.Auditoria, TipoPermissionamento.Livre)]
        public ActionResult Visualizar(int id)
        {
            //TODO: Criar a persistência e a model
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            AuditoriaModel auditoria = api.Get<AuditoriaModel>($"api/Auditoria/ObterAuditoria?codigo={id}");
            if (auditoria == null)
                auditoria = new AuditoriaModel();

            return View(auditoria);
        }

        [Permissao(PermissionamentoController.Auditoria, TipoPermissionamento.Listar)]
        public JsonResult ObterListagem()
        {
            ApiManager api = new ApiManager((string)Session[ConfigurationApiManager.TOKEN]);

            var param = HttpUtility.ParseQueryString(Request.Url.Query).RequestToJQueryDataTableParam();
            var auditoria = api.Post<TableAuditoria>("api/Auditoria/ObterDataTable", param);
            if (auditoria.Auditorias == null)
                auditoria.Auditorias = new List<AuditoriaTabela>();

            InsereBotaoVisualizacao(auditoria.Auditorias, "Auditoria");

            return Json(new
            {
                draw = param.sEcho,
                recordsTotal = auditoria.QuantidadeRegistros,
                recordsFiltered = auditoria.QuantidadeRegistros,
                data = auditoria.Auditorias
            }, JsonRequestBehavior.AllowGet);
        }
    }
}