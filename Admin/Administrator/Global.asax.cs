﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Administrator
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        public override void Init()
        {
            base.Error += MvcApplication_Error;
            base.Init();
        }

        /// <summary>
        /// Mostrar mensagem amigavel quando um erro não tratado acontecer 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MvcApplication_Error(object sender, EventArgs e)
        {
            //// se o erro gerado for do tipo UnauthorizedAccessException
            //// redireciona para pagina de login
            var error = (((HttpApplication)sender).Context.AllErrors).FirstOrDefault();
            if (error.GetType() == typeof(UnauthorizedAccessException))
            {
                Response.Redirect("~/Error/401");
            }
            else
            {
                // caso contrario vai para a página de erro padrão.
                Response.Redirect("~/Error/500");
            }
        }
    }
}
