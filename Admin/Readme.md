
# Projeto Administração

* Cliente web para gerenciamento dos aplicativos mobile e API.

# Software Utilizado

* Visual Studio 2015 Enterprise ou superior

# Frameworks utilizados

* Microsoft .NET 4.5.2
* Microsoft Asp.NET 4 MVC
* UIKit (http://getuikit.com/)
* Template Altair 2