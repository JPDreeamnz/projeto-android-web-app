## É recomendado que todos as aplicações a seguir sejam hospedadas em instâncias dedicadas.
---
# Internet Information Server (IIS) 8.5 ou superior

O IIS deve ser ativado no Windows, pois geralmente não vem instaldo por padrão;

Sistema suportados:

* Windows 8.1 Pro ou superior; (ambiente de desenvolvimento)
* Windows Server 2012 R2 ou superior; 

PS: O PROJETO NÃO RODA COM IIS EXPRESS É OBRIGATÓRIO USAR O IIS LOCAL.

# Sql Server 2012 Express ou Superior 
( https://www.microsoft.com/pt-br/download/details.aspx?id=42299 )

    Nome da instancia: express;
    Autenticação: Segurança integrada;

Quando configurado o IIS deve se usar o recurso de personificação do POOL para garantir o acesso aos dados de forma segura.

Script para adicionar o usuário inicial no sistema:

    USE DB01;
    INSERT INTO Logins (Active, Email, Name, Password) VALUES (1,'master@master.net','Master','master123');
    INSERT INTO Users (Active, Name, LastName, [Role], Login_LoginID) VALUES (1, 'Master','Administrador', 4, @@IDENTITY);    
    
# Visual Studio 2015 Enterprise 
( https://www.visualstudio.com/pt-br/downloads/download-visual-studio-vs.aspx )

Todos os modulos foram desenvolvidos no VS2015 Enterprise.
É possivel que funcione normalmente no VS2015 Community, porém não foi testado e por isso não podemos garantir o 
funcionamento em versões diferentes da VS2015 Enterprise.

Recursos necessários para compilação do projeto:

* .NET 4.5.2
* EntityFramework 6+
* WebAPI 2+
* Materialize CSS
* Material Icons 
* Jquery
* Newtonsoft.Json
* Autofac
* Asp.net 5
* Bower (opcional)
    
PS.: Os itens acima são somente para referencia, todas as bibliotecas necessárias estão inclusas no projeto.

# API
    web.config:
        dbConnection = conexao com o banco de dados real
        AuthUrl = endereço onde AuthFramework está hospedado.
# AuthFramework
	web.config
        dbConnection = conexao com o banco de dados real
# Api.Tests
	web.config
        dbConnection = conexao com o banco de dados real
# AuthModule
	app.config
        AuthUrl = endereço onde AuthFramework está hospedado.
# Administrator
    não possui conexão com o banco de dados.
        
# Exemplo de um conexão com o banco:
    Server=dns1.database.com\express;Database=DB01;Integrated Security=true;        