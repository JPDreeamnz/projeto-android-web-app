﻿using System.Collections.Generic;

namespace Api.Models.DataTables
{
    public class TableProduto
    {
        public int QuantidadeRegistros { get; set; }
        public List<ProdutoTabela> Produtos { get; set; }
    }

    public class ProdutoTabela : TabelaPadrao
    {
        public string Nome { get; set; }
        public string Marca { get; set; }
        public string ValorVenda { get; set; }
        public string QuantidadeEstoque { get; set; }
        public string Status { get; set; }
    }
}
