﻿using System.Collections.Generic;

namespace Api.Models.DataTables
{
    public class TablePedidoVenda
    {
        public int QuantidadeRegistros { get; set; }
        public List<PedidoVendaTabela> Pedidos { get; set; }
    }

    public class PedidoVendaTabela : TabelaPadrao
    {
        public string Nome { get; set; }
        public string DataPedido { get; set; }
        public string Valor { get; set; }
    }
}
