﻿
using System.Collections.Generic;

namespace Api.Models.DataTables
{
    public class TableAuditoria
    {
        public int QuantidadeRegistros { get; set; }
        public List<AuditoriaTabela> Auditorias { get; set; }
    }

    public class AuditoriaTabela : TabelaPadrao
    {
        public string Nome { get; set; }
        public string UsuarioEmail { get; set; }
        public string DataOperacao { get; set; }
        public string Operacao { get; set; }
        public string Tabela { get; set; }
        public string Objeto { get; set; }
    }
}
