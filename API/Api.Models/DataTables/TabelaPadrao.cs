﻿namespace Api.Models.DataTables
{
    /// <summary>
    /// Classe base para tabelas do DataTable
    /// Herdar e completar a classe com os itens da tabela de listagem
    /// </summary>
    public class TabelaPadrao
    {
        /// <summary>
        /// Todo Registro possui o Código único, seta-lo aqui.
        /// </summary>
        public int Codigo { get; set; }
        /// <summary>
        /// Campo para ser preenchido com os botões padrões para manipulação de dados.
        /// </summary>
        public string Botoes { get; set; }
    }
}
