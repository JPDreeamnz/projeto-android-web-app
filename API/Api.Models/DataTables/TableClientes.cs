﻿using Api.Enums;
using Api.Models.Model;
using System.Collections.Generic;

namespace Api.Models.DataTables
{
    public class TableClientesRetorno
    {
        public int QuantidadeRegistros { get; set; }
        public List<ClienteTabela> Clientes { get; set; }
    }

    public class ClienteTabela : TabelaPadrao
    {
        public string Nome { get; set; }
        public string TipoDocumento { get; set; }
        public long? Documento { get; set; }
        public string InscricaoEstadual { get; set; }
        public string StatusCliente { get; set; }
    }
}
