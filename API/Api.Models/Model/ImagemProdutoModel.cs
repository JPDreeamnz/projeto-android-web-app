﻿
using Api.Enums;

namespace Api.Models.Model
{
    public class ImagemProdutoModel
    {
        public int CodigoProduto { get; set; }
        public int Sequencial { get; set; }
        public string CaminhoPasta { get; set; }
        public string Extensao { get; set; }
        public EstadoEntidade Situacao { get; set; }
    }
}
