﻿namespace Api.Models.Model
{
    public class ItensPedidoModel
    {
        public int PedidoCodigo { get; set; }        
        public int ItemCodigo { get; set; }
        public int ProdutoCodigo { get; set; }
        public decimal Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorTotal { get; set; }
        public ProdutoModel Produto { get; set; }
    }
}
