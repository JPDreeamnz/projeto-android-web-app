﻿using System.Collections.Generic;

namespace Api.Models.Model
{
    public class GrupoUsuariosModel
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }

        public virtual List<PermissionamentoModel> Permissionamento { get; set; }
    }
}
