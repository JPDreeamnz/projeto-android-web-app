﻿using Api.Enums;

namespace Api.Models.Model
{
    public class ParametrosModel
    {
        public int Codigo { get; set; }
        public Situacao? ValidaEstoque { get; set; }
        public int? ClienteDefault { get; set; }
        public int? VendedorDefault { get; set; }
        public int? PlanoPagamentoDefault { get; set; }
        public Situacao? TipoIntegracao { get; set; }
    }
}
