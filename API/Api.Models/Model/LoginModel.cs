﻿namespace Api.Models
{
    /// <summary>
    /// Modelo para transito de informções de login.
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Nome do usuário
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Senha do usuário
        /// </summary>
        public string Password { get; set; }
    }
}