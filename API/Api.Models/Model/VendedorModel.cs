﻿using Api.Enums;
using System;

namespace Api.Models.Model
{
    public class VendedorModel
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Observacao { get; set; }
        public DateTime DataRegistro { get; set; }
        public Situacao StatusVendedor { get; set; }
    }
}
