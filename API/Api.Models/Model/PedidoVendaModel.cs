﻿using Api.Enums;
using System;
using System.Collections.Generic;

namespace Api.Models.Model
{
    public class PedidoVendaModel
    {
        public int PedidoCodigo { get; set; }
        public int ClienteCodigo { get; set; }
        public int VendedorCodigo { get; set; }
        public int PlanoPagamentoCodigo { get; set; }
        public DateTime DataPedido { get; set; }
        public decimal ValorProduto { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorAcrescimo { get; set; }
        public decimal Valor { get; set; }
        public string Observacao { get; set; }
        public DateTime DataRegistro { get; set; }
        public Situacao StatusIntegracao { get; set; }

        public virtual ClienteModel Cliente { get; set; }
        public virtual VendedorModel Vendedor { get; set; }
        public virtual PlanoPagamentoModel PlanoPagamento { get; set; }

        public virtual List<ItensPedidoModel> ItensPedido { get; set; }
    }
}
