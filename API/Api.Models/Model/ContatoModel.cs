﻿using Api.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models.Model
{
    public class ContatoModel
    {
        public int ContatoID { get; set; }
        public string DDD { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public TipoContato TipoContato { get; set; }
    }
}
