﻿using Api.Enums;
using Api.Models.Model;
using System;

namespace Api.Models.Model
{
    public class PermissionamentoModel
    {
        public int GrupoUsuariosCodigo { get; set; }
        public int PermissionamentoCodigo { get; set; }
        public PermissionamentoController ControllerCodigo { get; set; }
        public TipoPermissionamento TipoPermissaoCodigo { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
