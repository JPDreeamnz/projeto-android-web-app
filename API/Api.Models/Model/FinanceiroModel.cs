﻿using System;

namespace Api.Models.Model
{
    public class FinanceiroModel
    {
        public int ClienteCodigo { get; set; }
        public string Documento { get; set; }
        public DateTime Emissao { get; set; }
        public DateTime Vencimento { get; set; }
        public DateTime? Pagamento { get; set; }
        public decimal Valor { get; set; }
        public DateTime DataRegistro { get; set; }
        public ClienteModel Cliente { get; set; }
    }
}
