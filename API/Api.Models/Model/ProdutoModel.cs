﻿using Api.Enums;
using System;
using System.Collections.Generic;

namespace Api.Models.Model
{
    public class ProdutoModel
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string Unidade { get; set; }
        public string CodigoBarras { get; set; }
        public string CodigoFabrica { get; set; }
        public string CodigoOriginal { get; set; }
        public string Cor { get; set; }
        public string Tamanho { get; set; }
        public string Marca { get; set; }
        public decimal? ValorVenda { get; set; }
        public decimal? QuantidadeEstoque { get; set; }
        public DateTime DataRegistro { get; set; }
        public Situacao Status { get; set; }
        public virtual List<ImagemProdutoModel> ImagensProduto { get; set; }
    }
}
