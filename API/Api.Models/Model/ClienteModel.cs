﻿using Api.Enums;
using System;
using System.Collections.Generic;

namespace Api.Models.Model
{
    public class ClienteModel
    {
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public TipoDocumento TipoDocumento { get; set; }
        public long? Documento { get; set; }
        public string InscricaoEstadual { get; set; }
        public string Observacao { get; set; }
        public decimal? LimiteCredito { get; set; }
        public List<EnderecoModel> Endereco { get; set; }
        public List<ContatoModel> Contato { get; set; }
        public DateTime DataRegistro { get; set; }
        public Situacao StatusCliente { get; set; }
    }
}
