﻿using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models.POCO
{
    public class RequestImagens
    {
        public string imagens { get; set; }
        public string token { get; set; }
    }
}
