﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models.POCO
{
    public class ControleLogin
    {
        public string Token { get; set; }
        public UserModel Usuario { get; set; }
    }
}
