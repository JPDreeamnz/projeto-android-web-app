﻿
namespace Api.Models
{
    public class RetornoRequest
    {
        public bool Retorno { get; set; }
        public string Mensagem { get; set; }

        public RetornoRequest(bool Retorno, string Mensagem)
        {
            this.Retorno = Retorno;
            this.Mensagem = Mensagem;
        }
    }
}
