﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models.POCO
{
    public class RetornoExcluidos
    {
        public string Tabela { get; set; }
        public string ID { get; set; }
    }
}
