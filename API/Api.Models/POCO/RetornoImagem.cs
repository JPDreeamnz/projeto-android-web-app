﻿using System.Collections.Generic;

namespace Api.Models.POCO
{
    public class RetornoImagem
    {
        public int Sequencial { get; set; }
        public string Imagem { get; set; }
    }
}
