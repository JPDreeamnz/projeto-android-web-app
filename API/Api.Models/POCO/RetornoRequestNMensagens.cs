﻿using System.Collections.Generic;

namespace Api.Models.POCO
{
    public class RetornoRequestNMensagens
    {
        public bool Retorno { get; set; }
        public List<string> Mensagem { get; set; }

        public RetornoRequestNMensagens()
        {
            this.Mensagem = new List<string>();
        }
    }
}
