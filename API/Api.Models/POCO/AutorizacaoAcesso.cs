﻿using Api.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models.POCO
{
    public class AutorizacaoAcesso
    {
        public int GrupoUsuariosCodigo { get; set; }
        public PermissionamentoController Controller { get; set; }
        public TipoPermissionamento TipoPermissionamento { get; set; }
    }
}
