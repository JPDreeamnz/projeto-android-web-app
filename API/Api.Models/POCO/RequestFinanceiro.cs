﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Models.POCO
{
    public class RequestFinanceiro
    {
        public int ClienteCodigo { get; set; } 
        public string Documento { get; set; }
    }
}
