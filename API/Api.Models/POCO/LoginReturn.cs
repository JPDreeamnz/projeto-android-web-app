﻿namespace Api.Models
{
    public class LoginReturn
    {
        public Api.Enums.LoginMessages MessageId { get; set; }

        public string MessageText
        {
            get
            {
                return this.MessageId.ToString();
            }
            private set { }
        }

        public string Token { get; set; }

        public bool Status { get; set; }
    }
}