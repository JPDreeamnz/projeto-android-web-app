﻿namespace Api.Models.POCO
{
    public class RetornoVendedorPlanoPadrao
    {
        public int VendedorDefault { get; set; }
        public int PlanoPagamentoDefault { get; set; }
    }
}
