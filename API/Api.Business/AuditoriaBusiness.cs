﻿using System;
using System.Collections.Generic;
using Api.Business.Interfaces;
using Api.Models.Model;
using Api.Models.POCO;
using Api.Repository.Interfaces;
using Api.Models.DataTables;

namespace Api.Business
{
    public class AuditoriaBusiness : IAuditoriaBusiness
    {
        private IAuditoriaRepository _auditoriaRepository;

        public AuditoriaBusiness(IAuditoriaRepository auditoriaRepository)
        {
            this._auditoriaRepository = auditoriaRepository;
        }

        public AuditoriaModel ObterAuditoria(int codigo)
        {
            return this._auditoriaRepository.ObterAuditoria(codigo);
        }

        public List<AuditoriaModel> ObterLista()
        {
            return this._auditoriaRepository.ObterLista();
        }

        public List<AuditoriaTabela> ObterLista(JQueryDataTableParam param)
        {
            return _auditoriaRepository.ObterLista(param);
        }

        public List<RetornoExcluidos> ObterListaExcluidos(DateTime dataRegistros)
        {
            return _auditoriaRepository.ObterListaExcluidos(dataRegistros);
        }

        public int QuantidadeTotalRegistros()
        {
            return _auditoriaRepository.QuantidadeTotalRegistros();
        }
    }
}
