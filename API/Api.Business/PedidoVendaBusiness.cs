﻿
using System;
using System.Collections.Generic;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Models;
using Api.Models.DataTables;
using Api.Models.POCO;

namespace Api.Business.Interfaces
{
    public class PedidoVendaBusiness : IPedidoVendaBusiness
    {
        private IPedidoVendaRepository _pedidoVendaRepository;

        public PedidoVendaBusiness(IPedidoVendaRepository pedidoVendaRepository)
        {
            this._pedidoVendaRepository = pedidoVendaRepository;
        }

        public RetornoRequest Inserir(PedidoVendaModel pedido)
        {
            RetornoRequest resposta;

            try
            {
                if (_pedidoVendaRepository.Incluir(pedido))
                    resposta = new RetornoRequest(true, "Pedido incluído com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Inserir(List<PedidoVendaModel> pedidos)
        {
            RetornoRequest resposta;

            try
            {
                if(_pedidoVendaRepository.Incluir(pedidos))
                    resposta = new RetornoRequest(true, "Pedidos incluídos com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<PedidoVendaModel> ObterLista()
        {
            return this._pedidoVendaRepository.ObterLista();
        }

        public List<PedidoVendaTabela> ObterLista(JQueryDataTableParam param)
        {
            return _pedidoVendaRepository.ObterLista(param);
        }

        public List<PedidoVendaModel> ObterLista(DateTime dataRegistro)
        {
            return this._pedidoVendaRepository.ObterLista(dataRegistro);
        }

        public PedidoVendaModel ObterPedido(int codigo)
        {
            return this._pedidoVendaRepository.ObterPedido(codigo);
        }

        public int QuantidadeTotalRegistros()
        {
            return _pedidoVendaRepository.QuantidadeTotalRegistros();
        }
    }
}
