﻿using System;
using Api.Models;
using Api.Repository;
using Api.Enums;
using System.Diagnostics;
using Api.Business.Interfaces;
using Api.Repository.Interfaces;

namespace Api.Business
{
    public class LoginBusiness : ILoginBusiness
    {
        private ILoginRepository loginRepository;

        public LoginBusiness(ILoginRepository loginRepository)
        {
            this.loginRepository = loginRepository;
        }

        public LoginReturn Login(LoginModel model)
        {
            LoginReturn loginReturn = new LoginReturn();

            loginReturn.Status = loginRepository.Authenticate(model);

            loginReturn.Token = "TokenDeTeste:invalido:2015";

            loginReturn.MessageId = loginReturn.Status ? LoginMessages.LOGIN_SUCCESSFUL : LoginMessages.LOGIN_INVALID;

            return loginReturn;
        }

        public UserModel ObtemUsuario(LoginModel model)
        {
            return this.loginRepository.RetornaUsuarioAutenticado(model);
        }

        /// <summary>
        /// Register era usado para criar um login a partir dos dados enviados por ajax. 
        /// Com a remoção da tabela de login esse método não terá utilidade.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Obsolete]
        public LoginReturn Register(RegistrationModel model)
        {
            LoginReturn loginReturn = new LoginReturn();

            //loginReturn.Status = loginRepository.Add(model);
            loginReturn.MessageId = loginReturn.Status ? LoginMessages.LOGIN_SUCCESSFUL : LoginMessages.LOGIN_INVALID;

            return loginReturn;
        }
    }
}