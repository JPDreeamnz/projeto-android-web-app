﻿using Api.Business.Interfaces;
using Api.Repository.Interfaces;
using System;
using System.Collections.Generic;
using Api.Models;
using Api.Models.Model;

namespace Api.Business
{
    public class ContatoBusiness : IContatoBusiness
    {
        private IContatoRepository _contatoRepository;

        public ContatoBusiness(IContatoRepository contatoRepository)
        {
            this._contatoRepository = contatoRepository;
        }

        public RetornoRequest Editar(ContatoModel objeto)
        {
            RetornoRequest resposta;

            try
            {
                if (this._contatoRepository.Editar(objeto))
                    resposta = new RetornoRequest(true, "Contato alterado com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(ContatoModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (this._contatoRepository.Incluir(objeto))
                    resposta = new RetornoRequest(true, "Contato incluído com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<ContatoModel> ListarContatos()
        {
            return this._contatoRepository.ObterTodos();
        }

        public ContatoModel ObterContato(int contatoID)
        {
            return this._contatoRepository.ObterPorID(contatoID);
        }
    }
}
