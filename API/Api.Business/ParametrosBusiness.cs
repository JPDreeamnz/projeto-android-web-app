﻿
using System;
using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using Api.Repository.Interfaces;

namespace Api.Business
{
    public class ParametrosBusiness : IParametrosBusiness
    {
        private IParametrosRepository _parametrosRepository;

        public ParametrosBusiness(IParametrosRepository parametrosRepository)
        {
            this._parametrosRepository = parametrosRepository;
        }

        //TODO:Sumarizar
        public RetornoRequest Editar(ParametrosModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (!this._parametrosRepository.Editar(objeto))
                {
                    throw new ArgumentException("Não foi possível alterar os parâmetros do sistema na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Parâmetros de sistema alterados com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public ParametrosModel ObterParametros()
        {
            return this._parametrosRepository.ObterParametros();
        }
    }
}
