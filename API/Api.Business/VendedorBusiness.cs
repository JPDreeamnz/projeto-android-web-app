﻿using System;
using System.Collections.Generic;
using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Models.POCO;

namespace Api.Business
{
    public class VendedorBusiness : IVendedorBusiness
    {
        private IVendedorRepository _vendedorRepository;
        public VendedorBusiness(IVendedorRepository vendedorRepository)
        {
            this._vendedorRepository = vendedorRepository;
        }

        //TODO: Sumarizar
        public RetornoRequest Editar(VendedorModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if(!_vendedorRepository.Editar(objeto))
                {
                    throw new ArgumentException("Não foi possível alterar o vendedor na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Vendedor alterado com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequestNMensagens Editar(List<VendedorModel> vendedores)
        {
            RetornoRequestNMensagens resposta = new RetornoRequestNMensagens();
            try
            {
                var update = _vendedorRepository.Editar(vendedores);
                if (update.Count == 0)
                {
                    resposta.Retorno = true;
                }
                else
                {
                    resposta.Retorno = false;
                    resposta.Mensagem = update;
                }
            }
            catch (Exception e)
            {
                resposta.Retorno = false;
                resposta.Mensagem.Add(e.Message);
            }

            return resposta;
        }

        public bool Excluir(int codigo)
        {
            return _vendedorRepository.Excluir(codigo);
        }

        public RetornoRequestNMensagens Excluir(List<int> codigos)
        {
            RetornoRequestNMensagens resposta = new RetornoRequestNMensagens();
            try
            {
                var update = _vendedorRepository.Excluir(codigos);
                if (update.Count == 0)
                {
                    resposta.Retorno = true;
                }
                else
                {
                    resposta.Retorno = false;
                    resposta.Mensagem = update;
                }
            }
            catch (Exception e)
            {
                resposta.Retorno = false;
                resposta.Mensagem.Add(e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(VendedorModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (!_vendedorRepository.Incluir(objeto))
                {
                    throw new ArgumentException("Não foi possível inserir o vendedor na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Vendedor inserido com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(List<VendedorModel> vendedores)
        {
            RetornoRequest resposta;
            try
            {
                if (!_vendedorRepository.Incluir(vendedores))
                {
                    throw new ArgumentException("Não foi possível inserir os vendedores na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Vendedores inseridos com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<VendedorModel> ListarVendedores()
        {
            return _vendedorRepository.ObterTodos();
        }

        public List<VendedorModel> ListarVendedores(DateTime dataRegistro)
        {
            return _vendedorRepository.ObterTodos(dataRegistro);
        }

        public VendedorModel ObterVendedor(int codigo)
        {
            return _vendedorRepository.ObterPorID(codigo);
        }
    }
}
