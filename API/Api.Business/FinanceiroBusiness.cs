﻿using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using Api.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models.POCO;

namespace Api.Business
{
    public class FinanceiroBusiness : IFinanceiroBusiness
    {
        private IFinanceiroRepository _financeiroRepository;

        public FinanceiroBusiness(IFinanceiroRepository financeiroRepository)
        {
            this._financeiroRepository = financeiroRepository;
        }

        public RetornoRequest Editar(FinanceiroModel objeto)
        {
            RetornoRequest resposta;

            try
            {
                if (this._financeiroRepository.Editar(objeto))
                    resposta = new RetornoRequest(true, "Cadastro financeiro alterado com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(FinanceiroModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (this._financeiroRepository.Incluir(objeto))
                    resposta = new RetornoRequest(true, "Cadastro financeiro incluído com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(List<FinanceiroModel> financeiros)
        {
            RetornoRequest resposta;
            try
            {
                if (this._financeiroRepository.Incluir(financeiros))
                    resposta = new RetornoRequest(true, "Cadastros financeiros incluídos com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<FinanceiroModel> ListarCadastrosFinanceiros()
        {
            return this._financeiroRepository.ObterTodos();
        }

        public List<FinanceiroModel> ListarCadastrosFinanceiros(DateTime dataRegistro)
        {
            return this._financeiroRepository.ObterTodos(dataRegistro);
        }

        public FinanceiroModel ObterCadastroFinanceiro(int contatoCodigo, string documento)
        {
            return this._financeiroRepository.ObterPorID(contatoCodigo, documento);
        }

        public bool Excluir(int clienteCodigo, string documento)
        {
            return this._financeiroRepository.Excluir(clienteCodigo, documento);
        }

        public RetornoRequestNMensagens Editar(List<FinanceiroModel> financeiro)
        {
            RetornoRequestNMensagens resposta = new RetornoRequestNMensagens();
            try
            {
                var update = _financeiroRepository.Editar(financeiro);
                if (update.Count == 0)
                {
                    resposta.Retorno = true;
                }
                else
                {
                    resposta.Retorno = false;
                    resposta.Mensagem = update;
                }
            }
            catch (Exception e)
            {
                resposta.Retorno = false;
                resposta.Mensagem.Add(e.Message);
            }

            return resposta;
        }

        public RetornoRequestNMensagens Excluir(List<RequestFinanceiro> financeiro)
        {
            RetornoRequestNMensagens retorno = new RetornoRequestNMensagens();
            var excluidos = _financeiroRepository.Excluir(financeiro);
            if (excluidos.Count > 0)
            {
                retorno.Retorno = false;
                retorno.Mensagem = excluidos;
            }
            else
            {
                retorno.Retorno = true;
                retorno.Mensagem.Add("Itens excluidos com sucesso.");
            }

            return retorno;
        }
    }
}
