﻿using Api.Business.Interfaces;
using Api.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using Api.Models.DataTables;

namespace Api.Business
{
    public class ClienteBusiness : IClienteBusiness
    {
        private IClienteRepository _clienteRepository;

        public ClienteBusiness(IClienteRepository clienteRepository)
        {
            this._clienteRepository = clienteRepository;
        }

        public RetornoRequest Editar(ClienteModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (!_clienteRepository.Editar(objeto))
                {
                    throw new ArgumentException("Não foi possível alterar o cliente na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Cliente alterado com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequestNMensagens Editar(List<ClienteModel> clientes)
        {
            RetornoRequestNMensagens resposta = new RetornoRequestNMensagens();
            try
            {
                var update = _clienteRepository.Editar(clientes);
                if (update.Count == 0)
                {
                    resposta.Retorno = true;
                }
                else
                {
                    resposta.Retorno = false;
                    resposta.Mensagem = update;
                }
            }
            catch (Exception e)
            {
                resposta.Retorno = false;
                resposta.Mensagem.Add(e.Message);
            }

            return resposta;
        }

        public bool Excluir(int codigo)
        {
            return _clienteRepository.Excluir(codigo);
        }

        public RetornoRequestNMensagens Excluir(List<int> codigos)
        {
            RetornoRequestNMensagens retorno = new RetornoRequestNMensagens();
            var excluidos = _clienteRepository.Excluir(codigos);
            if(excluidos.Count > 0)
            {
                retorno.Retorno = false;
                retorno.Mensagem = excluidos;
            }
            else
            {
                retorno.Retorno = true;
                retorno.Mensagem.Add("Itens excluidos com sucesso.");
            }

            return retorno;
        }

        public RetornoRequest Incluir(ClienteModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (!_clienteRepository.Incluir(objeto))
                {
                    throw new ArgumentException("Não foi possível inserir o cliente na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Cliente inserido com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(List<ClienteModel> objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (!_clienteRepository.Incluir(objeto))
                {
                    throw new ArgumentException("Não foi possível inserir os clientes na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Clientes inseridos com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<ClienteModel> ListarClientes()
        {
            return _clienteRepository.ObterTodos();
        }

        public List<ClienteTabela> ListarClientes(JQueryDataTableParam param)
        {
            return _clienteRepository.ObterTodos(param);
        }

        public List<ClienteModel> ListarClientes(DateTime dataRegistro)
        {
            return _clienteRepository.ObterTodos(dataRegistro);
        }

        public ClienteModel ObterCliente(int codigo)
        {
            return _clienteRepository.ObterPorID(codigo);
        }

        public int QuantidadeTotalRegistros()
        {
            return _clienteRepository.QuantidadeTotalRegistros();
        }
    }
}
