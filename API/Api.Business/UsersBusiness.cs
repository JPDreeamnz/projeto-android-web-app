﻿using Api.Business.Interfaces;
using Api.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Api.Models;
using Api.Database.Entities;

namespace Api.Business
{
    public class UsersBusiness : IUsersBusiness
    {
        IUsersRepository _usersRepository;

        public UsersBusiness(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public RetornoRequest Add(UserModel model)
        {
            RetornoRequest resposta;
            try
            {
                if (_usersRepository.EmailIsValid(model.Email))
                    throw new ArgumentException(string.Format("Já existe um usuário cadastrado com o e-mail {0}, entre em contato com o administrador", model.Email));
                if (!model.Password.Equals(model.PasswordCheck))
                    throw new ArgumentException(string.Format("A senha não foi confirmada corretamente. Favor inserir novamente."));

                UserEntity newUser = new UserEntity();

                newUser.Active = model.Active;
                newUser.LastName = model.LastName;
                newUser.Name = model.Name;
                newUser.GrupoUsuariosCodigo = model.GrupoUsuariosCodigo;
                newUser.LoginName = model.Name;
                newUser.Email = model.Email;
                newUser.Password = model.Password;
                newUser.Active = model.Active;

                if (_usersRepository.Add(newUser))
                {
                    // retorna referencia do objeto adicionado.
                    model.UserID = newUser.UserID;
                    resposta = new RetornoRequest(true, "Usuário cadastrado com sucesso.");
                }
                else
                {
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
                }
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }
            
            return resposta;
        }

        public RetornoRequest Update(UserModel model)
        {
            RetornoRequest resposta;

            try
            {
                UserModel usuario = this.Get(model.UserID);
                if (usuario == null)
                    throw new ArgumentException("Não foi encontrado o usuário selecionado. Favor atualize a página.");

                if (!string.IsNullOrWhiteSpace(model.Password))
                {
                    if (!model.Password.Equals(model.PasswordCheck))
                        throw new ArgumentException("A senha não foi confirmada corretamente. Favor inserir novamente.");
                    if (model.Password.Equals(usuario.Password))
                        throw new ArgumentException("Favor inserir uma senha diferente da antiga.");
                }

                if (!model.Email.Equals(usuario.Email))
                {
                    List<UserModel> usuarios = this.GetAll();
                    if (usuarios.Any(x => x.Email.Equals(model.Email) && x.UserID != usuario.UserID))
                        throw new ArgumentException(string.Format("Já existe um usuário cadastrado com o e-mail {0}, entre em contato com o administrador", model.Email));
                }

                if (_usersRepository.Update(model))
                    resposta = new RetornoRequest(true, "Usuário atualizado com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<UserModel> GetAll()
        {
            return _usersRepository.GetAll();
        }

        public UserModel Get(int userID)
        {
            return _usersRepository.Get(userID);
        }

        public bool Delete(int id)
        {
            return _usersRepository.Delete(id);
        }
    }
}
