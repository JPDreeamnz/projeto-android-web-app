﻿using Api.Models;
using Api.Models.Model;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IEnderecoBusiness
    {
        RetornoRequest Incluir(EnderecoModel objeto);
        RetornoRequest Editar(EnderecoModel objeto);
        List<EnderecoModel> ListarContatos();
        EnderecoModel ObterContato(int enderecoID);
    }
}
