﻿using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IAuditoriaBusiness
    {
        AuditoriaModel ObterAuditoria(int codigo);
        List<AuditoriaModel> ObterLista();
        List<AuditoriaTabela> ObterLista(JQueryDataTableParam param);
        List<RetornoExcluidos> ObterListaExcluidos(DateTime dataRegistros);
        int QuantidadeTotalRegistros();
    }
}
