﻿using Api.Models;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IUsersBusiness
    {
        List<UserModel> GetAll();
        RetornoRequest Add(UserModel model);
        RetornoRequest Update(UserModel model);
        UserModel Get(int userID);
        bool Delete(int id);
    }
}