﻿
using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IImagemProdutoBusiness
    {
        RetornoRequest SalvarImagem(ImagemProdutoModel objeto, byte[] imagem);
        RetornoRequest ExcluirImagem(ImagemProdutoModel objeto);
        RetornoRequest ExcluirPastaImagens(string caminhoPasta);
        IEnumerable<ImagemProdutoModel> ObterListaImagens(int codigoProduto);
        IEnumerable<RetornoImagem> ListaImagens(List<ImagemProdutoModel> imagens);
        byte[] ObterImagem(string caminhoPasta);
        List<ImagemProdutoModel> ObterListaImagensIntegracao(int codigoProduto);
        bool RenomearPasta(int codigoProduto, string caminhoPasta);
        int ObtemProximoSequencial(string caminhoPasta);
    }
}
