﻿using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IGrupoUsuariosBusiness
    {
        RetornoRequest Editar(GrupoUsuariosModel objeto);
        RetornoRequest Incluir(GrupoUsuariosModel objeto);
        List<GrupoUsuariosModel> ListarGrupos();
        GrupoUsuariosModel ObterGrupo(int codigo);
        bool Excluir(int codigo);
        bool AutorizarAcesso(AutorizacaoAcesso model);
        List<PermissionamentoModel> ObterPermissoes(int GrupoUsuarioCodigo);
    }
}
