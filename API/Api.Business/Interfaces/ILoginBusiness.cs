﻿using System;
using Api.Models;

namespace Api.Business.Interfaces
{
    public interface ILoginBusiness
    {
        LoginReturn Login(LoginModel model);
        LoginReturn Register(RegistrationModel model);
        UserModel ObtemUsuario(LoginModel model);
    }
}