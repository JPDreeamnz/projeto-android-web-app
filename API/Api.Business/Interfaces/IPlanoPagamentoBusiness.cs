﻿
using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IPlanoPagamentoBusiness
    {
        RetornoRequest Incluir(PlanoPagamentoModel objeto);
        RetornoRequest Incluir(List<PlanoPagamentoModel> planos);
        RetornoRequest Editar(PlanoPagamentoModel objeto);
        RetornoRequestNMensagens Editar(List<PlanoPagamentoModel> planos);
        bool Excluir(int codigo);
        RetornoRequestNMensagens Excluir(List<int> planosCodigo);
        List<PlanoPagamentoModel> ListarPlanos();
        List<PlanoPagamentoModel> ListarPlanos(DateTime dataRegistro);
        PlanoPagamentoModel ObterPlano(int codigo);
    }
}
