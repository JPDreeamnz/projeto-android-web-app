﻿using Api.Models;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IPedidoVendaBusiness
    {
        RetornoRequest Inserir(PedidoVendaModel pedidos);
        RetornoRequest Inserir(List<PedidoVendaModel> pedidos);
        PedidoVendaModel ObterPedido(int codigo);
        List<PedidoVendaModel> ObterLista();
        List<PedidoVendaTabela> ObterLista(JQueryDataTableParam param);
        List<PedidoVendaModel> ObterLista(DateTime dataRegistro);
        int QuantidadeTotalRegistros();
    }
}
