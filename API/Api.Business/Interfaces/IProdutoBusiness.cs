﻿using Api.Models;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IProdutoBusiness
    {
        RetornoRequest Incluir(ProdutoModel objeto);
        RetornoRequest Incluir(List<ProdutoModel> produtos);
        RetornoRequest Editar(ProdutoModel objeto);
        RetornoRequestNMensagens Editar(List<ProdutoModel> produtos);
        List<ProdutoModel> ListarProdutos();
        List<ProdutoTabela> ListarProdutos(JQueryDataTableParam param);
        List<ProdutoModel> ListarProdutos(DateTime dataRegistro);
        ProdutoModel ObterProduto(int codigo);
        bool Excluir(int codigo);
        RetornoRequestNMensagens Excluir(List<int> codigos);
        int QuantidadeTotalRegistros();
    }
}
