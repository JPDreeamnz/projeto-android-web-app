﻿using Api.Models;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Business.Interfaces
{
    public interface IClienteBusiness
    {
        RetornoRequest Incluir(ClienteModel objeto);
        RetornoRequest Incluir(List<ClienteModel> clientes);
        RetornoRequest Editar(ClienteModel objeto);
        RetornoRequestNMensagens Editar(List<ClienteModel> clientes);
        bool Excluir(int codigo);
        RetornoRequestNMensagens Excluir(List<int> codigos);
        List<ClienteModel> ListarClientes();
        List<ClienteTabela> ListarClientes(JQueryDataTableParam param);
        List<ClienteModel> ListarClientes(DateTime dataRegistro);
        int QuantidadeTotalRegistros();
        ClienteModel ObterCliente(int codigo);
    }
}
