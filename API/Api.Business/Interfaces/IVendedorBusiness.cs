﻿using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IVendedorBusiness
    {
        RetornoRequest Incluir(VendedorModel objeto);
        RetornoRequest Incluir(List<VendedorModel> vendedores);
        RetornoRequest Editar(VendedorModel objeto);
        RetornoRequestNMensagens Editar(List<VendedorModel> vendedores);
        bool Excluir(int codigo);
        RetornoRequestNMensagens Excluir(List<int> codigos);
        List<VendedorModel> ListarVendedores();
        List<VendedorModel> ListarVendedores(DateTime dataRegistro);
        VendedorModel ObterVendedor(int codigo);
    }
}
