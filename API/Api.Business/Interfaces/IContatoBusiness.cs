﻿using Api.Models;
using Api.Models.Model;
using System.Collections.Generic;

namespace Api.Business.Interfaces
{
    public interface IContatoBusiness
    {
        RetornoRequest Incluir(ContatoModel objeto);
        RetornoRequest Editar(ContatoModel objeto);
        List<ContatoModel> ListarContatos();
        ContatoModel ObterContato(int contatoID);
    }
}
