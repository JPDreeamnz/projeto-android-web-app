﻿
using Api.Models;
using Api.Models.Model;

namespace Api.Business.Interfaces
{
    public interface IParametrosBusiness
    {
        RetornoRequest Editar(ParametrosModel objeto);
        ParametrosModel ObterParametros();
    }
}
