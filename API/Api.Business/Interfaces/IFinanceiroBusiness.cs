﻿using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Business.Interfaces
{
    public interface IFinanceiroBusiness
    {
        RetornoRequest Incluir(FinanceiroModel objeto);
        RetornoRequest Incluir(List<FinanceiroModel> financeiros);
        RetornoRequest Editar(FinanceiroModel objeto);
        RetornoRequestNMensagens Editar(List<FinanceiroModel> financeiro);
        List<FinanceiroModel> ListarCadastrosFinanceiros();
        List<FinanceiroModel> ListarCadastrosFinanceiros(DateTime dataRegistro);
        FinanceiroModel ObterCadastroFinanceiro(int clienteCodigo, string documento);
        bool Excluir(int clienteCodigo, string documento);
        RetornoRequestNMensagens Excluir(List<RequestFinanceiro> financeiro);
    }
}
