﻿
using System;
using Api.Business.Interfaces;
using System.IO;
using Api.Models;
using System.Drawing;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using Api.Models.Model;
using System.Collections.Generic;
using Api.Repository.Interfaces;
using Api.Models.POCO;
using System.Linq;

namespace Api.Business
{
    public class ImagemProdutoBusiness : IImagemProdutoBusiness
    {
        private IImagemProdutoRepository _imagemProdutoRepository;

        public ImagemProdutoBusiness(IImagemProdutoRepository imagemProdutoRepository)
        {
            this._imagemProdutoRepository = imagemProdutoRepository;
        }
        
        public RetornoRequest ExcluirImagem(ImagemProdutoModel objeto)
        {
            bool retorno = false;
            string mensagem = string.Empty;

            try
            {
                if(this._imagemProdutoRepository.ExcluirImagem(objeto))
                {
                    retorno = true;
                    mensagem = "Imagem excluída com sucesso";
                }
            }
            catch (FileNotFoundException f)
            {
                retorno = false;
                mensagem = f.Message;
            }
            catch (Exception e)
            {

                retorno = false;
                mensagem = e.Message;
            }

            this._imagemProdutoRepository.Dispose();
            return new RetornoRequest(retorno, mensagem);
        }

        public RetornoRequest ExcluirPastaImagens(string caminhoPasta)
        {
            bool retorno = false;
            string mensagem = string.Empty;

            try
            {
                if (this._imagemProdutoRepository.ExcluirPastaImagens(caminhoPasta))
                {
                    retorno = true;
                    mensagem = "Pasta excluída com sucesso";
                }
            }
            catch (FileNotFoundException f)
            {
                retorno = false;
                mensagem = f.Message;
            }
            catch (Exception e)
            {

                retorno = false;
                mensagem = e.Message;
            }

            this._imagemProdutoRepository.Dispose();
            return new RetornoRequest(retorno, mensagem);
        }

        public RetornoRequest SalvarImagem(ImagemProdutoModel objeto, byte[] imagem)
        {
            bool retorno = false;
            string mensagem = string.Empty;

            try
            {
                if(this._imagemProdutoRepository.SalvarImagem(objeto, imagem))
                {
                    retorno = true;
                    mensagem = "Imagem inserida com sucesso.";
                }
            }
            catch (Exception e)
            {
                retorno = false;
                mensagem = e.Message;
            }

            this._imagemProdutoRepository.Dispose();
            return new RetornoRequest(retorno, mensagem);
        }

        public IEnumerable<ImagemProdutoModel> ObterListaImagens(int codigoProduto)
        {
            return this._imagemProdutoRepository.ListaImagensArquivo(codigoProduto.ToString()).Select(caminho => new ImagemProdutoModel
            {
                CodigoProduto = codigoProduto,
                CaminhoPasta = codigoProduto.ToString(),
                Extensao = Path.GetExtension(caminho).Substring(1),
                Sequencial = int.Parse(Path.GetFileNameWithoutExtension(caminho)),
                Situacao = Enums.EstadoEntidade.VISUALIZADO
            });
        }

        public List<ImagemProdutoModel> ObterListaImagensIntegracao(int codigoProduto)
        {
            return this._imagemProdutoRepository.ListaImagensArquivo(codigoProduto.ToString()).Select(caminho => new ImagemProdutoModel
            {
                CodigoProduto = codigoProduto,
                CaminhoPasta = Path.Combine(this._imagemProdutoRepository.PASTA.Remove(0, 2), codigoProduto.ToString(), Path.GetFileNameWithoutExtension(caminho)),
                Extensao = Path.GetExtension(caminho).Substring(1),
                Sequencial = int.Parse(Path.GetFileNameWithoutExtension(caminho)),
                Situacao = Enums.EstadoEntidade.VISUALIZADO
            }).ToList();

        }

        public byte[] ObterImagem(string caminhoPasta)
        {
            return this._imagemProdutoRepository.ObterImagem(caminhoPasta);
        }

        public IEnumerable<RetornoImagem> ListaImagens(List<ImagemProdutoModel> imagens)
        {
            int[] sequencialMostrar = imagens.Where(img => img.Situacao != Enums.EstadoEntidade.EXCLUIDO).Select(img => img.Sequencial).ToArray();
            return this._imagemProdutoRepository.ObterImagens(imagens.First().CaminhoPasta)
                .Where(img => sequencialMostrar.Contains(img.Key))
                .Select(img => new RetornoImagem
                {
                    Sequencial = img.Key,
                    Imagem = Convert.ToBase64String(img.Value)
                });
        }

        public bool RenomearPasta(int codigoProduto, string caminhoPasta)
        {
            return this._imagemProdutoRepository.RenomearPasta(codigoProduto, caminhoPasta);
        }

        public int ObtemProximoSequencial(string caminhoPasta)
        {
            int maiorSequencial = 0;
            if (this._imagemProdutoRepository.VerificaExistenciaPastaSistema(caminhoPasta))
            {
                foreach (var imagem in this._imagemProdutoRepository.ListaImagensArquivo(caminhoPasta))
                {
                    int sequencial = int.Parse(Path.GetFileNameWithoutExtension(imagem));
                    if (sequencial > maiorSequencial)
                        maiorSequencial = sequencial;
                }
            }

            return maiorSequencial + 1;
        }
    }
}
