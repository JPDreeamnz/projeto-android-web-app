﻿
using Api.Business.Interfaces;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using Api.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Business
{
    public class GrupoUsuariosBusiness : IGrupoUsuariosBusiness
    {
        private IGrupoUsuariosRepository _grupoUsuariosRespository;

        public GrupoUsuariosBusiness(IGrupoUsuariosRepository grupoUsuariosRepository)
        {
            this._grupoUsuariosRespository = grupoUsuariosRepository;
        }

        public RetornoRequest Editar(GrupoUsuariosModel objeto)
        {
            RetornoRequest resposta;

            try
            {
                objeto.Permissionamento.ForEach(perm => {
                    perm.DataAlteracao = DateTime.Now;
                });

                if (this._grupoUsuariosRespository.Editar(objeto))
                    resposta = new RetornoRequest(true, "Grupo de Usuários alterado com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(GrupoUsuariosModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                objeto.Permissionamento.ForEach(perm => {
                    perm.DataAlteracao = DateTime.Now;
                });

                if (this._grupoUsuariosRespository.Incluir(objeto))
                    resposta = new RetornoRequest(true, "Grupo de Usuários incluído com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<GrupoUsuariosModel> ListarGrupos()
        {
            return this._grupoUsuariosRespository.ObterTodos();
        }

        public GrupoUsuariosModel ObterGrupo(int codigo)
        {
            return this._grupoUsuariosRespository.ObterPorID(codigo);
        }

        public bool Excluir(int codigo)
        {
            return this._grupoUsuariosRespository.Excluir(codigo);
        }

        public bool AutorizarAcesso(AutorizacaoAcesso model)
        {
            return this._grupoUsuariosRespository.AutorizarAcesso(model);
        }

        public List<PermissionamentoModel> ObterPermissoes(int GrupoUsuarioCodigo)
        {
            return this._grupoUsuariosRespository.ObterPermissionamento(GrupoUsuarioCodigo);
        }
    }
}
