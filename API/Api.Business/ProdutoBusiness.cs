﻿using System;
using System.Collections.Generic;
using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using Api.Repository.Interfaces;
using System.Transactions;
using System.Linq;
using Api.Models.POCO;
using Api.Models.DataTables;

namespace Api.Business
{
    public class ProdutoBusiness : IProdutoBusiness
    {
        private IProdutoRepository _produtoRepository;
        private IImagemProdutoBusiness _imagemProdutoBusiness;

        public ProdutoBusiness(IProdutoRepository produtoRepository, IImagemProdutoBusiness imagemProdutoBusiness)
        {
            this._produtoRepository = produtoRepository;
            this._imagemProdutoBusiness = imagemProdutoBusiness;
        }
        
        public RetornoRequest Editar(ProdutoModel objeto)
        {
            RetornoRequest resposta;

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (this._produtoRepository.Editar(objeto))
                    {
                        if (objeto.ImagensProduto != null)
                        {
                            if (objeto.ImagensProduto.Count > 0)
                            {
                                foreach (var imagemExcluir in objeto.ImagensProduto.Where(img => img.Situacao == Enums.EstadoEntidade.EXCLUIDO))
                                {
                                    this._imagemProdutoBusiness.ExcluirImagem(imagemExcluir);
                                }
                            }
                            else
                            {
                                this._imagemProdutoBusiness.ExcluirPastaImagens(objeto.ImagensProduto.First().CaminhoPasta);
                            }
                        }
                        resposta = new RetornoRequest(true, "Produto alterado com sucesso.");
                        scope.Complete();
                    }
                    else
                    {
                        scope.Dispose();
                        throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
                    }
                }
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequestNMensagens Editar(List<ProdutoModel> produtos)
        {
            RetornoRequestNMensagens resposta = new RetornoRequestNMensagens();
            try
            {
                var update = _produtoRepository.Editar(produtos);
                if (update.Count == 0)
                {
                    resposta.Retorno = true;
                }
                else
                {
                    resposta.Retorno = false;
                    resposta.Mensagem = update;
                }
            }
            catch (Exception e)
            {
                resposta.Retorno = false;
                resposta.Mensagem.Add(e.Message);
            }

            return resposta;
        }

        public bool Excluir(int codigo)
        {
            if (this._produtoRepository.Excluir(codigo))
            {
                this._imagemProdutoBusiness.ExcluirPastaImagens(codigo.ToString());
            }

            return true;
        }

        public RetornoRequestNMensagens Excluir(List<int> codigos)
        {
            RetornoRequestNMensagens retorno = new RetornoRequestNMensagens();
            var excluidos = _produtoRepository.Excluir(codigos);
            if (excluidos.Count > 0)
            {
                retorno.Retorno = false;
                retorno.Mensagem = excluidos;
            }
            else
            {
                retorno.Retorno = true;
                retorno.Mensagem.Add("Itens excluidos com sucesso.");
            }

            return retorno;
        }

        public RetornoRequest Incluir(ProdutoModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    if (this._produtoRepository.Incluir(ref objeto))
                    {
                        if (objeto.ImagensProduto != null)
                        {
                            foreach (var imagemExcluir in objeto.ImagensProduto.Where(img => img.Situacao == Enums.EstadoEntidade.EXCLUIDO))
                            {
                                this._imagemProdutoBusiness.ExcluirImagem(imagemExcluir);
                            }
                            
                            if(objeto.ImagensProduto.Any(img => img.Situacao != Enums.EstadoEntidade.EXCLUIDO))
                            { 
                                this._imagemProdutoBusiness.RenomearPasta(objeto.Codigo, objeto.ImagensProduto.First().CaminhoPasta);
                            }
                        }
                        
                        resposta = new RetornoRequest(true, "Produto incluído com sucesso.");
                        scope.Complete();
                    }
                    else
                    {
                        scope.Dispose();
                        throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
                    }
                }
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }
            
            return resposta;
        }

        public RetornoRequest Incluir(List<ProdutoModel> produtos)
        {
            RetornoRequest resposta;
            try
            {
                if (this._produtoRepository.Incluir(produtos))
                    resposta = new RetornoRequest(true, "Produtos incluídos com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<ProdutoModel> ListarProdutos()
        {
            return this._produtoRepository.ObterTodos();
        }

        public List<ProdutoTabela> ListarProdutos(JQueryDataTableParam param)
        {
            return this._produtoRepository.ObterTodos(param);
        }

        public List<ProdutoModel> ListarProdutos(DateTime dataRegistro)
        {
            List<ProdutoModel> produtos = this._produtoRepository.ObterTodos(dataRegistro);
            foreach (var produto in produtos)
            {
                produto.ImagensProduto = this._imagemProdutoBusiness.ObterListaImagensIntegracao(produto.Codigo);
            }

            return produtos;
        }

        public ProdutoModel ObterProduto(int codigo)
        {
            return this._produtoRepository.ObterPorID(codigo);
        }

        public int QuantidadeTotalRegistros()
        {
            return this._produtoRepository.QuantidadeTotalRegistros();
        }
    }
}
