﻿using Api.Business.Interfaces;
using Api.Repository.Interfaces;
using System;
using System.Collections.Generic;
using Api.Models;
using Api.Models.Model;

namespace Api.Business
{
    public class EnderecoBusiness : IEnderecoBusiness
    {
        private IEnderecoRepository _enderecoRepository;

        public EnderecoBusiness(IEnderecoRepository enderecoRepository)
        {
            this._enderecoRepository = enderecoRepository;
        }

        public RetornoRequest Editar(EnderecoModel objeto)
        {
            RetornoRequest resposta;

            try
            {
                if (this._enderecoRepository.Editar(objeto))
                    resposta = new RetornoRequest(true, "Contato alterado com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(EnderecoModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (this._enderecoRepository.Incluir(objeto))
                    resposta = new RetornoRequest(true, "Contato incluído com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<EnderecoModel> ListarContatos()
        {
            return this._enderecoRepository.ObterTodos();
        }

        public EnderecoModel ObterContato(int enderecoID)
        {
            return this._enderecoRepository.ObterPorID(enderecoID);
        }
    }
}
