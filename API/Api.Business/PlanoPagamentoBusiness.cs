﻿
using System;
using System.Collections.Generic;
using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Models.POCO;

namespace Api.Business
{
    public class PlanoPagamentoBusiness : IPlanoPagamentoBusiness
    {
        private IPlanoPagamentoRepository _planoPagamentoRepository;
        public PlanoPagamentoBusiness(IPlanoPagamentoRepository planoPagamentoRepository)
        {
            this._planoPagamentoRepository = planoPagamentoRepository;
        }

        //TODO: Sumarizar
        public RetornoRequest Editar(PlanoPagamentoModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (!_planoPagamentoRepository.Editar(objeto))
                {
                    throw new ArgumentException("Não foi possível alterar o plano de pagamento na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Plano de pagamento alterado com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequestNMensagens Editar(List<PlanoPagamentoModel> planos)
        {
            RetornoRequestNMensagens resposta = new RetornoRequestNMensagens();
            try
            {
                var update = _planoPagamentoRepository.Editar(planos);
                if (update.Count == 0)
                {
                    resposta.Retorno = true;
                }
                else
                {
                    resposta.Retorno = false;
                    resposta.Mensagem = update;
                }
            }
            catch (Exception e)
            {
                resposta.Retorno = false;
                resposta.Mensagem.Add(e.Message);
            }

            return resposta;
        }

        public bool Excluir(int codigo)
        {
            return _planoPagamentoRepository.Excluir(codigo);
        }

        public RetornoRequestNMensagens Excluir(List<int> planosCodigo)
        {
            RetornoRequestNMensagens retorno = new RetornoRequestNMensagens();
            var excluidos = _planoPagamentoRepository.Excluir(planosCodigo);
            if (excluidos.Count > 0)
            {
                retorno.Retorno = false;
                retorno.Mensagem = excluidos;
            }
            else
            {
                retorno.Retorno = true;
                retorno.Mensagem.Add("Itens excluidos com sucesso.");
            }

            return retorno;
        }

        public RetornoRequest Incluir(PlanoPagamentoModel objeto)
        {
            RetornoRequest resposta;
            try
            {
                if (!_planoPagamentoRepository.Incluir(objeto))
                {
                    throw new ArgumentException("Não foi possível inserir o plano de pagamento na base de dados. Tente novamente em alguns minutos.");
                }

                resposta = new RetornoRequest(true, "Plano de pagamento inserido com sucesso.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public RetornoRequest Incluir(List<PlanoPagamentoModel> planos)
        {
            RetornoRequest resposta;
            try
            {
                if (this._planoPagamentoRepository.Incluir(planos))
                    resposta = new RetornoRequest(true, "Cadastros planos de pagamento incluídos com sucesso.");
                else
                    throw new ArgumentException("Não foi possível contatar a base de dados, favor entrar em contato com o suporte do sistema.");
            }
            catch (ArgumentException arg)
            {
                resposta = new RetornoRequest(false, arg.Message);
            }
            catch (Exception e)
            {
                resposta = new RetornoRequest(false, e.Message);
            }

            return resposta;
        }

        public List<PlanoPagamentoModel> ListarPlanos()
        {
            return _planoPagamentoRepository.ObterTodos();
        }

        public List<PlanoPagamentoModel> ListarPlanos(DateTime dataRegistro)
        {
            return this._planoPagamentoRepository.ObterTodos(dataRegistro);
        }

        public PlanoPagamentoModel ObterPlano(int codigo)
        {
            return _planoPagamentoRepository.ObterPorID(codigo);
        }
    }
}
