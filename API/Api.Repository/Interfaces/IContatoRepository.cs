﻿using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Repository.Interfaces
{
    public interface IContatoRepository
    {
        bool Incluir(ContatoModel objeto);
        bool Editar(ContatoModel objeto);
        bool Excluir(int contatoID);
        ContatoModel ObterPorID(int contatoID);
        List<ContatoModel> ObterTodos();
    }
}
