﻿using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IPedidoVendaRepository
    {
        bool Incluir(PedidoVendaModel pedido);
        bool Incluir(List<PedidoVendaModel> pedidos);
        List<PedidoVendaModel> ObterLista();
        List<PedidoVendaTabela> ObterLista(JQueryDataTableParam param);
        List<PedidoVendaModel> ObterLista(DateTime dataRegistro);
        PedidoVendaModel ObterPedido(int codigo);
        int QuantidadeTotalRegistros();
    }
}
