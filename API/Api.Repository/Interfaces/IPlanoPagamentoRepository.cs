﻿
using Api.Database.Entities;
using Api.Models.Model;
using System;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IPlanoPagamentoRepository
    {
        bool Incluir(PlanoPagamentoModel objeto);
        bool Incluir(List<PlanoPagamentoModel> planos);
        bool Editar(PlanoPagamentoModel objeto);
        List<string> Editar(List<PlanoPagamentoModel> planos);
        bool Excluir(int codigo);
        List<string> Excluir(List<int> planosCodigo);
        PlanoPagamentoModel ObterPorID(int codigo);
        List<PlanoPagamentoModel> ObterTodos();
        List<PlanoPagamentoModel> ObterTodos(DateTime dataRegistro);
        PlanoPagamentoEntity MapPlanoPagamentoEntity(PlanoPagamentoModel objeto);
    }
}
