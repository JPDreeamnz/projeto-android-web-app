﻿using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IProdutoRepository
    {
        bool Incluir(ref ProdutoModel objeto);
        bool Incluir(List<ProdutoModel> produtos);
        bool Editar(ProdutoModel objeto);
        List<string> Editar(List<ProdutoModel> produtos);
        bool Excluir(int codigo);
        List<string> Excluir(List<int> produtosCodigo);
        ProdutoModel ObterPorID(int codigo);
        List<ProdutoModel> ObterTodos();
        List<ProdutoTabela> ObterTodos(JQueryDataTableParam param);
        List<ProdutoModel> ObterTodos(DateTime dataRegistro);
        int QuantidadeTotalRegistros();
    }
}
