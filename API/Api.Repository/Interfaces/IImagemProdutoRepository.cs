﻿
using Api.Models.Model;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IImagemProdutoRepository
    {
        string PASTA { get; }
        bool ExcluirImagem(ImagemProdutoModel objeto);
        bool ExcluirPastaImagens(string caminhoPasta);
        bool SalvarImagem(ImagemProdutoModel objeto, byte[] imagem);
        IEnumerable<string> ListaImagensArquivo(string caminhoPasta);
        List<KeyValuePair<int, byte[]>> ObterImagens(string caminhoPasta);
        byte[] ObterImagem(string caminhoPasta);
        bool RenomearPasta(int codigoProduto, string caminhoPasta);
        bool VerificaExistenciaPastaSistema(string caminhoPasta);
        void CriaPastaSistema(string caminhoPasta);
        void Dispose();
    }
}
