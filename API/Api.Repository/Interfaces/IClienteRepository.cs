﻿using Api.Database.Entities;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IClienteRepository
    {
        bool Incluir(ClienteModel objeto);
        bool Incluir(List<ClienteModel> clientes);
        bool Editar(ClienteModel objeto);
        List<string> Editar(List<ClienteModel> clientes);
        bool Excluir(int codigo);
        List<string> Excluir(List<int> codigos);
        ClienteModel ObterPorID(int codigo);
        List<ClienteModel> ObterTodos();
        List<ClienteTabela> ObterTodos(JQueryDataTableParam param);
        List<ClienteModel> ObterTodos(DateTime dataRegistro);
        int QuantidadeTotalRegistros();
        ClienteEntity MapClienteEntity(ClienteModel objeto);       
    }
}
