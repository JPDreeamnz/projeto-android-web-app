﻿using Api.Models.Model;
using Api.Models.POCO;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IGrupoUsuariosRepository
    {
        bool Editar(GrupoUsuariosModel objeto);
        bool Excluir(int codigo);
        bool Incluir(GrupoUsuariosModel objeto);
        GrupoUsuariosModel ObterPorID(int codigo);
        List<GrupoUsuariosModel> ObterTodos();
        bool AutorizarAcesso(AutorizacaoAcesso model);
        List<PermissionamentoModel> ObterPermissionamento(int GrupoUsuariosCodigo);
    }
}
