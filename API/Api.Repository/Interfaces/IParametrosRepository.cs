﻿using Api.Database.Entities;
using Api.Models.Model;

namespace Api.Repository.Interfaces
{
    public interface IParametrosRepository
    {
        bool Editar(ParametrosModel objeto);
        ParametrosModel ObterParametros();
    }
}
