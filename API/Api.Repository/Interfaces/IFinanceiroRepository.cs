﻿using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IFinanceiroRepository
    {
        bool Incluir(FinanceiroModel objeto);
        bool Incluir(List<FinanceiroModel> financeiros);
        bool Editar(FinanceiroModel objeto);
        List<string> Editar(List<FinanceiroModel> objeto);
        bool Excluir(int clienteCodigo, string documento);
        List<string> Excluir(List<RequestFinanceiro> financeiros);
        FinanceiroModel ObterPorID(int clienteCodigo, string documento);
        List<FinanceiroModel> ObterTodos();
        List<FinanceiroModel> ObterTodos(DateTime dataRegistro);
    }
}
