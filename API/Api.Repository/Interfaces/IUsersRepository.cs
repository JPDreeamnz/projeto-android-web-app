﻿using System.Collections.Generic;
using Api.Models;
using Api.Database.Entities;

namespace Api.Repository.Interfaces
{
    public interface IUsersRepository
    {
        List<UserModel> GetAll();
        bool Add(UserEntity newUser);
        bool EmailIsValid(string email);
        bool Update(UserModel model);
        UserModel Get(int userID);
        bool Delete(int id);
    }
}