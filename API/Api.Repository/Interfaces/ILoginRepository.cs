﻿using Api.Models;

namespace Api.Repository.Interfaces
{
    public interface ILoginRepository
    {
        bool Authenticate(Api.Models.LoginModel model);
        UserModel RetornaUsuarioAutenticado(LoginModel model);
    }
}