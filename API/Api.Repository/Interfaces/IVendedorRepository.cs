﻿using Api.Database.Entities;
using Api.Models.Model;
using System;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IVendedorRepository
    {
        bool Incluir(VendedorModel objeto);
        bool Incluir(List<VendedorModel> vendedores);
        bool Editar(VendedorModel objeto);
        List<string> Editar(List<VendedorModel> objeto);
        bool Excluir(int codigo);
        List<string> Excluir(List<int> codigos);
        VendedorModel ObterPorID(int codigo);
        List<VendedorModel> ObterTodos();
        List<VendedorModel> ObterTodos(DateTime dataRegistro);
        VendedorEntity MapVendedorEntity(VendedorModel objeto);
    }
}
