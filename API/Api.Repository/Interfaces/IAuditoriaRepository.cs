﻿using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;

namespace Api.Repository.Interfaces
{
    public interface IAuditoriaRepository
    {
        List<AuditoriaModel> ObterLista();
        List<AuditoriaTabela> ObterLista(JQueryDataTableParam param);
        AuditoriaModel ObterAuditoria(int codigo);
        List<RetornoExcluidos> ObterListaExcluidos(DateTime dataRegistros);
        int QuantidadeTotalRegistros();
    }
}
