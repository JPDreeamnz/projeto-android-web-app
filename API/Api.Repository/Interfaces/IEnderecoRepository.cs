﻿using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Repository.Interfaces
{
    public interface IEnderecoRepository
    {
        bool Incluir(EnderecoModel objeto);
        bool Editar(EnderecoModel objeto);
        bool Excluir(int enderecoID);
        EnderecoModel ObterPorID(int enderecoID);
        List<EnderecoModel> ObterTodos();
    }
}
