﻿using Api.Repository.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using Api.Models.Model;
using Api.Database;
using Api.Database.Entities;
using AutoMapper;
using System.Data.Entity;
using Api.Models.POCO;
using System.Data.Entity.SqlServer;
using Api.Enums;
using Api.Enums.DataTables;
using Api.Models.DataTables;

namespace Api.Repository.Repositorio
{
    public class ClienteRepository : IClienteRepository
    {
        private IContext _contexto;

        public ClienteRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        public bool Editar(ClienteModel objeto)
        {
            ClienteEntity cliente = this._contexto.Clientes.SingleOrDefault(cli => cli.Codigo == objeto.Codigo);

            cliente.Documento = objeto.Documento;
            cliente.InscricaoEstadual = objeto.InscricaoEstadual;
            cliente.LimiteCredito = objeto.LimiteCredito;
            cliente.Nome = objeto.Nome;
            cliente.Observacao = objeto.Observacao;
            cliente.StatusCliente = objeto.StatusCliente;
            cliente.TipoDocumento = objeto.TipoDocumento;
            cliente.DataRegistro = DateTime.Now;

            if(cliente != null)
            {
                if (objeto.Endereco != null)
                {
                    bool enderecosAtualizar = (cliente.Endereco.Any(end => objeto.Endereco.Any(obj => obj.EnderecoID == end.EnderecoID)) || objeto.Endereco.Any(map => map.EnderecoID == 0));
                    if (enderecosAtualizar)
                    {
                        foreach (var endereco in objeto.Endereco)
                        {
                            if (endereco.EnderecoID != 0)
                            {
                                EnderecoEntity enderecoAntigo = cliente.Endereco.SingleOrDefault(end => end.EnderecoID == endereco.EnderecoID);
                                if (enderecoAntigo != null && !enderecoAntigo.Equals(endereco))
                                {
                                    enderecoAntigo.Endereco = endereco.Endereco;
                                    enderecoAntigo.Numero = endereco.Numero;
                                    enderecoAntigo.Complemento = endereco.Complemento;
                                    enderecoAntigo.Bairro = endereco.Bairro;
                                    enderecoAntigo.Cidade = endereco.Cidade;
                                    enderecoAntigo.Estado = endereco.Estado;
                                    enderecoAntigo.CEP = endereco.CEP;
                                }
                            }
                            else
                            {
                                EnderecoEntity novoEndereco = new EnderecoEntity()
                                {
                                    Endereco = endereco.Endereco,
                                    Numero = endereco.Numero,
                                    Complemento = endereco.Complemento,
                                    Bairro = endereco.Bairro,
                                    Cidade = endereco.Cidade,
                                    Estado = endereco.Estado,
                                    CEP = endereco.CEP
                                };

                                cliente.Endereco.Add(novoEndereco);
                            }
                        }
                    }

                    var enderecosExcluir = cliente.Endereco.Where(end => !objeto.Endereco.Any(obj => obj.EnderecoID == end.EnderecoID)).ToList();
                    if (enderecosExcluir.Count > 0)
                    {
                        foreach (var endereco in enderecosExcluir)
                        {
                            this._contexto.Endereco.Remove(endereco);
                        }
                    }
                }
                else
                {
                    this._contexto.Endereco.RemoveRange(cliente.Endereco);
                }



                if (objeto.Contato != null)
                {
                    bool contatoAtualizar = (cliente.Contato.Any(ctto => objeto.Contato.Any(obj => obj.ContatoID == ctto.ContatoID)) ||
                        (objeto.Contato != null && objeto.Contato.Any(obj => obj.ContatoID == 0)));
                    if (contatoAtualizar)
                    {
                        foreach (var contato in objeto.Contato)
                        {
                            if (contato.ContatoID != 0)
                            {
                                ContatoEntity contatoAntigo = cliente.Contato.SingleOrDefault(cont => cont.ContatoID == contato.ContatoID);
                                if (contatoAntigo != null && !contatoAntigo.Equals(contato)) 
                                {
                                    contatoAntigo.DDD = contato.DDD;
                                    contatoAntigo.Telefone = contato.Telefone;
                                    contatoAntigo.Email = contato.Email;
                                    contatoAntigo.TipoContato = contato.TipoContato;
                                }
                            }
                            else
                            {
                                ContatoEntity novoContato = new ContatoEntity()
                                {
                                    DDD = contato.DDD,
                                    Telefone = contato.Telefone,
                                    Email = contato.Email,
                                    TipoContato = contato.TipoContato
                                };

                                cliente.Contato.Add(novoContato);
                            }
                        }
                    }

                    var contatoExcluir = cliente.Contato.Where(ctto => !objeto.Contato.Any(obj => obj.ContatoID == ctto.ContatoID)).ToList();
                    if (contatoExcluir.Count > 0)
                    {
                        foreach (var contato in contatoExcluir)
                        {
                            this._contexto.Contato.Remove(contato);
                        }
                    }
                }
                else
                {
                    this._contexto.Contato.RemoveRange(cliente.Contato);
                }
                
                this._contexto.SaveChanges();
                return true;
            }
            else
                return false;

        }

        public List<string> Editar(List<ClienteModel> clientes)
        {
            List<string> erros = new List<string>();

            foreach (var objeto in clientes)
            {
                ClienteEntity cliente = this._contexto.Clientes.SingleOrDefault(cli => cli.Codigo == objeto.Codigo);

                cliente.Documento = objeto.Documento;
                cliente.InscricaoEstadual = objeto.InscricaoEstadual;
                cliente.LimiteCredito = objeto.LimiteCredito;
                cliente.Nome = objeto.Nome;
                cliente.Observacao = objeto.Observacao;
                cliente.StatusCliente = objeto.StatusCliente;
                cliente.TipoDocumento = objeto.TipoDocumento;
                cliente.DataRegistro = DateTime.Now;

                if (cliente != null)
                {
                    if (objeto.Endereco != null)
                    {
                        bool enderecosAtualizar = (cliente.Endereco.Any(end => objeto.Endereco.Any(obj => obj.EnderecoID == end.EnderecoID)) || objeto.Endereco.Any(map => map.EnderecoID == 0));
                        if (enderecosAtualizar)
                        {
                            foreach (var endereco in objeto.Endereco)
                            {
                                if (endereco.EnderecoID != 0)
                                {
                                    EnderecoEntity enderecoAntigo = cliente.Endereco.SingleOrDefault(end => end.EnderecoID == endereco.EnderecoID);
                                    if (enderecoAntigo != null && !enderecoAntigo.Equals(endereco))
                                    {
                                        enderecoAntigo.Endereco = endereco.Endereco;
                                        enderecoAntigo.Numero = endereco.Numero;
                                        enderecoAntigo.Complemento = endereco.Complemento;
                                        enderecoAntigo.Bairro = endereco.Bairro;
                                        enderecoAntigo.Cidade = endereco.Cidade;
                                        enderecoAntigo.Estado = endereco.Estado;
                                        enderecoAntigo.CEP = endereco.CEP;
                                    }
                                }
                                else
                                {
                                    EnderecoEntity novoEndereco = new EnderecoEntity()
                                    {
                                        Endereco = endereco.Endereco,
                                        Numero = endereco.Numero,
                                        Complemento = endereco.Complemento,
                                        Bairro = endereco.Bairro,
                                        Cidade = endereco.Cidade,
                                        Estado = endereco.Estado,
                                        CEP = endereco.CEP
                                    };

                                    cliente.Endereco.Add(novoEndereco);
                                }
                            }
                        }

                        var enderecosExcluir = cliente.Endereco.Where(end => !objeto.Endereco.Any(obj => obj.EnderecoID == end.EnderecoID)).ToList();
                        if (enderecosExcluir.Count > 0)
                        {
                            foreach (var endereco in enderecosExcluir)
                            {
                                this._contexto.Endereco.Remove(endereco);
                            }
                        }
                    }
                    else
                    {
                        this._contexto.Endereco.RemoveRange(cliente.Endereco);
                    }



                    if (objeto.Contato != null)
                    {
                        bool contatoAtualizar = (cliente.Contato.Any(ctto => objeto.Contato.Any(obj => obj.ContatoID == ctto.ContatoID)) ||
                            (objeto.Contato != null && objeto.Contato.Any(obj => obj.ContatoID == 0)));
                        if (contatoAtualizar)
                        {
                            foreach (var contato in objeto.Contato)
                            {
                                if (contato.ContatoID != 0)
                                {
                                    ContatoEntity contatoAntigo = cliente.Contato.SingleOrDefault(cont => cont.ContatoID == contato.ContatoID);
                                    if (contatoAntigo != null && !contatoAntigo.Equals(contato))
                                    {
                                        contatoAntigo.DDD = contato.DDD;
                                        contatoAntigo.Telefone = contato.Telefone;
                                        contatoAntigo.Email = contato.Email;
                                        contatoAntigo.TipoContato = contato.TipoContato;
                                    }
                                }
                                else
                                {
                                    ContatoEntity novoContato = new ContatoEntity()
                                    {
                                        DDD = contato.DDD,
                                        Telefone = contato.Telefone,
                                        Email = contato.Email,
                                        TipoContato = contato.TipoContato
                                    };

                                    cliente.Contato.Add(novoContato);
                                }
                            }
                        }

                        var contatoExcluir = cliente.Contato.Where(ctto => !objeto.Contato.Any(obj => obj.ContatoID == ctto.ContatoID)).ToList();
                        if (contatoExcluir.Count > 0)
                        {
                            foreach (var contato in contatoExcluir)
                            {
                                this._contexto.Contato.Remove(contato);
                            }
                        }
                    }
                    else
                    {
                        this._contexto.Contato.RemoveRange(cliente.Contato);
                    }
                }
                else
                {
                    erros.Add("O cliente de código " + objeto.Codigo + " não foi encontrado");
                }
            }

            this._contexto.SaveChanges();
            return erros;
        }

        public bool Excluir(int codigo)
        {
            ClienteEntity cliente = this._contexto.Clientes.SingleOrDefault(cli => cli.Codigo == codigo);
            if (cliente != null)
            {
                this._contexto.Clientes.Remove(cliente);
                this._contexto.SaveChanges();

                return true;
            }

            return false;
        }

        public List<string> Excluir(List<int> codigos)
        {
            List<string> retorno = new List<string>();

            var clientes = this._contexto.Clientes.Where(cli => codigos.Contains(cli.Codigo));
            var naoEncontrados = codigos.Where(cod => !clientes.Any(cli => cli.Codigo == cod)).ToList();

            this._contexto.Clientes.RemoveRange(clientes);
            this._contexto.SaveChanges();

            if(naoEncontrados.Count > 0)
            {
                foreach (var naoEncontrado in naoEncontrados)
                {
                    retorno.Add("O Cliente de código " + naoEncontrado + " não foi encontrado");
                }
            }

            return retorno;
        }

        public bool Incluir(ClienteModel objeto)
        {
            ClienteEntity novoCliente = this.MapClienteEntity(objeto);
            this._contexto.Clientes.Add(novoCliente);
            this._contexto.SaveChanges();
            return true;
        }

        public bool Incluir(List<ClienteModel> clientes)
        {
            foreach (var cliente in clientes)
            {
                ClienteEntity novoCliente = this.MapClienteEntity(cliente);
                this._contexto.Clientes.Add(novoCliente);
            }

            this._contexto.SaveChanges();
            return true;
        }

        public ClienteModel ObterPorID(int codigo)
        {
            return this.MapClienteModel(this._contexto.Clientes.SingleOrDefault(cli => cli.Codigo == codigo));
        }

        public List<ClienteModel> ObterTodos()
        {
            List<ClienteModel> retorno = new List<ClienteModel>();
            try
            {
                retorno = this._contexto.Clientes
                    .Select(cli => new ClienteModel
                    {
                        Codigo = cli.Codigo,
                        Nome = cli.Nome,
                        TipoDocumento = cli.TipoDocumento,
                        Documento = cli.Documento,
                        InscricaoEstadual = cli.InscricaoEstadual,
                        StatusCliente = cli.StatusCliente,
                    }).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
            

            return retorno;
        }

        public int QuantidadeTotalRegistros()
        {
            return this._contexto.Clientes.Count();
        }

        public List<ClienteTabela> ObterTodos(JQueryDataTableParam param)
        {
            var query = this._contexto.Clientes.AsQueryable();

            List<TipoDocumento> consultaTipo = new List<TipoDocumento>();
            if ("CNPJ".Contains(param.sSearch))
                consultaTipo.Add(TipoDocumento.CNPJ);
            if ("CPF".Contains(param.sSearch))
                consultaTipo.Add(TipoDocumento.CPF);

            List<Situacao> consultaSituacao = new List<Situacao>();
            if ("ATIVO".Contains(param.sSearch))
                consultaSituacao.Add(Situacao.ATIVO);
            if ("INATIVO".Contains(param.sSearch))
                consultaSituacao.Add(Situacao.INATIVO);
            
            if(!string.IsNullOrWhiteSpace(param.sSearch))
                query = query.Where(x => x.Nome.Contains(param.sSearch) ||
                            (x.Documento.HasValue && SqlFunctions.StringConvert((decimal)x.Documento).Contains(param.sSearch)) ||
                            consultaTipo.Contains(x.TipoDocumento) ||
                            x.InscricaoEstadual.Contains(param.sSearch) ||
                            consultaSituacao.Contains(x.StatusCliente));

            switch ((ClienteTableEnum)param.iSortingCols)
            {
                case ClienteTableEnum.Nome:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Nome);
                    else
                        query = query.OrderByDescending(x => x.Nome);
                    break;
                case ClienteTableEnum.TipoDocumento:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.TipoDocumento);
                    else
                        query = query.OrderByDescending(x => x.TipoDocumento);
                    break;
                case ClienteTableEnum.Documento:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Documento);
                    else
                        query = query.OrderByDescending(x => x.Documento);
                    break;
                case ClienteTableEnum.InscricaoEstadual:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.InscricaoEstadual);
                    else
                        query = query.OrderByDescending(x => x.InscricaoEstadual);
                    break;
                case ClienteTableEnum.Situacao:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.StatusCliente);
                    else
                        query = query.OrderByDescending(x => x.StatusCliente);
                    break;
                default:
                    query = query.OrderBy(x => x.Nome);
                    break;
            }

            return query.Skip(param.iDisplayStart)
                .Take(param.iDisplayLength)
                .Select(cli => new ClienteTabela {
                    Codigo = cli.Codigo,
                    Nome = cli.Nome,
                    TipoDocumento = ((int)cli.TipoDocumento == 1) ? "CNPJ" : "CPF",
                    Documento = cli.Documento,
                    InscricaoEstadual = cli.InscricaoEstadual,
                    StatusCliente = ((int)cli.StatusCliente == 1) ? "Ativo" : "Inativo"
                }).ToList();
        }

        public List<ClienteModel> ObterTodos(DateTime dataRegistro)
        {
            List<ClienteModel> retorno = new List<ClienteModel>();
            this._contexto.Clientes.Where(cli => cli.DataRegistro >= dataRegistro)
                .ToList().ForEach(
                delegate (ClienteEntity item)
                {
                    var cliente = new ClienteModel()
                    {
                        Codigo = item.Codigo,
                        DataRegistro = item.DataRegistro,
                        Documento = item.Documento,
                        InscricaoEstadual = item.InscricaoEstadual,
                        LimiteCredito = item.LimiteCredito,
                        Nome = item.Nome,
                        Observacao = item.Observacao,
                        StatusCliente = item.StatusCliente,
                        TipoDocumento = item.TipoDocumento,
                        Contato = new List<ContatoModel>(),
                        Endereco = new List<EnderecoModel>()
                    };

                    foreach (var contato in item.Contato)
                    {
                        var novoContato = new ContatoModel()
                        {
                            ContatoID = contato.ContatoID,
                            DDD = contato.DDD,
                            Email = contato.Email,
                            Telefone = contato.Telefone,
                            TipoContato = contato.TipoContato
                        };

                        cliente.Contato.Add(novoContato);
                    }

                    foreach (var endereco in item.Endereco)
                    {
                        var novoEndereco = new EnderecoModel()
                        {
                            Bairro = endereco.Bairro,
                            CEP = endereco.CEP,
                            Cidade = endereco.Cidade,
                            Complemento = endereco.Complemento,
                            Endereco = endereco.Endereco,
                            EnderecoID = endereco.EnderecoID,
                            Estado = endereco.Estado,
                            Numero = endereco.Numero
                        };

                        cliente.Endereco.Add(novoEndereco);
                    }

                    retorno.Add(cliente);
                });

            return retorno;
        }

        public ClienteEntity MapClienteEntity(ClienteModel objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg => 
            {
                cfg.CreateMap<ClienteModel, ClienteEntity>()
                   .ForMember(cli => cli.Documento,
                    opt => opt.MapFrom(
                        src => (src.Documento.HasValue && src.Documento.Value == 0) ? new Nullable<long>() : src.Documento.Value))
                   .ForMember(cli => cli.DataRegistro, opt => opt.UseValue(DateTime.Now));
                cfg.CreateMap<EnderecoModel, EnderecoEntity>();
                cfg.CreateMap<ContatoModel, ContatoEntity>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<ClienteEntity>(objeto);
        }

        private ClienteModel MapClienteModel(ClienteEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ClienteEntity, ClienteModel>();
                cfg.CreateMap<EnderecoEntity, EnderecoModel>();
                cfg.CreateMap<ContatoEntity, ContatoModel>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<ClienteModel>(objeto);
        }
    }
}
