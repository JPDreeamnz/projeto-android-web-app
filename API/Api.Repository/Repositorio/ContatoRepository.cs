﻿using Api.Database;
using Api.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Api.Models.Model;
using Api.Database.Entities;

namespace Api.Repository.Repositorio
{
    public class ContatoRepository : IContatoRepository
    {
        private IContext _contexto;

        public ContatoRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        public bool Editar(ContatoModel objeto)
        {
            ContatoEntity contato = this._contexto.Contato.FirstOrDefault(ctt => ctt.ContatoID == objeto.ContatoID);
            if (contato != null)
            {
                contato.DDD = objeto.DDD;
                contato.Email = objeto.Email;
                contato.Telefone = objeto.Telefone;
                contato.TipoContato = objeto.TipoContato;

                this._contexto.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool Excluir(int contatoID)
        {
            ContatoEntity contato = this._contexto.Contato.FirstOrDefault(ctt => ctt.ContatoID == contatoID);
            if (contato != null)
            {
                this._contexto.Contato.Remove(contato);
                this._contexto.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool Incluir(ContatoModel objeto)
        {
            ContatoEntity novoContato = new ContatoEntity()
            {
                DDD = objeto.DDD,
                Email = objeto.Email,
                Telefone = objeto.Telefone,
                TipoContato = objeto.TipoContato
            };

            this._contexto.Contato.Add(novoContato);
            this._contexto.SaveChanges();

            return true;
        }

        public ContatoModel ObterPorID(int ContatoID)
        {
            return this._contexto.Contato.Where(ctt => ctt.ContatoID == ContatoID)
                .Select(ctt => new ContatoModel()
                {
                    ContatoID = ctt.ContatoID,
                    DDD = ctt.DDD,
                    Email = ctt.Email,
                    Telefone = ctt.Telefone,
                    TipoContato = ctt.TipoContato
                }).SingleOrDefault();
        }

        public List<ContatoModel> ObterTodos()
        {
            return this._contexto.Contato.Select(ctt => new ContatoModel()
            {
                ContatoID = ctt.ContatoID,
                DDD = ctt.DDD,
                Email = ctt.Email,
                Telefone = ctt.Telefone,
                TipoContato = ctt.TipoContato
            }).ToList();
        }
    }
}
