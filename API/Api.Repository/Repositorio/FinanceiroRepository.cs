﻿using System;
using System.Collections.Generic;
using System.Linq;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Database;
using Api.Database.Entities;
using AutoMapper;
using Api.Models.POCO;

namespace Api.Repository.Repositorio
{
    public class FinanceiroRepository : IFinanceiroRepository
    {
        private IContext _contexto;

        public FinanceiroRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        public bool Editar(FinanceiroModel objeto)
        {
            FinanceiroEntity financeiro = this._contexto.Financeiro
                .Where(fin => fin.ClienteCodigo == objeto.ClienteCodigo && fin.Documento.Equals(objeto.Documento)).SingleOrDefault();
            if (financeiro != null)
            {
                financeiro.Emissao = objeto.Emissao;
                financeiro.Pagamento = objeto.Pagamento;
                financeiro.Valor = objeto.Valor;
                financeiro.Vencimento = objeto.Vencimento;
                financeiro.DataRegistro = DateTime.Now;

                this._contexto.SaveChanges();
                return true;
            }

            return false;
        }

        public List<string> Editar(List<FinanceiroModel> objeto)
        {
            List<string> erros = new List<string>();

            foreach (var item in objeto)
            {
                FinanceiroEntity financeiro = this._contexto.Financeiro
                .Where(fin => fin.ClienteCodigo == item.ClienteCodigo && fin.Documento.Equals(item.Documento)).SingleOrDefault();
                if (financeiro != null)
                {
                    financeiro.Emissao = item.Emissao;
                    financeiro.Pagamento = item.Pagamento;
                    financeiro.Valor = item.Valor;
                    financeiro.Vencimento = item.Vencimento;
                    financeiro.DataRegistro = DateTime.Now;
                }
                else
                    erros.Add("O Financeiro de Cliente Código " + item.ClienteCodigo + " e Documento " + item.Documento + " não foi encontrado");
            }

            this._contexto.SaveChanges();
            return erros;
        }

        public bool Excluir(int clienteCodigo, string documento)
        {
            FinanceiroEntity financeiro = this._contexto.Financeiro.Where(fin => fin.ClienteCodigo == clienteCodigo && fin.Documento.Equals(documento)).SingleOrDefault();
            if (financeiro != null)
            {
                this._contexto.Financeiro.Remove(financeiro);
                this._contexto.SaveChanges();
                return true;
            }
            return false;
        }

        public List<string> Excluir(List<RequestFinanceiro> financeiros)
        {
            List<string> retorno = new List<string>();

            var listaFin = this._contexto.Financeiro.Where(fin => financeiros.Any(el => el.ClienteCodigo == fin.ClienteCodigo && el.Documento.Equals(fin.Documento)));
            var naoEncontrados = financeiros.Where(fin => !listaFin.Any(el => el.ClienteCodigo == fin.ClienteCodigo && el.Documento.Equals(fin.Documento))).ToList();

            this._contexto.Financeiro.RemoveRange(listaFin);
            this._contexto.SaveChanges();
            
            if (naoEncontrados.Count > 0)
            {
                foreach (var naoEncontrado in naoEncontrados)
                {
                    retorno.Add("O Financeiro de código " + naoEncontrado.ClienteCodigo + " e documento " + naoEncontrado.Documento + " não foi encontrado");
                }
            }

            return retorno;
        }

        public bool Incluir(FinanceiroModel objeto)
        {
            FinanceiroEntity novoFinanceiro = this.MapFinanceiroEntity(objeto);
            this._contexto.Financeiro.Add(novoFinanceiro);
            this._contexto.SaveChanges();
            return true;
        }

        public bool Incluir(List<FinanceiroModel> financeiros)
        {
            foreach (var financeiro in financeiros)
            {
                FinanceiroEntity novoFinanceiro = this.MapFinanceiroEntity(financeiro);
                this._contexto.Financeiro.Add(novoFinanceiro);
            }
            
            this._contexto.SaveChanges();
            return true;
        }

        public FinanceiroModel ObterPorID(int clienteCodigo, string documento)
        {
            return this.MapFinanceiroModel(this._contexto.Financeiro.SingleOrDefault(fin => fin.ClienteCodigo == clienteCodigo && fin.Documento.Equals(documento)));
        }

        public List<FinanceiroModel> ObterTodos()
        {
            List<FinanceiroModel> retorno = new List<FinanceiroModel>();
            this._contexto.Financeiro.ToList().ForEach(delegate (FinanceiroEntity financeiro) {
                retorno.Add(this.MapFinanceiroModel(financeiro));
            });

            return retorno;
        }

        public List<FinanceiroModel> ObterTodos(DateTime dataRegistro)
        {
            List<FinanceiroModel> retorno = new List<FinanceiroModel>();
            this._contexto.Financeiro
                .Where(fin => fin.DataRegistro >= dataRegistro)
                .ToList().ForEach(delegate (FinanceiroEntity financeiro) {
                retorno.Add(this.MapFinanceiroModel(financeiro));
            });

            return retorno;
        }

        public FinanceiroEntity MapFinanceiroEntity(FinanceiroModel objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<FinanceiroModel, FinanceiroEntity>()
                   .ForMember(fin => fin.DataRegistro, opt => opt.UseValue(DateTime.Now));
                cfg.CreateMap<ClienteModel, ClienteEntity>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<FinanceiroEntity>(objeto);
        }

        private FinanceiroModel MapFinanceiroModel(FinanceiroEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<FinanceiroEntity, FinanceiroModel>();
                cfg.CreateMap<ClienteEntity, ClienteModel>();
                cfg.CreateMap<ContatoEntity, ContatoModel>();
                cfg.CreateMap<EnderecoEntity, EnderecoModel>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<FinanceiroModel>(objeto);
        }
    }
}
