﻿using Api.Repository.Interfaces;
using System;
using Api.Database.Entities;
using Api.Models.Model;
using System.Collections.Generic;
using Api.Database;
using System.Linq;
using AutoMapper;

namespace Api.Repository.Repositorio
{
    public class PlanoPagamentoRepository : IPlanoPagamentoRepository
    {
        private IContext _contexto;
        public PlanoPagamentoRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        //TODO: Sumarizar
        public bool Editar(PlanoPagamentoModel objeto)
        {
            PlanoPagamentoEntity planoPagamento = this._contexto.PlanoPagamento.SingleOrDefault(pp => pp.Codigo == objeto.Codigo);
            if (planoPagamento != null)
            {
                planoPagamento.Nome = objeto.Nome;
                planoPagamento.Observacao = objeto.Observacao;
                planoPagamento.StatusPlano = objeto.StatusPlano;
                planoPagamento.DataRegistro = DateTime.Now;

                this._contexto.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<string> Editar(List<PlanoPagamentoModel> planos)
        {
            List<string> erros = new List<string>();

            foreach (var plano in planos)
            {
                PlanoPagamentoEntity planoPagamento = this._contexto.PlanoPagamento.SingleOrDefault(pp => pp.Codigo == plano.Codigo);
                if (planoPagamento != null)
                {
                    planoPagamento.Nome = plano.Nome;
                    planoPagamento.Observacao = plano.Observacao;
                    planoPagamento.StatusPlano = plano.StatusPlano;
                    planoPagamento.DataRegistro = DateTime.Now;
                }
                else
                    erros.Add("O Plano de Pagamento de Codigo " + plano.Codigo + " não foi encontrado");
            }

            this._contexto.SaveChanges();
            return erros;
        }

        public bool Excluir(int codigo)
        {
            PlanoPagamentoEntity planoPagamento = this._contexto.PlanoPagamento.SingleOrDefault(pp => pp.Codigo == codigo);
            if (planoPagamento != null)
            {
                this._contexto.PlanoPagamento.Remove(planoPagamento);
                this._contexto.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<string> Excluir(List<int> planosCodigo)
        {
            List<string> retorno = new List<string>();

            var listaPlanos = this._contexto.PlanoPagamento.Where(fin => planosCodigo.Contains(fin.Codigo));
            var naoEncontrados = listaPlanos.Where(fin => !planosCodigo.Contains(fin.Codigo)).ToList();

            this._contexto.PlanoPagamento.RemoveRange(listaPlanos);
            this._contexto.SaveChanges();

            if (naoEncontrados.Count > 0)
            {
                foreach (var naoEncontrado in naoEncontrados)
                {
                    retorno.Add("O Plano de Pagemento de código " + naoEncontrado.Codigo + " não foi encontrado");
                }
            }

            return retorno;
        }

        public bool Incluir(PlanoPagamentoModel objeto)
        {
            PlanoPagamentoEntity planoPagamento = this.MapPlanoPagamentoEntity(objeto);
            this._contexto.PlanoPagamento.Add(planoPagamento);
            this._contexto.SaveChanges();
            return true;
        }
        
        public bool Incluir(List<PlanoPagamentoModel> planos)
        {
            foreach (var plano in planos)
            {
                if (plano.StatusPlano == 0) plano.StatusPlano = Enums.Situacao.ATIVO;
                PlanoPagamentoEntity planoPagamento = this.MapPlanoPagamentoEntity(plano);
                this._contexto.PlanoPagamento.Add(planoPagamento);
            }
            
            this._contexto.SaveChanges();
            return true;
        }

        public PlanoPagamentoModel ObterPorID(int codigo)
        {
            return this.MapPlanoPagamentoModel(this._contexto.PlanoPagamento.SingleOrDefault(pp => pp.Codigo == codigo));
        }

        public List<PlanoPagamentoModel> ObterTodos()
        {
            List<PlanoPagamentoModel> retorno = new List<PlanoPagamentoModel>();
            this._contexto.PlanoPagamento.ToList().ForEach(delegate (PlanoPagamentoEntity instancia)
            {
                retorno.Add(this.MapPlanoPagamentoModel(instancia));
            });
            return retorno;
        }

        public List<PlanoPagamentoModel> ObterTodos(DateTime dataRegistro)
        {
            List<PlanoPagamentoModel> retorno = new List<PlanoPagamentoModel>();
            this._contexto.PlanoPagamento
                .Where(plan => plan.DataRegistro >= dataRegistro)
                .ToList().ForEach(delegate (PlanoPagamentoEntity instancia)
            {
                retorno.Add(this.MapPlanoPagamentoModel(instancia));
            });
            return retorno;
        }

        public PlanoPagamentoEntity MapPlanoPagamentoEntity(PlanoPagamentoModel objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg => 
                cfg.CreateMap<PlanoPagamentoModel, PlanoPagamentoEntity>()
                    .ForMember(plano => plano.DataRegistro, opt => opt.UseValue(DateTime.Now)));
            IMapper mapper = mapperConfig.CreateMapper();
            return mapper.Map<PlanoPagamentoEntity>(objeto);
        }

        private PlanoPagamentoModel MapPlanoPagamentoModel(PlanoPagamentoEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<PlanoPagamentoEntity, PlanoPagamentoModel>());
            IMapper mapper = mapperConfig.CreateMapper();
            return mapper.Map<PlanoPagamentoModel>(objeto);
        }

    }
}
