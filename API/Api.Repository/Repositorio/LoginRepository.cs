﻿using System.Linq;
using Api.Models;
using Api.Database;
using Api.Repository.Interfaces;
using System;

namespace Api.Repository
{
    public class LoginRepository : ILoginRepository
    {
        private IContext _context;

        public LoginRepository(IContext contexto)
        {
            _context = contexto;
        }
        
        public bool Authenticate(LoginModel model)
        {
            return _context.Users.Any(x => (x.LoginName.Equals(model.Name) || x.Email.Equals(model.Name)) && x.Password == model.Password && x.Active);
        }

        public UserModel RetornaUsuarioAutenticado(LoginModel model)
        {
            return _context.Users
                .Where(x => (x.LoginName.Equals(model.Name) || x.Email.Equals(model.Name)) && x.Password == model.Password && x.Active)
                .Select(user => new UserModel
                {
                    Active = user.Active,
                    Name = user.Name,
                    Email = user.Email,
                    LastName = user.LastName,
                    GrupoUsuariosCodigo = user.GrupoUsuariosCodigo,
                    Password = "-",
                    UserID = user.UserID
                }).Single();
        }
    }
}
