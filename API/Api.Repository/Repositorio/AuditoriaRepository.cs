﻿using System;
using System.Collections.Generic;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Database;
using System.Linq;
using Api.Database.Entities;
using AutoMapper;
using Api.Models.POCO;
using System.Data.Entity.SqlServer;
using Api.Enums;
using Api.Enums.DataTables;
using Api.Models.DataTables;

namespace Api.Repository.Repositorio
{
    public class AuditoriaRepository : IAuditoriaRepository
    {
        private IContext _contexto;

        public AuditoriaRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        public AuditoriaModel ObterAuditoria(int codigo)
        {
            return this.MapAuditoriaModel(this._contexto.Auditoria.SingleOrDefault(aud => aud.Codigo == codigo));
        }

        public List<AuditoriaModel> ObterLista()
        {
            List<AuditoriaModel> retorno = new List<AuditoriaModel>();
            //this._contexto.Auditoria.ToList().ForEach(delegate (AuditoriaEntity auditoria) {
            //    retorno.Add(this.MapAuditoriaModel(auditoria));
            //});

            //return retorno;

            return this._contexto.Auditoria.Select(x => new AuditoriaModel
            {
                Codigo = x.Codigo,
                DataOperacao = x.DataOperacao,
                Objeto = x.Objeto,
                Operacao = x.Operacao,
                Tabela = x.Tabela,
                UsuarioCodigo = x.UsuarioCodigo,
                UsuarioEmail = x.UsuarioEmail,
                UsuarioNome = x.UsuarioNome,
                UsuarioSobrenome = x.UsuarioSobrenome
            }).ToList();
        }

        public List<AuditoriaTabela> ObterLista(JQueryDataTableParam param)
        {
            var query = this._contexto.Auditoria.AsQueryable();

            List<EstadoEntidade> operacoes = new List<EstadoEntidade>();
            if (param.sSearch.ToUpper().Contains("INSERIDO"))
                operacoes.Add(EstadoEntidade.INSERIDO);
            if (param.sSearch.ToUpper().Contains("EDITADO"))
                operacoes.Add(EstadoEntidade.EDITADO);
            if (param.sSearch.ToUpper().Contains("EXCLUIDO"))
                operacoes.Add(EstadoEntidade.EXCLUIDO);
            if (param.sSearch.ToUpper().Contains("VISUALIZADO"))
                operacoes.Add(EstadoEntidade.VISUALIZADO);

            if (!string.IsNullOrWhiteSpace(param.sSearch))
            {
                query = query.Where(x => x.UsuarioNome.Contains(param.sSearch) ||
                                         x.UsuarioSobrenome.Contains(param.sSearch) ||
                                         SqlFunctions.StringConvert((decimal)x.DataOperacao.Day).Contains(param.sSearch) ||
                                         SqlFunctions.StringConvert((decimal)x.DataOperacao.Month).Contains(param.sSearch) ||
                                         SqlFunctions.StringConvert((decimal)x.DataOperacao.Year).Contains(param.sSearch) ||
                                         operacoes.Contains(x.Operacao) ||
                                         x.Tabela.Contains(param.sSearch) ||
                                         x.Objeto.Contains(param.sSearch));
            }

            switch ((AuditoriaTableEnum) param.iSortingCols)
            {
                case AuditoriaTableEnum.Nome:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => new { x.UsuarioNome, x.UsuarioSobrenome });
                    else
                        query = query.OrderByDescending(x => new { x.UsuarioNome, x.UsuarioSobrenome});
                    break;
                case AuditoriaTableEnum.UsuarioEmail:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.UsuarioEmail);
                    else
                        query = query.OrderByDescending(x => x.UsuarioEmail);
                    break;
                case AuditoriaTableEnum.DataOperacao:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.DataOperacao);
                    else
                        query = query.OrderByDescending(x => x.DataOperacao);
                    break;
                case AuditoriaTableEnum.Operacao:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Operacao);
                    else
                        query = query.OrderByDescending(x => x.Operacao);
                    break;
                case AuditoriaTableEnum.Tabela:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Tabela);
                    else
                        query = query.OrderByDescending(x => x.Tabela);
                    break;
                case AuditoriaTableEnum.Objeto:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Objeto);
                    else
                        query = query.OrderByDescending(x => x.Objeto);
                    break;
                default:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => new { x.UsuarioNome, x.UsuarioSobrenome });
                    else
                        query = query.OrderByDescending(x => new { x.UsuarioNome, x.UsuarioSobrenome });
                    break;
            }

            return query.Skip(param.iDisplayStart)
                        .Take(param.iDisplayLength)
                        .ToList()
                        .Select(aud => new AuditoriaTabela
                        {
                            Codigo = aud.Codigo,
                            DataOperacao = aud.DataOperacao.ToShortDateString(),
                            Nome = aud.UsuarioNome + " - " + aud.UsuarioSobrenome,
                            Objeto = aud.Objeto,
                            Tabela = aud.Tabela,
                            Operacao = aud.Operacao.ToString(),
                            UsuarioEmail = aud.UsuarioEmail
                        }).ToList();
        }

        public List<RetornoExcluidos> ObterListaExcluidos(DateTime dataRegistros)
        {
            return this._contexto.Auditoria
                .Where(x => x.Operacao == EstadoEntidade.EXCLUIDO && x.DataOperacao.CompareTo(dataRegistros) > 0 && x.ID != null)
                .Select(x => new RetornoExcluidos
                {
                    Tabela = x.Tabela,
                    ID = x.ID
                }).ToList();
        }

        public int QuantidadeTotalRegistros()
        {
            return _contexto.Auditoria.Count();
        }

        private AuditoriaModel MapAuditoriaModel(AuditoriaEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<AuditoriaEntity, AuditoriaModel>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<AuditoriaModel>(objeto);
        }
    }
}
