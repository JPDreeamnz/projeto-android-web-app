﻿using System.Collections.Generic;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Database;
using Api.Database.Entities;
using System.Linq;
using AutoMapper;
using Api.Models.POCO;
using System;
using Api.Models.DataTables;
using System.Data.Entity.SqlServer;
using Api.Enums.DataTables;

namespace Api.Repository.Repositorio
{
    public class ProdutoRepository : IProdutoRepository
    {
        private IContext _contexto;

        public ProdutoRepository(IContext contexto)
        {
            this._contexto = contexto;
        }
        
        public bool Editar(ProdutoModel objeto)
        {
            ProdutoEntity produto = this._contexto.Produto.SingleOrDefault(prod => prod.Codigo == objeto.Codigo);

            if (produto != null)
            {
                produto.CodigoBarras = objeto.CodigoBarras;
                produto.CodigoFabrica = objeto.CodigoFabrica;
                produto.CodigoOriginal = objeto.CodigoOriginal;
                produto.Cor = objeto.Cor;
                produto.Marca = objeto.Marca;
                produto.Nome = objeto.Nome;
                produto.QuantidadeEstoque = objeto.QuantidadeEstoque;
                produto.Status = objeto.Status;
                produto.Tamanho = objeto.Tamanho;
                produto.Unidade = objeto.Unidade;
                produto.ValorVenda = objeto.ValorVenda.Value;
                produto.DataRegistro = DateTime.Now;

                this._contexto.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<string> Editar(List<ProdutoModel> produtos)
        {
            List<string> erros = new List<string>();

            foreach (var objeto in produtos)
            {
                ProdutoEntity produto = this._contexto.Produto.SingleOrDefault(prod => prod.Codigo == objeto.Codigo);
                if (produto != null)
                {
                    produto.CodigoBarras = objeto.CodigoBarras;
                    produto.CodigoFabrica = objeto.CodigoFabrica;
                    produto.CodigoOriginal = objeto.CodigoOriginal;
                    produto.Cor = objeto.Cor;
                    produto.Marca = objeto.Marca;
                    produto.Nome = objeto.Nome;
                    produto.QuantidadeEstoque = objeto.QuantidadeEstoque;
                    produto.Status = objeto.Status;
                    produto.Tamanho = objeto.Tamanho;
                    produto.Unidade = objeto.Unidade;
                    produto.ValorVenda = objeto.ValorVenda.Value;
                    produto.DataRegistro = DateTime.Now;
                }
                else
                    erros.Add("O Produto de Codigo " + objeto.Codigo + " não foi encontrado");
            }

            this._contexto.SaveChanges();
            return erros;
        }

        public bool Excluir(int codigo)
        {
            ProdutoEntity produto = this._contexto.Produto.Where(prod => prod.Codigo == codigo).SingleOrDefault();
            if (produto != null)
            {
                this._contexto.Produto.Remove(produto);
                this._contexto.SaveChanges();
                return true;
            }
            return false;
        }

        public List<string> Excluir(List<int> produtosCodigo)
        {
            List<string> retorno = new List<string>();

            var listaProdutos = this._contexto.Produto.Where(prod => produtosCodigo.Contains(prod.Codigo));
            var naoEncontrados = listaProdutos.Where(prod => !produtosCodigo.Contains(prod.Codigo)).ToList();

            this._contexto.Produto.RemoveRange(listaProdutos);
            this._contexto.SaveChanges();

            if (naoEncontrados.Count > 0)
            {
                foreach (var naoEncontrado in naoEncontrados)
                {
                    retorno.Add("O Produto de código " + naoEncontrado.Codigo + " não foi encontrado");
                }
            }

            return retorno;
        }

        public bool Incluir(ref ProdutoModel objeto)
        {
            ProdutoEntity novoProduto = this.MapProdutoEntity(objeto);
            this._contexto.Produto.Add(novoProduto);
            this._contexto.SaveChanges();
            objeto.Codigo = novoProduto.Codigo;

            return true;
        }

        public bool Incluir(List<ProdutoModel> produtos)
        {
            foreach (var produto in produtos)
            {
                ProdutoEntity novoProduto = this.MapProdutoEntity(produto);
                this._contexto.Produto.Add(novoProduto);
            }

            this._contexto.SaveChanges();
            return true;
        }

        public ProdutoModel ObterPorID(int codigo)
        {
            return this.MapProdutoModel(this._contexto.Produto.SingleOrDefault(prod => prod.Codigo == codigo));
        }

        public List<ProdutoModel> ObterTodos()
        {
            List<ProdutoModel> retorno = new List<ProdutoModel>();
            this._contexto.Produto.ToList().ForEach(delegate (ProdutoEntity produto) {
                retorno.Add(this.MapProdutoModel(produto));
            });

            return retorno;
        }

        public List<ProdutoModel> ObterTodos(DateTime dataRegistro)
        {
            List<ProdutoModel> retorno = new List<ProdutoModel>();
            this._contexto.Produto
                .Where(prod => prod.DataRegistro >= dataRegistro)
                .ToList().ForEach(
                delegate (ProdutoEntity produto) 
                {
                    var prod = new ProdutoModel()
                    {
                        Codigo = produto.Codigo,
                        CodigoBarras = produto.CodigoBarras,
                        CodigoFabrica = produto.CodigoFabrica,
                        CodigoOriginal = produto.CodigoOriginal,
                        Cor = produto.Cor,
                        DataRegistro = produto.DataRegistro,
                        Marca = produto.Marca,
                        Nome = produto.Nome,
                        QuantidadeEstoque = produto.QuantidadeEstoque,
                        Status = produto.Status,
                        Tamanho = produto.Tamanho,
                        Unidade = produto.Unidade,
                        ValorVenda = produto.ValorVenda,
                        ImagensProduto = new List<ImagemProdutoModel>()
                    };
                    
                    retorno.Add(prod);
                });

            return retorno;
        }

        public ProdutoEntity MapProdutoEntity(ProdutoModel objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProdutoModel, ProdutoEntity>()
                   .ForMember(prod => prod.DataRegistro, opt => opt.UseValue(DateTime.Now));
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<ProdutoEntity>(objeto);
        }

        private ProdutoModel MapProdutoModel(ProdutoEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ProdutoEntity, ProdutoModel>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<ProdutoModel>(objeto);
        }

        public List<ProdutoTabela> ObterTodos(JQueryDataTableParam param)
        {
            var query = this._contexto.Produto.AsQueryable();

            if(!string.IsNullOrWhiteSpace(param.sSearch))
            {
                query = query.Where(x => SqlFunctions.StringConvert((decimal)x.Codigo).Contains(param.sSearch) ||
                                         x.Nome.Contains(param.sSearch) || x.Marca.Contains(param.sSearch) ||
                                         SqlFunctions.StringConvert(x.ValorVenda).Contains(param.sSearch) ||
                                         SqlFunctions.StringConvert(x.QuantidadeEstoque).Contains(param.sSearch));
            }

            switch ((ProdutoTableEnum) param.iSortingCols)
            {
                case ProdutoTableEnum.Codigo:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Codigo);
                    else
                        query = query.OrderByDescending(x => x.Codigo);
                    break;
                case ProdutoTableEnum.Nome:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Nome);
                    else
                        query = query.OrderByDescending(x => x.Nome);
                    break;
                case ProdutoTableEnum.Marca:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Marca);
                    else
                        query = query.OrderByDescending(x => x.Marca);
                    break;
                case ProdutoTableEnum.ValorVenda:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.ValorVenda);
                    else
                        query = query.OrderByDescending(x => x.ValorVenda);
                    break;
                case ProdutoTableEnum.QuantidadeEstoque:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.QuantidadeEstoque);
                    else
                        query = query.OrderByDescending(x => x.QuantidadeEstoque);
                    break;
                case ProdutoTableEnum.Status:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Status);
                    else
                        query = query.OrderByDescending(x => x.Status);
                    break;
                default:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Codigo);
                    else
                        query = query.OrderByDescending(x => x.Codigo);
                    break;
            }

            return query.Skip(param.iDisplayStart)
                        .Take(param.iDisplayLength)
                        .ToList()
                        .Select(prod => new ProdutoTabela
                        {
                            Codigo = prod.Codigo,
                            Nome = prod.Nome,
                            Marca = prod.Marca,
                            ValorVenda = prod.ValorVenda.ToString("N2"),
                            QuantidadeEstoque = prod.QuantidadeEstoque.HasValue ? prod.QuantidadeEstoque.Value.ToString("N2") : "0",
                            Status = prod.Status == Enums.Situacao.ATIVO ? "Ativo" : "Inativo"
                        }).ToList();
        }

        public int QuantidadeTotalRegistros()
        {
            return this._contexto.Produto.Count();
        }
    }
}
