﻿using System;
using System.Collections.Generic;
using Api.Database.Entities;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Database;
using AutoMapper;
using System.Linq;

namespace Api.Repository.Repositorio
{
    public class VendedorRepository : IVendedorRepository
    {
        private IContext _contexto;
        public VendedorRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        //TODO: Sumarizar
        public bool Editar(VendedorModel objeto)
        {
            VendedorEntity vendedor = this._contexto.Vendedor.SingleOrDefault(vend => vend.Codigo == objeto.Codigo);
            if (vendedor != null)
            {
                vendedor.Nome = objeto.Nome;
                vendedor.Observacao = objeto.Observacao;
                vendedor.StatusVendedor = objeto.StatusVendedor;
                vendedor.DataRegistro = DateTime.Now;

                this._contexto.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<string> Editar(List<VendedorModel> objeto)
        {
            List<string> erros = new List<string>();

            foreach (var item in objeto)
            {
                VendedorEntity vendedor = this._contexto.Vendedor.SingleOrDefault(vend => vend.Codigo == item.Codigo);
                if (vendedor != null)
                {
                    vendedor.Nome = item.Nome;
                    vendedor.Observacao = item.Observacao;
                    vendedor.StatusVendedor = item.StatusVendedor;
                    vendedor.DataRegistro = DateTime.Now;
                }
                else
                {
                    erros.Add("O Vendedor de Codigo " + item.Codigo + " não foi encontrado");
                }
            }

            this._contexto.SaveChanges();
            return erros;
        }

        public bool Excluir(int codigo)
        {
            VendedorEntity vendedor = this._contexto.Vendedor.SingleOrDefault(vend => vend.Codigo == codigo);
            if(vendedor != null)
            {
                this._contexto.Vendedor.Remove(vendedor);
                this._contexto.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<string> Excluir(List<int> codigos)
        {
            List<string> retorno = new List<string>();

            var listaVendedores = this._contexto.Vendedor.Where(ven => codigos.Contains(ven.Codigo));
            var naoEncontrados = listaVendedores.Where(ven => !codigos.Contains(ven.Codigo)).ToList();

            this._contexto.Vendedor.RemoveRange(listaVendedores);
            this._contexto.SaveChanges();

            if (naoEncontrados.Count > 0)
            {
                foreach (var naoEncontrado in naoEncontrados)
                {
                    retorno.Add("O Vendedor de código " + naoEncontrado.Codigo + " não foi encontrado");
                }
            }

            return retorno;
        }

        public bool Incluir(VendedorModel objeto)
        {
            VendedorEntity vendedor = this.MapVendedorEntity(objeto);
            this._contexto.Vendedor.Add(vendedor);
            this._contexto.SaveChanges();
            return true;
        }

        public bool Incluir(List<VendedorModel> vendedores)
        {
            foreach (var vendedor in vendedores)
            {
                VendedorEntity novoVendedor = this.MapVendedorEntity(vendedor);
                this._contexto.Vendedor.Add(novoVendedor);
            }

            this._contexto.SaveChanges();
            return true;
        }

        public VendedorModel ObterPorID(int codigo)
        {
            return this.MapVendedorModel(this._contexto.Vendedor.SingleOrDefault(vend => vend.Codigo == codigo));
        }

        public List<VendedorModel> ObterTodos()
        {
            List<VendedorModel> retorno = new List<VendedorModel>();
            this._contexto.Vendedor.ToList().ForEach(delegate (VendedorEntity instancia)
            {
                retorno.Add(this.MapVendedorModel(instancia));
            });
            return retorno;
        }

        public List<VendedorModel> ObterTodos(DateTime dataRegistro)
        {
            List<VendedorModel> retorno = new List<VendedorModel>();
            this._contexto.Vendedor
                .Where(ven => ven.DataRegistro >= dataRegistro)
                .ToList().ForEach(delegate (VendedorEntity instancia)
            {
                retorno.Add(this.MapVendedorModel(instancia));
            });
            return retorno;
        }

        public VendedorEntity MapVendedorEntity(VendedorModel objeto)
        {
            var mapperConfig = new MapperConfiguration(
                cfg => cfg.CreateMap<VendedorModel, VendedorEntity>()
                          .ForMember(vend => vend.DataRegistro, opt => opt.UseValue(DateTime.Now)));
            IMapper mapper = mapperConfig.CreateMapper();
            return mapper.Map<VendedorEntity>(objeto);
        }

        private VendedorModel MapVendedorModel(VendedorEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<VendedorEntity, VendedorModel>());
            IMapper mapper = mapperConfig.CreateMapper();
            return mapper.Map<VendedorModel>(objeto);
        }
    }
}
