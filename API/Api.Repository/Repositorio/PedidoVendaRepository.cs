﻿
using System;
using System.Collections.Generic;
using Api.Database;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Database.Entities;
using AutoMapper;
using System.Linq;
using Api.Models.DataTables;
using Api.Models.POCO;
using System.Data.Entity.SqlServer;
using Api.Enums.DataTables;

namespace Api.Repository.Repositorio
{
    public class PedidoVendaRepository : IPedidoVendaRepository
    {
        private IContext _contexto;

        public PedidoVendaRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        public bool Incluir(PedidoVendaModel pedido)
        {
            PedidoVendaEntity novoPedido = this.MapPedidoVendaEntity(pedido);
            this._contexto.PedidoVenda.Add(novoPedido);
            this._contexto.SaveChanges();

            return true;
        }

        public bool Incluir(List<PedidoVendaModel> pedidos)
        {
            foreach (var pedido in pedidos)
            {
                PedidoVendaEntity novoPedido = this.MapPedidoVendaEntity(pedido);
                if(!this._contexto.Clientes.Any(x => x.Codigo == novoPedido.ClienteCodigo))
                {
                    RemoveReferenciaCliente(novoPedido);
                }

                this._contexto.PedidoVenda.Add(novoPedido);
            }

            this._contexto.SaveChanges();
            return true;
        }

        public List<PedidoVendaModel> ObterLista()
        {
            List<PedidoVendaModel> retorno = new List<PedidoVendaModel>();
            this._contexto.PedidoVenda.ToList().ForEach(delegate (PedidoVendaEntity auditoria) {
                retorno.Add(this.MapPedidoVendaModel(auditoria));
            });

            return retorno;
        }

        public List<PedidoVendaModel> ObterLista(DateTime dataRegistro)
        {
            List<PedidoVendaModel> retorno = new List<PedidoVendaModel>();
            this._contexto.PedidoVenda
                .Where(ped => ped.DataRegistro >= dataRegistro)
                .ToList().ForEach(delegate (PedidoVendaEntity auditoria) {
                retorno.Add(this.MapPedidoVendaModel(auditoria));
            });

            return retorno;
        }

        public PedidoVendaModel ObterPedido(int codigo)
        {
            return this.MapPedidoVendaModel(this._contexto.PedidoVenda.SingleOrDefault(ped => ped.PedidoCodigo == codigo));
        }

        private PedidoVendaModel MapPedidoVendaModel(PedidoVendaEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PedidoVendaEntity, PedidoVendaModel>();

                cfg.CreateMap<ClienteEntity, ClienteModel>();
                cfg.CreateMap<EnderecoEntity, EnderecoModel>();
                cfg.CreateMap<ContatoEntity, ContatoModel>();

                cfg.CreateMap<VendedorEntity, VendedorModel>();
                cfg.CreateMap<PlanoPagamentoEntity, PlanoPagamentoModel>();

                cfg.CreateMap<ItensPedidoEntity, ItensPedidoModel>();
                cfg.CreateMap<ProdutoEntity, ProdutoModel>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<PedidoVendaModel>(objeto);
        }

        public PedidoVendaEntity MapPedidoVendaEntity(PedidoVendaModel objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PedidoVendaModel, PedidoVendaEntity>()
                    .ForMember(plano => plano.DataRegistro, opt => opt.UseValue(DateTime.Now));

                cfg.CreateMap<ClienteModel, ClienteEntity>()
                    .ForMember(cli => cli.DataRegistro, opt => opt.UseValue(DateTime.Now));
                cfg.CreateMap<EnderecoModel, EnderecoEntity>();
                cfg.CreateMap<ContatoModel, ContatoEntity>()
                    .ForMember(ctto => ctto.DDD, opt => opt.Condition(x => x.DDD != "null"))
                    .ForMember(ctto => ctto.Telefone, opt => opt.Condition(x => x.Telefone != "null"))
                    .ForMember(ctto => ctto.Email, opt => opt.Condition(x => x.Email != "null"));

                cfg.CreateMap<VendedorModel, VendedorEntity>();
                cfg.CreateMap<PlanoPagamentoModel, PlanoPagamentoEntity>();

                cfg.CreateMap<ItensPedidoModel, ItensPedidoEntity>();
                cfg.CreateMap<ProdutoModel, ProdutoEntity>();
            });
            IMapper mapper = mapperConfig.CreateMapper();
            return mapper.Map<PedidoVendaEntity>(objeto);
        }

        public List<PedidoVendaTabela> ObterLista(JQueryDataTableParam param)
        {
            var query = this._contexto.PedidoVenda.AsQueryable();

            if(!string.IsNullOrWhiteSpace(param.sSearch))
            {
                query = query.Where(x => SqlFunctions.StringConvert((decimal)x.PedidoCodigo).Contains(param.sSearch) ||
                                        x.Cliente.Nome.Contains(param.sSearch) ||
                                        SqlFunctions.StringConvert((decimal)x.DataPedido.Day).Contains(param.sSearch) ||
                                        SqlFunctions.StringConvert((decimal)x.DataPedido.Month).Contains(param.sSearch) ||
                                        SqlFunctions.StringConvert((decimal)x.DataPedido.Year).Contains(param.sSearch) ||
                                        SqlFunctions.StringConvert((decimal)x.Valor).Contains(param.sSearch));
            }

            switch ((PedidoVendaTableEnum) param.iSortingCols)
            {
                case PedidoVendaTableEnum.Codigo:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.PedidoCodigo);
                    else
                        query = query.OrderByDescending(x => x.PedidoCodigo);
                    break;
                case PedidoVendaTableEnum.ClienteNome:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Cliente.Nome);
                    else
                        query = query.OrderByDescending(x => x.Cliente.Nome);
                    break;
                case PedidoVendaTableEnum.DataPedido:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.DataPedido);
                    else
                        query = query.OrderByDescending(x => x.DataPedido);
                    break;
                case PedidoVendaTableEnum.Valor:
                    if (param.oSearch.Equals("asc"))
                        query = query.OrderBy(x => x.Valor);
                    else
                        query = query.OrderByDescending(x => x.Valor);
                    break;
                default:
                    query = query.OrderBy(x => x.ClienteCodigo);
                    break;
            }

            return query.Skip(param.iDisplayStart)
                .Take(param.iDisplayLength)
                .ToList()
                .Select(ped => new PedidoVendaTabela
                {
                    Codigo = ped.PedidoCodigo,
                    Nome = ped.Cliente.Nome,
                    DataPedido = ped.DataPedido.ToShortDateString(),
                    Valor = ped.Valor.ToString("N2")
                }).ToList();
        }

        public int QuantidadeTotalRegistros()
        {
            return this._contexto.PedidoVenda.Count();
        }

        private PedidoVendaEntity RemoveReferenciaCliente(PedidoVendaEntity pedido)
        {
            pedido.ClienteCodigo = 0;
            if (pedido.Cliente != null)
            {
                pedido.Cliente.Codigo = 0;
                pedido.Cliente.DataRegistro = DateTime.Now;
                pedido.Cliente.StatusCliente = Enums.Situacao.ATIVO;
                if (pedido.Cliente.Endereco != null)
                {
                    foreach (var endereco in pedido.Cliente.Endereco)
                    {
                        endereco.EnderecoID = 0;
                        endereco.ClienteCodigo = 0;
                    }
                }

                if (pedido.Cliente.Contato != null)
                {
                    foreach (var contato in pedido.Cliente.Contato)
                    {
                        contato.ContatoID = 0;
                        contato.ClienteCodigo = 0;
                    }
                }
            }

            return pedido;
        }
    }
}
