﻿using Api.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Api.Models;
using Api.Database;
using Api.Database.Entities;
using System;

namespace Api.Repository
{
    public class UsersRepository : IUsersRepository
    {
        IContext _context;
        
        public UsersRepository(IContext context)
        {
            _context = context;
        }

        public bool Add(UserEntity newUser)
        {
            _context.Users.Add(newUser);
            _context.SaveChanges();
            return true;
        }

        public bool EmailIsValid(string email)
        {
            return _context.Users.Any(x => x.Email == email);
        }

        //TODO: Verificar a retirada do Password do retorno.
        public List<UserModel> GetAll()
        {
            return _context.Users.Select(element => new UserModel
            {
                Active = element.Active,
                Email = element.Email,
                LastName = element.LastName,
                Name = element.Name,
                Password = element.Password,
                GrupoUsuariosCodigo = element.GrupoUsuariosCodigo,
                UserID = element.UserID
            }).ToList();
        }

        public UserModel Get(int userID)
        {
            return _context.Users
                .Where(x => x.UserID == userID)
                .Select(element => new UserModel
                {
                    Active = element.Active,
                    Email = element.Email,
                    LastName = element.LastName,
                    Name = element.Name,
                    Password = element.Password,
                    GrupoUsuariosCodigo = element.GrupoUsuariosCodigo,
                    UserID = element.UserID
                }).FirstOrDefault();
        }

        public bool Update(UserModel model)
        {
            UserEntity updateUser = _context.Users.FirstOrDefault(x => x.UserID == model.UserID);

            updateUser.Active = model.Active;
            updateUser.LastName = model.LastName;
            updateUser.Name = model.Name;
            updateUser.GrupoUsuariosCodigo = model.GrupoUsuariosCodigo;
            updateUser.Active = model.Active;

            updateUser.Name = model.Name;
            updateUser.Email = model.Email;
            updateUser.Active = model.Active;

            if (!string.IsNullOrEmpty(model.Password))
                updateUser.Password = model.Password;

            _context.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            UserEntity user = _context.Users.FirstOrDefault(x => x.UserID == id);
            
            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
                return true;
            }
            else
                return false;
        }
    }
}
