﻿using Api.Database;
using Api.Database.Entities;
using Api.Enums;
using Api.Models.Model;
using Api.Models.POCO;
using Api.Repository.Interfaces;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Api.Repository.Repositorio
{
    public class GrupoUsuariosRepository : IGrupoUsuariosRepository
    {
        private IContext _contexto;

        public GrupoUsuariosRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        public bool Editar(GrupoUsuariosModel objeto)
        {
            GrupoUsuariosEntity grupoUsuarios = this._contexto.GrupoUsuarios
                .Where(gru => gru.Codigo == objeto.Codigo).SingleOrDefault();
            if (grupoUsuarios != null)
            {
                grupoUsuarios.Nome = objeto.Nome;

                if (objeto.Permissionamento != null)
                {
                    bool permissionamentoAtualizar = (grupoUsuarios.Permissionamento.Any(end => objeto.Permissionamento.Any(obj => obj.GrupoUsuariosCodigo == end.GrupoUsuariosCodigo)) || objeto.Permissionamento.Any(map => map.GrupoUsuariosCodigo == 0));
                    if (permissionamentoAtualizar)
                    {
                        foreach (var permissionamento in objeto.Permissionamento)
                        {
                            if (permissionamento.GrupoUsuariosCodigo != 0)
                            {
                                PermissionamentoEntity permissionamentoAntigo = grupoUsuarios.Permissionamento
                                    .SingleOrDefault(per => per.GrupoUsuariosCodigo == permissionamento.GrupoUsuariosCodigo && per.PermissionamentoCodigo == permissionamento.PermissionamentoCodigo);
                                if (permissionamentoAntigo != null && !permissionamentoAntigo.Equals(permissionamento))
                                {
                                    permissionamentoAntigo.ControllerCodigo = permissionamento.ControllerCodigo;
                                    permissionamentoAntigo.TipoPermissaoCodigo = permissionamento.TipoPermissaoCodigo;
                                    permissionamentoAntigo.DataAlteracao = DateTime.Now;
                                }
                            }
                            else
                            {
                                PermissionamentoEntity novoPermissionamento = new PermissionamentoEntity()
                                {
                                    ControllerCodigo = permissionamento.ControllerCodigo,
                                    TipoPermissaoCodigo = permissionamento.TipoPermissaoCodigo,
                                    DataAlteracao = DateTime.Now
                                };

                                grupoUsuarios.Permissionamento.Add(novoPermissionamento);
                            }
                        }
                    }

                    var permissionamentoExcluir = grupoUsuarios.Permissionamento
                        .Where(perm => !objeto.Permissionamento.Any(obj => obj.PermissionamentoCodigo == perm.PermissionamentoCodigo && obj.GrupoUsuariosCodigo == perm.GrupoUsuariosCodigo)).ToList();
                    if (permissionamentoExcluir.Count > 0)
                    {
                        foreach (var permissionamento in permissionamentoExcluir)
                        {
                            this._contexto.Permissionamento.Remove(permissionamento);
                        }
                    }

                    this._contexto.SaveChanges();
                    return true;
                }
            }

            return false;
        }

        public bool Excluir(int codigo)
        {
            GrupoUsuariosEntity grupoUsuarios = this._contexto.GrupoUsuarios.Where(grp => grp.Codigo == codigo).SingleOrDefault();
            if (grupoUsuarios != null)
            {
                this._contexto.GrupoUsuarios.Remove(grupoUsuarios);
                this._contexto.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Incluir(GrupoUsuariosModel objeto)
        {
            GrupoUsuariosEntity novoGrupo = this.MapGrupoUsuariosEntity(objeto);
            this._contexto.GrupoUsuarios.Add(novoGrupo);
            this._contexto.SaveChanges();
            return true;
        }

        public GrupoUsuariosModel ObterPorID(int codigo)
        {
            return this.MapGrupoUsuariosModel(this._contexto.GrupoUsuarios.SingleOrDefault(grp => grp.Codigo == codigo));
        }

        public List<GrupoUsuariosModel> ObterTodos()
        {
            List<GrupoUsuariosModel> retorno = new List<GrupoUsuariosModel>();
            this._contexto.GrupoUsuarios.ToList().ForEach(delegate (GrupoUsuariosEntity grupo) {
                retorno.Add(this.MapGrupoUsuariosModel(grupo));
            });

            return retorno;
        }

        public bool AutorizarAcesso(AutorizacaoAcesso model)
        {
            bool retorno = false;

            var grupo = this._contexto.GrupoUsuarios.SingleOrDefault(user => user.Codigo == model.GrupoUsuariosCodigo);
            if(grupo != null)
            {
                retorno = grupo.Permissionamento.Any(perm => perm.ControllerCodigo == model.Controller && perm.TipoPermissaoCodigo == model.TipoPermissionamento);
            }

            return retorno;
        }

        public List<PermissionamentoModel> ObterPermissionamento(int GrupoUsuariosCodigo)
        {
            List<PermissionamentoModel> retorno = new List<PermissionamentoModel>();
            GrupoUsuariosEntity grupo = this._contexto.GrupoUsuarios.SingleOrDefault(gu => gu.Codigo == GrupoUsuariosCodigo);
            if(grupo != null)
            {
                foreach (var permissao in grupo.Permissionamento)
                {
                    retorno.Add(this.MapPermissionamentoModel(permissao));
                }
            }

            return retorno;
        }

        public GrupoUsuariosEntity MapGrupoUsuariosEntity(GrupoUsuariosModel objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GrupoUsuariosModel, GrupoUsuariosEntity>();
                cfg.CreateMap<PermissionamentoModel, PermissionamentoEntity>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<GrupoUsuariosEntity>(objeto);
        }

        private GrupoUsuariosModel MapGrupoUsuariosModel(GrupoUsuariosEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GrupoUsuariosEntity, GrupoUsuariosModel>();
                cfg.CreateMap<PermissionamentoEntity, PermissionamentoModel>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<GrupoUsuariosModel>(objeto);
        }

        private PermissionamentoModel MapPermissionamentoModel(PermissionamentoEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<PermissionamentoEntity, PermissionamentoModel>();
                cfg.CreateMap<GrupoUsuariosEntity, GrupoUsuariosModel>();
            });
            IMapper mapper = mapperConfig.CreateMapper();

            return mapper.Map<PermissionamentoModel>(objeto);
        }
    }
}
