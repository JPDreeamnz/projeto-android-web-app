﻿using Api.Repository.Interfaces;
using Api.Models.Model;
using System;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using Api.Models.POCO;
using System.Linq;

namespace Api.Repository.Repositorio
{
    public class ImagemProdutoRepository : IImagemProdutoRepository, IDisposable
    {
        private String BASE_FOLDER;
        private SafeHandle _handle = new SafeFileHandle(IntPtr.Zero, true);

        public ImagemProdutoRepository(string baseFolder)
        {
            this.BASE_FOLDER = baseFolder;
        }

        public String PASTA {
            get { return this.BASE_FOLDER; }
        }

        public bool ExcluirImagem(ImagemProdutoModel objeto)
        {
            string caminho = this.RetornaCaminhoCompleto(objeto.CaminhoPasta, objeto.Sequencial, objeto.Extensao);
            FileInfo imagem = new FileInfo(caminho);
            if (imagem.Exists)
            {
                File.Delete(caminho);
            }

            return true;
        }

        public bool ExcluirPastaImagens(string caminhoPasta)
        {
            string caminho = Path.Combine(this.BASE_FOLDER, caminhoPasta);
            
            if(Directory.Exists(caminho))
            {
                Directory.Delete(caminho, true);
            }

            return true;
        }

        public bool SalvarImagem(ImagemProdutoModel objeto, byte[] imagem)
        {
            this.CriaPastaSistema(objeto.CaminhoPasta);
            using (MemoryStream ms = new MemoryStream(imagem))
            {
                using (Bitmap bm = new Bitmap(ms))
                {
                    bm.Save(Path.Combine(this.RetornaCaminhoCompleto(objeto.CaminhoPasta, objeto.Sequencial, objeto.Extensao)));
                }
            }

            return true;
        }

        public List<KeyValuePair<int, byte[]>> ObterImagens(string caminhoPasta)
        {
            var retorno = new List<KeyValuePair<int, byte[]>>();
            if (Directory.Exists(Path.Combine(BASE_FOLDER, caminhoPasta)))
            {
                IEnumerable<string> imagens = this.ListaImagensArquivo(caminhoPasta);

                foreach (var imagem in imagens)
                {
                    retorno.Add(new KeyValuePair<int, byte[]>(int.Parse(Path.GetFileNameWithoutExtension(imagem)), File.ReadAllBytes(imagem)));
                }
            }

            return retorno;
        }

        public byte[] ObterImagem(string caminhoPasta)
        {
            string[] produtoImagem = caminhoPasta.Split('\\');
            var caminhoImagem = Path.Combine(BASE_FOLDER, produtoImagem[1]);

            DirectoryInfo dir = new DirectoryInfo(caminhoImagem);
            FileInfo[] files = dir.GetFiles(produtoImagem[2] + ".*");
            
            if (files.Length > 0)
            {
                return File.ReadAllBytes(files.First().FullName);
            }

            return null;
        }

        public IEnumerable<string> ListaImagensArquivo(string caminhoPasta)
        {
            IEnumerable<string> imagens = Enumerable.Empty<string>();
            string caminho = Path.Combine(this.BASE_FOLDER, caminhoPasta);

            if (Directory.Exists(caminho))
                imagens = Directory.EnumerateFiles(caminho);

            return imagens;
        }

        public bool RenomearPasta(int codigoProduto, string caminhoPasta)
        {
            Directory.Move(Path.Combine(this.BASE_FOLDER, caminhoPasta), Path.Combine(this.BASE_FOLDER, codigoProduto.ToString()));
            return true;
        }

        public bool VerificaExistenciaPastaSistema(string caminhoPasta)
        {
            return Directory.Exists(Path.Combine(this.BASE_FOLDER, caminhoPasta));
        }

        public void CriaPastaSistema(string caminhoPasta)
        {
            Directory.CreateDirectory(Path.Combine(this.BASE_FOLDER, caminhoPasta));
        }

        private void ExcluirPastaSistema(string subFolder)
        {
            string caminho = Path.Combine(this.BASE_FOLDER, subFolder);
            if (Directory.GetFiles(caminho).Length <= 0)
            {
                Directory.Delete(caminho);
            }
        }

        private string RetornaCaminhoCompleto(string caminho, int sequencial, string extensao)
        {
            return Path.Combine(this.BASE_FOLDER, caminho, sequencial.ToString() + '.' + extensao);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._handle.Dispose();
            }
        }

    }
}
