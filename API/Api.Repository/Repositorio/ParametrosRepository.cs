﻿using Api.Database.Entities;
using Api.Models.Model;
using Api.Repository.Interfaces;
using Api.Database;
using AutoMapper;
using System.Linq;

namespace Api.Repository.Repositorio
{
    public class ParametrosRepository : IParametrosRepository
    {
        private IContext _contexto;
        public ParametrosRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        //TODO: Sumarizar
        public bool Editar(ParametrosModel objeto)
        {
            ParametrosEntity parametro = this._contexto.Parametros.FirstOrDefault();
            if (parametro != null)
            {
                parametro.ClienteDefault = objeto.ClienteDefault;
                parametro.PlanoPagamentoDefault = objeto.PlanoPagamentoDefault;
                parametro.TipoIntegracao = objeto.TipoIntegracao;
                parametro.ValidaEstoque = objeto.ValidaEstoque;
                parametro.VendedorDefault = objeto.VendedorDefault;

                this._contexto.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public ParametrosModel ObterParametros()
        {
            return this.MapParametrosModel(this._contexto.Parametros.FirstOrDefault());
        }
        
        private ParametrosModel MapParametrosModel(ParametrosEntity objeto)
        {
            var mapperConfig = new MapperConfiguration(cfg => cfg.CreateMap<ParametrosEntity, ParametrosModel>());
            IMapper mapper = mapperConfig.CreateMapper();
            return mapper.Map<ParametrosModel>(objeto);
        }
    }
}
