﻿using Api.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models.Model;
using Api.Database;
using Api.Database.Entities;

namespace Api.Repository.Repositorio
{
    public class EnderecoRepository : IEnderecoRepository
    {
        private IContext _contexto;

        public EnderecoRepository(IContext contexto)
        {
            this._contexto = contexto;
        }

        public bool Editar(EnderecoModel objeto)
        {
            EnderecoEntity endereco = this._contexto.Endereco.SingleOrDefault(end => end.EnderecoID == objeto.EnderecoID);
            if (endereco != null)
            {
                endereco.Endereco = objeto.Endereco;
                endereco.Complemento = objeto.Complemento;
                endereco.Bairro = objeto.Bairro;
                endereco.Cidade = objeto.Cidade;
                endereco.Numero = objeto.Numero;
                endereco.CEP = objeto.CEP;

                this._contexto.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool Excluir(int enderecoID)
        {
            EnderecoEntity endereco = this._contexto.Endereco.SingleOrDefault(end => end.EnderecoID == enderecoID);
            if (endereco != null)
            {
                this._contexto.Endereco.Remove(endereco);
                this._contexto.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool Incluir(EnderecoModel objeto)
        {
            try
            {
                this._contexto.Endereco.Add(new EnderecoEntity()
                {
                    Endereco = objeto.Endereco,
                    Complemento = objeto.Complemento,
                    Numero = objeto.Numero,
                    Bairro = objeto.Bairro,
                    Cidade = objeto.Cidade,
                    Estado = objeto.Estado,
                    CEP = objeto.CEP
                });
                this._contexto.SaveChanges();
                return true;
            }
            catch 
            {
                return false;
            }
        }

        public EnderecoModel ObterPorID(int enderecoID)
        {
            return this._contexto.Endereco.Where(end => end.EnderecoID == enderecoID)
                .Select(end => new EnderecoModel()
                {
                    EnderecoID = end.EnderecoID,
                    Endereco = end.Endereco,
                    Complemento = end.Complemento,
                    Numero = end.Numero,
                    Bairro = end.Bairro,
                    Cidade = end.Cidade,
                    Estado = end.Estado,
                    CEP = end.CEP
                }).SingleOrDefault();
        }

        public List<EnderecoModel> ObterTodos()
        {
            return this._contexto.Endereco.Select(end => new EnderecoModel()
                {
                    EnderecoID = end.EnderecoID,
                    Endereco = end.Endereco,
                    Complemento = end.Complemento,
                    Numero = end.Numero,
                    Bairro = end.Bairro,
                    Cidade = end.Cidade,
                    Estado = end.Estado,
                    CEP = end.CEP
                }).ToList();
        }
    }
}
