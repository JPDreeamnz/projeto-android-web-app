﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.SqlServer;

namespace Api.Repository
{
    public class Server
    {
        Api.Database.Context context = new Database.Context();

        public Server()
        {
            try {

                // workaround para forçar deploy da dll do sql provider;
                // somente bibliotecas realmente em uso no codigo fonte são 
                // copiadas para a pasta BIN e para o deploy.
                // TODO: pesquisar uma solução melhor.

                SqlFunctions.StringConvert((decimal)12);
            }
            catch
            {

            }
        }

        public void Drop()
        {
            context.Database.Delete();
        }

        public void Create()
        {
            context.Database.Create();
        }
    }
}
