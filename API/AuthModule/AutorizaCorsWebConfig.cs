﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Cors;
using System.Web.Http.Cors;

namespace System
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AutorizaCorsWebConfig : Attribute, ICorsPolicyProvider
    {
        private CorsPolicy _corsPolicy;

        public AutorizaCorsWebConfig(string appSettingsString)
        {
            _corsPolicy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true
            };

            string origemString = ConfigurationManager.AppSettings[appSettingsString];
            if (!string.IsNullOrEmpty(origemString))
            {
                foreach (var origem in origemString.Split(','))
                {
                    _corsPolicy.Origins.Add(origem);
                }
            }
        }

        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_corsPolicy);
        }
    }
}