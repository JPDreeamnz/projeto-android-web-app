﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace System
{
    public class AuthModule : DelegatingHandler
    {
        private string AuthUrl = TryGetValue("AuthUrl");
        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            if (request.RequestUri.AbsoluteUri.Contains("ImagemProduto") && request.Headers.Authorization == null)
            {
                string[] separador = new string[] { "token" };
                string token = string.Empty;

                if (string.IsNullOrEmpty(request.RequestUri.Query))
                    token = request.Content.ReadAsStringAsync().Result.Split(separador, StringSplitOptions.None)[1].Substring(1);
                else
                    token = request.RequestUri.Query.Split('=')[2];

                if (request.Method.Method.Equals("OPTIONS"))
                    request.Method = new HttpMethod("POST");
                
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }

            HttpContext.Current.Session["AUTH_URL"] = this.AuthUrl;
            HttpContext.Current.Session["TOKEN_AUDITOR"] = request.Headers.Authorization.Parameter;

            if (request.RequestUri.AbsoluteUri.Contains("Account"))
                return base.SendAsync(request, cancellationToken);

            if (ValidateBearer(request.Headers.Authorization))
                return base.SendAsync(request, cancellationToken);
            
            HttpResponseMessage reply = request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Usuário não autorizado.");
            string hint = string.Format("Bearer realm={0} erro=Invalid_Token error_description=Você não possui um token válido", new Uri(AuthUrl).Authority);
            reply.Headers.Add("WWW-Authenticate", hint);

            return Task.FromResult(reply);
        }

        private bool ValidateBearer(Net.Http.Headers.AuthenticationHeaderValue authenticationHeaderValue)
        {
            if (authenticationHeaderValue != null && authenticationHeaderValue.Scheme == "Bearer")
            {
                return ValidateToken(authenticationHeaderValue.Parameter);
            }

            return false;
        }

        private bool ValidateToken(string Token)
        {
            WebClient web = new WebClient();

            try
            {
                string result = web.DownloadString(AuthUrl + "/Get?Token=" + Token);

                bool response = false;
                bool.TryParse(result, out response);
                
                return response;
            }
            catch
            {
                return false;
            }
            //catch (WebException ex )
            //{
            //    return false;
            //}
            //catch (NotSupportedException ex )
            //{
            //    return false;
            //}
        }

        /// <summary>
        /// Obter a url do servidor de autenticação. 
        /// </summary>
        /// <param name="Key">chave do registo no 'appSettings'</param>
        /// <returns></returns>
        internal static string TryGetValue(string Key)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings[Key];

            return value;
        }
    }
}