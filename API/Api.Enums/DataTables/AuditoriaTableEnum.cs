﻿
namespace Api.Enums.DataTables
{
    public enum AuditoriaTableEnum
    {
        Nome = 0,
        UsuarioEmail = 1,
        DataOperacao = 2,
        Operacao = 3,
        Tabela = 4,
        Objeto = 5
    }
}
