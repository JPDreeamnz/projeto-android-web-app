﻿namespace Api.Enums.DataTables
{
    public enum PedidoVendaTableEnum
    {
        Codigo = 0,
        ClienteNome = 1,
        DataPedido = 2,
        Valor = 3
    }
}
