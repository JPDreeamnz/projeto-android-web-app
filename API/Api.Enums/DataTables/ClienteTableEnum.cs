﻿namespace Api.Enums.DataTables
{
    public enum ClienteTableEnum
    {
        Nome = 0,
        TipoDocumento = 1,
        Documento = 2,
        InscricaoEstadual = 3,
        Situacao = 4
    }
}
