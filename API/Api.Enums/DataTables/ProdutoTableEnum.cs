﻿
namespace Api.Enums.DataTables
{
    public enum ProdutoTableEnum
    {
        Codigo = 0,
        Nome = 1,
        Marca = 2,
        ValorVenda = 3,
        QuantidadeEstoque = 4,
        Status = 5
    }
}
