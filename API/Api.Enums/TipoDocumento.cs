﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Enums
{
    public enum TipoDocumento
    {
        CNPJ = 1,
        CPF = 2
    }
}
