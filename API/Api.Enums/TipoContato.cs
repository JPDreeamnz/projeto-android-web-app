﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Enums
{
    public enum TipoContato
    {
        TELEFONE_FIXO = 1,
        TELEFONE_CELULAR = 2,
        EMAIL = 3
    }
}
