﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Enums
{
    public enum UserRole
    {
        USER = 0,
        ADMIN = 1,
        SUPERVISOR = 2,
        MASTER = 3
    }
}
