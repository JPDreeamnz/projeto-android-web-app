﻿
namespace Api.Enums
{
    public enum EstadoEntidade
    {
        INSERIDO = 1,
        EDITADO = 2,
        EXCLUIDO = 3,
        VISUALIZADO = 4
    }
}
