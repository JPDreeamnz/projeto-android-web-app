﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Enums
{
    public enum LoginMessages
    {
        LOGIN_SUCCESSFUL = 0,
        LOGIN_INVALID = 1,
        LOGIN_ERROR = 2
    }
}
