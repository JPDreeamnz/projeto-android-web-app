﻿namespace Api.Enums
{
    public enum PermissionamentoController
    {
        Nulo = 0,
        Home = 1,
        Auditoria = 2,
        Clientes = 3,
        Financeiro = 4,
        Parametros = 5,
        PedidoVenda = 6,
        PlanoPagamento = 7,
        Produto = 8,
        Usuarios = 9,
        Vendedor = 10,
        GrupoUsuarios = 11
    }
}
