﻿namespace Api.Enums
{
    public enum TipoPermissionamento
    {
        Nulo = 0,
        Criar = 1,
        Editar = 2,
        Excluir = 3,
        Listar = 4,
        Livre = 9
    }
}
