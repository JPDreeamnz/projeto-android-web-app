﻿
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;

namespace Api.Database.Entities
{
    interface IAuditor
    {
        void Auditar(IEnumerable<ObjectStateEntry> entidadesAlteradas);
    }
}
