﻿using System.Data.Entity;
using Api.Database.Entities;
using System.Data.Entity.Infrastructure;

namespace Api.Database
{
    public interface IContext
    {
        //DBSets
        DbSet<UserEntity> Users { get; set; }
        DbSet<ContatoEntity> Contato { get; set; }
        DbSet<EnderecoEntity> Endereco { get; set; }
        DbSet<ClienteEntity> Clientes { get; set; }
        DbSet<FinanceiroEntity> Financeiro { get; set; }
        DbSet<ParametrosEntity> Parametros { get; set; }
        DbSet<PlanoPagamentoEntity> PlanoPagamento { get; set; }
        DbSet<VendedorEntity> Vendedor { get; set; }
        DbSet<ProdutoEntity> Produto { get; set; }
        DbSet<AuditoriaEntity> Auditoria { get; set; }
        DbSet<ItensPedidoEntity> ItensPedido { get; set; }
        DbSet<PedidoVendaEntity> PedidoVenda { get; set; }
        DbSet<GrupoUsuariosEntity> GrupoUsuarios { get; set; }
        DbSet<PermissionamentoEntity> Permissionamento { get; set; }

        //Métodos de contexto
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        DbSet<TEntity> Set<TEntity>() where TEntity : class; 

        int ExecutarSQL(string sql);
        int SaveChanges();
    }
}