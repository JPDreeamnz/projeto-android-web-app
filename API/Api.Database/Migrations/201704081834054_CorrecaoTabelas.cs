namespace Api.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrecaoTabelas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Auditoria", "ID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Auditoria", "ID");
        }
    }
}
