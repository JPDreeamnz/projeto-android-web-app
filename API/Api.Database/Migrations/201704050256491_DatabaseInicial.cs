namespace Api.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auditoria",
                c => new
                    {
                        Codigo = c.Int(nullable: false, identity: true),
                        UsuarioCodigo = c.Int(nullable: false),
                        UsuarioEmail = c.String(),
                        UsuarioNome = c.String(),
                        UsuarioSobrenome = c.String(),
                        Operacao = c.Int(nullable: false),
                        Tabela = c.String(),
                        Objeto = c.String(),
                        DataOperacao = c.DateTime(nullable: false),
                        ID = c.String(),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        Codigo = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 60),
                        TipoDocumento = c.Int(nullable: false),
                        Documento = c.Long(),
                        InscricaoEstadual = c.String(maxLength: 20),
                        Observacao = c.String(maxLength: 200),
                        LimiteCredito = c.Decimal(precision: 18, scale: 2),
                        DataRegistro = c.DateTime(nullable: false),
                        StatusCliente = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dbo.Contato",
                c => new
                    {
                        ContatoID = c.Int(nullable: false, identity: true),
                        DDD = c.String(maxLength: 3),
                        Telefone = c.String(maxLength: 15),
                        Email = c.String(maxLength: 100),
                        TipoContato = c.Int(nullable: false),
                        ClienteCodigo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ContatoID)
                .ForeignKey("dbo.Clientes", t => t.ClienteCodigo, cascadeDelete: true)
                .Index(t => t.ClienteCodigo);
            
            CreateTable(
                "dbo.Endereco",
                c => new
                    {
                        EnderecoID = c.Int(nullable: false, identity: true),
                        Endereco = c.String(maxLength: 60),
                        Numero = c.String(maxLength: 10),
                        Complemento = c.String(maxLength: 10),
                        Bairro = c.String(maxLength: 60),
                        Cidade = c.String(maxLength: 60),
                        Estado = c.String(maxLength: 2),
                        CEP = c.Int(nullable: false),
                        ClienteCodigo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EnderecoID)
                .ForeignKey("dbo.Clientes", t => t.ClienteCodigo, cascadeDelete: true)
                .Index(t => t.ClienteCodigo);
            
            CreateTable(
                "dbo.PedidoVenda",
                c => new
                    {
                        PedidoCodigo = c.Int(nullable: false, identity: true),
                        ClienteCodigo = c.Int(nullable: false),
                        VendedorCodigo = c.Int(nullable: false),
                        PlanoPagamentoCodigo = c.Int(nullable: false),
                        DataPedido = c.DateTime(nullable: false),
                        ValorProduto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorDesconto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorAcrescimo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Observacao = c.String(maxLength: 200),
                        DataRegistro = c.DateTime(nullable: false),
                        StatusIntegracao = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PedidoCodigo)
                .ForeignKey("dbo.Clientes", t => t.ClienteCodigo)
                .ForeignKey("dbo.PlanoPagamento", t => t.PlanoPagamentoCodigo)
                .ForeignKey("dbo.Vendedor", t => t.VendedorCodigo)
                .Index(t => t.ClienteCodigo)
                .Index(t => t.VendedorCodigo)
                .Index(t => t.PlanoPagamentoCodigo);
            
            CreateTable(
                "dbo.ItensPedido",
                c => new
                    {
                        PedidoCodigo = c.Int(nullable: false),
                        ItemCodigo = c.Int(nullable: false, identity: true),
                        ProdutoCodigo = c.Int(nullable: false),
                        Quantidade = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorDesconto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.PedidoCodigo, t.ItemCodigo })
                .ForeignKey("dbo.PedidoVenda", t => t.PedidoCodigo)
                .ForeignKey("dbo.Produto", t => t.ProdutoCodigo)
                .Index(t => t.PedidoCodigo)
                .Index(t => t.ProdutoCodigo);
            
            CreateTable(
                "dbo.Produto",
                c => new
                    {
                        Codigo = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Unidade = c.String(nullable: false, maxLength: 3),
                        CodigoBarras = c.String(maxLength: 20),
                        CodigoFabrica = c.String(maxLength: 20),
                        CodigoOriginal = c.String(maxLength: 20),
                        Cor = c.String(maxLength: 30),
                        Tamanho = c.String(maxLength: 10),
                        Marca = c.String(maxLength: 30),
                        ValorVenda = c.Decimal(nullable: false, precision: 18, scale: 2),
                        QuantidadeEstoque = c.Decimal(precision: 18, scale: 2),
                        DataRegistro = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dbo.PlanoPagamento",
                c => new
                    {
                        Codigo = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Observacao = c.String(maxLength: 200),
                        StatusPlano = c.Int(nullable: false),
                        DataRegistro = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dbo.Vendedor",
                c => new
                    {
                        Codigo = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Observacao = c.String(maxLength: 200),
                        DataRegistro = c.DateTime(nullable: false),
                        StatusVendedor = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dbo.Financeiro",
                c => new
                    {
                        ClienteCodigo = c.Int(nullable: false),
                        Documento = c.String(nullable: false, maxLength: 30),
                        Emissao = c.DateTime(nullable: false),
                        Vencimento = c.DateTime(nullable: false),
                        Pagamento = c.DateTime(),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DataRegistro = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.ClienteCodigo, t.Documento })
                .ForeignKey("dbo.Clientes", t => t.ClienteCodigo, cascadeDelete: true)
                .Index(t => t.ClienteCodigo);
            
            CreateTable(
                "dbo.GrupoUsuarios",
                c => new
                    {
                        Codigo = c.Int(nullable: false, identity: true),
                        Nome = c.String(),
                    })
                .PrimaryKey(t => t.Codigo);
            
            CreateTable(
                "dbo.Permissionamento",
                c => new
                    {
                        GrupoUsuariosCodigo = c.Int(nullable: false),
                        PermissionamentoCodigo = c.Int(nullable: false, identity: true),
                        ControllerCodigo = c.Int(nullable: false),
                        TipoPermissaoCodigo = c.Int(nullable: false),
                        DataAlteracao = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.GrupoUsuariosCodigo, t.PermissionamentoCodigo })
                .ForeignKey("dbo.GrupoUsuarios", t => t.GrupoUsuariosCodigo, cascadeDelete: true)
                .Index(t => t.GrupoUsuariosCodigo);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastName = c.String(),
                        LoginName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        GrupoUsuariosCodigo = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserID)
                .ForeignKey("dbo.GrupoUsuarios", t => t.GrupoUsuariosCodigo)
                .Index(t => t.GrupoUsuariosCodigo);
            
            CreateTable(
                "dbo.Parametros",
                c => new
                    {
                        Codigo = c.Int(nullable: false, identity: true),
                        ValidaEstoque = c.Int(),
                        ClienteDefault = c.Int(),
                        VendedorDefault = c.Int(),
                        PlanoPagamentoDefault = c.Int(),
                        TipoIntegracao = c.Int(),
                    })
                .PrimaryKey(t => t.Codigo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "GrupoUsuariosCodigo", "dbo.GrupoUsuarios");
            DropForeignKey("dbo.Permissionamento", "GrupoUsuariosCodigo", "dbo.GrupoUsuarios");
            DropForeignKey("dbo.Financeiro", "ClienteCodigo", "dbo.Clientes");
            DropForeignKey("dbo.PedidoVenda", "VendedorCodigo", "dbo.Vendedor");
            DropForeignKey("dbo.PedidoVenda", "PlanoPagamentoCodigo", "dbo.PlanoPagamento");
            DropForeignKey("dbo.ItensPedido", "ProdutoCodigo", "dbo.Produto");
            DropForeignKey("dbo.ItensPedido", "PedidoCodigo", "dbo.PedidoVenda");
            DropForeignKey("dbo.PedidoVenda", "ClienteCodigo", "dbo.Clientes");
            DropForeignKey("dbo.Endereco", "ClienteCodigo", "dbo.Clientes");
            DropForeignKey("dbo.Contato", "ClienteCodigo", "dbo.Clientes");
            DropIndex("dbo.Users", new[] { "GrupoUsuariosCodigo" });
            DropIndex("dbo.Permissionamento", new[] { "GrupoUsuariosCodigo" });
            DropIndex("dbo.Financeiro", new[] { "ClienteCodigo" });
            DropIndex("dbo.ItensPedido", new[] { "ProdutoCodigo" });
            DropIndex("dbo.ItensPedido", new[] { "PedidoCodigo" });
            DropIndex("dbo.PedidoVenda", new[] { "PlanoPagamentoCodigo" });
            DropIndex("dbo.PedidoVenda", new[] { "VendedorCodigo" });
            DropIndex("dbo.PedidoVenda", new[] { "ClienteCodigo" });
            DropIndex("dbo.Endereco", new[] { "ClienteCodigo" });
            DropIndex("dbo.Contato", new[] { "ClienteCodigo" });
            DropTable("dbo.Parametros");
            DropTable("dbo.Users");
            DropTable("dbo.Permissionamento");
            DropTable("dbo.GrupoUsuarios");
            DropTable("dbo.Financeiro");
            DropTable("dbo.Vendedor");
            DropTable("dbo.PlanoPagamento");
            DropTable("dbo.Produto");
            DropTable("dbo.ItensPedido");
            DropTable("dbo.PedidoVenda");
            DropTable("dbo.Endereco");
            DropTable("dbo.Contato");
            DropTable("dbo.Clientes");
            DropTable("dbo.Auditoria");
        }
    }
}
