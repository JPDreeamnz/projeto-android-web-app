﻿using Api.Database.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Api.Database
{
    public class Context : DbContext, IContext
    {
        #region DbSet
        public virtual DbSet<UserEntity> Users { get; set; }
        public virtual DbSet<ContatoEntity> Contato { get; set; }
        public virtual DbSet<EnderecoEntity> Endereco { get; set; }
        public virtual DbSet<ClienteEntity> Clientes { get; set; }
        public virtual DbSet<FinanceiroEntity> Financeiro { get; set; }
        public virtual DbSet<ParametrosEntity> Parametros { get; set; }
        public virtual DbSet<PlanoPagamentoEntity> PlanoPagamento { get; set; }
        public virtual DbSet<VendedorEntity> Vendedor { get; set; }
        public virtual DbSet<ProdutoEntity> Produto { get; set; }
        public virtual DbSet<AuditoriaEntity> Auditoria { get; set; }
        public virtual DbSet<ItensPedidoEntity> ItensPedido { get; set; }
        public virtual DbSet<PedidoVendaEntity> PedidoVenda { get; set; }
        public virtual DbSet<GrupoUsuariosEntity> GrupoUsuarios { get; set; }
        public virtual DbSet<PermissionamentoEntity> Permissionamento { get; set; }
        #endregion

        public Context() : base("DB01_ProjetoAndroidSoftline")
        { }

        /// <summary>
        /// Método que configura os atributos das tabelas gerais.
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Clientes
            modelBuilder.Entity<ContatoEntity>()
                .HasRequired(ctto => ctto.Cliente)
                .WithMany(cli => cli.Contato)
                .HasForeignKey(ctto => ctto.ClienteCodigo)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<EnderecoEntity>()
                .HasRequired(end => end.Cliente)
                .WithMany(cli => cli.Endereco)
                .HasForeignKey(end => end.ClienteCodigo)
                .WillCascadeOnDelete(true);
            #endregion

            #region ItensPedido
            modelBuilder.Entity<ItensPedidoEntity>()
                .HasRequired(itt => itt.Pedido)
                .WithMany(itt => itt.ItensPedido)
                .HasForeignKey(itt => itt.PedidoCodigo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ItensPedidoEntity>()
                .HasRequired(itt => itt.Produto)
                .WithMany(itt => itt.ItensPedido)
                .HasForeignKey(itt => itt.ProdutoCodigo)
                .WillCascadeOnDelete(false);
            #endregion

            #region PedidoVenda
            modelBuilder.Entity<PedidoVendaEntity>()
                .HasRequired(ped => ped.Cliente)
                .WithMany(ped => ped.PedidoVenda)
                .HasForeignKey(ped => ped.ClienteCodigo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PedidoVendaEntity>()
                .HasRequired(ped => ped.Vendedor)
                .WithMany(ped => ped.PedidoVenda)
                .HasForeignKey(ped => ped.VendedorCodigo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PedidoVendaEntity>()
                .HasRequired(ped => ped.PlanoPagamento)
                .WithMany(ped => ped.PedidoVenda)
                .HasForeignKey(ped => ped.PlanoPagamentoCodigo)
                .WillCascadeOnDelete(false);
            #endregion

            #region Permissionamento
            modelBuilder.Entity<PermissionamentoEntity>()
                .HasRequired(gr => gr.GrupoUsuarios)
                .WithMany(gr => gr.Permissionamento)
                .HasForeignKey(gr => gr.GrupoUsuariosCodigo)
                .WillCascadeOnDelete(true);
            #endregion

            #region GrupoUsuarios
            modelBuilder.Entity<UserEntity>()
                .HasRequired(user => user.GrupoUsuarios)
                .WithMany(user => user.Usuarios)
                .HasForeignKey(user => user.GrupoUsuariosCodigo)
                .WillCascadeOnDelete(false);
            #endregion

            base.OnModelCreating(modelBuilder);
        }
            
        public override int SaveChanges()
        {
            this.ChangeTracker.DetectChanges();

            ObjectContext objetoContexto = ((IObjectContextAdapter)this).ObjectContext;
            IEnumerable<ObjectStateEntry> entidadesAlteradas =
                objetoContexto.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Deleted | EntityState.Modified);

            int codigo = 0;
            IAuditor auditor = new Auditor(this);

            if (Transaction.Current != null)
            {
                auditor.Auditar(entidadesAlteradas);
                codigo = base.SaveChanges();
            }
            else
            {
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    auditor.Auditar(entidadesAlteradas);
                    codigo = base.SaveChanges();
                    scope.Complete();
                }
            }
            
            return codigo;
        }

        public int ExecutarSQL(string sql)
        {
            return this.Database.ExecuteSqlCommand(sql);
        }
        
    }
}
