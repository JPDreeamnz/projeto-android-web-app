﻿
using Api.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Entities
{
    [Table("Auditoria")]
    public class AuditoriaEntity
    {
        [Key]
        public int Codigo { get; set; }
        public int UsuarioCodigo { get; set; }
        public string UsuarioEmail { get; set; }
        public string UsuarioNome { get; set; }
        public string UsuarioSobrenome { get; set; }
        public EstadoEntidade Operacao { get; set; }
        public string Tabela { get; set; }
        public string Objeto { get; set; }
        public DateTime DataOperacao { get; set; }
        public string ID { get; set; }
    }
}
