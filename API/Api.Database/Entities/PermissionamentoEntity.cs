﻿using Api.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Permissionamento")]
    public class PermissionamentoEntity
    {
        [Key, Column(Order = 0), ForeignKey("GrupoUsuarios")]
        public int GrupoUsuariosCodigo { get; set; }

        [Key, Column(Order = 1), DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int PermissionamentoCodigo { get; set; }
        public PermissionamentoController ControllerCodigo { get; set; }
        public TipoPermissionamento TipoPermissaoCodigo { get; set; }
        public DateTime DataAlteracao { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual GrupoUsuariosEntity GrupoUsuarios { get; set; }
    }
}
