﻿using Api.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Contato")]
    public class ContatoEntity
    {
        [Key]
        public int ContatoID { get; set; }
        [MaxLength(3)]
        public string DDD { get; set; }
        [MaxLength(15)]
        public string Telefone { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        public TipoContato TipoContato { get; set; }
        [ForeignKey("Cliente")]
        public int ClienteCodigo { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public ClienteEntity Cliente { get; set; }
    }
}
