﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Endereco")]
    public class EnderecoEntity
    {
        [Key]
        public int EnderecoID { get; set; }
        [MaxLength(60)]
        public string Endereco { get; set; }
        [MaxLength(10)]
        public string Numero { get; set; }
        [MaxLength(10)]
        public string Complemento { get; set; }
        [MaxLength(60)]
        public string Bairro { get; set; }
        [MaxLength(60)]
        public string Cidade { get; set; }
        [MaxLength(2)]
        public string Estado { get; set; }
        public int CEP { get; set; }
        [ForeignKey("Cliente")]
        public int ClienteCodigo { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public ClienteEntity Cliente { get; set; }
    }
}
