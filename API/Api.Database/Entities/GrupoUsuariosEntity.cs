﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("GrupoUsuarios")]
    public class GrupoUsuariosEntity
    {
        [Key]
        public int Codigo { get; set; }
        public string Nome { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<PermissionamentoEntity> Permissionamento { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<UserEntity> Usuarios { get; set; }
    }
}
