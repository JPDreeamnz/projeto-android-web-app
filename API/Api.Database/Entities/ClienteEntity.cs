﻿using Api.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Clientes")]
    public class ClienteEntity
    {
        [Key]
        public int Codigo { get; set; }

        [Required, MaxLength(60)]
        public string Nome { get; set; }
        public TipoDocumento TipoDocumento { get; set; }
        public long? Documento { get; set; }
        [MaxLength(20)]
        public string InscricaoEstadual { get; set; }
        [MaxLength(200)]
        public string Observacao { get; set; }
        public decimal? LimiteCredito { get; set; }
        [Required]
        public DateTime DataRegistro { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<EnderecoEntity> Endereco { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<ContatoEntity> Contato { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<PedidoVendaEntity> PedidoVenda { get; set; }
        
        [Required]
        public Situacao StatusCliente { get; set; }
    }
}
