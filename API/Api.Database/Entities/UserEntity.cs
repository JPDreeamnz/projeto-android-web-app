﻿using Api.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Users")]
    public class UserEntity
    {
        [Key]
        public int UserID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string LoginName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        [ForeignKey("GrupoUsuarios")]
        public int GrupoUsuariosCodigo { get; set; }

        public bool Active { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual GrupoUsuariosEntity GrupoUsuarios { get; set; }
    }
}
