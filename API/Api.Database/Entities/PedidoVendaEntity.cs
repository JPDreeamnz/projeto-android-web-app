﻿using Api.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("PedidoVenda")]
    public class PedidoVendaEntity
    {
        [Key]
        public int PedidoCodigo { get; set; }
        [ForeignKey("Cliente")]
        public int ClienteCodigo { get; set; }
        [ForeignKey("Vendedor")]
        public int VendedorCodigo { get; set; }
        [ForeignKey("PlanoPagamento")]
        public int PlanoPagamentoCodigo { get; set; }
        public DateTime DataPedido { get; set; }
        public decimal ValorProduto { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorAcrescimo { get; set; }
        public decimal Valor { get; set; }
        [MaxLength(200)]
        public string Observacao { get; set; }
        [Required]
        public DateTime DataRegistro { get; set; }
        public Situacao StatusIntegracao { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual ClienteEntity Cliente { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual VendedorEntity Vendedor { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual PlanoPagamentoEntity PlanoPagamento { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<ItensPedidoEntity> ItensPedido { get; set; }
    }
}
