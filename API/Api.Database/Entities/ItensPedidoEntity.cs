﻿
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("ItensPedido")]
    public class ItensPedidoEntity
    {
        [Key, Column(Order = 0), ForeignKey("Pedido")]
        public int PedidoCodigo { get; set; }
        [Key, Column(Order = 1), DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int ItemCodigo { get; set; }
        [ForeignKey("Produto")]
        public int ProdutoCodigo { get; set; }
        public decimal Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal ValorDesconto { get; set; }
        public decimal ValorTotal { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual PedidoVendaEntity Pedido { get; set; }
        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual ProdutoEntity Produto { get; set; }
    }
}
