﻿
using Api.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Entities
{
    [Table("Parametros")]
    public class ParametrosEntity
    {
        [Key]
        public int Codigo { get; set; }
        public Situacao? ValidaEstoque { get; set; }
        public int? ClienteDefault { get; set; }
        public int? VendedorDefault { get; set; }
        public int? PlanoPagamentoDefault { get; set; }
        public Situacao? TipoIntegracao { get; set; }
    }
}
