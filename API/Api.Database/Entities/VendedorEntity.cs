﻿
using Api.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Vendedor")]
    public class VendedorEntity
    {
        [Key]
        public int Codigo { get; set; }
        [Required, MaxLength(60)]
        public string Nome { get; set; }
        [MaxLength(200)]
        public string Observacao { get; set; }
        [Required]
        public DateTime DataRegistro { get; set; }
        [Required]
        public Situacao StatusVendedor { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<PedidoVendaEntity> PedidoVenda { get; set; }
    }
}
