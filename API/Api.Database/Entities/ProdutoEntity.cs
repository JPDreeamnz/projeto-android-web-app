﻿
using Api.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Produto")]
    public class ProdutoEntity
    {
        [Key]
        public int Codigo { get; set; }
        [MaxLength(60), Required]
        public string Nome { get; set; }
        [MaxLength(3), Required]
        public string Unidade { get; set; }
        [MaxLength(20)]
        public string CodigoBarras { get; set; }
        [MaxLength(20)]
        public string CodigoFabrica { get; set; }
        [MaxLength(20)]
        public string CodigoOriginal { get; set; }
        [MaxLength(30)]
        public string Cor { get; set; }
        [MaxLength(10)]
        public string Tamanho { get; set; }
        [MaxLength(30)]
        public string Marca { get; set; }
        [Required]
        public decimal ValorVenda { get; set; }
        public decimal? QuantidadeEstoque { get; set; }
        [Required]
        public DateTime DataRegistro { get; set; }
        [Required]
        public Situacao Status { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual List<ItensPedidoEntity> ItensPedido { get; set; }
    }
}
