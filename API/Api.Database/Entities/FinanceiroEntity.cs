﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Api.Database.Entities
{
    [Table("Financeiro")]
    public class FinanceiroEntity
    {
        [Required]
        [Key, ForeignKey("Cliente"), Column(Order = 1)]
        public int ClienteCodigo { get; set; }
        [Required]
        [Key, Column(Order = 2), MaxLength(30)]
        public string Documento { get; set; }
        [Required]
        public DateTime Emissao { get; set; }
        [Required]
        public DateTime Vencimento { get; set; }

        public DateTime? Pagamento { get; set; }
        [Required]
        public decimal Valor { get; set; }
        [Required]
        public DateTime DataRegistro { get; set; }

        [ScriptIgnore(ApplyToOverrides = false)]
        public virtual ClienteEntity Cliente { get; set; }
    }
}
