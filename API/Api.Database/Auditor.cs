﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using Api.Database.Entities;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using System.Web;
using System.Net;
using System.Web.Script.Serialization;
using System;
using Api.Enums;
using System.Linq;
using Newtonsoft.Json;
using System.Text;

namespace Api.Database
{
    public class Auditor : IAuditor
    {
        private IContext _contexto;
        private JavaScriptSerializer _serializer;

        public Auditor(IContext contexto)
        {
            this._contexto = contexto;
            this._serializer = new JavaScriptSerializer() { MaxJsonLength = Int32.MaxValue };
        }

        public void Auditar(IEnumerable<ObjectStateEntry> entidadesAlteradas)
        {
            if (entidadesAlteradas.Count() > 0)
            {
                UserEntity usuario = ObtemUsuarioLogado();

                string insert = string.Empty;
                foreach (var entidade in entidadesAlteradas)
                {
                    AuditoriaEntity auditoria = new AuditoriaEntity();
                    auditoria.DataOperacao = DateTime.Now;
                    auditoria.Objeto = JsonConvert.SerializeObject(entidade.Entity, 
                        Formatting.None, 
                        new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    //auditoria.Objeto = this._serializer.Serialize(entidade.Entity);
                    auditoria.Operacao = this.RetornaEstadoEntidade(entidade.State);
                    auditoria.Tabela = entidade.EntitySet.Name;
                    auditoria.UsuarioCodigo = usuario.UserID;
                    auditoria.UsuarioEmail = usuario.Email;
                    auditoria.UsuarioNome = usuario.Name;
                    auditoria.UsuarioSobrenome = usuario.LastName;
                    auditoria.ID = auditoria.Operacao == EstadoEntidade.INSERIDO ? null : entidade.EntityKey.EntityKeyValues[0].Value.ToString();

                    insert += this.MontaInserStatement(auditoria);
                }

                this._contexto.ExecutarSQL(insert);
            }

        }

        private UserEntity ObtemUsuarioLogado()
        {
            string token = HttpContext.Current.Session["TOKEN_AUDITOR"].ToString();
            string AuthUrl = HttpContext.Current.Session["AUTH_URL"].ToString();
            WebClient web = new WebClient();
            web.Encoding = Encoding.UTF8;
            
            return this._serializer.Deserialize<UserEntity>(web.DownloadString(AuthUrl + "/ObterUsuario?token=" + token));
        }

        private string MontaInserStatement(AuditoriaEntity auditoria)
        {
            return $"INSERT INTO dbo.Auditoria(DataOperacao, Objeto, Tabela, Operacao, UsuarioCodigo, UsuarioEmail, UsuarioNome, UsuarioSobrenome, ID) " +
                $"VALUES ('{auditoria.DataOperacao}', '{auditoria.Objeto}', '{auditoria.Tabela}', '{(int)auditoria.Operacao}', {auditoria.UsuarioCodigo}, '{auditoria.UsuarioEmail}', " +
                $"'{auditoria.UsuarioNome}', '{auditoria.UsuarioSobrenome}', '{auditoria.ID}');";
        }

        private EstadoEntidade RetornaEstadoEntidade(EntityState state)
        {
            switch (state)
            {
                case EntityState.Added:
                    return EstadoEntidade.INSERIDO;
                case EntityState.Deleted:
                    return EstadoEntidade.EXCLUIDO;
                case EntityState.Modified:
                    return EstadoEntidade.EDITADO;
                case EntityState.Detached:
                case EntityState.Unchanged:
                default:
                    return EstadoEntidade.VISUALIZADO;
            }
        }
    }
}
