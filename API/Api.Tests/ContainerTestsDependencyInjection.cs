﻿using Api.Business;
using Api.Business.Interfaces;
using Api.Database;
using Api.Repository;
using Api.Repository.Interfaces;
using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Tests
{
    public class ContainerTestsDependencyInjection
    {
        public IContainer Container { get; set; }

        public ContainerTestsDependencyInjection()
        {
            var build = new ContainerBuilder();

            build.RegisterType<UsersRepository>().As<IUsersRepository>();
            build.RegisterType<UsersBusiness>().As<IUsersBusiness>();
            build.RegisterType<LoginBusiness>().As<ILoginBusiness>();
            build.RegisterType<LoginRepository>().As<ILoginRepository>();
            build.RegisterType<Context>().As<IContext>();

            Container = build.Build();
        }
    }
}
