﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Business;
using Api.Models;
using Api.Enums;
using Autofac;
using Api.Business.Interfaces;
using System.Collections.Generic;
using Api.Database;
using System.Linq;

namespace Api.Tests
{
    [TestClass]
    public class UsersTests
    {
        private ContainerTestsDependencyInjection _conteiner;

        [TestInitialize]
        public void Init()
        {
            _conteiner = new ContainerTestsDependencyInjection();
        }

        [TestMethod]
        public void AddNewUser()
        {
            using (var scope = _conteiner.Container.BeginLifetimeScope())
            {
                IUsersBusiness users = scope.Resolve<IUsersBusiness>();

                UserModel model = new UserModel();
                model.Active = true;
                model.Email = DateTime.Now.ToBinary().ToString() + "@users.teste";
                model.LastName = "teste";
                model.Name = "usuario";
                model.Password = "senha123";
                model.GrupoUsuariosCodigo = 1;

                RetornoRequest addResult = users.Add(model);

                Assert.AreEqual(true, addResult.Retorno);
                Assert.AreNotEqual(0, model.UserID);
            }
        }

        [TestMethod]
        public void UpdateUser()
        {
            using (var scope = _conteiner.Container.BeginLifetimeScope())
            {
                IUsersBusiness users = scope.Resolve<IUsersBusiness>();
                IContext contexto = scope.Resolve<IContext>();

                UserModel model = new UserModel();
                model.UserID = 1;
                model.Active = true;
                model.Email = DateTime.Now.ToBinary().ToString() + "@users.teste";
                model.LastName = "teste";
                model.Name = "usuario " + DateTime.Now.ToShortTimeString();
                model.Password = "senha123";
                model.GrupoUsuariosCodigo = 1;

                RetornoRequest addResult = users.Update(model);

                Assert.AreEqual(true, addResult.Retorno);
            }
        }

        [TestMethod]
        public void GetAllUsersFromDatabase()
        {
            using (var scope = _conteiner.Container.BeginLifetimeScope())
            {
                IUsersBusiness users = scope.Resolve<IUsersBusiness>();

                var itens = users.GetAll();

                Assert.IsInstanceOfType(itens, typeof(List<UserModel>));
            }
        }

        [TestMethod]
        public void GetUserByID()
        {
            using (var scope = _conteiner.Container.BeginLifetimeScope())
            {
                IUsersBusiness users = scope.Resolve<IUsersBusiness>();

                UserModel userSelected = users.Get(2);


                Assert.AreEqual(2, userSelected.UserID);
            }
        }
    }
}
