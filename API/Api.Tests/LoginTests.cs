﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Business;
using Api.Models;
using Api.Enums;
using Autofac;
using Api.Business.Interfaces;

namespace Api.Tests
{
    [TestClass]
    public class LoginTests
    {
        private ContainerTestsDependencyInjection _conteiner;

        [TestInitialize]
        public void Init()
        {
            _conteiner = new ContainerTestsDependencyInjection();
        }

        [TestMethod]
        public void AddUserToDatabase()
        {
            using (var scope = _conteiner.Container.BeginLifetimeScope())
            {
                ILoginBusiness login = scope.Resolve<ILoginBusiness>();

                RegistrationModel model = new RegistrationModel();

                model.Name = "user2";
                model.Password = "senha1232";
                model.Email = model.Email = DateTime.Now.ToBinary().ToString() + "@Login.teste";

                LoginReturn loginReturn = login.Register(model);

                Assert.AreEqual(true, loginReturn.Status);
                Assert.AreEqual(LoginMessages.LOGIN_SUCCESSFUL, loginReturn.MessageId);
            }
        }

    }
}
