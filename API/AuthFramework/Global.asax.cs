﻿using AuthFramework.Models.Auth;
using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace AuthFramework
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public IContainer Container;

        private static List<AuthResult> _autorization;
        public static List<AuthResult> Autorization
        {
            get
            {
                if (_autorization == null)
                    _autorization = new List<AuthResult>();

                return _autorization;
            }
            set
            {
                if (_autorization == null)
                    _autorization = new List<AuthResult>();

                _autorization = value;
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            CreateIocContainer();
        }

        private void CreateIocContainer()
        {
            var factory = new IocContainerFactory();
            Container = factory.Create();

            // Create the depenedcy resolver and configure Web API with it
            var resolver = new AutofacWebApiDependencyResolver(Container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}
