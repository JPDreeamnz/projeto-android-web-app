﻿using Api.Business;
using Api.Business.Interfaces;
using Api.Models.Model;
using Api.Models.POCO;
using AuthFramework.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthFramework.Database
{
    internal class MemoryDataBase : IAuthDatabase
    {
        public ILoginBusiness _loginBusiness;
        public IGrupoUsuariosBusiness _grupoUsuariosBusiness;

        public MemoryDataBase(ILoginBusiness loginBusiness, IGrupoUsuariosBusiness grupoUsuariosBusiness)
        {
            this._loginBusiness = loginBusiness;
            this._grupoUsuariosBusiness = grupoUsuariosBusiness;
        }

        public void Add(AuthResult authResult)
        {
            WebApiApplication.Autorization.Add(authResult);
        }

        public AuthResult GetToken(string token)
        {
            return WebApiApplication.Autorization.SingleOrDefault(x => x.Token == token);
        }

        public bool isValid(string token)
        {
            return WebApiApplication.Autorization.Any(x => x.Token == token && x.Expires > DateTime.Now);
        }

        public void Remove(AuthResult token)
        {
            WebApiApplication.Autorization.Remove(token);
        }

        public AuthResult GenerateToken(AuthCredentials credential, int SessionTimeout)
        {
            DateTime Seed = DateTime.Now;
            Random rand = new Random(Seed.Second);

            string keyMaster = AuthFramework.Util.Security.GenerateToken(credential.Username);
            string keySlave = AuthFramework.Util.Security.GenerateToken(credential.Password);

            string myToken = AuthFramework.Util.Security.GenerateToken(keyMaster + keySlave + Seed.ToString());

            var auth = new AuthResult
            {
                Token = myToken,
                Seed = Seed,
                KeyMaster = keyMaster,
                KeySlave = keySlave,
                Status = "Sucesso",
                Pin = rand.Next(1000, 9999),
                Pin2 = rand.Next(10000, 99999),
                Expires = DateTime.Now.AddMinutes(SessionTimeout),
                Usuario = this._loginBusiness.ObtemUsuario(new Api.Models.LoginModel { Name = credential.Username, Password = credential.Password })
            };

            return auth;
        }

        public bool CredentialIsValid(AuthCredentials credentials)
        {
            return (_loginBusiness.Login(
                    new Api.Models.LoginModel { Name = credentials.Username, Password = credentials.Password }) ??
                    new Api.Models.LoginReturn())
                    .Status;
        }

        public List<AuthResult> GetAll()
        {
            return WebApiApplication.Autorization;
        }

        public bool AutorizarAcesso(AutorizacaoAcesso model)
        {
            return this._grupoUsuariosBusiness.AutorizarAcesso(model);
        }

        public List<PermissionamentoModel> ObterPermissionamento(int GrupoUsuariosCodigo)
        {
            return this._grupoUsuariosBusiness.ObterPermissoes(GrupoUsuariosCodigo);
        }
    }
}