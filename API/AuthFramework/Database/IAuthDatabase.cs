﻿using System.Collections.Generic;
using AuthFramework.Models.Auth;
using Api.Models.POCO;
using Api.Models.Model;

namespace AuthFramework.Database
{
    public interface IAuthDatabase
    {
        AuthResult GenerateToken(AuthCredentials credential, int SessionTimeout);
        AuthResult GetToken(string token);
        void Add(AuthResult authResult);
        void Remove(AuthResult token);
        bool isValid(string token);
        bool CredentialIsValid(AuthCredentials credentials);
        List<AuthResult> GetAll();
        bool AutorizarAcesso(AutorizacaoAcesso model);
        List<PermissionamentoModel> ObterPermissionamento(int GrupoUsuariosCodigo);
    }
}