﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthFramework.Models.Auth
{
    /// <summary>
    /// Objeto contendo informações paga autenticação no sistema.
    /// </summary>
    public class AuthCredentials
    {
        /// <summary>
        /// Nome do usuário, email, login, ad etc.
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Senha do usuário.
        /// </summary>
        public string Password { get; set; }
    }
}