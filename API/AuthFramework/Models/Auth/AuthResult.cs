﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthFramework.Models.Auth
{
    /// <summary>
    /// Objeto com informações de loging
    /// </summary>
    public class AuthResult
    {
        /// <summary>
        /// token usado para requisições rest
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// valor usado para incrementar a complexidade do token
        /// </summary>
        public DateTime Seed { get; set; }
        /// <summary>
        /// primeira porção de texto usada na geração do token
        /// </summary>
        public string KeyMaster { get; set; }
        /// <summary>
        /// segunda porção de texto usada na geração do token
        /// </summary>
        public string KeySlave { get; set; }
        /// <summary>
        /// código de segurança para acesso de emergência
        /// </summary>
        public int Pin { get; set; }
        /// <summary>
        /// código secundario de segurança para acesso de emergência
        /// </summary>
        public int Pin2 { get; set; }
        /// <summary>
        /// Informações do usuário logado
        /// </summary>
        public UserModel Usuario { get; set; }
        /// <summary>
        /// situação da operação de autenticação
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// data de expiração
        /// </summary>
        public DateTime Expires { get; set; }

        /// <summary>
        /// mesmo valor de "Expires" porém formatado para ser mais facilmente manipulado no javascript.
        /// </summary>
        public string ExpiresTimeForJs
        {
            get
            {
                return Expires.ToString("R");
            }
            private set { }
        }

        public string ExpiresTimeForAndroid
        {
            get
            {
                return Expires.ToString("dd-MM-yyyy HH:mm:ss");
            }

            private set { }
        }

        /// <summary>
        /// campo calculado com o tempo restante para inativação do token atual.
        /// </summary>
        public TimeSpan Remaning { get { return Expires.Subtract(DateTime.Now); } private set { } }
    }
}