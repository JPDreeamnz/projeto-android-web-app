﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac.Integration.WebApi;
using Api.Business;
using Api.Database;
using Api.Repository;
using AuthFramework.Database;
using Api.Business.Interfaces;
using Api.Repository.Interfaces;
using Api.Repository.Repositorio;

namespace AuthFramework
{
    public class IocContainerFactory
    {
        public IContainer Create()
        {
            var builder = new ContainerBuilder();

            // Register WebApi controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            ConfigureIocForRepositories(builder);

            // Build and return the Container
            var container = builder.Build();
            return container;
        }

        private void ConfigureIocForRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<LoginBusiness>().As<ILoginBusiness>();
            builder.RegisterType<LoginRepository>().As<ILoginRepository>();

            builder.RegisterType<GrupoUsuariosBusiness>().As<IGrupoUsuariosBusiness>();
            builder.RegisterType<GrupoUsuariosRepository>().As<IGrupoUsuariosRepository>();

            builder.RegisterType<Context>().As<IContext>();
            builder.RegisterType<MemoryDataBase>().As<IAuthDatabase>();
        }
    }
}