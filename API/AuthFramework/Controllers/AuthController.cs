﻿using Api.Business;
using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using AuthFramework.Attributes;
using AuthFramework.Database;
using AuthFramework.Models.Auth;
using AuthFramework.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using System.Web.Http.Cors;

namespace AuthFramework.Repositorio.Controllers
{
    /// <summary>
    /// Api responsável por gerar e manter token de comunicação etre apis e aplicações.
    /// </summary>
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    [RequireHttps]
    public class AuthController : ApiController
    {
        IAuthDatabase _database;
        internal int SESSION_TIMEOUT = AppSettingManager.TryGetValue("SESSION_TIMEOUT", 30);

        /// <summary>
        /// contrutor da class
        /// </summary>
        /// <param name="database">autofac</param>
        public AuthController(IAuthDatabase database)
        {
            this._database = database;
        }

        /// <summary>
        /// Autenticar usuário.
        /// </summary>
        /// <param name="Credentials">Credenciais de usuario para obter token de autorização.</param>
        /// <returns>Token, status, duração do token etc..</returns>
        [Route("Api/Auth/Post")]
        public AuthResult Post(AuthCredentials Credentials)
        {
            if (!this.CredentialIsValid(Credentials).Retorno)
            {
                return new AuthResult
                {
                    Status = "Usuário Inválido",
                    Token = string.Empty,
                    Expires = DateTime.MinValue
                };
            }

            this.FecharUsuarioSessao(Credentials);
            var auth = _database.GenerateToken(Credentials, SESSION_TIMEOUT);
            var Token = _database.GetToken(auth.Token);

            if (Token == null) // not exist yet
            {
                _database.Add(auth);
                return auth;
            }
            
            _database.Remove(Token);
            _database.Add(auth);

            return auth;
        }

        /// <summary>
        /// Testar se o token é válido.
        /// </summary>
        /// <param name="Token">token recebibo ao autenticar no sistema.</param>
        /// <returns>situação do token informado.</returns>
        [Route("Api/Auth/Get")]
        public bool Get(string Token)
        {
            return _database.isValid(Token);
        }

        [HttpGet]
        [Route("Api/Auth/ObterUsuario")]
        public UserModel ObterUsuario(string token)
        {
            return this._database.GetToken(token).Usuario;
        }

        //TODO: Sumarizar
        [Route("Api/Auth/CredentialIsValid")]
        public RetornoRequest CredentialIsValid(AuthCredentials model)
        {
            var retorno = new RetornoRequest(true, "USUARIO_NAO_LOGADO");
            if(!_database.CredentialIsValid(model))
            {
                retorno.Retorno = false;
                retorno.Mensagem = "Credenciais não autorizadas.";
            }
            else
            {
                if (this.SessaoUsuario(model))
                {
                    retorno.Mensagem = "USUARIO_LOGADO";
                }
            }
            
            return retorno;

        }

        //TODO: Sumarizar
        [Route("Api/Auth/SessaoUsuario")]
        public bool SessaoUsuario(AuthCredentials model)
        {
            return _database.GetAll().Any(cre => (cre.Usuario.Name.ToUpper().Equals(model.Username.ToUpper()) || cre.Usuario.Email.ToUpper().Equals(model.Username.ToUpper())));
        }

        //TODO: Sumarizar
        public void FecharUsuarioSessao(AuthCredentials model)
        {
            this.GetUsers()
                .Where(cre => (cre.Usuario.Name.Equals(model.Username) || cre.Usuario.Email.Equals(model.Username)))
                .ToList()
                .ForEach(usuario => _database.Remove(usuario));
        }

        /// <summary>
        /// usárido para atualizar data de expiração do token.
        /// </summary>
        /// <param name="Token">token atual que esteja válido.</param>
        /// <returns>retorna se a atualização foi efetuada com sucesso ou não.</returns>
        [Route("Api/Auth/Put")]
        public bool Put(string Token)
        {
            var item = _database.GetToken(Token);

            if (item != null)
            {
                item.Expires = DateTime.Now.AddMinutes(SESSION_TIMEOUT);

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Remove o token gerado da base de dados.
        /// Fazendo assim que todas as requisições usando o token informado tenham o acesso negado.
        /// </summary>
        /// <param name="Token">token que será invalidado.</param>
        /// <returns>situação da operação.</returns>
        [Route("Api/Auth/Delete")]
        [HttpGet]
        public bool Delete(string Token)
        {
            var auth = _database.GetToken(Token);

            if (auth != null)
            {
                _database.Remove(auth);

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Obter todos os usuário autenticados no sistema.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Api/Auth/GetUsers")]
        public List<AuthResult> GetUsers()
        {
            return _database.GetAll();
        }

        [HttpPost]
        [Route("Api/Auth/AutorizarAcesso")]
        public bool AutorizarAcesso(AutorizacaoAcesso model)
        {
            return this._database.AutorizarAcesso(model);
        }

        [HttpGet]
        [Route("Api/Auth/ObterPermissoes")]
        public List<PermissionamentoModel> ObterPermissoes(int GrupoUsuariosCodigo)
        {
            return this._database.ObterPermissionamento(GrupoUsuariosCodigo);
        }
    }
}
