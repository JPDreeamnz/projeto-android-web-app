﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuthFramework.Util
{
    internal static class AppSettingManager
    {
        internal static int TryGetValue(string Key, int Default)
        {
            string value = System.Configuration.ConfigurationManager.AppSettings[Key];

            if (!string.IsNullOrWhiteSpace(value))
            {
                int number = Default;
                int.TryParse(value, out number);

                return number;
            }
            else
                return Default;
        }
    }
}