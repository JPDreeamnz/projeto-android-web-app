﻿using Api.Business.Interfaces;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class AuditoriaController : ApiController
    {
        private IAuditoriaBusiness _auditoriaBusiness;

        public AuditoriaController(IAuditoriaBusiness auditoriaBusiness)
        {
            this._auditoriaBusiness = auditoriaBusiness;
        }
        
        [Route("api/Auditoria/ObterAuditoria")]
        [HttpGet]
        public AuditoriaModel ObterAuditoria(int codigo)
        {
            return this._auditoriaBusiness.ObterAuditoria(codigo);
        }
        
        [Route("api/Auditoria/ObterLista")]
        [HttpGet]
        public List<AuditoriaModel> ObterLista()
        {
            return this._auditoriaBusiness.ObterLista();
        }

        [Route("api/Auditoria/ObterDataTable")]
        [HttpPost]
        public TableAuditoria ObterDataTable(JQueryDataTableParam param)
        {
            return new TableAuditoria()
            {
                QuantidadeRegistros = _auditoriaBusiness.QuantidadeTotalRegistros(),
                Auditorias = _auditoriaBusiness.ObterLista(param)
            };
        }
    }
}