﻿using Api.Business.Interfaces;
using Api.Models;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System.Collections.Generic;
using System.Web.Http;

namespace Api.Controllers
{
    public class ProdutoController : ApiController
    {
        IProdutoBusiness _produtoBusiness;

        public ProdutoController(IProdutoBusiness produtoBusiness)
        {
            this._produtoBusiness = produtoBusiness;
        }
        
        [Route("api/Produto/ListarProdutos")]
        [HttpGet]
        public List<ProdutoModel> ListarProdutos()
        {
            return this._produtoBusiness.ListarProdutos();
        }


        [Route("api/Produto/ObterProduto")]
        [HttpGet]
        public ProdutoModel ObterProduto(int codigo)
        {
            return this._produtoBusiness.ObterProduto(codigo);
        }

        [Route("api/Produto/Incluir")]
        [HttpPost]
        public RetornoRequest Incluir(ProdutoModel model)
        {
            return this._produtoBusiness.Incluir(model);
        }

        [Route("api/Produto/Editar")]
        [HttpPost]
        public RetornoRequest Editar(ProdutoModel model)
        {
            return this._produtoBusiness.Editar(model);
        }

        [Route("api/Produto/Excluir")]
        [HttpGet]
        public bool Excluir(int codigo)
        {
            return this._produtoBusiness.Excluir(codigo);
        }

        [Route("api/Produto/ObterDataTable")]
        public TableProduto ObterDataTable(JQueryDataTableParam param)
        {
            return new TableProduto()
            {
                QuantidadeRegistros = _produtoBusiness.QuantidadeTotalRegistros(),
                Produtos = _produtoBusiness.ListarProdutos(param)
            };
        }
    }
}