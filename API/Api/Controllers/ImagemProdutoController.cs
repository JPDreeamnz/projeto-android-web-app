﻿using Api.Business.Interfaces;
using Api.Enums;
using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Script.Serialization;

namespace Api.Controllers
{
    public class ImagemProdutoController : ApiController
    {
        private IImagemProdutoBusiness _imagemProdutoBusiness;

        public ImagemProdutoController(IImagemProdutoBusiness imagemProdutoBusiness)
        {
            this._imagemProdutoBusiness = imagemProdutoBusiness;
        }

        [Route("api/ImagemProduto/SalvarImagem")]
        [HttpPost]
        public ImagemProdutoModel SalvarImagem(int codigoProduto, string token)
        {
            try
            {
                var trava = new object();
                ImagemProdutoModel objeto = new ImagemProdutoModel();
                var arquivos = HttpContext.Current.Request.Files;

                foreach (string nomeArquivo in arquivos)
                {
                    var arquivo = arquivos[nomeArquivo];

                    MemoryStream ms = new MemoryStream();
                    arquivo.InputStream.CopyTo(ms);

                    lock (trava)
                    {
                        objeto.CodigoProduto = codigoProduto;
                        objeto.CaminhoPasta = (codigoProduto != 0 ? codigoProduto.ToString() : token);
                        objeto.Sequencial = this._imagemProdutoBusiness.ObtemProximoSequencial(objeto.CaminhoPasta);
                        objeto.Extensao = arquivo.FileName.Split('.')[1];
                        objeto.Situacao = EstadoEntidade.INSERIDO;

                        this._imagemProdutoBusiness.SalvarImagem(objeto, ms.ToArray());
                    }
                }

                return objeto;
            }
            catch
            {
                return null;
            }
        }

        [Route("api/ImagemProduto/ExcluirImagem")]
        [HttpGet]
        public RetornoRequest ExcluirImagem(ImagemProdutoModel imagemProduto, string token)
        {
            return this._imagemProdutoBusiness.ExcluirImagem(imagemProduto);
        }

        [Route("api/ImagemProduto/ObterListaImagens")]
        [HttpGet]
        public List<ImagemProdutoModel> ObterListaImagens(int codigoProduto)
        {
            return this._imagemProdutoBusiness.ObterListaImagens(codigoProduto).ToList();
        }

        [Route("api/ImagemProduto/ListaImagens")]
        [HttpPost]
        public List<RetornoImagem> ListaImagens([FromBody]RequestImagens request)
        {
            var imagens = new JavaScriptSerializer().Deserialize<List<ImagemProdutoModel>>(request.imagens);
            return this._imagemProdutoBusiness.ListaImagens(imagens).ToList();
        }
    }
}