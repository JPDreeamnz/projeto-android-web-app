﻿using Api.Business.Interfaces;
using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    /// <summary>
    /// Responsavel pela manutenção dos usuários do sistema.
    /// </summary>
    public class UsersController : ApiController
    {
        IUsersBusiness _usersBusiness;

        public UsersController(IUsersBusiness usersBusiness)
        {
            _usersBusiness = usersBusiness;
        }

        /// <summary>
        /// Obter todos so usuario registrados no sistema.
        /// </summary>
        /// <returns>lista com todos os usuários</returns>
        [Route("api/Users/v1/GetAll")]
        [HttpGet]
        public List<UserModel> GetAll()
        {
            return _usersBusiness.GetAll();
        }

        /// <summary>
        /// Obter um único usuário pelo ID
        /// </summary>
        /// <param name="id">ID do usuário desejado</param>
        /// <returns>Usuário selecionado ou NULL</returns>
        [Route("api/Users/v1/Get")]
        [HttpGet]
        public UserModel Get(int UserID)
        {
            return _usersBusiness.Get(UserID);
        }

        /// <summary>
        /// Adicionar Usuário ao sistema.
        /// </summary>
        /// <param name="model">Informações necessário para o cadastro</param>
        /// <returns>situaçõa da operação</returns>
        [Route("api/Users/v1/Post")]
        [HttpPost]
        public RetornoRequest Post(UserModel model)
        {
            return _usersBusiness.Add(model);
        }


        /// <summary>
        /// Atualiza as informações do usuário
        /// </summary>
        /// <param name="model">Novas informações para o cadastro.</param>
        /// <returns>situação da operação</returns>
        [Route("api/Users/v1/Put")]
        [HttpPost]
        public RetornoRequest Put(UserModel model)
        {
            return _usersBusiness.Update(model);
        }

        /// <summary>
        /// Remove usuário da base de dados.
        /// </summary>
        /// <param name="id">id do usuário a ser removido.</param>
        /// <returns>situação da operação</returns>
        [Route("api/Users/v1/Delete")]
        [HttpGet]
        public bool Delete(int UserID)
        {
            return _usersBusiness.Delete(UserID);
        }

    }
}
