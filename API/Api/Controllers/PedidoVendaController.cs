﻿using Api.Business.Interfaces;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System.Collections.Generic;
using System.Web.Http;

namespace Api.Controllers
{
    public class PedidoVendaController : ApiController
    {
        private IPedidoVendaBusiness _pedidoVendaBusiness;

        public PedidoVendaController(IPedidoVendaBusiness pedidoVendaBusiness)
        {
            this._pedidoVendaBusiness = pedidoVendaBusiness;
        }

        [Route("api/PedidoVenda/ObterPedido")]
        [HttpGet]
        public PedidoVendaModel ObterPedidoVenda(int codigo)
        {
            return this._pedidoVendaBusiness.ObterPedido(codigo);
        }

        [Route("api/PedidoVenda/ObterLista")]
        [HttpGet]
        public List<PedidoVendaModel> ObterLista()
        {
            return this._pedidoVendaBusiness.ObterLista();
        }

        [Route("api/PedidoVenda/ObterDataTable")]
        [HttpPost]
        public TablePedidoVenda ObterDataTable(JQueryDataTableParam param)
        {
            return new TablePedidoVenda()
            {
                QuantidadeRegistros = _pedidoVendaBusiness.QuantidadeTotalRegistros(),
                Pedidos = _pedidoVendaBusiness.ObterLista(param)
            };
        }
    }
}