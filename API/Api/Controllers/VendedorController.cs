﻿using System.Web.Http;
using Api.Business.Interfaces;
using System.Collections.Generic;
using Api.Models.Model;
using Api.Models;

namespace Api.Controllers
{
    public class VendedorController : ApiController
    {
        private IVendedorBusiness _vendedorBusiness;
        public VendedorController(IVendedorBusiness vendedorBusiness)
        {
            this._vendedorBusiness = vendedorBusiness;
        }

        //TODO: Sumarizar
        [Route("api/Vendedor/ObterLista")]
        [HttpGet]
        public List<VendedorModel> ObterLista()
        {
            return this._vendedorBusiness.ListarVendedores();
        }

        //TODO: Sumarizar
        [Route("api/Vendedor/ObterVendedor")]
        [HttpGet]
        public VendedorModel ObterVendedor(int codigo)
        {
            return this._vendedorBusiness.ObterVendedor(codigo);
        }

        //TODO: Sumarizar
        [Route("api/Vendedor/Incluir")]
        [HttpPost]
        public RetornoRequest Incluir(VendedorModel objeto)
        {
            return _vendedorBusiness.Incluir(objeto);
        }

        //TODO: Sumarizar
        [Route("api/Vendedor/Editar")]
        [HttpPost]
        public RetornoRequest Editar(VendedorModel objeto)
        {
            return _vendedorBusiness.Editar(objeto);
        }

        //TODO: Sumarizar
        [Route("api/Vendedor/Excluir")]
        [HttpGet]
        public bool Excluir(int codigo)
        {
            return _vendedorBusiness.Excluir(codigo);
        }

    }
}