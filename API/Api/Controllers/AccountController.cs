﻿using Api.Business;
using Api.Business.Interfaces;
using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    /// <summary>
    /// API responsavel pela manutenção dos usuário do sistema.
    /// </summary>
    public class AccountController : ApiController
    {
        private ILoginBusiness _loginBusiness;

        /// <summary>
        /// Inicializa business para a controller.
        /// </summary>
        /// <param name="loginBusiness"></param>
        public AccountController(ILoginBusiness loginBusiness)
        {
            this._loginBusiness = loginBusiness;
        }

        /// <summary>
        /// Adicionar usuario ao sistema para autenticação
        /// </summary>
        /// <param name="model">informações para registro do usuario</param>
        /// <returns>Status da operação</returns>
        [HttpPost]
        [Route("api/Account/v1/Registration")]
        public IHttpActionResult Registration(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    LoginReturn loginReturn = _loginBusiness.Register(model);

                    return Content(
                        loginReturn.Status ? System.Net.HttpStatusCode.OK : System.Net.HttpStatusCode.NotAcceptable,
                        loginReturn.MessageText);
                }
                catch (Exception ex)
                {
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
                }
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
