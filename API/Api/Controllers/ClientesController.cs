﻿using Api.Business.Interfaces;
using Api.Models;
using Api.Models.DataTables;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{

    /// <summary>
    /// Responsável pelo controle de usuários do sistema.
    /// </summary>
    public class ClientesController : ApiController
    {
        IClienteBusiness _clienteBusiness;

        public ClientesController(IClienteBusiness clienteBusiness)
        {
            this._clienteBusiness = clienteBusiness;
        }

        /// <summary>
        /// Obter listagem dos clientes.
        /// </summary>
        /// <returns>Lista com todos os clientes cadastrados</returns>
        [Route("api/Clientes/ObterLista")]
        [HttpGet]
        public List<ClienteModel> ObterLista()
        {
            return _clienteBusiness.ListarClientes();
        }
        
        /// <summary>
        /// Obtem um cliente a partir do código.
        /// </summary>
        /// <param name="codigo">Valor inteiro que representa o código do cliente</param>
        /// <returns>Um objeto contendo todas as informações do cliente</returns>
        [Route("api/Clientes/ObterCliente")]
        [HttpGet]
        public ClienteModel ObterCliente(int codigo)
        {
            return _clienteBusiness.ObterCliente(codigo);
        }

        /// <summary>
        /// Insere um novo cliente na base de dados.
        /// </summary>
        /// <param name="novoCliente">Objeto do tipo ClienteModel</param>
        /// <returns>Objeto do tipo RetornoRequest contendo o status e a mensagem de retorno</returns>
        [Route("api/Clientes/Incluir")]
        [HttpPost]
        public RetornoRequest Incluir(ClienteModel novoCliente)
        {
            return _clienteBusiness.Incluir(novoCliente);
        }

        /// <summary>
        /// Edita o cliente informado.
        /// </summary>
        /// <param name="cliente">Objeto do tipo ClienteModel</param>
        /// <returns>Objeto do tipo RetornoRequest contendo o status e a mensagem de retorno</returns>
        [Route("api/Clientes/Editar")]
        [HttpPost]
        public RetornoRequest Editar(ClienteModel cliente)
        {
            return _clienteBusiness.Editar(cliente);
        }

        /// <summary>
        /// Deleta o cliente de acordo com o código informado.
        /// </summary>
        /// <param name="codigo">Código do cliente na base de dados.</param>
        /// <returns>Retorna true para a exclusão com sucesso</returns>
        [Route("api/Clientes/Excluir")]
        [HttpGet]
        public bool Deletar(int codigo)
        {
            return _clienteBusiness.Excluir(codigo);
        }
        
        [Route("api/Clientes/ObterDataTable")]
        [HttpPost]
        public TableClientesRetorno ObterDataTable(JQueryDataTableParam param)
        {
            return new TableClientesRetorno()
            {
                QuantidadeRegistros = _clienteBusiness.QuantidadeTotalRegistros(),
                Clientes = _clienteBusiness.ListarClientes(param)
            };
        }
    }
}