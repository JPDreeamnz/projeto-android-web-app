﻿
using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using System.Collections.Generic;
using System.Web.Http;

namespace Api.Controllers
{
    public class PlanoPagamentoController : ApiController
    {
        private IPlanoPagamentoBusiness _planoPagamentoBusiness;
        public PlanoPagamentoController(IPlanoPagamentoBusiness planoPagamentoBusiness)
        {
            this._planoPagamentoBusiness = planoPagamentoBusiness;
        }

        //TODO: Sumarizar
        [Route("api/PlanoPagamento/ObterLista")]
        [HttpGet]
        public List<PlanoPagamentoModel> ObterLista()
        {
            return this._planoPagamentoBusiness.ListarPlanos();
        }

        //TODO: Sumarizar
        [Route("api/PlanoPagamento/ObterPlano")]
        [HttpGet]
        public PlanoPagamentoModel ObterPlano(int codigo)
        {
            return this._planoPagamentoBusiness.ObterPlano(codigo);
        }

        //TODO: Sumarizar
        [Route("api/PlanoPagamento/Incluir")]
        [HttpPost]
        public RetornoRequest Incluir(PlanoPagamentoModel objeto)
        {
            return _planoPagamentoBusiness.Incluir(objeto);
        }

        //TODO: Sumarizar
        [Route("api/PlanoPagamento/Editar")]
        [HttpPost]
        public RetornoRequest Editar(PlanoPagamentoModel objeto)
        {
            return _planoPagamentoBusiness.Editar(objeto);
        }

        //TODO: Sumarizar
        [Route("api/PlanoPagamento/Excluir")]
        [HttpGet]
        public bool Excluir(int codigo)
        {
            return _planoPagamentoBusiness.Excluir(codigo);
        }
    }
}