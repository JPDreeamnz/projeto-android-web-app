﻿using Api.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.Controllers
{
    /// <summary>
    /// comando usados no desenvolvimento
    /// </summary>
    public class ServerInfoController : ApiController
    {
        [HttpGet]
        [Route("api/ServerInfo/v1/ping")]
        public string Ping()
        {
            return "Pong";
        }

        [HttpGet]
        [Route("api/ServerInfo/v1/DropDatabase")]
        public string DropDatabase()
        {
            Server server = new Server();

            server.Drop();

            return "Concluido.";
        }

        [HttpGet]
        [Route("api/ServerInfo/v1/CreateDatabase")]
        public string CreateDatabase()
        {
            Server server = new Server();

            server.Create();

            return "Concluido.";
        }
    }
}
