﻿using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class FinanceiroController : ApiController
    {
        IFinanceiroBusiness _financeiroBusiness;

        public FinanceiroController(IFinanceiroBusiness financeiroBusiness)
        {
            this._financeiroBusiness = financeiroBusiness;
        }

        
        [Route("api/Financeiro/ListarCadastrosFinanceiros")]
        [HttpGet]
        public List<FinanceiroModel> ListarCadastrosFinanceiros()
        {
            return this._financeiroBusiness.ListarCadastrosFinanceiros();
        }

        
        [Route("api/Financeiro/ObterCadastroFinanceiro")]
        [HttpGet]
        public FinanceiroModel ObterCadastroFinanceiro(int clienteCodigo, string documento)
        {
            return this._financeiroBusiness.ObterCadastroFinanceiro(clienteCodigo, documento);
        }

        [Route("api/Financeiro/Incluir")]
        [HttpPost]
        public RetornoRequest Incluir(FinanceiroModel model)
        {
            return this._financeiroBusiness.Incluir(model);
        }

        [Route("api/Financeiro/Editar")]
        [HttpPost]
        public RetornoRequest Editar(FinanceiroModel model)
        {
            return this._financeiroBusiness.Editar(model);
        }

        [Route("api/Financeiro/Excluir")]
        [HttpGet]
        public bool Excluir(int clienteCodigo, string documento)
        {
            return this._financeiroBusiness.Excluir(clienteCodigo, documento);
        }
    }
}