﻿
using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using System.Web.Http;

namespace Api.Controllers
{
    public class ParametrosController : ApiController
    {
        private IParametrosBusiness _parametrosBusiness;

        public ParametrosController(IParametrosBusiness parametrosBusiness)
        {
            this._parametrosBusiness = parametrosBusiness;
        }
        
        //TODO: Sumarizar
        [Route("api/Parametros/Editar")]
        [HttpPost]
        public RetornoRequest Editar(ParametrosModel objeto)
        {
            return this._parametrosBusiness.Editar(objeto);
        }

        [Route("api/Parametros/ObterParametros")]
        [HttpGet]
        public ParametrosModel ObterParametros()
        {
            return this._parametrosBusiness.ObterParametros();
        }

    }
}