﻿using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using Api.Models.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Api.Controllers
{
    [EnableCors(origins: "*", methods: "*", headers: "*")]
    [RoutePrefix("api/Integracao")]
    public class IntegracaoController : ApiController
    {
        private IClienteBusiness _clienteBusiness;
        private IFinanceiroBusiness _financeiroBusiness;
        private IPedidoVendaBusiness _pedidosBusiness;
        private IPlanoPagamentoBusiness _planoPagamentoBusiness;
        private IProdutoBusiness _produtoBusiness;
        private IVendedorBusiness _vendedorBusiness;
        private IImagemProdutoBusiness _imagemProdutoBusiness;
        private IParametrosBusiness _parametrosBusiness;
        private IAuditoriaBusiness _auditoria;

        public IntegracaoController(
            IClienteBusiness clienteBusiness,
            IFinanceiroBusiness financeiroBusiness,
            IPedidoVendaBusiness pedidosBusiness,
            IPlanoPagamentoBusiness planoPagamentoBusiness,
            IProdutoBusiness produtoBusiness,
            IVendedorBusiness vendedorBusiness,
            IImagemProdutoBusiness imagemProdutoBusiness,
            IParametrosBusiness parametrosBusiness,
            IAuditoriaBusiness auditoria)
        {
            this._clienteBusiness = clienteBusiness;
            this._financeiroBusiness = financeiroBusiness;
            this._pedidosBusiness = pedidosBusiness;
            this._planoPagamentoBusiness = planoPagamentoBusiness;
            this._produtoBusiness = produtoBusiness;
            this._vendedorBusiness = vendedorBusiness;
            this._imagemProdutoBusiness = imagemProdutoBusiness;
            this._parametrosBusiness = parametrosBusiness;
            this._auditoria = auditoria;
        }

        #region Clientes
        [Route("ObterClientes")]
        [HttpGet]
        public List<ClienteModel> ObterClientes(DateTime dataRegistro)
        {
            try
            {
                return _clienteBusiness.ListarClientes(dataRegistro);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [Route("BulkInsertClientes")]
        [HttpPost]
        public RetornoRequest BulkInsertClientes(List<ClienteModel> clientes)
        {
            try
            {
                return _clienteBusiness.Incluir(clientes);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkUpdateClientes")]
        [HttpPost]
        public RetornoRequestNMensagens BulkUpdateClientes(List<ClienteModel> clientes)
        {
            try
            {
                return _clienteBusiness.Editar(clientes);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkDeleteClientes")]
        [HttpPost]
        public RetornoRequestNMensagens BulkDeleteClientes(List<int> codigos)
        {
            try
            {
                return _clienteBusiness.Excluir(codigos);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Financeiro
        [Route("ObterFinanceiro")]
        [HttpGet]
        public List<FinanceiroModel> ObterFinanceiro(DateTime dataRegistro)
        {
            try
            {
                return _financeiroBusiness.ListarCadastrosFinanceiros(dataRegistro);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkInsertFinanceiro")]
        [HttpPost]
        public RetornoRequest BulkInsertFinanceiro(List<FinanceiroModel> financeiro)
        {
            try
            {
                return _financeiroBusiness.Incluir(financeiro);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkUpdateFinanceiro")]
        [HttpPost]
        public RetornoRequestNMensagens BulkUpdateFinanceiro(List<FinanceiroModel> financeiro)
        {
            try
            {
                return _financeiroBusiness.Editar(financeiro);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkDeleteFinanceiro")]
        [HttpPost]
        public RetornoRequestNMensagens BulkDeleteFinanceiro(List<RequestFinanceiro> financeiro)
        {
            try
            {
                return _financeiroBusiness.Excluir(financeiro);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Pedido
        [Route("ObterPedidos")]
        [HttpGet]
        public List<PedidoVendaModel> ObterPedidos(DateTime dataRegistros)
        {
            try
            {
                return this._pedidosBusiness.ObterLista(dataRegistros);
            }
            catch
            {
                return null;
            }
        }

        [Route("InserirPedido")]
        [HttpPost]
        public RetornoRequest InserirPedido(PedidoVendaModel pedido)
        {
            try
            {
                return _pedidosBusiness.Inserir(pedido);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkInsertPedidos")]
        [HttpPost]
        public RetornoRequest BulkInsertPedidos(List<PedidoVendaModel> pedidos)
        {
            try
            {
                return _pedidosBusiness.Inserir(pedidos);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Plano Pagamento
        [Route("ObterPlanoPagamento")]
        [HttpGet]
        public List<PlanoPagamentoModel> ObterPlanoPagamento(DateTime dataRegistros)
        {
            try
            {
                return _planoPagamentoBusiness.ListarPlanos(dataRegistros);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkInsertPlanoPagamento")]
        [HttpPost]
        public RetornoRequest BulkInsertPlanoPagamento(List<PlanoPagamentoModel> planos)
        {
            try
            {
                return _planoPagamentoBusiness.Incluir(planos);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkUpdatePlanoPagamento")]
        [HttpPost]
        public RetornoRequestNMensagens BulkUpdatePlanoPagamento(List<PlanoPagamentoModel> planos)
        {
            try
            {
                return _planoPagamentoBusiness.Editar(planos);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkDeletePlanoPagamento")]
        [HttpPost]
        public RetornoRequestNMensagens BulkDeletePlanoPagamento(List<int> codigos)
        {
            try
            {
                return _planoPagamentoBusiness.Excluir(codigos);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Imagem Produto
        [Route("ObterImagem")]
        [HttpGet]
        public byte[] ObterImagem(string caminhoPasta)
        {
            //Envia o caminho da pasta removendo caso a pasta venha na requisição junto
            return this._imagemProdutoBusiness.ObterImagem(caminhoPasta.Replace(ConfigurationManager.AppSettings["ImagensPastaSemUnidadeFisica"], ""));
        }
        #endregion

        #region Produtos
        [Route("ObterProdutos")]
        [HttpGet]
        public List<ProdutoModel> ObterProdutos(DateTime dataRegistros)
        {
            try
            {
                return _produtoBusiness.ListarProdutos(dataRegistros);
            }
            catch
            {
                return null;
            }
        }

        [Route("ObterProdutosComPreco")]
        [HttpGet]
        public List<ProdutoModel> ObterProdutosComPreco(DateTime dataRegistros)
        {
            try
            {
                return _produtoBusiness.ListarProdutos(dataRegistros).Where(x => x.ValorVenda > 0).ToList();
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkInsertProduto")]
        [HttpPost]
        public RetornoRequest BulkInsertProduto(List<ProdutoModel> produtos)
        {
            try
            {
                return _produtoBusiness.Incluir(produtos);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkUpdateProduto")]
        [HttpPost]
        public RetornoRequestNMensagens BulkUpdateProduto(List<ProdutoModel> produtos)
        {
            try
            {
                return _produtoBusiness.Editar(produtos);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkDeleteProduto")]
        [HttpPost]
        public RetornoRequestNMensagens BulkDeleteProduto(List<int> codigos)
        {
            try
            {
                return _produtoBusiness.Excluir(codigos);
            }
            catch
            {
                return null;
            }
        }
        #endregion
        
        #region Vendedor
        [Route("ObterVendedor")]
        [HttpGet]
        public List<VendedorModel> ObterVendedores(DateTime dataRegistros)
        {
            try
            {
                return _vendedorBusiness.ListarVendedores(dataRegistros);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkInsertVendedor")]
        [HttpPost]
        public RetornoRequest BulkInsertVendedor(List<VendedorModel> vendedores)
        {
            try
            {
                return _vendedorBusiness.Incluir(vendedores);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkUpdateVendedor")]
        [HttpPost]
        public RetornoRequestNMensagens BulkUpdateVendedor(List<VendedorModel> vendedores)
        {
            try
            {
                return _vendedorBusiness.Editar(vendedores);
            }
            catch
            {
                return null;
            }
        }

        [Route("BulkDeleteVendedor")]
        [HttpPost]
        public RetornoRequestNMensagens BulkDeleteVendedor(List<int> codigos)
        {
            try
            {
                return _vendedorBusiness.Excluir(codigos);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Vendedor Plano Pagamento Padrão

        [Route("ObterVendedorPlanoPadrao")]
        [HttpGet]
        public RetornoVendedorPlanoPadrao ObterVendedorPlanoPadrao()
        {
            var parametros = _parametrosBusiness.ObterParametros();
            return new RetornoVendedorPlanoPadrao()
            {
                VendedorDefault = parametros.VendedorDefault.HasValue ? parametros.VendedorDefault.Value : 0,
                PlanoPagamentoDefault = parametros.PlanoPagamentoDefault.HasValue ? parametros.PlanoPagamentoDefault.Value : 0
            };
        }

        #endregion

        #region ExclusoesApp
        [Route("ObterRegistrosExcluidos")]
        [HttpGet]
        public List<RetornoExcluidos> ObterRegistrosExcluidos(DateTime dataRegistros)
        {
            try
            {
                return _auditoria.ObterListaExcluidos(dataRegistros);
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}