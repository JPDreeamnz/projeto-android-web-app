﻿using Api.Business.Interfaces;
using Api.Models;
using Api.Models.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class GrupoUsuariosController : ApiController
    {
        IGrupoUsuariosBusiness _grupoUsuariosBusiness;

        public GrupoUsuariosController(IGrupoUsuariosBusiness grupoUsuariosBusiness)
        {
            this._grupoUsuariosBusiness = grupoUsuariosBusiness;
        }


        [Route("api/GrupoUsuarios/ListarGrupos")]
        [HttpGet]
        public List<GrupoUsuariosModel> ListarGrupos()
        {
            return this._grupoUsuariosBusiness.ListarGrupos();
        }


        [Route("api/GrupoUsuarios/ObterGrupo")]
        [HttpGet]
        public GrupoUsuariosModel ObterGrupo(int codigo)
        {
            return this._grupoUsuariosBusiness.ObterGrupo(codigo);
        }

        [Route("api/GrupoUsuarios/Incluir")]
        [HttpPost]
        public RetornoRequest Incluir(GrupoUsuariosModel model)
        {
            return this._grupoUsuariosBusiness.Incluir(model);
        }

        [Route("api/GrupoUsuarios/Editar")]
        [HttpPost]
        public RetornoRequest Editar(GrupoUsuariosModel model)
        {
            return this._grupoUsuariosBusiness.Editar(model);
        }

        [Route("api/GrupoUsuarios/Excluir")]
        [HttpGet]
        public bool Excluir(int codigo)
        {
            return this._grupoUsuariosBusiness.Excluir(codigo);
        }
    }
}