﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using Autofac.Integration.WebApi;
using Api.Business;
using Api.Database;
using Api.Repository;
using Api.Business.Interfaces;
using Api.Repository.Interfaces;
using Api.Repository.Repositorio;
using System.Configuration;

namespace Api
{
    public class IocContainerFactory
    {
        public IContainer Create()
        {
            var builder = new ContainerBuilder();

            // Register WebApi controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            ConfigureIocForRepositories(builder);

            // Build and return the Container
            var container = builder.Build();
            return container;
        }

        private void ConfigureIocForRepositories(ContainerBuilder builder)
        {
            builder.RegisterType<UsersRepository>().As<IUsersRepository>();
            builder.RegisterType<UsersBusiness>().As<IUsersBusiness>();

            builder.RegisterType<LoginBusiness>().As<ILoginBusiness>();
            builder.RegisterType<LoginRepository>().As<ILoginRepository>();

            builder.RegisterType<ContatoRepository>().As<IContatoRepository>();
            builder.RegisterType<ContatoBusiness>().As<IContatoBusiness>();

            builder.RegisterType<EnderecoRepository>().As<IEnderecoRepository>();
            builder.RegisterType<EnderecoBusiness>().As<IEnderecoBusiness>();

            builder.RegisterType<ClienteRepository>().As<IClienteRepository>();
            builder.RegisterType<ClienteBusiness>().As<IClienteBusiness>();

            builder.RegisterType<FinanceiroRepository>().As<IFinanceiroRepository>();
            builder.RegisterType<FinanceiroBusiness>().As<IFinanceiroBusiness>();

            builder.RegisterType<VendedorRepository>().As<IVendedorRepository>();
            builder.RegisterType<VendedorBusiness>().As<IVendedorBusiness>();

            builder.RegisterType<PlanoPagamentoRepository>().As<IPlanoPagamentoRepository>();
            builder.RegisterType<PlanoPagamentoBusiness>().As<IPlanoPagamentoBusiness>();

            builder.RegisterType<ParametrosRepository>().As<IParametrosRepository>();
            builder.RegisterType<ParametrosBusiness>().As<IParametrosBusiness>();

            builder.RegisterType<ProdutoRepository>().As<IProdutoRepository>();
            builder.RegisterType<ProdutoBusiness>().As<IProdutoBusiness>();

            builder.RegisterType<ImagemProdutoRepository>().As<IImagemProdutoRepository>()
                .WithParameter(new TypedParameter(typeof(string), ConfigurationManager.AppSettings["ImagensPasta"]));
            builder.RegisterType<ImagemProdutoBusiness>().As<IImagemProdutoBusiness>();

            builder.RegisterType<AuditoriaRepository>().As<IAuditoriaRepository>();
            builder.RegisterType<AuditoriaBusiness>().As<IAuditoriaBusiness>();

            builder.RegisterType<PedidoVendaRepository>().As<IPedidoVendaRepository>();
            builder.RegisterType<PedidoVendaBusiness>().As<IPedidoVendaBusiness>();

            builder.RegisterType<GrupoUsuariosRepository>().As<IGrupoUsuariosRepository>();
            builder.RegisterType<GrupoUsuariosBusiness>().As<IGrupoUsuariosBusiness>();

            builder.RegisterType<Context>().As<IContext>();
        }
    }
}