﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;

namespace Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public IContainer Container;

        public override void Init()
        {
            this.PostAuthenticateRequest += ApiApplication_PostAuthenticateRequest;
            base.Init();
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            CreateIocContainer();
        }

        private void CreateIocContainer()
        {
            var factory = new IocContainerFactory();
            Container = factory.Create();

            // Create the depenedcy resolver and configure Web API with it
            var resolver = new AutofacWebApiDependencyResolver(Container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }

        private void ApiApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(
                SessionStateBehavior.Required);
        }
    }
}
