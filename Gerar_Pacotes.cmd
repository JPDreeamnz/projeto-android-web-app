# batch para gerar os pacotes de publicação.
@echo off
cls

echo Projeto de complicacao de pacotes de publicacao.
echo.
echo E necessario o visual studio 2015 instalado na maquina e uma conexao ativa com acesso a internet.
echo.

pause

SET MSBUILD="C:\Program Files (x86)\MSBuild\14.0\Bin\msbuild"

%MSBUILD% .\API\AuthFramework\AuthFramework.csproj /p:DeployOnBuild=true /p:PublishProfile=Pacote /p:DebugSymbols=false

%MSBUILD% .\Admin\Administrator\Administrator.csproj /p:DeployOnBuild=true /p:PublishProfile=Pacote /p:DebugSymbols=false

%MSBUILD% .\API\Api\Api.csproj /p:DeployOnBuild=true /p:PublishProfile=Pacote /p:DebugSymbols=false

pause
@echo on